# mycelium-app

Spiking neural network written in golang and based on the Izhikevich spiking neural network model.

## Requirements

    docker >= 17.12.0
    docker-compose >= 1.19.0

## Getting Started

1. Checkout the project and the dependent submodules with the following command:

        git clone --recursive git@gitlab.com:andriyr/mycelium-app.git

2. Compile the protobuf files to ts format. Change directory into /api and run:

    protoc --plugin=protoc-gen-ts=../node_modules/.bin/protoc-gen-ts \
            --js_out=import_style=commonjs,binary:../src/proto \
            --ts_out=service=true:../src/proto \
            *.proto && \
    protoc --plugin=protoc-gen-js_service=../node_modules/.bin/protoc-gen-js_service \
            --js_out=import_style=commonjs,binary:../src/proto \
            --js_service_out=../src/proto \
            *.proto

    Add /* eslint-disable */ at the end of the generated js proto files to prevent app compilation errors. The js proto files are located here:

3. Start the grpc web proxy by running the deploy_to_local.sh script

4. start the app by executing the following command in the root directory:

    npm start
