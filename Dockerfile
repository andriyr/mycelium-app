FROM node:9.6.1

RUN mkdir /usr/src/app
WORKDIR /usr/src/app

ENV PATH /usr/src/app/node_modules/.bin:$PATH

COPY ./app/public /usr/src/app/public
COPY ./app/src /usr/src/app/src
COPY ./app/package.json /usr/src/app/package.json

# TODO generate proto files and add jslint ignore

RUN npm install --silent
RUN npm install react-scripts@1.1.1 -g --silent

EXPOSE 3000

CMD ["npm", "start"]
