import update from 'immutability-helper';
import {
    ADD_WIDGET,
    REMOVE_WIDGET,
    STREAM_NEURON_SPIKE_EVENT_SUCCESS,
    STREAM_NEURON_SPIKE_EVENT_FAILURE
} from "../actions/index";

const initialState = {
    widgets: []
}

const monitorReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_WIDGET:
            return update(state, {widgets: {$push: [action.payload]}});
        case REMOVE_WIDGET:
            var widgetIndex = state.widgets.findIndex((widget) => {return widget.id == action.payload.id})
            return update(state, {widgets: {$splice: [[widgetIndex, 1]]}});
        case STREAM_NEURON_SPIKE_EVENT_SUCCESS:
            return state;
        case STREAM_NEURON_SPIKE_EVENT_FAILURE:
            // TODO:
            return state;
        default:
            return state;
    }
}

export default monitorReducer;
