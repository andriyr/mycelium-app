import update from 'immutability-helper';
import {
    CREATE_NEURON_CLUSTER_SUCCESS,
    CREATE_NEURON_CLUSTER_FAILURE,
    DELETE_NEURON_CLUSTER_SUCCESS,
    DELETE_NEURON_CLUSTER_FAILURE,
    SET_NEURON_CLUSTER_POSITION_SUCCESS,
    SET_NEURON_CLUSTER_POSITION_FAILURE,
    CREATE_SYNAPSE_CLUSTER_SUCCESS,
    CREATE_SYNAPSE_CLUSTER_FAILURE,
    DELETE_SYNAPSE_CLUSTER_SUCCESS,
    DELETE_SYNAPSE_CLUSTER_FAILURE,
    ATTACH_SYNAPSE_CLUSTER_SUCCESS,
    ATTACH_SYNAPSE_CLUSTER_FAILURE,
    DETACH_SYNAPSE_CLUSTER_SUCCESS,
    DETACH_SYNAPSE_CLUSTER_FAILURE,
    SET_SYNAPSE_CLUSTER_POSITION_SUCCESS,
    SET_SYNAPSE_CLUSTER_POSITION_FAILURE,
    CREATE_STDP_PRECEPTOR_SUCCESS,
    CREATE_STDP_PRECEPTOR_FAILURE,
    DELETE_STDP_PRECEPTOR_SUCCESS,
    DELETE_STDP_PRECEPTOR_FAILURE,
    ATTACH_STDP_PRECEPTOR_SUCCESS,
    ATTACH_STDP_PRECEPTOR_FAILURE,
    DETACH_STDP_PRECEPTOR_SUCCESS,
    DETACH_STDP_PRECEPTOR_FAILURE,
    SET_STDP_PRECEPTOR_POSITION_SUCCESS,
    SET_STDP_PRECEPTOR_POSITION_FAILURE,
    CREATE_MODULATED_STDP_PRECEPTOR_SUCCESS,
    CREATE_MODULATED_STDP_PRECEPTOR_FAILURE,
    DELETE_MODULATED_STDP_PRECEPTOR_SUCCESS,
    DELETE_MODULATED_STDP_PRECEPTOR_FAILURE,
    ATTACH_MODULATED_STDP_PRECEPTOR_SUCCESS,
    ATTACH_MODULATED_STDP_PRECEPTOR_FAILURE,
    DETACH_MODULATED_STDP_PRECEPTOR_SUCCESS,
    DETACH_MODULATED_STDP_PRECEPTOR_FAILURE,
    SET_MODULATED_STDP_PRECEPTOR_POSITION_SUCCESS,
    SET_MODULATED_STDP_PRECEPTOR_POSITION_FAILURE,
    CREATE_DOPAMINE_ADAPTER_SUCCESS,
    CREATE_DOPAMINE_ADAPTER_FAILURE,
    DELETE_DOPAMINE_ADAPTER_SUCCESS,
    DELETE_DOPAMINE_ADAPTER_FAILURE,
    ATTACH_DOPAMINE_ADAPTER_SUCCESS,
    ATTACH_DOPAMINE_ADAPTER_FAILURE,
    DETACH_DOPAMINE_ADAPTER_SUCCESS,
    DETACH_DOPAMINE_ADAPTER_FAILURE,
    SET_DOPAMINE_ADAPTER_POSITION_SUCCESS,
    SET_DOPAMINE_ADAPTER_POSITION_FAILURE,
    CREATE_INPUT_ADAPTER_SUCCESS,
    CREATE_INPUT_ADAPTER_FAILURE,
    DELETE_INPUT_ADAPTER_SUCCESS,
    DELETE_INPUT_ADAPTER_FAILURE,
    ATTACH_INPUT_ADAPTER_SUCCESS,
    ATTACH_INPUT_ADAPTER_FAILURE,
    DETACH_INPUT_ADAPTER_SUCCESS,
    DETACH_INPUT_ADAPTER_FAILURE,
    SET_INPUT_ADAPTER_POSITION_SUCCESS,
    SET_INPUT_ADAPTER_POSITION_FAILURE,
    CREATE_OUTPUT_ADAPTER_SUCCESS,
    CREATE_OUTPUT_ADAPTER_FAILURE,
    DELETE_OUTPUT_ADAPTER_SUCCESS,
    DELETE_OUTPUT_ADAPTER_FAILURE,
    ATTACH_OUTPUT_ADAPTER_SUCCESS,
    ATTACH_OUTPUT_ADAPTER_FAILURE,
    DETACH_OUTPUT_ADAPTER_SUCCESS,
    DETACH_OUTPUT_ADAPTER_FAILURE,
    SET_OUTPUT_ADAPTER_POSITION_SUCCESS,
    SET_OUTPUT_ADAPTER_POSITION_FAILURE,
    NODE_LOADED
} from "../actions/index";

const initialState = {
    neuronClusters: [],
    synapseClusters: [],
    STDPPreceptors: [],
    modulatedSTDPPreceptors: [],
    dopamineAdapters: [],
    inputAdapters: [],
    outputAdapters: [],
    nodeLoaded: []
};

const configurationReducer = (state = initialState, action) => {
    switch (action.type) {
        case CREATE_NEURON_CLUSTER_SUCCESS:
            var neuronClusterIndex = state.neuronClusters.findIndex((neuronCluster) => {return neuronCluster.id == action.payload.id});
            if (neuronClusterIndex === -1) {
                return update(state, {neuronClusters: {$push: [action.payload]}});
            }
        case CREATE_NEURON_CLUSTER_FAILURE:
            return state;
        case DELETE_NEURON_CLUSTER_SUCCESS:
            var neuronClusterIndex = state.neuronClusters.findIndex((neuronCluster) => {return neuronCluster.id == action.payload.id});
            return update(state, {neuronClusters: {$splice: [[neuronClusterIndex, 1]]}});
        case DELETE_NEURON_CLUSTER_FAILURE:
            return state;
        case SET_NEURON_CLUSTER_POSITION_SUCCESS:
            var neuronClusterIndex = state.neuronClusters.findIndex((neuronCluster) => {return neuronCluster.id == action.payload.id});
            return update(state, {neuronClusters: {[neuronClusterIndex]: {x: {$set: action.payload.x}, y: {$set: action.payload.y}}}});
        case SET_NEURON_CLUSTER_POSITION_FAILURE:
            return state;
        case CREATE_SYNAPSE_CLUSTER_SUCCESS:
            var synapseClusterIndex = state.synapseClusters.findIndex((synapseCluster) => {return synapseCluster.id == action.payload.id});
            if (synapseClusterIndex === -1) {
                return update(state, {synapseClusters: {$push: [action.payload]}});
            }
        case CREATE_SYNAPSE_CLUSTER_FAILURE:
            return state;
        case DELETE_SYNAPSE_CLUSTER_SUCCESS:
            var synapseClusterIndex = state.synapseClusters.findIndex((synapseCluster) => {return synapseCluster.id == action.payload.id});
            return update(state, {synapseClusters: {$splice: [[synapseClusterIndex, 1]]}});
        case DELETE_SYNAPSE_CLUSTER_FAILURE:
            return state;
        case ATTACH_SYNAPSE_CLUSTER_SUCCESS:
            var synapseClusterIndex = state.synapseClusters.findIndex((synapseCluster) => {return synapseCluster.id == action.payload.synapseClusterId});
            return update(state, {synapseClusters: {[synapseClusterIndex]: {neuronClusterIds: {$push: [action.payload.neuronClusterId]}}}});
        case ATTACH_SYNAPSE_CLUSTER_FAILURE:
            return state;
        case DETACH_SYNAPSE_CLUSTER_SUCCESS:
            var synapseClusterIndex = state.synapseClusters.findIndex((synapseCluster) => {return synapseCluster.id == action.payload.synapseClusterId});
            var neuronClusterIdsIndex = state.synapseClusters[synapseClusterIndex].neuronClusterIds.findIndex((neuronClusterId) => {return neuronClusterId == action.payload.neuronClusterId});
            return update(state, {synapseClusters: {[synapseClusterIndex]: {neuronClusterIds: {$splice: [[neuronClusterIdsIndex, 1]]}}}});
        case DETACH_SYNAPSE_CLUSTER_FAILURE:
            return state;
        case SET_SYNAPSE_CLUSTER_POSITION_SUCCESS:
            var synapseClusterIndex = state.synapseClusters.findIndex((synapseCluster) => {return synapseCluster.id == action.payload.id});
            return update(state, {synapseClusters: {[synapseClusterIndex]: {x: {$set: action.payload.x}, y: {$set: action.payload.y}}}});
        case SET_SYNAPSE_CLUSTER_POSITION_FAILURE:
            return state;
        case CREATE_STDP_PRECEPTOR_SUCCESS:
            var STDPPreceptorIndex = state.STDPPreceptors.findIndex((STDPPreceptor) => {return STDPPreceptor.id == action.payload.id});
            if (STDPPreceptorIndex === -1) {
                return update(state, {STDPPreceptors: {$push: [action.payload]}});
            }
        case CREATE_STDP_PRECEPTOR_FAILURE:
            return state;
        case DELETE_STDP_PRECEPTOR_SUCCESS:
            var STDPPreceptorIndex = state.STDPPreceptors.findIndex((STDPPreceptor) => {return STDPPreceptor.id == action.payload.id});
            return update(state, {STDPPreceptors: {$splice: [[STDPPreceptorIndex, 1]]}});
        case DELETE_STDP_PRECEPTOR_FAILURE:
            return state;
        case ATTACH_STDP_PRECEPTOR_SUCCESS:
            var STDPPreceptorIndex = state.STDPPreceptors.findIndex((STDPPreceptor) => {return STDPPreceptor.id == action.payload.STDPPreceptorId});
            return update(state, {STDPPreceptors: {[STDPPreceptorIndex]: {synapseClusterId: {$set: action.payload.synapseClusterId}}}});
        case ATTACH_STDP_PRECEPTOR_FAILURE:
            return state;
        case DETACH_STDP_PRECEPTOR_SUCCESS:
            var STDPPreceptorIndex = state.STDPPreceptors.findIndex((STDPPreceptor) => {return STDPPreceptor.id == action.payload.STDPPreceptorId});
            return update(state, {STDPPreceptors: {[STDPPreceptorIndex]: {synapseClusterId: {$set: undefined}}}});
        case DETACH_STDP_PRECEPTOR_FAILURE:
            return state;
        case SET_STDP_PRECEPTOR_POSITION_SUCCESS:
            var STDPPreceptorIndex = state.STDPPreceptors.findIndex((STDPPreceptor) => {return STDPPreceptor.id == action.payload.id});
            return update(state, {STDPPreceptors: {[STDPPreceptorIndex]: {x: {$set: action.payload.x}, y: {$set: action.payload.y}}}});
        case SET_STDP_PRECEPTOR_POSITION_FAILURE:
            return state;
        case CREATE_MODULATED_STDP_PRECEPTOR_SUCCESS:
            var modulatedSTDPPreceptorIndex = state.modulatedSTDPPreceptors.findIndex((modulatedSTDPPreceptor) => {return modulatedSTDPPreceptor.id == action.payload.id});
            if (modulatedSTDPPreceptorIndex === -1) {
                return update(state, {modulatedSTDPPreceptors: {$push: [action.payload]}});
            }
        case CREATE_MODULATED_STDP_PRECEPTOR_FAILURE:
            return state;
        case DELETE_MODULATED_STDP_PRECEPTOR_SUCCESS:
            var modulatedSTDPPreceptorIndex = state.modulatedSTDPPreceptors.findIndex((modulatedSTDPPreceptor) => {return modulatedSTDPPreceptor.id == action.payload.id});
            return update(state, {modulatedSTDPPreceptors: {$splice: [[modulatedSTDPPreceptorIndex, 1]]}});
        case DELETE_MODULATED_STDP_PRECEPTOR_FAILURE:
            return state;
        case ATTACH_MODULATED_STDP_PRECEPTOR_SUCCESS:
            var modulatedSTDPPreceptorIndex = state.modulatedSTDPPreceptors.findIndex((modulatedSTDPPreceptor) => {return modulatedSTDPPreceptor.id == action.payload.modulatedSTDPPreceptorId});
            return update(state, {modulatedSTDPPreceptors: {[modulatedSTDPPreceptorIndex]: {synapseClusterId: {$set: action.payload.synapseClusterId}}}});
        case ATTACH_MODULATED_STDP_PRECEPTOR_FAILURE:
            return state;
        case DETACH_MODULATED_STDP_PRECEPTOR_SUCCESS:
            var modulatedSTDPPreceptorIndex = state.modulatedSTDPPreceptors.findIndex((modulatedSTDPPreceptor) => {return modulatedSTDPPreceptor.id == action.payload.modulatedSTDPPreceptorId});
            return update(state, {modulatedSTDPPreceptors: {[modulatedSTDPPreceptorIndex]: {synapseClusterIds: {$set: undefined}}}});
        case DETACH_MODULATED_STDP_PRECEPTOR_FAILURE:
            return state;
        case SET_MODULATED_STDP_PRECEPTOR_POSITION_SUCCESS:
            var modulatedSTDPPreceptorIndex = state.modulatedSTDPPreceptors.findIndex((modulatedSTDPPreceptor) => {return modulatedSTDPPreceptor.id == action.payload.id});
            return update(state, {modulatedSTDPPreceptors: {[modulatedSTDPPreceptorIndex]: {x: {$set: action.payload.x}, y: {$set: action.payload.y}}}});
        case SET_MODULATED_STDP_PRECEPTOR_POSITION_FAILURE:
            return state;
        case CREATE_DOPAMINE_ADAPTER_SUCCESS:
            var dopamineAdapterIndex = state.dopamineAdapters.findIndex((dopamineAdapter) => {return dopamineAdapter.id == action.payload.id});
            if (dopamineAdapterIndex === -1) {
                return update(state, {dopamineAdapters: {$push: [action.payload]}});
            }
        case CREATE_DOPAMINE_ADAPTER_FAILURE:
            return state;
        case DELETE_DOPAMINE_ADAPTER_SUCCESS:
            var dopamineAdapterIndex = state.dopamineAdapters.findIndex((dopamineAdapter) => {return dopamineAdapter.id == action.payload.dopamineAdapterId});
            return update(state, {dopamineAdapters: {$splice: [[dopamineAdapterIndex, 1]]}});
        case DELETE_DOPAMINE_ADAPTER_FAILURE:
            return state;
        case ATTACH_DOPAMINE_ADAPTER_SUCCESS:
            var modulatedSTDPPreceptorIndex = state.modulatedSTDPPreceptors.findIndex((modulatedSTDPPreceptor) => {return modulatedSTDPPreceptor.id == action.payload.modulatedSTDPPreceptorId});
            return update(state, {modulatedSTDPPreceptors: {[modulatedSTDPPreceptorIndex]: {dopamineAdapterId: {$set: action.payload.dopamineAdapterId}}}});
        case ATTACH_DOPAMINE_ADAPTER_FAILURE:
            return state;
        case DETACH_DOPAMINE_ADAPTER_SUCCESS:
            var modulatedSTDPPreceptorIndex = state.modulatedSTDPPreceptors.findIndex((modulatedSTDPPreceptor) => {return modulatedSTDPPreceptor.id == action.payload.modulatedSTDPPreceptorId});
            if (modulatedSTDPPreceptorIndex > -1) {
                return update(state, {modulatedSTDPPreceptors: {[modulatedSTDPPreceptorIndex]: {dopamineAdapterId: {$set: undefined}}}});
            }
        case DETACH_DOPAMINE_ADAPTER_FAILURE:
            return state;
        case SET_DOPAMINE_ADAPTER_POSITION_SUCCESS:
            var dopamineAdapterIndex = state.dopamineAdapters.findIndex((dopamineAdapter) => {return dopamineAdapter.id == action.payload.id});
            return update(state, {dopamineAdapters: {[dopamineAdapterIndex]: {x: {$set: action.payload.x}, y: {$set: action.payload.y}}}});
        case SET_DOPAMINE_ADAPTER_POSITION_FAILURE:
            return state;
        case CREATE_INPUT_ADAPTER_SUCCESS:
            var inputAdapterIndex = state.inputAdapters.findIndex((inputAdapter) => {return inputAdapter.id == action.payload.id});
            if (inputAdapterIndex === -1) {
                return update(state, {inputAdapters: {$push: [action.payload]}});
            }
        case CREATE_INPUT_ADAPTER_FAILURE:
            return state;
        case DELETE_INPUT_ADAPTER_SUCCESS:
            var inputAdapterIndex = state.inputAdapters.findIndex((inputAdapter) => {return inputAdapter.id == action.payload.inputAdapterId});
            return update(state, {inputAdapters: {$splice: [[inputAdapterIndex, 1]]}});
        case DELETE_INPUT_ADAPTER_FAILURE:
            return state;
        case ATTACH_INPUT_ADAPTER_SUCCESS:
            var neuronClusterIndex = state.neuronClusters.findIndex((neuronCluster) => {return neuronCluster.id == action.payload.neuronClusterId});
            return update(state, {neuronClusters: {[neuronClusterIndex]: {inputAdapterIds: {$push: [action.payload.inputAdapterId]}}}});
        case ATTACH_INPUT_ADAPTER_FAILURE:
            return state;
        case DETACH_INPUT_ADAPTER_SUCCESS:
            var neuronClusterIndex = state.neuronClusters.findIndex((neuronCluster) => {return neuronCluster.id == action.payload.neuronClusterId});
            if (neuronClusterIndex > -1) {
                var inputAdapterIdsIndex = state.neuronClusters[neuronClusterIndex].inputAdapterIds.findIndex((inputAdapterId) => {return inputAdapterId == action.payload.inputAdapterId});
                return update(state, {neuronClusters: {[neuronClusterIndex]: {inputAdapterIds: {$splice: [[inputAdapterIdsIndex, 1]]}}}});
            }
        case DETACH_INPUT_ADAPTER_FAILURE:
            return state;
        case SET_INPUT_ADAPTER_POSITION_SUCCESS:
            var inputAdapterIndex = state.inputAdapters.findIndex((inputAdapter) => {return inputAdapter.id == action.payload.id});
            return update(state, {inputAdapters: {[inputAdapterIndex]: {x: {$set: action.payload.x}, y: {$set: action.payload.y}}}});
        case SET_INPUT_ADAPTER_POSITION_FAILURE:
            return state;
        case CREATE_OUTPUT_ADAPTER_SUCCESS:
            var outputAdapterIndex = state.outputAdapters.findIndex((outputAdapter) => {return outputAdapter.id == action.payload.id});
            if (outputAdapterIndex === -1) {
                return update(state, {outputAdapters: {$push: [action.payload]}});
            }
        case CREATE_OUTPUT_ADAPTER_FAILURE:
            return state;
        case DELETE_OUTPUT_ADAPTER_SUCCESS:
            var outputAdapterIndex = state.outputAdapters.findIndex((outputAdapter) => {return outputAdapter.id == action.payload.id});
            return update(state, {outputAdapters: {$splice: [[outputAdapterIndex, 1]]}});
        case DELETE_OUTPUT_ADAPTER_FAILURE:
            return state;
        case ATTACH_OUTPUT_ADAPTER_SUCCESS:
            var outputAdapterIndex = state.outputAdapters.findIndex((outputAdapter) => {return outputAdapter.id == action.payload.outputAdapterId});
            return update(state, {outputAdapters: {[outputAdapterIndex]: {neuronClusterId: {$set: action.payload.neuronClusterId}}}});
        case ATTACH_OUTPUT_ADAPTER_FAILURE:
            return state;
        case DETACH_OUTPUT_ADAPTER_SUCCESS:
            var outputAdapterIndex = state.outputAdapters.findIndex((outputAdapter) => {return outputAdapter.id == action.payload.outputAdapterId});
            return update(state, {outputAdapters: {[outputAdapterIndex]: {neuronClusterId: {$set: undefined}}}});
        case DETACH_OUTPUT_ADAPTER_FAILURE:
            return state;
        case SET_OUTPUT_ADAPTER_POSITION_SUCCESS:
            var outputAdapterIndex = state.outputAdapters.findIndex((outputAdapter) => {return outputAdapter.id == action.payload.id});
            return update(state, {outputAdapters: {[outputAdapterIndex]: {x: {$set: action.payload.x}, y: {$set: action.payload.y}}}});
        case SET_INPUT_ADAPTER_POSITION_FAILURE:
            return state;
        case NODE_LOADED:
            return update(state, {nodeLoaded: {$push: [action.payload]}});
        default:
            return state;
    }
};

export default configurationReducer;
