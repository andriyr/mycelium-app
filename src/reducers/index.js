import { combineReducers } from 'redux';
import configurationReducer from "./ConfigurationReducer";
import monitorReducer from "./MonitorReducer";

const rootReducer = combineReducers({
  configuration: configurationReducer,
  monitor: monitorReducer
})

export default rootReducer;
