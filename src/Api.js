import { grpc } from "grpc-web-client";
import { Node } from "./proto/api_pb_service";
import {
    NodeRequest,
    CreateNeuronClusterRequest,
    DeleteNeuronClusterRequest,
    SetNeuronClusterPositionRequest,
    CreateSynapseClusterRequest,
    DeleteSynapseClusterRequest,
    AttachSynapseClusterRequest,
    DetachSynapseClusterRequest,
    SetSynapseClusterPositionRequest,
    CreateSTDPPreceptorRequest,
    DeleteSTDPPreceptorRequest,
    AttachSTDPPreceptorRequest,
    DetachSTDPPreceptorRequest,
    SetSTDPPreceptorPositionRequest,
    CreateModulatedSTDPPreceptorRequest,
    DeleteModulatedSTDPPreceptorRequest,
    AttachModulatedSTDPPreceptorRequest,
    DetachModulatedSTDPPreceptorRequest,
    SetModulatedSTDPPreceptorPositionRequest,
    CreateDopamineAdapterRequest,
    DeleteDopamineAdapterRequest,
    AttachDopamineAdapterRequest,
    DetachDopamineAdapterRequest,
    SetDopamineAdapterPositionRequest,
    CreateInputAdapterRequest,
    DeleteInputAdapterRequest,
    AttachInputAdapterRequest,
    DetachInputAdapterRequest,
    SetInputAdapterPositionRequest,
    CreateOutputAdapterRequest,
    DeleteOutputAdapterRequest,
    AttachOutputAdapterRequest,
    DetachOutputAdapterRequest,
    SetOutputAdapterPositionRequest,
    StreamNeuronSpikeEventRequest,
    StreamNeuronSpikeRelayEventRequest,
    Component,
    Graph
} from "./proto/api_pb";

class Api {

    constructor(url: string) {
        this.url = url;
    }

    loadNode(onSuccess, onFailure) {
        var request  = new NodeRequest();

        this.unary(Node.Node, request, onSuccess, onFailure);
    }

    createNeuronCluster(name, size, ratio, x, y, onSuccess, onFailure) {
        var request = new CreateNeuronClusterRequest();
        var graph = new Graph();
        request.setName(name);
        request.setSize(size);
        request.setRatio(ratio);
        graph.setX(x);
        graph.setY(y);
        request.setGraph(graph);

        this.unary(Node.CreateNeuronCluster, request, onSuccess, onFailure);
    }

    deleteNeuronCluster(id, onSuccess, onFailure) {
        var request = new DeleteNeuronClusterRequest();
        request.setId(id);

        this.unary(Node.DeleteNeuronCluster, request, onSuccess, onFailure);
    }

    setNeuronClusterPosition(id, x, y, onSuccess, onFailure) {
        var request = new SetNeuronClusterPositionRequest();
        var graph = new Graph();
        graph.setX(x);
        graph.setY(y);
        request.setId(id);
        request.setGraph(graph);

        this.unary(Node.SetNeuronClusterPosition, request, onSuccess, onFailure);
    }

    createSynapseCluster(name, neuronsCovered, initialSynapseWeight, x, y, onSuccess, onFailure) {
        var request = new CreateSynapseClusterRequest();
        var graph = new Graph();
        request.setName(name);
        request.setNeuronscovered(neuronsCovered);
        request.setInitialsynapseweight(initialSynapseWeight);
        graph.setX(x);
        graph.setY(y);
        request.setGraph(graph);

        this.unary(Node.CreateSynapseCluster, request, onSuccess, onFailure);
    }

    deleteSynapseCluster(id, onSuccess, onFailure) {
        var request = new DeleteSynapseClusterRequest();
        request.setId(id);

        this.unary(Node.DeleteSynapseCluster, request, onSuccess, onFailure);
    }

    attachSynapseCluster(synapseClusterId, neuronClusterId, onSuccess, onFailure) {
        var request = new AttachSynapseClusterRequest();
        request.setSynapseclusterid(synapseClusterId);
        request.setNeuronclusterid(neuronClusterId);

        this.unary(Node.AttachSynapseCluster, request, onSuccess, onFailure);
    }

    detachSynapseCluster(synapseClusterId, neuronClusterId, onSuccess, onFailure) {
        var request = new DetachSynapseClusterRequest();
        request.setSynapseclusterid(synapseClusterId);
        request.setNeuronclusterid(neuronClusterId);

        this.unary(Node.DetachSynapseCluster, request, onSuccess, onFailure);
    }

    setSynapseClusterPosition(id, x, y, onSuccess, onFailure) {
        var request = new SetSynapseClusterPositionRequest();
        var graph = new Graph();
        graph.setX(x);
        graph.setY(y);
        request.setId(id);
        request.setGraph(graph);

        this.unary(Node.SetSynapseClusterPosition, request, onSuccess, onFailure);
    }

    createSTDPPreceptor(name, maxQueueSize, x, y, onSuccess, onFailure) {
        var request = new CreateSTDPPreceptorRequest();
        var graph = new Graph();
        request.setName(name);
        request.setMaxqueuesize(maxQueueSize);
        graph.setX(x);
        graph.setY(y);
        request.setGraph(graph);

        this.unary(Node.CreateSTDPPreceptor, request, onSuccess, onFailure);
    }

    deleteSTDPPreceptor(id, onSuccess, onFailure) {
        var request = new DeleteSTDPPreceptorRequest();
        request.setId(id);

        this.unary(Node.DeleteSTDPPreceptor, request, onSuccess, onFailure);
    }

    attachSTDPPreceptor(STDPPreceptorId, synapseClusterId, onSuccess, onFailure) {
        var request = new AttachSTDPPreceptorRequest();
        request.setStdppreceptorid(STDPPreceptorId);
        request.setSynapseclusterid(synapseClusterId);

        this.unary(Node.AttachSTDPPreceptor, request, onSuccess, onFailure);
    }

    detachSTDPPreceptor(STDPPreceptorId, onSuccess, onFailure) {
        var request = new DetachSTDPPreceptorRequest();
        request.setStdppreceptorid(STDPPreceptorId);

        this.unary(Node.DetachSTDPPreceptor, request, onSuccess, onFailure);
    }

    setSTDPPreceptorPosition(id, x, y, onSuccess, onFailure) {
        var request = new SetSTDPPreceptorPositionRequest();
        var graph = new Graph();
        graph.setX(x);
        graph.setY(y);
        request.setId(id);
        request.setGraph(graph);

        this.unary(Node.SetSTDPPreceptorPosition, request, onSuccess, onFailure);
    }

    createModulatedSTDPPreceptor(name, maxQueueSize, sensitivity, x, y, onSuccess, onFailure) {
        var request = new CreateModulatedSTDPPreceptorRequest();
        var graph = new Graph();
        request.setName(name);
        request.setMaxqueuesize(maxQueueSize);
        request.setSensitivity(sensitivity);
        graph.setX(x);
        graph.setY(y);
        request.setGraph(graph);

        this.unary(Node.CreateModulatedSTDPPreceptor, request, onSuccess, onFailure);
    }

    deleteModulatedSTDPPreceptor(id, onSuccess, onFailure) {
        var request = new DeleteModulatedSTDPPreceptorRequest();
        request.setId(id);

        this.unary(Node.DeleteModulatedSTDPPreceptor, request, onSuccess, onFailure);
    }

    attachModulatedSTDPPreceptor(modulatedSTDPPreceptorId, synapseClusterId, onSuccess, onFailure) {
        var request = new AttachModulatedSTDPPreceptorRequest();
        request.setModulatedstdppreceptorid(modulatedSTDPPreceptorId);
        request.setSynapseclusterid(synapseClusterId);

        this.unary(Node.AttachModulatedSTDPPreceptor, request, onSuccess, onFailure);
    }

    detachModulatedSTDPPreceptor(modulatedSTDPPreceptorId, synapseClusterId, onSuccess, onFailure) {
        var request = new DetachModulatedSTDPPreceptorRequest();
        request.setModulatedstdppreceptorid(modulatedSTDPPreceptorId);
        request.setSynapseclusterid(synapseClusterId);

        this.unary(Node.DetachModulatedSTDPPreceptor, request, onSuccess, onFailure);
    }

    setModulatedSTDPPreceptorPosition(id, x, y, onSuccess, onFailure) {
        var request = new SetModulatedSTDPPreceptorPositionRequest();
        var graph = new Graph();
        graph.setX(x);
        graph.setY(y);
        request.setId(id);
        request.setGraph(graph);

        this.unary(Node.SetModulatedSTDPPreceptorPosition, request, onSuccess, onFailure);
    }

    createDopamineAdapter(name, x, y, onSuccess, onFailure) {
        var request = new CreateDopamineAdapterRequest();
        var graph = new Graph();
        request.setName(name);
        graph.setX(x);
        graph.setY(y);
        request.setGraph(graph);

        this.unary(Node.CreateDopamineAdapter, request, onSuccess, onFailure);
    }

    deleteDopamineAdapter(id, onSuccess, onFailure) {
        var request = new DeleteDopamineAdapterRequest();
        request.setId(id);

        this.unary(Node.DeleteDopamineAdapter, request, onSuccess, onFailure);
    }

    attachDopamineAdapter(dopamineAdapterId, modulatedSTDPPreceptorId, onSuccess, onFailure) {
        var request = new AttachDopamineAdapterRequest();
        request.setDopamineadapterid(dopamineAdapterId);
        request.setModulatedstdppreceptorid(modulatedSTDPPreceptorId);

        this.unary(Node.AttachDopamineAdapter, request, onSuccess, onFailure);
    }

    detachDopamineAdapter(dopamineAdapterId, modulatedSTDPPreceptorId, onSuccess, onFailure) {
        var request = new DetachDopamineAdapterRequest();
        request.setDopamineadapterid(dopamineAdapterId);
        request.setModulatedstdppreceptorid(modulatedSTDPPreceptorId);

        this.unary(Node.DetachDopamineAdapter, request, onSuccess, onFailure);
    }

    setDopamineAdapterPosition(id, x, y, onSuccess, onFailure) {
        var request = new SetDopamineAdapterPositionRequest();
        var graph = new Graph();
        graph.setX(x);
        graph.setY(y);
        request.setId(id);
        request.setGraph(graph);

        this.unary(Node.SetDopamineAdapterPosition, request, onSuccess, onFailure);
    }

    createInputAdapter(name, size, encodingWindow, x, y, onSuccess, onFailure) {
        var request = new CreateInputAdapterRequest();
        var graph = new Graph();
        request.setName(name);
        request.setSize(size);
        request.setEncodingwindow(encodingWindow);
        graph.setX(x);
        graph.setY(y);
        request.setGraph(graph);

        this.unary(Node.CreateInputAdapter, request, onSuccess, onFailure);
    }

    deleteInputAdapter(id, onSuccess, onFailure) {
        var request = new DeleteInputAdapterRequest();
        request.setId(id);

        this.unary(Node.DeleteInputAdapter, request, onSuccess, onFailure);
    }

    attachInputAdapter(inputAdapterId, neuronClusterId, onSuccess, onFailure) {
        var request = new AttachInputAdapterRequest();
        request.setInputadapterid(inputAdapterId);
        request.setNeuronclusterid(neuronClusterId);

        this.unary(Node.AttachInputAdapter, request, onSuccess, onFailure);
    }

    detachInputAdapter(inputAdapterId, neuronClusterId, onSuccess, onFailure) {
        var request = new DetachInputAdapterRequest();
        request.setInputadapterid(inputAdapterId);
        request.setNeuronclusterid(neuronClusterId);

        this.unary(Node.DetachInputAdapter, request, onSuccess, onFailure);
    }

    setInputAdapterPosition(id, x, y, onSuccess, onFailure) {
        var request = new SetInputAdapterPositionRequest();
        var graph = new Graph();
        graph.setX(x);
        graph.setY(y);
        request.setId(id);
        request.setGraph(graph);

        this.unary(Node.SetInputAdapterPosition, request, onSuccess, onFailure);
    }

    createOutputAdapter (name, size, decodingWindow, x, y, onSuccess, onFailure) {
        var request = new CreateOutputAdapterRequest();
        var graph = new Graph();
        request.setName(name);
        request.setSize(size);
        request.setDecodingwindow(decodingWindow);
        graph.setX(x);
        graph.setY(y);
        request.setGraph(graph);

        this.unary(Node.CreateOutputAdapter, request, onSuccess, onFailure);
    }

    deleteOutputAdapter(id, onSuccess, onFailure) {
        var request = new DeleteOutputAdapterRequest();
        request.setId(id);

        this.unary(Node.DeleteOutputAdapter, request, onSuccess, onFailure);
    }

    attachOutputAdapter(outputAdapterId, neuronClusterId, onSuccess, onFailure) {
        var request = new AttachOutputAdapterRequest();
        request.setOutputadapterid(outputAdapterId);
        request.setNeuronclusterid(neuronClusterId);

        this.unary(Node.AttachOutputAdapter, request, onSuccess, onFailure);
    }

    detachOutputAdapter(outputAdapterId, onSuccess, onFailure) {
        var request = new DetachOutputAdapterRequest();
        request.setOutputadapterid(outputAdapterId);

        this.unary(Node.DetachOutputAdapter, request, onSuccess, onFailure);
    }

    setOutputAdapterPosition(id, x, y, onSuccess, onFailure) {
        var request = new SetOutputAdapterPositionRequest();
        var graph = new Graph();
        graph.setX(x);
        graph.setY(y);
        request.setId(id);
        request.setGraph(graph);

        this.unary(Node.SetOutputAdapterPosition, request, onSuccess, onFailure);
    }

    streamNeuronSpikeEvent(neuronClusterId, onMessage, onFailure) {
        var request = new StreamNeuronSpikeEventRequest();
        request.setId(neuronClusterId);
        // TODO hardcoded value
        request.setSamplinginterval(0);
        request.setComponent(Component.NEURON_CLUSTER);

        var client = this.client(Node.StreamNeuronSpikeEvent, onMessage, onFailure);
        client.onMessage((res) => {
            onMessage(res);
        });
        client.start();
        client.send(request);
    }

    streamNeuronSpikeRelayEvent(synapseClusterId, onMessage, onFailure) {
        var request = new StreamNeuronSpikeRelayEventRequest();
        request.setId(synapseClusterId);
        // TODO hardcoded value
        request.setSamplinginterval(0);
        request.setComponent(Component.SYNAPSE_CLUSTER);

        var client = this.client(Node.StreamNeuronSpikeRelayEvent, onMessage, onFailure);
        client.onMessage((res) => {
            onMessage(res);
        });
        client.start();
        client.send(request);
    }

    unary(type, request, onSuccess, onFailure) {
        grpc.unary(type, {
            request: request,
            host: this.url,
            onEnd: (res) => {
                const {status, message} = res;
                if (onSuccess) {
                    onSuccess(message);
                }
            }
        });
    }

    client(type, onMessage, onFailure) {
        return grpc.client(type, {
            host: this.url,
            onMessage: (res) => {
                const {status, message} = res;
                if (onMessage) {
                    onMessage(message);
                }
            }
        });
    }
}

export default Api;
