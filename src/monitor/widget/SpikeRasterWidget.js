import React, { Component } from "react";
import { Card, Header, Button } from 'semantic-ui-react';
import { AxisLeft, AxisBottom } from '@vx/axis';
import { scaleTime, scaleLinear } from '@vx/scale';
import { GlyphDot } from '@vx/glyph';
import { Group } from '@vx/group';
import { extent, max } from 'd3-array';

class SpikeRasterWidget extends Component {

    constructor() {
        super()

        this.state = {
            open: true,
            data: [],
            interval: 50,
            maxLength: 500,
            last: new Date(),
            neurons: {}
        };
    }

    componentDidMount() {
        var count = 0
        var neurons = {}

        this.props.neuronCluster.neuronIds.map(function (neuronId) {
            neurons[neuronId] = count;
            count++;
        })

        this.setState({neurons: neurons});

        this.props.streamNeuronSpikeEvent(this.props.neuronCluster.id, this.handleNeuronSpikeEvent);
    }

    handleNeuronSpikeEvent = (neuronSpikeEvent) => {
        var now = new Date();

        this.state.count += 1;

        if (this.state.data.length > this.state.maxLength) {
            this.state.data.shift();
        }

        this.state.data.push({time: now, count: this.state.neurons[neuronSpikeEvent.getNeuronid()]});

        if (now - this.state.last >= this.state.interval) {
            this.state.last = now;
            this.forceUpdate();
            console.log("called force update")
        }
    }

    toggle() {
        this.setState({
            open: !this.state.open
        });
    }

    onClose() {
        this.props.close(this);
    }

    render() {
        const x = (d) => {return d.time};
        const y = (d) => {return d.count};
        const margin = {top: 10, bottom: 10, left: 10, right: 10};
        const xMax = 1200 - margin.left - margin.right;
        const yMax = 400 - margin.top - margin.bottom;
        const xScale = scaleTime({
            range: [0, xMax],
            domain: extent(this.state.data, x)
        });
        const yScale = scaleLinear({
            range: [yMax, 0],
            domain: [0, this.props.neuronCluster.size]
        });

        return (
            <Card fluid className="monitorWidgetContainer">
                <div>
                    <Header floated='left' size='tiny'>{this.props.neuronCluster.name + ' - Spike Raster'}</Header>
                    <Button floated='right' icon='window close' size='mini' onClick={this.onClose.bind(this)}/>
                    <Button floated='right' icon='window minimize' size='mini' onClick={this.toggle.bind(this)}/>
                </div>
                <Card.Content className={"spikeFrequencyWidgetBody collapse" + (this.state.open ? ' in' : '')}>
                    <svg width='100%' height='400px'>
                        <Group>
                            <AxisLeft
                                scale={yScale}
                                top={0}
                                left={0}
                                stroke={'white'}
                                tickTextFill={'white'}
                                strokeWidth={1}
                            />
                            <AxisBottom
                                scale={xScale}
                                top={yMax}
                                stroke={'white'}
                                tickTextFill={'white'}
                                strokeWidth={1}
                            />
                            {this.state.data.map((point, i) => {
                                return (
                                    <GlyphDot
                                        className={"glyph-dots"}
                                        cx={xScale(x(point))}
                                        cy={yScale(y(point))}
                                        r={1}
                                        fill={"white"}
                                    />
                                  );
                             })}
                        </Group>
                    </svg>
                </Card.Content>
            </Card>
        )
    }
}

export default SpikeRasterWidget;
