import React, { Component } from "react";
import { Card, Header, Button } from 'semantic-ui-react';
import { LinearGradient } from '@vx/gradient';
import { AxisLeft, AxisBottom } from '@vx/axis';
import { scaleTime, scaleLinear } from '@vx/scale';
import { Group } from '@vx/group';
import { AreaClosed } from '@vx/shape';
import { extent, max } from 'd3-array';

class SpikeFrequencyWidget extends Component {

    constructor() {
        super();

        this.state = {
            open: true,
            data: [],
            interval: 50,
            maxLength: 200,
            last: new Date(),
            count: 0
        };
    }

    componentDidMount() {
        this.props.streamNeuronSpikeEvent(this.props.neuronCluster.id, this.handleNeuronSpikeEvent);
    }

    handleNeuronSpikeEvent = (neuronSpikeEvent) => {
        var now = new Date();

        this.state.count += 1;

        if (now - this.state.last >= this.state.interval) {
            if (this.state.data.length > this.state.maxLength) {
                this.state.data.shift();
            }
            this.state.data.push({time: now, count: this.state.count});
            this.state.last = now;
            this.state.count = 0;
            this.forceUpdate();
        }
    }

    toggle() {
        this.setState({
            open: !this.state.open
        });
    }

    onClose() {
        this.props.close(this);
    }

    render() {
        console.log(this);
        const x = (d) => {return d.time};
        const y = (d) => {return d.count};
        const margin = {top: 10, bottom: 10, left: 10, right: 10};
        const xMax = 1200 - margin.left - margin.right;
        const yMax = 150 - margin.top - margin.bottom;
        const xScale = scaleTime({
            range: [0, xMax],
            domain: extent(this.state.data, x)
        });
        const yScale = scaleLinear({
            range: [yMax, 0],
            domain: [0, max(this.state.data, y)]
        });

        return (
            <Card fluid className="monitorWidgetContainer">
                <div>
                    <Header floated='left' size='tiny'>{this.props.neuronCluster.name + ' - Spike Frequency'}</Header>
                    <Button floated='right' icon='window close' size='mini' onClick={this.onClose.bind(this)}/>
                    <Button floated='right' icon='window minimize' size='mini' onClick={this.toggle.bind(this)}/>
                </div>
                    <Card.Content className={"spikeFrequencyWidgetBody collapse" + (this.state.open ? ' in' : '')}>
                        <svg width='100%' height='170px'>
                            <Group top={10} left={40}>
                                <LinearGradient
                                    id="fill"
                                    from="#6086d6"
                                    to="#6086d6"
                                    fromOpacity={0.2}
                                    toOpacity={0}
                                />
                                <AxisLeft
                                    scale={yScale}
                                    top={0}
                                    left={0}
                                    stroke={'white'}
                                    tickTextFill={'white'}
                                    strokeWidth={1}
                                />
                                <AxisBottom
                                    scale={xScale}
                                    top={yMax}
                                    stroke={'white'}
                                    tickTextFill={'white'}
                                    strokeWidth={1}
                                />
                                <AreaClosed
                                    data={this.state.data}
                                    xScale={xScale}
                                    yScale={yScale}
                                    x={x}
                                    y={y}
                                    fill={"url(#fill)"}
                                    stroke={"white"}
                                    strokeWidth={1}
                                />
                            </Group>
                        </svg>
                    </Card.Content>
            </Card>
        )
    }
}

export default SpikeFrequencyWidget;
