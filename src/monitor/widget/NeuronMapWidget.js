import React, { Component } from "react";
import { Card, Header, Button } from 'semantic-ui-react';
import * as THREE from 'three';
import ball from './sprites/ball.png';

var OrbitControls = require('three-orbit-controls')(THREE);

class NeuronMapWidget extends Component {

    constructor() {
        super();

        this.state = {
            open: true
        };
    }

    componentDidMount() {
        var controls, camera, scene, renderer, size, points, positions, geometry, material, colors, color, sprite;
        var arrayBuffer, positionBuffer, colorBuffer;
        var width = this.mount.clientWidth;
        var height = this.mount.clientHeight;

        size = this.props.neuronCluster.size;
        this.neurons = {};

        for (i = 0; i < size; i++) {
            var neuronId = this.props.neuronCluster.neuronIds[i];
            this.neurons[neuronId] = {
                r: i*16-4,
                g: i*16-3,
                b: i*16-2
            };
        }

        camera = new THREE.PerspectiveCamera(50, width / height, 0.1, size/1);
        camera.position.z = size/300;
        scene = new THREE.Scene();
        scene.background = new THREE.Color(0x797e83);

        geometry = new THREE.BufferGeometry();
        positions = [];
        colors = [];
        color = new THREE.Color();

        var n = size/500, n2 = n / 2; // size spread in the cube n=1000
        for (var i = 0; i < size; i ++) {
            // positions
            var x = Math.random() * n - n2;
            var y = Math.random() * n - n2;
            var z = Math.random() * n - n2;
            positions.push(x, y, z);
            // colors
            color.setRGB(0, 0, 0);
            colors.push(color.r, color.g, color.b);

            var neuronId = this.props.neuronCluster.neuronIds[i];
            this.neurons[neuronId] = {
                r: i*3+0,
                g: i*3+1,
                b: i*3+2
            };
        }


        var colorAttribute = new THREE.Float32BufferAttribute(colors, 3, true);
        var positionAttribute = new THREE.Float32BufferAttribute(positions, 3);
        colorAttribute.setDynamic(true);
        geometry.addAttribute('position', positionAttribute);
        geometry.addAttribute('color', colorAttribute);
        geometry.computeBoundingSphere();

        sprite = THREE.ImageUtils.loadTexture(ball);
        material = new THREE.PointsMaterial({size: n/200, map: sprite, vertexColors: THREE.VertexColors});
        points = new THREE.Points(geometry, material);
        scene.add(points);

        renderer = new THREE.WebGLRenderer();
        renderer.setPixelRatio(window.devicePixelRatio);
        renderer.setSize(width, height);

        controls = new OrbitControls(camera, renderer.domElement);
        controls.maxPolarAngle = Math.PI / 2;

        this.camera = camera;
        this.scene = scene;
        this.renderer = renderer;
        this.material = material;
        this.controls = controls;
        this.last = 0;

        this.mount.appendChild(this.renderer.domElement);
        this.start();
    }

    componentWillUnmount = () => {
        this.stop();
        this.mount.removeChild(this.renderer.domElement);
    }

    start = () => {
        if (!this.frameId) {
            this.frameId = requestAnimationFrame(this.animate);
            this.camera.lookAt(this.scene.position);
            this.renderer.render(this.scene, this.camera);
        }
        this.props.streamNeuronSpikeEvent(this.props.neuronCluster.id, this.handleNeuronSpikeEvent);
    }

    stop = () => {
        cancelAnimationFrame(this.frameId);
        // TODO stop stream
    }

    animate = (now) => {
        // TODO remvoe 0 timeout and create a keep-alive event to keep stream running
        if(!this.last || now - this.last >= 0) {
            this.last = now;
            this.resetParticleColor();
        }
        this.renderer.render(this.scene, this.camera);
        this.frameId = window.requestAnimationFrame(this.animate);
    }

    toggle() {
        this.setState({
            open: !this.state.open
        });
    }

    onClose() {
        this.props.close(this);
    }

    handleContentMouseDown = (e) => {
        e.stopPropagation();
    }

    handleNeuronSpikeEvent = (neuronSpikeEvent) => {
        var neuronId = neuronSpikeEvent.getNeuronid();
        this.scene.children[0].geometry.attributes.color.array[this.neurons[neuronId].r] = 255;
        this.scene.children[0].geometry.attributes.color.array[this.neurons[neuronId].g] = 255;
        this.scene.children[0].geometry.attributes.color.array[this.neurons[neuronId].b] = 255;
        this.scene.children[0].geometry.attributes.color.needsUpdate = true;
    }

    resetParticleColor = () => {
        for (var i=0; i < this.scene.children[0].geometry.attributes.color.array.length; i++) {
            this.scene.children[0].geometry.attributes.color.array[i] = this.scene.children[0].geometry.attributes.color.array[i] * 0.7;
        }
        this.scene.children[0].geometry.attributes.color.needsUpdate = true;
    }

    render() {
        return (
            <Card fluid className="monitorWidgetContainer">
                <div>
                    <Header floated='left' size='tiny'>{this.props.neuronCluster.name + ' - Neuron Map'}</Header>
                    <Button floated='right' icon='window close' size='mini' onClick={this.onClose.bind(this)}/>
                    <Button floated='right' icon='window minimize' size='mini' onClick={this.toggle.bind(this)}/>
                </div>
                <Card.Content className={"collapse" + (this.state.open ? ' in' : '')}>
                    <div style={{ width: '100%', height: '400px' }} ref={(mount) => { this.mount = mount }} onMouseDown={this.handleContentMouseDown}/>
                </Card.Content>
            </Card>
        )
    }

}

export default NeuronMapWidget;
