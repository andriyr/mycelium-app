import React, { Component } from "react";
import { Accordion, Menu } from 'semantic-ui-react';
import {
    NEURON_MAP,
    SPIKE_RASTER,
    SPIKE_FREQUENCY,
    DOPAMINE
} from "./widget/Type";

class MonitorMenu extends Component {

    state = {
        activeComponentIndex: 0,
        activeNeuronClusterIndex: 0,
        activeSynapseClusterIndex: 0,
        activeSTDPPreceptorIndex: 0,
        activeModulatedSTDPPreceptorIndex: 0,
        activeDopamineAdapterIndex: 0,
        activeInputAdapterIndex: 0,
        activeOutputAdapterIndex: 0
    }

    handleComponentMenuClick = (e, titleProps) => {
        const { index } = titleProps
        const { activeComponentIndex} = this.state
        const newIndex = activeComponentIndex === index ? -1 : index

        this.setState({ activeComponentIndex: newIndex })
    }

    handleNeuronClusterMenuClick = (e, titleProps) => {
        const { index } = titleProps
        const { activeNeuronClusterIndex} = this.state
        const newIndex = activeNeuronClusterIndex === index ? -1 : index

        this.setState({ activeNeuronClusterIndex: newIndex })
    }

    handleSynapseClusterMenuClick = (e, titleProps) => {
        const { index } = titleProps
        const { activeSynapseClusterIndex} = this.state
        const newIndex = activeSynapseClusterIndex === index ? -1 : index

        this.setState({ activeSynapseClusterIndex: newIndex })
    }

    handleSTDPPreceptorMenuClick = (e, titleProps) => {
        const { index } = titleProps
        const { activeSTDPPreceptorIndex} = this.state
        const newIndex = activeSTDPPreceptorIndex === index ? -1 : index

        this.setState({ activeSTDPPreceptorIndex: newIndex })
    }

    handleModulatedSTDPPreceptorMenuClick = (e, titleProps) => {
        const { index } = titleProps
        const { activeModulatedSTDPPreceptorIndex} = this.state
        const newIndex = activeModulatedSTDPPreceptorIndex === index ? -1 : index

        this.setState({ activeModulatedSTDPPreceptorIndex: newIndex })
    }

    handleDopamineAdapterMenuClick = (e, titleProps) => {
        const { index } = titleProps
        const { activeDopamineAdapterIndex} = this.state
        const newIndex = activeDopamineAdapterIndex === index ? -1 : index

        this.setState({ activeDopamineAdapterIndex: newIndex })
    }

    handleInputAdapterMenuClick = (e, titleProps) => {
        const { index } = titleProps
        const { activeInputAdapterIndex} = this.state
        const newIndex = activeInputAdapterIndex === index ? -1 : index

        this.setState({ activeInputAdapterIndex: newIndex })
    }

    handleOutputAdapterMenuClick = (e, titleProps) => {
        const { index } = titleProps
        const { activeOutputAdapterIndex} = this.state
        const newIndex = activeOutputAdapterIndex === index ? -1 : index

        this.setState({ activeOutputAdapterIndex: newIndex })
    }

    render() {
        const { activeComponentIndex, activeNeuronClusterIndex, activeSynapseClusterIndex, activeSTDPPreceptorIndex, activeModulatedSTDPPreceptorIndex, activeDopamineAdapterIndex, activeInputAdapterIndex, activeOutputAdapterIndex } = this.state

        return (
            <Accordion as={Menu} vertical inverted>
                <Menu.Item disabled>
                    <Accordion.Title
                        active={activeComponentIndex === 0}
                        content='Neuron Cluster'
                        index={0}
                        onClick={this.handleComponentMenuClick}
                    />
                    <Accordion.Content active={activeComponentIndex === 0}>
                        <Accordion as={Menu} vertical inverted>
                            {this.props.neuronClusters.map((neuronCluster, index) => {
                                return (
                                    <Menu.Item>
                                        <Accordion.Title
                                            active={activeNeuronClusterIndex === index}
                                            content={neuronCluster.name}
                                            index={index}
                                            onClick={this.handleNeuronClusterMenuClick}
                                        />
                                        <Accordion.Content active={activeNeuronClusterIndex === index}>
                                            <Menu.Item name="Neuron Map" onClick={this.props.addWidget} widget={{id: neuronCluster.id, type: NEURON_MAP}}/>
                                            <Menu.Item name="Spike Raster" onClick={this.props.addWidget} widget={{id: neuronCluster.id, type: SPIKE_RASTER}}/>
                                            <Menu.Item name="Spike Frequency" onClick={this.props.addWidget} widget={{id: neuronCluster.id, type: SPIKE_FREQUENCY}}/>
                                        </Accordion.Content>
                                    </Menu.Item>)
                            })}
                        </Accordion>
                    </Accordion.Content>
                </Menu.Item>
                <Menu.Item disabled={this.props.synapseClusters.length==0}>
                    <Accordion.Title
                        active={activeComponentIndex === 1}
                        content='Synapse Cluster'
                        index={1}
                        onClick={this.handleComponentMenuClick}
                    />
                    <Accordion.Content active={activeComponentIndex === 1}>
                        <Accordion as={Menu} vertical inverted>
                            {this.props.synapseClusters.map((synapseCluster, index) => {
                                return (
                                    <Menu.Item>
                                        <Accordion.Title
                                            active={activeSynapseClusterIndex === index}
                                            content={synapseCluster.name}
                                            index={index}
                                            onClick={this.handleSynapseClusterMenuClick}
                                        />
                                        <Accordion.Content active={activeSynapseClusterIndex === index}>
                                        </Accordion.Content>
                                    </Menu.Item>)
                            })}
                        </Accordion>
                    </Accordion.Content>
                </Menu.Item>
                <Menu.Item disabled={this.props.STDPPreceptors.length==0}>
                    <Accordion.Title
                        active={activeComponentIndex === 2}
                        content='STDP Preceptor'
                        index={2}
                        onClick={this.handleComponentMenuClick}
                    />
                    <Accordion.Content active={activeComponentIndex === 2}>
                        <Accordion as={Menu} vertical inverted>
                            {this.props.STDPPreceptors.map((STDPPreceptor, index) => {
                                return (
                                    <Menu.Item>
                                        <Accordion.Title
                                            active={activeSTDPPreceptorIndex === index}
                                            content={STDPPreceptor.name}
                                            index={index}
                                            onClick={this.handleSTDPPreceptorMenuClick}
                                        />
                                        <Accordion.Content active={activeSTDPPreceptorIndex === index}>
                                        </Accordion.Content>
                                    </Menu.Item>)
                            })}
                        </Accordion>
                    </Accordion.Content>
                </Menu.Item>
                <Menu.Item disabled={this.props.modulatedSTDPPreceptors.length==0}>
                    <Accordion.Title
                        active={activeComponentIndex === 3}
                        content='Modulated STDP Preceptor'
                        index={3}
                        onClick={this.handleComponentMenuClick}
                    />
                    <Accordion.Content active={activeComponentIndex === 3}>
                        <Accordion as={Menu} vertical inverted>
                            {this.props.modulatedSTDPPreceptors.map((modulatedSTDPPreceptor, index) => {
                                return (
                                    <Menu.Item>
                                        <Accordion.Title
                                            active={activeModulatedSTDPPreceptorIndex === index}
                                            content={modulatedSTDPPreceptor.name}
                                            index={index}
                                            onClick={this.handleModulatedSTDPPreceptorMenuClick}
                                        />
                                        <Accordion.Content active={activeModulatedSTDPPreceptorIndex === index}>
                                            <Menu.Item name="Modulated STDP" onClick={this.props.addWidget} widget={{id: modulatedSTDPPreceptor.id, type: DOPAMINE}}/>
                                        </Accordion.Content>
                                    </Menu.Item>)
                            })}
                        </Accordion>
                    </Accordion.Content>
                </Menu.Item>
                <Menu.Item disabled={this.props.dopamineAdapters.length==0}>
                    <Accordion.Title
                        active={activeComponentIndex === 4}
                        content='Dopamine Adapter'
                        index={4}
                        onClick={this.handleComponentMenuClick}
                    />
                    <Accordion.Content active={activeComponentIndex === 4}>
                        <Accordion as={Menu} vertical inverted>
                            {this.props.dopamineAdapters.map((dopamineAdapter, index) => {
                                return (
                                    <Menu.Item>
                                        <Accordion.Title
                                            active={activeDopamineAdapterIndex === index}
                                            content={dopamineAdapter.name}
                                            index={index}
                                            onClick={this.handleDopamineAdapterMenuClick}
                                        />
                                        <Accordion.Content active={activeDopamineAdapterIndex === index}>
                                            <Menu.Item name="Dopamine Rate"/>
                                        </Accordion.Content>
                                    </Menu.Item>)
                            })}
                        </Accordion>
                    </Accordion.Content>
                </Menu.Item>
                <Menu.Item disabled={this.props.inputAdapters.length==0}>
                    <Accordion.Title
                        active={activeComponentIndex === 5}
                        content='Input Adapter'
                        index={5}
                        onClick={this.handleComponentMenuClick}
                    />
                    <Accordion.Content active={activeComponentIndex === 5}>
                        <Accordion as={Menu} vertical inverted>
                            {this.props.inputAdapters.map((inputAdapter, index) => {
                                return (
                                    <Menu.Item>
                                        <Accordion.Title
                                            active={activeInputAdapterIndex === index}
                                            content={inputAdapter.name}
                                            index={index}
                                            onClick={this.handleInputAdapterMenuClick}
                                        />
                                        <Accordion.Content active={activeInputAdapterIndex === index}>
                                            <Menu.Item name="Input Rate"/>
                                        </Accordion.Content>
                                    </Menu.Item>)
                            })}
                        </Accordion>
                    </Accordion.Content>
                </Menu.Item>
                <Menu.Item disabled={this.props.outputAdapters.length==0}>
                    <Accordion.Title
                        active={activeComponentIndex === 6}
                        content='Output Adapter'
                        index={6}
                        onClick={this.handleComponentMenuClick}
                    />
                    <Accordion.Content active={activeComponentIndex === 6}>
                        <Accordion as={Menu} vertical inverted>
                            {this.props.outputAdapters.map((outputAdapter, index) => {
                                return (
                                    <Menu.Item>
                                        <Accordion.Title
                                            active={activeOutputAdapterIndex === index}
                                            content={outputAdapter.name}
                                            index={index}
                                            onClick={this.handleOutputAdapterMenuClick}
                                        />
                                        <Accordion.Content active={activeOutputAdapterIndex === index}>
                                            <Menu.Item name="Output Rate"/>
                                        </Accordion.Content>
                                    </Menu.Item>)
                            })}
                        </Accordion>
                    </Accordion.Content>
                </Menu.Item>
            </Accordion>
        )
    }
}

export default MonitorMenu;
