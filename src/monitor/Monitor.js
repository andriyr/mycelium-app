import React, { Component } from "react";
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import {
    NEURON_MAP,
    SPIKE_RASTER,
    SPIKE_FREQUENCY
} from "./widget/Type";
import {
    addWidget,
    removeWidget,
    streamNeuronSpikeEvent,
    streamNeuronSpikeRelayEvent
} from '../actions/index';
import NeuronMapWidget from "./widget/NeuronMapWidget";
import SpikeRasterWidget from "./widget/SpikeRasterWidget";
import SpikeFrequencyWidget from "./widget/SpikeFrequencyWidget";
import MonitorMenu from "./MonitorMenu";


class Monitor extends Component {

    addWidget = (e, data) => {
        this.props.addWidget({
            id: data.widget.id,
            type: data.widget.type
        });
    }

    removeWidget = (widget) => {
        this.props.removeWidget({id: widget.id});
    }

    render() {
        return (
            <div className="monitor">
                <div className="monitorMenu">
                    <MonitorMenu
                        neuronClusters={this.props.neuronClusters}
                        synapseClusters={this.props.synapseClusters}
                        STDPPreceptors={this.props.STDPPreceptors}
                        modulatedSTDPPreceptors={this.props.modulatedSTDPPreceptors}
                        dopamineAdapters={this.props.dopamineAdapters}
                        inputAdapters={this.props.inputAdapters}
                        outputAdapters={this.props.outputAdapters}
                        addWidget={this.addWidget}
                    />
                </div>
                <div className="monitorContent">
                    <DragDropContext>
                        <Droppable droppableId="droppable">
                            {(provided, snapshot) => (
                                <div ref={provided.innerRef} style={{width: '100%'}}>
                                    {this.props.widgets.map((widget, index) => {
                                        switch (widget.type) {
                                            case NEURON_MAP:
                                                var neuronCluster = this.props.neuronClusters.find((neuronCluster) => {return neuronCluster.id === widget.id})
                                                return (
                                                    <Draggable key={widget.id} draggableId={widget.id} index={index}>
                                                        {(provided, snapshot) => (
                                                            <div>
                                                                <div ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps}>
                                                                    <NeuronMapWidget streamNeuronSpikeEvent={this.props.streamNeuronSpikeEvent} close={this.removeWidget} neuronCluster={neuronCluster}/>
                                                                </div>
                                                                {provided.placeholder}
                                                            </div>
                                                        )}
                                                    </Draggable>
                                                )
                                            case SPIKE_RASTER:
                                                var neuronCluster = this.props.neuronClusters.find((neuronCluster) => {return neuronCluster.id === widget.id})
                                                return (
                                                    <Draggable key={widget.id} draggableId={widget.id} index={index}>
                                                        {(provided, snapshot) => (
                                                            <div>
                                                                <div ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps}>
                                                                    <SpikeRasterWidget streamNeuronSpikeEvent={this.props.streamNeuronSpikeEvent} close={this.removeWidget} neuronCluster={neuronCluster}/>
                                                                </div>
                                                                {provided.placeholder}
                                                            </div>
                                                        )}
                                                    </Draggable>
                                                )
                                            case SPIKE_FREQUENCY:
                                                var neuronCluster = this.props.neuronClusters.find((neuronCluster) => {return neuronCluster.id === widget.id})
                                                return (
                                                    <Draggable key={widget.id} draggableId={widget.id} index={index}>
                                                        {(provided, snapshot) => (
                                                            <div>
                                                                <div ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps}>
                                                                    <SpikeFrequencyWidget streamNeuronSpikeEvent={this.props.streamNeuronSpikeEvent} close={this.removeWidget} neuronCluster={neuronCluster}/>
                                                                </div>
                                                                {provided.placeholder}
                                                            </div>
                                                        )}
                                                    </Draggable>
                                                )
                                            default:
                                                // TODO
                                        }
                                    })}
                                    {provided.placeholder}
                                </div>
                            )}
                        </Droppable>
                    </DragDropContext>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        widgets: state.monitor.widgets,
        neuronClusters: state.configuration.neuronClusters,
        synapseClusters: state.configuration.synapseClusters,
        STDPPreceptors: state.configuration.STDPPreceptors,
        modulatedSTDPPreceptors: state.configuration.modulatedSTDPPreceptors,
        dopamineAdapters: state.configuration.dopamineAdapters,
        inputAdapters: state.configuration.inputAdapters,
        outputAdapters: state.configuration.outputAdapters
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        addWidget: addWidget,
        removeWidget: removeWidget,
        streamNeuronSpikeEvent: streamNeuronSpikeEvent,
        streamNeuronSpikeRelayEvent: streamNeuronSpikeRelayEvent
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Monitor);
