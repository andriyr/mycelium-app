/* eslint-disable */
// package: api
// file: api.proto

import * as jspb from "google-protobuf";

export class NodeRequest extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): NodeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: NodeRequest): NodeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: NodeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): NodeRequest;
  static deserializeBinaryFromReader(message: NodeRequest, reader: jspb.BinaryReader): NodeRequest;
}

export namespace NodeRequest {
  export type AsObject = {
  }
}

export class NodeResponse extends jspb.Message {
  clearNeuronclustersList(): void;
  getNeuronclustersList(): Array<NeuronClusterResponse>;
  setNeuronclustersList(value: Array<NeuronClusterResponse>): void;
  addNeuronclusters(value?: NeuronClusterResponse, index?: number): NeuronClusterResponse;

  clearSynapseclustersList(): void;
  getSynapseclustersList(): Array<SynapseClusterResponse>;
  setSynapseclustersList(value: Array<SynapseClusterResponse>): void;
  addSynapseclusters(value?: SynapseClusterResponse, index?: number): SynapseClusterResponse;

  clearStdppreceptorsList(): void;
  getStdppreceptorsList(): Array<STDPPreceptorResponse>;
  setStdppreceptorsList(value: Array<STDPPreceptorResponse>): void;
  addStdppreceptors(value?: STDPPreceptorResponse, index?: number): STDPPreceptorResponse;

  clearModulatedstdppreceptorsList(): void;
  getModulatedstdppreceptorsList(): Array<ModulatedSTDPPreceptorResponse>;
  setModulatedstdppreceptorsList(value: Array<ModulatedSTDPPreceptorResponse>): void;
  addModulatedstdppreceptors(value?: ModulatedSTDPPreceptorResponse, index?: number): ModulatedSTDPPreceptorResponse;

  clearInputadaptersList(): void;
  getInputadaptersList(): Array<InputAdapterResponse>;
  setInputadaptersList(value: Array<InputAdapterResponse>): void;
  addInputadapters(value?: InputAdapterResponse, index?: number): InputAdapterResponse;

  clearOutputadaptersList(): void;
  getOutputadaptersList(): Array<OutputAdapterResponse>;
  setOutputadaptersList(value: Array<OutputAdapterResponse>): void;
  addOutputadapters(value?: OutputAdapterResponse, index?: number): OutputAdapterResponse;

  clearDopamineadaptersList(): void;
  getDopamineadaptersList(): Array<DopamineAdapterResponse>;
  setDopamineadaptersList(value: Array<DopamineAdapterResponse>): void;
  addDopamineadapters(value?: DopamineAdapterResponse, index?: number): DopamineAdapterResponse;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): NodeResponse.AsObject;
  static toObject(includeInstance: boolean, msg: NodeResponse): NodeResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: NodeResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): NodeResponse;
  static deserializeBinaryFromReader(message: NodeResponse, reader: jspb.BinaryReader): NodeResponse;
}

export namespace NodeResponse {
  export type AsObject = {
    neuronclustersList: Array<NeuronClusterResponse.AsObject>,
    synapseclustersList: Array<SynapseClusterResponse.AsObject>,
    stdppreceptorsList: Array<STDPPreceptorResponse.AsObject>,
    modulatedstdppreceptorsList: Array<ModulatedSTDPPreceptorResponse.AsObject>,
    inputadaptersList: Array<InputAdapterResponse.AsObject>,
    outputadaptersList: Array<OutputAdapterResponse.AsObject>,
    dopamineadaptersList: Array<DopamineAdapterResponse.AsObject>,
  }
}

export class NeuronClusterRequest extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): NeuronClusterRequest.AsObject;
  static toObject(includeInstance: boolean, msg: NeuronClusterRequest): NeuronClusterRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: NeuronClusterRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): NeuronClusterRequest;
  static deserializeBinaryFromReader(message: NeuronClusterRequest, reader: jspb.BinaryReader): NeuronClusterRequest;
}

export namespace NeuronClusterRequest {
  export type AsObject = {
    id: string,
  }
}

export class NeuronClusterResponse extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getName(): string;
  setName(value: string): void;

  getSize(): number;
  setSize(value: number): void;

  getRatio(): number;
  setRatio(value: number): void;

  clearNeuronidsList(): void;
  getNeuronidsList(): Array<string>;
  setNeuronidsList(value: Array<string>): void;
  addNeuronids(value: string, index?: number): string;

  clearInputadapteridsList(): void;
  getInputadapteridsList(): Array<string>;
  setInputadapteridsList(value: Array<string>): void;
  addInputadapterids(value: string, index?: number): string;

  hasGraph(): boolean;
  clearGraph(): void;
  getGraph(): Graph | undefined;
  setGraph(value?: Graph): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): NeuronClusterResponse.AsObject;
  static toObject(includeInstance: boolean, msg: NeuronClusterResponse): NeuronClusterResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: NeuronClusterResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): NeuronClusterResponse;
  static deserializeBinaryFromReader(message: NeuronClusterResponse, reader: jspb.BinaryReader): NeuronClusterResponse;
}

export namespace NeuronClusterResponse {
  export type AsObject = {
    id: string,
    name: string,
    size: number,
    ratio: number,
    neuronidsList: Array<string>,
    inputadapteridsList: Array<string>,
    graph?: Graph.AsObject,
  }
}

export class CreateNeuronClusterRequest extends jspb.Message {
  getName(): string;
  setName(value: string): void;

  getSize(): number;
  setSize(value: number): void;

  getRatio(): number;
  setRatio(value: number): void;

  hasGraph(): boolean;
  clearGraph(): void;
  getGraph(): Graph | undefined;
  setGraph(value?: Graph): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateNeuronClusterRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CreateNeuronClusterRequest): CreateNeuronClusterRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CreateNeuronClusterRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateNeuronClusterRequest;
  static deserializeBinaryFromReader(message: CreateNeuronClusterRequest, reader: jspb.BinaryReader): CreateNeuronClusterRequest;
}

export namespace CreateNeuronClusterRequest {
  export type AsObject = {
    name: string,
    size: number,
    ratio: number,
    graph?: Graph.AsObject,
  }
}

export class DeleteNeuronClusterRequest extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteNeuronClusterRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteNeuronClusterRequest): DeleteNeuronClusterRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeleteNeuronClusterRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteNeuronClusterRequest;
  static deserializeBinaryFromReader(message: DeleteNeuronClusterRequest, reader: jspb.BinaryReader): DeleteNeuronClusterRequest;
}

export namespace DeleteNeuronClusterRequest {
  export type AsObject = {
    id: string,
  }
}

export class DeleteNeuronClusterResponse extends jspb.Message {
  getStatus(): string;
  setStatus(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteNeuronClusterResponse.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteNeuronClusterResponse): DeleteNeuronClusterResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeleteNeuronClusterResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteNeuronClusterResponse;
  static deserializeBinaryFromReader(message: DeleteNeuronClusterResponse, reader: jspb.BinaryReader): DeleteNeuronClusterResponse;
}

export namespace DeleteNeuronClusterResponse {
  export type AsObject = {
    status: string,
  }
}

export class SetNeuronClusterPositionRequest extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  hasGraph(): boolean;
  clearGraph(): void;
  getGraph(): Graph | undefined;
  setGraph(value?: Graph): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SetNeuronClusterPositionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SetNeuronClusterPositionRequest): SetNeuronClusterPositionRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SetNeuronClusterPositionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SetNeuronClusterPositionRequest;
  static deserializeBinaryFromReader(message: SetNeuronClusterPositionRequest, reader: jspb.BinaryReader): SetNeuronClusterPositionRequest;
}

export namespace SetNeuronClusterPositionRequest {
  export type AsObject = {
    id: string,
    graph?: Graph.AsObject,
  }
}

export class SetNeuronClusterPositionResponse extends jspb.Message {
  getStatus(): string;
  setStatus(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SetNeuronClusterPositionResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SetNeuronClusterPositionResponse): SetNeuronClusterPositionResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SetNeuronClusterPositionResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SetNeuronClusterPositionResponse;
  static deserializeBinaryFromReader(message: SetNeuronClusterPositionResponse, reader: jspb.BinaryReader): SetNeuronClusterPositionResponse;
}

export namespace SetNeuronClusterPositionResponse {
  export type AsObject = {
    status: string,
  }
}

export class SynapseClusterRequest extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SynapseClusterRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SynapseClusterRequest): SynapseClusterRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SynapseClusterRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SynapseClusterRequest;
  static deserializeBinaryFromReader(message: SynapseClusterRequest, reader: jspb.BinaryReader): SynapseClusterRequest;
}

export namespace SynapseClusterRequest {
  export type AsObject = {
    id: string,
  }
}

export class SynapseClusterResponse extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getName(): string;
  setName(value: string): void;

  getNeuronscovered(): number;
  setNeuronscovered(value: number): void;

  getInitialsynapseweight(): number;
  setInitialsynapseweight(value: number): void;

  clearNeuronclusteridsList(): void;
  getNeuronclusteridsList(): Array<string>;
  setNeuronclusteridsList(value: Array<string>): void;
  addNeuronclusterids(value: string, index?: number): string;

  hasGraph(): boolean;
  clearGraph(): void;
  getGraph(): Graph | undefined;
  setGraph(value?: Graph): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SynapseClusterResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SynapseClusterResponse): SynapseClusterResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SynapseClusterResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SynapseClusterResponse;
  static deserializeBinaryFromReader(message: SynapseClusterResponse, reader: jspb.BinaryReader): SynapseClusterResponse;
}

export namespace SynapseClusterResponse {
  export type AsObject = {
    id: string,
    name: string,
    neuronscovered: number,
    initialsynapseweight: number,
    neuronclusteridsList: Array<string>,
    graph?: Graph.AsObject,
  }
}

export class CreateSynapseClusterRequest extends jspb.Message {
  getName(): string;
  setName(value: string): void;

  getNeuronscovered(): number;
  setNeuronscovered(value: number): void;

  getInitialsynapseweight(): number;
  setInitialsynapseweight(value: number): void;

  hasGraph(): boolean;
  clearGraph(): void;
  getGraph(): Graph | undefined;
  setGraph(value?: Graph): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateSynapseClusterRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CreateSynapseClusterRequest): CreateSynapseClusterRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CreateSynapseClusterRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateSynapseClusterRequest;
  static deserializeBinaryFromReader(message: CreateSynapseClusterRequest, reader: jspb.BinaryReader): CreateSynapseClusterRequest;
}

export namespace CreateSynapseClusterRequest {
  export type AsObject = {
    name: string,
    neuronscovered: number,
    initialsynapseweight: number,
    graph?: Graph.AsObject,
  }
}

export class DeleteSynapseClusterRequest extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteSynapseClusterRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteSynapseClusterRequest): DeleteSynapseClusterRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeleteSynapseClusterRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteSynapseClusterRequest;
  static deserializeBinaryFromReader(message: DeleteSynapseClusterRequest, reader: jspb.BinaryReader): DeleteSynapseClusterRequest;
}

export namespace DeleteSynapseClusterRequest {
  export type AsObject = {
    id: string,
  }
}

export class DeleteSynapseClusterResponse extends jspb.Message {
  getStatus(): string;
  setStatus(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteSynapseClusterResponse.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteSynapseClusterResponse): DeleteSynapseClusterResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeleteSynapseClusterResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteSynapseClusterResponse;
  static deserializeBinaryFromReader(message: DeleteSynapseClusterResponse, reader: jspb.BinaryReader): DeleteSynapseClusterResponse;
}

export namespace DeleteSynapseClusterResponse {
  export type AsObject = {
    status: string,
  }
}

export class AttachSynapseClusterRequest extends jspb.Message {
  getSynapseclusterid(): string;
  setSynapseclusterid(value: string): void;

  getNeuronclusterid(): string;
  setNeuronclusterid(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AttachSynapseClusterRequest.AsObject;
  static toObject(includeInstance: boolean, msg: AttachSynapseClusterRequest): AttachSynapseClusterRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: AttachSynapseClusterRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AttachSynapseClusterRequest;
  static deserializeBinaryFromReader(message: AttachSynapseClusterRequest, reader: jspb.BinaryReader): AttachSynapseClusterRequest;
}

export namespace AttachSynapseClusterRequest {
  export type AsObject = {
    synapseclusterid: string,
    neuronclusterid: string,
  }
}

export class AttachSynapseClusterResponse extends jspb.Message {
  getStatus(): string;
  setStatus(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AttachSynapseClusterResponse.AsObject;
  static toObject(includeInstance: boolean, msg: AttachSynapseClusterResponse): AttachSynapseClusterResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: AttachSynapseClusterResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AttachSynapseClusterResponse;
  static deserializeBinaryFromReader(message: AttachSynapseClusterResponse, reader: jspb.BinaryReader): AttachSynapseClusterResponse;
}

export namespace AttachSynapseClusterResponse {
  export type AsObject = {
    status: string,
  }
}

export class DetachSynapseClusterRequest extends jspb.Message {
  getSynapseclusterid(): string;
  setSynapseclusterid(value: string): void;

  getNeuronclusterid(): string;
  setNeuronclusterid(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DetachSynapseClusterRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DetachSynapseClusterRequest): DetachSynapseClusterRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DetachSynapseClusterRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DetachSynapseClusterRequest;
  static deserializeBinaryFromReader(message: DetachSynapseClusterRequest, reader: jspb.BinaryReader): DetachSynapseClusterRequest;
}

export namespace DetachSynapseClusterRequest {
  export type AsObject = {
    synapseclusterid: string,
    neuronclusterid: string,
  }
}

export class DetachSynapseClusterResponse extends jspb.Message {
  getStatus(): string;
  setStatus(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DetachSynapseClusterResponse.AsObject;
  static toObject(includeInstance: boolean, msg: DetachSynapseClusterResponse): DetachSynapseClusterResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DetachSynapseClusterResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DetachSynapseClusterResponse;
  static deserializeBinaryFromReader(message: DetachSynapseClusterResponse, reader: jspb.BinaryReader): DetachSynapseClusterResponse;
}

export namespace DetachSynapseClusterResponse {
  export type AsObject = {
    status: string,
  }
}

export class SetSynapseClusterPositionRequest extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  hasGraph(): boolean;
  clearGraph(): void;
  getGraph(): Graph | undefined;
  setGraph(value?: Graph): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SetSynapseClusterPositionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SetSynapseClusterPositionRequest): SetSynapseClusterPositionRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SetSynapseClusterPositionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SetSynapseClusterPositionRequest;
  static deserializeBinaryFromReader(message: SetSynapseClusterPositionRequest, reader: jspb.BinaryReader): SetSynapseClusterPositionRequest;
}

export namespace SetSynapseClusterPositionRequest {
  export type AsObject = {
    id: string,
    graph?: Graph.AsObject,
  }
}

export class SetSynapseClusterPositionResponse extends jspb.Message {
  getStatus(): string;
  setStatus(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SetSynapseClusterPositionResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SetSynapseClusterPositionResponse): SetSynapseClusterPositionResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SetSynapseClusterPositionResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SetSynapseClusterPositionResponse;
  static deserializeBinaryFromReader(message: SetSynapseClusterPositionResponse, reader: jspb.BinaryReader): SetSynapseClusterPositionResponse;
}

export namespace SetSynapseClusterPositionResponse {
  export type AsObject = {
    status: string,
  }
}

export class STDPPreceptorRequest extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): STDPPreceptorRequest.AsObject;
  static toObject(includeInstance: boolean, msg: STDPPreceptorRequest): STDPPreceptorRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: STDPPreceptorRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): STDPPreceptorRequest;
  static deserializeBinaryFromReader(message: STDPPreceptorRequest, reader: jspb.BinaryReader): STDPPreceptorRequest;
}

export namespace STDPPreceptorRequest {
  export type AsObject = {
    id: string,
  }
}

export class STDPPreceptorResponse extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getName(): string;
  setName(value: string): void;

  getMaxqueuesize(): number;
  setMaxqueuesize(value: number): void;

  getSynapseclusterid(): string;
  setSynapseclusterid(value: string): void;

  hasGraph(): boolean;
  clearGraph(): void;
  getGraph(): Graph | undefined;
  setGraph(value?: Graph): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): STDPPreceptorResponse.AsObject;
  static toObject(includeInstance: boolean, msg: STDPPreceptorResponse): STDPPreceptorResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: STDPPreceptorResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): STDPPreceptorResponse;
  static deserializeBinaryFromReader(message: STDPPreceptorResponse, reader: jspb.BinaryReader): STDPPreceptorResponse;
}

export namespace STDPPreceptorResponse {
  export type AsObject = {
    id: string,
    name: string,
    maxqueuesize: number,
    synapseclusterid: string,
    graph?: Graph.AsObject,
  }
}

export class CreateSTDPPreceptorRequest extends jspb.Message {
  getName(): string;
  setName(value: string): void;

  getMaxqueuesize(): number;
  setMaxqueuesize(value: number): void;

  hasGraph(): boolean;
  clearGraph(): void;
  getGraph(): Graph | undefined;
  setGraph(value?: Graph): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateSTDPPreceptorRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CreateSTDPPreceptorRequest): CreateSTDPPreceptorRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CreateSTDPPreceptorRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateSTDPPreceptorRequest;
  static deserializeBinaryFromReader(message: CreateSTDPPreceptorRequest, reader: jspb.BinaryReader): CreateSTDPPreceptorRequest;
}

export namespace CreateSTDPPreceptorRequest {
  export type AsObject = {
    name: string,
    maxqueuesize: number,
    graph?: Graph.AsObject,
  }
}

export class DeleteSTDPPreceptorRequest extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteSTDPPreceptorRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteSTDPPreceptorRequest): DeleteSTDPPreceptorRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeleteSTDPPreceptorRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteSTDPPreceptorRequest;
  static deserializeBinaryFromReader(message: DeleteSTDPPreceptorRequest, reader: jspb.BinaryReader): DeleteSTDPPreceptorRequest;
}

export namespace DeleteSTDPPreceptorRequest {
  export type AsObject = {
    id: string,
  }
}

export class DeleteSTDPPreceptorResponse extends jspb.Message {
  getStatus(): string;
  setStatus(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteSTDPPreceptorResponse.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteSTDPPreceptorResponse): DeleteSTDPPreceptorResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeleteSTDPPreceptorResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteSTDPPreceptorResponse;
  static deserializeBinaryFromReader(message: DeleteSTDPPreceptorResponse, reader: jspb.BinaryReader): DeleteSTDPPreceptorResponse;
}

export namespace DeleteSTDPPreceptorResponse {
  export type AsObject = {
    status: string,
  }
}

export class AttachSTDPPreceptorRequest extends jspb.Message {
  getStdppreceptorid(): string;
  setStdppreceptorid(value: string): void;

  getSynapseclusterid(): string;
  setSynapseclusterid(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AttachSTDPPreceptorRequest.AsObject;
  static toObject(includeInstance: boolean, msg: AttachSTDPPreceptorRequest): AttachSTDPPreceptorRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: AttachSTDPPreceptorRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AttachSTDPPreceptorRequest;
  static deserializeBinaryFromReader(message: AttachSTDPPreceptorRequest, reader: jspb.BinaryReader): AttachSTDPPreceptorRequest;
}

export namespace AttachSTDPPreceptorRequest {
  export type AsObject = {
    stdppreceptorid: string,
    synapseclusterid: string,
  }
}

export class AttachSTDPPreceptorResponse extends jspb.Message {
  getStatus(): string;
  setStatus(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AttachSTDPPreceptorResponse.AsObject;
  static toObject(includeInstance: boolean, msg: AttachSTDPPreceptorResponse): AttachSTDPPreceptorResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: AttachSTDPPreceptorResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AttachSTDPPreceptorResponse;
  static deserializeBinaryFromReader(message: AttachSTDPPreceptorResponse, reader: jspb.BinaryReader): AttachSTDPPreceptorResponse;
}

export namespace AttachSTDPPreceptorResponse {
  export type AsObject = {
    status: string,
  }
}

export class DetachSTDPPreceptorRequest extends jspb.Message {
  getStdppreceptorid(): string;
  setStdppreceptorid(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DetachSTDPPreceptorRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DetachSTDPPreceptorRequest): DetachSTDPPreceptorRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DetachSTDPPreceptorRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DetachSTDPPreceptorRequest;
  static deserializeBinaryFromReader(message: DetachSTDPPreceptorRequest, reader: jspb.BinaryReader): DetachSTDPPreceptorRequest;
}

export namespace DetachSTDPPreceptorRequest {
  export type AsObject = {
    stdppreceptorid: string,
  }
}

export class DetachSTDPPreceptorResponse extends jspb.Message {
  getStatus(): string;
  setStatus(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DetachSTDPPreceptorResponse.AsObject;
  static toObject(includeInstance: boolean, msg: DetachSTDPPreceptorResponse): DetachSTDPPreceptorResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DetachSTDPPreceptorResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DetachSTDPPreceptorResponse;
  static deserializeBinaryFromReader(message: DetachSTDPPreceptorResponse, reader: jspb.BinaryReader): DetachSTDPPreceptorResponse;
}

export namespace DetachSTDPPreceptorResponse {
  export type AsObject = {
    status: string,
  }
}

export class SetSTDPPreceptorPositionRequest extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  hasGraph(): boolean;
  clearGraph(): void;
  getGraph(): Graph | undefined;
  setGraph(value?: Graph): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SetSTDPPreceptorPositionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SetSTDPPreceptorPositionRequest): SetSTDPPreceptorPositionRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SetSTDPPreceptorPositionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SetSTDPPreceptorPositionRequest;
  static deserializeBinaryFromReader(message: SetSTDPPreceptorPositionRequest, reader: jspb.BinaryReader): SetSTDPPreceptorPositionRequest;
}

export namespace SetSTDPPreceptorPositionRequest {
  export type AsObject = {
    id: string,
    graph?: Graph.AsObject,
  }
}

export class SetSTDPPreceptorPositionResponse extends jspb.Message {
  getStatus(): string;
  setStatus(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SetSTDPPreceptorPositionResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SetSTDPPreceptorPositionResponse): SetSTDPPreceptorPositionResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SetSTDPPreceptorPositionResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SetSTDPPreceptorPositionResponse;
  static deserializeBinaryFromReader(message: SetSTDPPreceptorPositionResponse, reader: jspb.BinaryReader): SetSTDPPreceptorPositionResponse;
}

export namespace SetSTDPPreceptorPositionResponse {
  export type AsObject = {
    status: string,
  }
}

export class ModulatedSTDPPreceptorRequest extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ModulatedSTDPPreceptorRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ModulatedSTDPPreceptorRequest): ModulatedSTDPPreceptorRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ModulatedSTDPPreceptorRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ModulatedSTDPPreceptorRequest;
  static deserializeBinaryFromReader(message: ModulatedSTDPPreceptorRequest, reader: jspb.BinaryReader): ModulatedSTDPPreceptorRequest;
}

export namespace ModulatedSTDPPreceptorRequest {
  export type AsObject = {
    id: string,
  }
}

export class ModulatedSTDPPreceptorResponse extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getName(): string;
  setName(value: string): void;

  getMaxqueuesize(): number;
  setMaxqueuesize(value: number): void;

  getSensitivity(): number;
  setSensitivity(value: number): void;

  getSynapseclusterid(): string;
  setSynapseclusterid(value: string): void;

  getDopamineadapterid(): string;
  setDopamineadapterid(value: string): void;

  hasGraph(): boolean;
  clearGraph(): void;
  getGraph(): Graph | undefined;
  setGraph(value?: Graph): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ModulatedSTDPPreceptorResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ModulatedSTDPPreceptorResponse): ModulatedSTDPPreceptorResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ModulatedSTDPPreceptorResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ModulatedSTDPPreceptorResponse;
  static deserializeBinaryFromReader(message: ModulatedSTDPPreceptorResponse, reader: jspb.BinaryReader): ModulatedSTDPPreceptorResponse;
}

export namespace ModulatedSTDPPreceptorResponse {
  export type AsObject = {
    id: string,
    name: string,
    maxqueuesize: number,
    sensitivity: number,
    synapseclusterid: string,
    dopamineadapterid: string,
    graph?: Graph.AsObject,
  }
}

export class CreateModulatedSTDPPreceptorRequest extends jspb.Message {
  getName(): string;
  setName(value: string): void;

  getMaxqueuesize(): number;
  setMaxqueuesize(value: number): void;

  getSensitivity(): number;
  setSensitivity(value: number): void;

  hasGraph(): boolean;
  clearGraph(): void;
  getGraph(): Graph | undefined;
  setGraph(value?: Graph): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateModulatedSTDPPreceptorRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CreateModulatedSTDPPreceptorRequest): CreateModulatedSTDPPreceptorRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CreateModulatedSTDPPreceptorRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateModulatedSTDPPreceptorRequest;
  static deserializeBinaryFromReader(message: CreateModulatedSTDPPreceptorRequest, reader: jspb.BinaryReader): CreateModulatedSTDPPreceptorRequest;
}

export namespace CreateModulatedSTDPPreceptorRequest {
  export type AsObject = {
    name: string,
    maxqueuesize: number,
    sensitivity: number,
    graph?: Graph.AsObject,
  }
}

export class DeleteModulatedSTDPPreceptorRequest extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteModulatedSTDPPreceptorRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteModulatedSTDPPreceptorRequest): DeleteModulatedSTDPPreceptorRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeleteModulatedSTDPPreceptorRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteModulatedSTDPPreceptorRequest;
  static deserializeBinaryFromReader(message: DeleteModulatedSTDPPreceptorRequest, reader: jspb.BinaryReader): DeleteModulatedSTDPPreceptorRequest;
}

export namespace DeleteModulatedSTDPPreceptorRequest {
  export type AsObject = {
    id: string,
  }
}

export class DeleteModulatedSTDPPreceptorResponse extends jspb.Message {
  getStatus(): string;
  setStatus(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteModulatedSTDPPreceptorResponse.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteModulatedSTDPPreceptorResponse): DeleteModulatedSTDPPreceptorResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeleteModulatedSTDPPreceptorResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteModulatedSTDPPreceptorResponse;
  static deserializeBinaryFromReader(message: DeleteModulatedSTDPPreceptorResponse, reader: jspb.BinaryReader): DeleteModulatedSTDPPreceptorResponse;
}

export namespace DeleteModulatedSTDPPreceptorResponse {
  export type AsObject = {
    status: string,
  }
}

export class AttachModulatedSTDPPreceptorRequest extends jspb.Message {
  getModulatedstdppreceptorid(): string;
  setModulatedstdppreceptorid(value: string): void;

  getSynapseclusterid(): string;
  setSynapseclusterid(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AttachModulatedSTDPPreceptorRequest.AsObject;
  static toObject(includeInstance: boolean, msg: AttachModulatedSTDPPreceptorRequest): AttachModulatedSTDPPreceptorRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: AttachModulatedSTDPPreceptorRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AttachModulatedSTDPPreceptorRequest;
  static deserializeBinaryFromReader(message: AttachModulatedSTDPPreceptorRequest, reader: jspb.BinaryReader): AttachModulatedSTDPPreceptorRequest;
}

export namespace AttachModulatedSTDPPreceptorRequest {
  export type AsObject = {
    modulatedstdppreceptorid: string,
    synapseclusterid: string,
  }
}

export class AttachModulatedSTDPPreceptorResponse extends jspb.Message {
  getStatus(): string;
  setStatus(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AttachModulatedSTDPPreceptorResponse.AsObject;
  static toObject(includeInstance: boolean, msg: AttachModulatedSTDPPreceptorResponse): AttachModulatedSTDPPreceptorResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: AttachModulatedSTDPPreceptorResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AttachModulatedSTDPPreceptorResponse;
  static deserializeBinaryFromReader(message: AttachModulatedSTDPPreceptorResponse, reader: jspb.BinaryReader): AttachModulatedSTDPPreceptorResponse;
}

export namespace AttachModulatedSTDPPreceptorResponse {
  export type AsObject = {
    status: string,
  }
}

export class DetachModulatedSTDPPreceptorRequest extends jspb.Message {
  getModulatedstdppreceptorid(): string;
  setModulatedstdppreceptorid(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DetachModulatedSTDPPreceptorRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DetachModulatedSTDPPreceptorRequest): DetachModulatedSTDPPreceptorRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DetachModulatedSTDPPreceptorRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DetachModulatedSTDPPreceptorRequest;
  static deserializeBinaryFromReader(message: DetachModulatedSTDPPreceptorRequest, reader: jspb.BinaryReader): DetachModulatedSTDPPreceptorRequest;
}

export namespace DetachModulatedSTDPPreceptorRequest {
  export type AsObject = {
    modulatedstdppreceptorid: string,
  }
}

export class DetachModulatedSTDPPreceptorResponse extends jspb.Message {
  getStatus(): string;
  setStatus(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DetachModulatedSTDPPreceptorResponse.AsObject;
  static toObject(includeInstance: boolean, msg: DetachModulatedSTDPPreceptorResponse): DetachModulatedSTDPPreceptorResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DetachModulatedSTDPPreceptorResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DetachModulatedSTDPPreceptorResponse;
  static deserializeBinaryFromReader(message: DetachModulatedSTDPPreceptorResponse, reader: jspb.BinaryReader): DetachModulatedSTDPPreceptorResponse;
}

export namespace DetachModulatedSTDPPreceptorResponse {
  export type AsObject = {
    status: string,
  }
}

export class SetModulatedSTDPPreceptorPositionRequest extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  hasGraph(): boolean;
  clearGraph(): void;
  getGraph(): Graph | undefined;
  setGraph(value?: Graph): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SetModulatedSTDPPreceptorPositionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SetModulatedSTDPPreceptorPositionRequest): SetModulatedSTDPPreceptorPositionRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SetModulatedSTDPPreceptorPositionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SetModulatedSTDPPreceptorPositionRequest;
  static deserializeBinaryFromReader(message: SetModulatedSTDPPreceptorPositionRequest, reader: jspb.BinaryReader): SetModulatedSTDPPreceptorPositionRequest;
}

export namespace SetModulatedSTDPPreceptorPositionRequest {
  export type AsObject = {
    id: string,
    graph?: Graph.AsObject,
  }
}

export class SetModulatedSTDPPreceptorPositionResponse extends jspb.Message {
  getStatus(): string;
  setStatus(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SetModulatedSTDPPreceptorPositionResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SetModulatedSTDPPreceptorPositionResponse): SetModulatedSTDPPreceptorPositionResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SetModulatedSTDPPreceptorPositionResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SetModulatedSTDPPreceptorPositionResponse;
  static deserializeBinaryFromReader(message: SetModulatedSTDPPreceptorPositionResponse, reader: jspb.BinaryReader): SetModulatedSTDPPreceptorPositionResponse;
}

export namespace SetModulatedSTDPPreceptorPositionResponse {
  export type AsObject = {
    status: string,
  }
}

export class InputAdapterRequest extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): InputAdapterRequest.AsObject;
  static toObject(includeInstance: boolean, msg: InputAdapterRequest): InputAdapterRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: InputAdapterRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): InputAdapterRequest;
  static deserializeBinaryFromReader(message: InputAdapterRequest, reader: jspb.BinaryReader): InputAdapterRequest;
}

export namespace InputAdapterRequest {
  export type AsObject = {
    id: string,
  }
}

export class InputAdapterResponse extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getName(): string;
  setName(value: string): void;

  getSize(): number;
  setSize(value: number): void;

  getEncodingwindow(): number;
  setEncodingwindow(value: number): void;

  hasGraph(): boolean;
  clearGraph(): void;
  getGraph(): Graph | undefined;
  setGraph(value?: Graph): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): InputAdapterResponse.AsObject;
  static toObject(includeInstance: boolean, msg: InputAdapterResponse): InputAdapterResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: InputAdapterResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): InputAdapterResponse;
  static deserializeBinaryFromReader(message: InputAdapterResponse, reader: jspb.BinaryReader): InputAdapterResponse;
}

export namespace InputAdapterResponse {
  export type AsObject = {
    id: string,
    name: string,
    size: number,
    encodingwindow: number,
    graph?: Graph.AsObject,
  }
}

export class CreateInputAdapterRequest extends jspb.Message {
  getName(): string;
  setName(value: string): void;

  getSize(): number;
  setSize(value: number): void;

  getEncodingwindow(): number;
  setEncodingwindow(value: number): void;

  hasGraph(): boolean;
  clearGraph(): void;
  getGraph(): Graph | undefined;
  setGraph(value?: Graph): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateInputAdapterRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CreateInputAdapterRequest): CreateInputAdapterRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CreateInputAdapterRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateInputAdapterRequest;
  static deserializeBinaryFromReader(message: CreateInputAdapterRequest, reader: jspb.BinaryReader): CreateInputAdapterRequest;
}

export namespace CreateInputAdapterRequest {
  export type AsObject = {
    name: string,
    size: number,
    encodingwindow: number,
    graph?: Graph.AsObject,
  }
}

export class DeleteInputAdapterRequest extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteInputAdapterRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteInputAdapterRequest): DeleteInputAdapterRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeleteInputAdapterRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteInputAdapterRequest;
  static deserializeBinaryFromReader(message: DeleteInputAdapterRequest, reader: jspb.BinaryReader): DeleteInputAdapterRequest;
}

export namespace DeleteInputAdapterRequest {
  export type AsObject = {
    id: string,
  }
}

export class DeleteInputAdapterResponse extends jspb.Message {
  getStatus(): string;
  setStatus(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteInputAdapterResponse.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteInputAdapterResponse): DeleteInputAdapterResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeleteInputAdapterResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteInputAdapterResponse;
  static deserializeBinaryFromReader(message: DeleteInputAdapterResponse, reader: jspb.BinaryReader): DeleteInputAdapterResponse;
}

export namespace DeleteInputAdapterResponse {
  export type AsObject = {
    status: string,
  }
}

export class AttachInputAdapterRequest extends jspb.Message {
  getInputadapterid(): string;
  setInputadapterid(value: string): void;

  getNeuronclusterid(): string;
  setNeuronclusterid(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AttachInputAdapterRequest.AsObject;
  static toObject(includeInstance: boolean, msg: AttachInputAdapterRequest): AttachInputAdapterRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: AttachInputAdapterRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AttachInputAdapterRequest;
  static deserializeBinaryFromReader(message: AttachInputAdapterRequest, reader: jspb.BinaryReader): AttachInputAdapterRequest;
}

export namespace AttachInputAdapterRequest {
  export type AsObject = {
    inputadapterid: string,
    neuronclusterid: string,
  }
}

export class AttachInputAdapterResponse extends jspb.Message {
  getStatus(): string;
  setStatus(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AttachInputAdapterResponse.AsObject;
  static toObject(includeInstance: boolean, msg: AttachInputAdapterResponse): AttachInputAdapterResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: AttachInputAdapterResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AttachInputAdapterResponse;
  static deserializeBinaryFromReader(message: AttachInputAdapterResponse, reader: jspb.BinaryReader): AttachInputAdapterResponse;
}

export namespace AttachInputAdapterResponse {
  export type AsObject = {
    status: string,
  }
}

export class DetachInputAdapterRequest extends jspb.Message {
  getInputadapterid(): string;
  setInputadapterid(value: string): void;

  getNeuronclusterid(): string;
  setNeuronclusterid(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DetachInputAdapterRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DetachInputAdapterRequest): DetachInputAdapterRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DetachInputAdapterRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DetachInputAdapterRequest;
  static deserializeBinaryFromReader(message: DetachInputAdapterRequest, reader: jspb.BinaryReader): DetachInputAdapterRequest;
}

export namespace DetachInputAdapterRequest {
  export type AsObject = {
    inputadapterid: string,
    neuronclusterid: string,
  }
}

export class DetachInputAdapterResponse extends jspb.Message {
  getStatus(): string;
  setStatus(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DetachInputAdapterResponse.AsObject;
  static toObject(includeInstance: boolean, msg: DetachInputAdapterResponse): DetachInputAdapterResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DetachInputAdapterResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DetachInputAdapterResponse;
  static deserializeBinaryFromReader(message: DetachInputAdapterResponse, reader: jspb.BinaryReader): DetachInputAdapterResponse;
}

export namespace DetachInputAdapterResponse {
  export type AsObject = {
    status: string,
  }
}

export class SetInputAdapterPositionRequest extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  hasGraph(): boolean;
  clearGraph(): void;
  getGraph(): Graph | undefined;
  setGraph(value?: Graph): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SetInputAdapterPositionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SetInputAdapterPositionRequest): SetInputAdapterPositionRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SetInputAdapterPositionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SetInputAdapterPositionRequest;
  static deserializeBinaryFromReader(message: SetInputAdapterPositionRequest, reader: jspb.BinaryReader): SetInputAdapterPositionRequest;
}

export namespace SetInputAdapterPositionRequest {
  export type AsObject = {
    id: string,
    graph?: Graph.AsObject,
  }
}

export class SetInputAdapterPositionResponse extends jspb.Message {
  getStatus(): string;
  setStatus(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SetInputAdapterPositionResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SetInputAdapterPositionResponse): SetInputAdapterPositionResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SetInputAdapterPositionResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SetInputAdapterPositionResponse;
  static deserializeBinaryFromReader(message: SetInputAdapterPositionResponse, reader: jspb.BinaryReader): SetInputAdapterPositionResponse;
}

export namespace SetInputAdapterPositionResponse {
  export type AsObject = {
    status: string,
  }
}

export class OutputAdapterRequest extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): OutputAdapterRequest.AsObject;
  static toObject(includeInstance: boolean, msg: OutputAdapterRequest): OutputAdapterRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: OutputAdapterRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): OutputAdapterRequest;
  static deserializeBinaryFromReader(message: OutputAdapterRequest, reader: jspb.BinaryReader): OutputAdapterRequest;
}

export namespace OutputAdapterRequest {
  export type AsObject = {
    id: string,
  }
}

export class OutputAdapterResponse extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getName(): string;
  setName(value: string): void;

  getSize(): number;
  setSize(value: number): void;

  getDecodingwindow(): number;
  setDecodingwindow(value: number): void;

  getNeuronclusterid(): string;
  setNeuronclusterid(value: string): void;

  hasGraph(): boolean;
  clearGraph(): void;
  getGraph(): Graph | undefined;
  setGraph(value?: Graph): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): OutputAdapterResponse.AsObject;
  static toObject(includeInstance: boolean, msg: OutputAdapterResponse): OutputAdapterResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: OutputAdapterResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): OutputAdapterResponse;
  static deserializeBinaryFromReader(message: OutputAdapterResponse, reader: jspb.BinaryReader): OutputAdapterResponse;
}

export namespace OutputAdapterResponse {
  export type AsObject = {
    id: string,
    name: string,
    size: number,
    decodingwindow: number,
    neuronclusterid: string,
    graph?: Graph.AsObject,
  }
}

export class CreateOutputAdapterRequest extends jspb.Message {
  getName(): string;
  setName(value: string): void;

  getSize(): number;
  setSize(value: number): void;

  getDecodingwindow(): number;
  setDecodingwindow(value: number): void;

  hasGraph(): boolean;
  clearGraph(): void;
  getGraph(): Graph | undefined;
  setGraph(value?: Graph): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateOutputAdapterRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CreateOutputAdapterRequest): CreateOutputAdapterRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CreateOutputAdapterRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateOutputAdapterRequest;
  static deserializeBinaryFromReader(message: CreateOutputAdapterRequest, reader: jspb.BinaryReader): CreateOutputAdapterRequest;
}

export namespace CreateOutputAdapterRequest {
  export type AsObject = {
    name: string,
    size: number,
    decodingwindow: number,
    graph?: Graph.AsObject,
  }
}

export class DeleteOutputAdapterRequest extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteOutputAdapterRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteOutputAdapterRequest): DeleteOutputAdapterRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeleteOutputAdapterRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteOutputAdapterRequest;
  static deserializeBinaryFromReader(message: DeleteOutputAdapterRequest, reader: jspb.BinaryReader): DeleteOutputAdapterRequest;
}

export namespace DeleteOutputAdapterRequest {
  export type AsObject = {
    id: string,
  }
}

export class DeleteOutputAdapterResponse extends jspb.Message {
  getStatus(): string;
  setStatus(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteOutputAdapterResponse.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteOutputAdapterResponse): DeleteOutputAdapterResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeleteOutputAdapterResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteOutputAdapterResponse;
  static deserializeBinaryFromReader(message: DeleteOutputAdapterResponse, reader: jspb.BinaryReader): DeleteOutputAdapterResponse;
}

export namespace DeleteOutputAdapterResponse {
  export type AsObject = {
    status: string,
  }
}

export class AttachOutputAdapterRequest extends jspb.Message {
  getOutputadapterid(): string;
  setOutputadapterid(value: string): void;

  getNeuronclusterid(): string;
  setNeuronclusterid(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AttachOutputAdapterRequest.AsObject;
  static toObject(includeInstance: boolean, msg: AttachOutputAdapterRequest): AttachOutputAdapterRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: AttachOutputAdapterRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AttachOutputAdapterRequest;
  static deserializeBinaryFromReader(message: AttachOutputAdapterRequest, reader: jspb.BinaryReader): AttachOutputAdapterRequest;
}

export namespace AttachOutputAdapterRequest {
  export type AsObject = {
    outputadapterid: string,
    neuronclusterid: string,
  }
}

export class AttachOutputAdapterResponse extends jspb.Message {
  getStatus(): string;
  setStatus(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AttachOutputAdapterResponse.AsObject;
  static toObject(includeInstance: boolean, msg: AttachOutputAdapterResponse): AttachOutputAdapterResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: AttachOutputAdapterResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AttachOutputAdapterResponse;
  static deserializeBinaryFromReader(message: AttachOutputAdapterResponse, reader: jspb.BinaryReader): AttachOutputAdapterResponse;
}

export namespace AttachOutputAdapterResponse {
  export type AsObject = {
    status: string,
  }
}

export class DetachOutputAdapterRequest extends jspb.Message {
  getOutputadapterid(): string;
  setOutputadapterid(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DetachOutputAdapterRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DetachOutputAdapterRequest): DetachOutputAdapterRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DetachOutputAdapterRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DetachOutputAdapterRequest;
  static deserializeBinaryFromReader(message: DetachOutputAdapterRequest, reader: jspb.BinaryReader): DetachOutputAdapterRequest;
}

export namespace DetachOutputAdapterRequest {
  export type AsObject = {
    outputadapterid: string,
  }
}

export class DetachOutputAdapterResponse extends jspb.Message {
  getStatus(): string;
  setStatus(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DetachOutputAdapterResponse.AsObject;
  static toObject(includeInstance: boolean, msg: DetachOutputAdapterResponse): DetachOutputAdapterResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DetachOutputAdapterResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DetachOutputAdapterResponse;
  static deserializeBinaryFromReader(message: DetachOutputAdapterResponse, reader: jspb.BinaryReader): DetachOutputAdapterResponse;
}

export namespace DetachOutputAdapterResponse {
  export type AsObject = {
    status: string,
  }
}

export class SetOutputAdapterPositionRequest extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  hasGraph(): boolean;
  clearGraph(): void;
  getGraph(): Graph | undefined;
  setGraph(value?: Graph): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SetOutputAdapterPositionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SetOutputAdapterPositionRequest): SetOutputAdapterPositionRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SetOutputAdapterPositionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SetOutputAdapterPositionRequest;
  static deserializeBinaryFromReader(message: SetOutputAdapterPositionRequest, reader: jspb.BinaryReader): SetOutputAdapterPositionRequest;
}

export namespace SetOutputAdapterPositionRequest {
  export type AsObject = {
    id: string,
    graph?: Graph.AsObject,
  }
}

export class SetOutputAdapterPositionResponse extends jspb.Message {
  getStatus(): string;
  setStatus(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SetOutputAdapterPositionResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SetOutputAdapterPositionResponse): SetOutputAdapterPositionResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SetOutputAdapterPositionResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SetOutputAdapterPositionResponse;
  static deserializeBinaryFromReader(message: SetOutputAdapterPositionResponse, reader: jspb.BinaryReader): SetOutputAdapterPositionResponse;
}

export namespace SetOutputAdapterPositionResponse {
  export type AsObject = {
    status: string,
  }
}

export class DopamineAdapterRequest extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DopamineAdapterRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DopamineAdapterRequest): DopamineAdapterRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DopamineAdapterRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DopamineAdapterRequest;
  static deserializeBinaryFromReader(message: DopamineAdapterRequest, reader: jspb.BinaryReader): DopamineAdapterRequest;
}

export namespace DopamineAdapterRequest {
  export type AsObject = {
    id: string,
  }
}

export class DopamineAdapterResponse extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getName(): string;
  setName(value: string): void;

  hasGraph(): boolean;
  clearGraph(): void;
  getGraph(): Graph | undefined;
  setGraph(value?: Graph): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DopamineAdapterResponse.AsObject;
  static toObject(includeInstance: boolean, msg: DopamineAdapterResponse): DopamineAdapterResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DopamineAdapterResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DopamineAdapterResponse;
  static deserializeBinaryFromReader(message: DopamineAdapterResponse, reader: jspb.BinaryReader): DopamineAdapterResponse;
}

export namespace DopamineAdapterResponse {
  export type AsObject = {
    id: string,
    name: string,
    graph?: Graph.AsObject,
  }
}

export class CreateDopamineAdapterRequest extends jspb.Message {
  getName(): string;
  setName(value: string): void;

  hasGraph(): boolean;
  clearGraph(): void;
  getGraph(): Graph | undefined;
  setGraph(value?: Graph): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateDopamineAdapterRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CreateDopamineAdapterRequest): CreateDopamineAdapterRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CreateDopamineAdapterRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateDopamineAdapterRequest;
  static deserializeBinaryFromReader(message: CreateDopamineAdapterRequest, reader: jspb.BinaryReader): CreateDopamineAdapterRequest;
}

export namespace CreateDopamineAdapterRequest {
  export type AsObject = {
    name: string,
    graph?: Graph.AsObject,
  }
}

export class DeleteDopamineAdapterRequest extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteDopamineAdapterRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteDopamineAdapterRequest): DeleteDopamineAdapterRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeleteDopamineAdapterRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteDopamineAdapterRequest;
  static deserializeBinaryFromReader(message: DeleteDopamineAdapterRequest, reader: jspb.BinaryReader): DeleteDopamineAdapterRequest;
}

export namespace DeleteDopamineAdapterRequest {
  export type AsObject = {
    id: string,
  }
}

export class DeleteDopamineAdapterResponse extends jspb.Message {
  getStatus(): string;
  setStatus(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteDopamineAdapterResponse.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteDopamineAdapterResponse): DeleteDopamineAdapterResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeleteDopamineAdapterResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteDopamineAdapterResponse;
  static deserializeBinaryFromReader(message: DeleteDopamineAdapterResponse, reader: jspb.BinaryReader): DeleteDopamineAdapterResponse;
}

export namespace DeleteDopamineAdapterResponse {
  export type AsObject = {
    status: string,
  }
}

export class AttachDopamineAdapterRequest extends jspb.Message {
  getDopamineadapterid(): string;
  setDopamineadapterid(value: string): void;

  getModulatedstdppreceptorid(): string;
  setModulatedstdppreceptorid(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AttachDopamineAdapterRequest.AsObject;
  static toObject(includeInstance: boolean, msg: AttachDopamineAdapterRequest): AttachDopamineAdapterRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: AttachDopamineAdapterRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AttachDopamineAdapterRequest;
  static deserializeBinaryFromReader(message: AttachDopamineAdapterRequest, reader: jspb.BinaryReader): AttachDopamineAdapterRequest;
}

export namespace AttachDopamineAdapterRequest {
  export type AsObject = {
    dopamineadapterid: string,
    modulatedstdppreceptorid: string,
  }
}

export class AttachDopamineAdapterResponse extends jspb.Message {
  getStatus(): string;
  setStatus(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AttachDopamineAdapterResponse.AsObject;
  static toObject(includeInstance: boolean, msg: AttachDopamineAdapterResponse): AttachDopamineAdapterResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: AttachDopamineAdapterResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AttachDopamineAdapterResponse;
  static deserializeBinaryFromReader(message: AttachDopamineAdapterResponse, reader: jspb.BinaryReader): AttachDopamineAdapterResponse;
}

export namespace AttachDopamineAdapterResponse {
  export type AsObject = {
    status: string,
  }
}

export class DetachDopamineAdapterRequest extends jspb.Message {
  getDopamineadapterid(): string;
  setDopamineadapterid(value: string): void;

  getModulatedstdppreceptorid(): string;
  setModulatedstdppreceptorid(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DetachDopamineAdapterRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DetachDopamineAdapterRequest): DetachDopamineAdapterRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DetachDopamineAdapterRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DetachDopamineAdapterRequest;
  static deserializeBinaryFromReader(message: DetachDopamineAdapterRequest, reader: jspb.BinaryReader): DetachDopamineAdapterRequest;
}

export namespace DetachDopamineAdapterRequest {
  export type AsObject = {
    dopamineadapterid: string,
    modulatedstdppreceptorid: string,
  }
}

export class DetachDopamineAdapterResponse extends jspb.Message {
  getStatus(): string;
  setStatus(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DetachDopamineAdapterResponse.AsObject;
  static toObject(includeInstance: boolean, msg: DetachDopamineAdapterResponse): DetachDopamineAdapterResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DetachDopamineAdapterResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DetachDopamineAdapterResponse;
  static deserializeBinaryFromReader(message: DetachDopamineAdapterResponse, reader: jspb.BinaryReader): DetachDopamineAdapterResponse;
}

export namespace DetachDopamineAdapterResponse {
  export type AsObject = {
    status: string,
  }
}

export class SetDopamineAdapterPositionRequest extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  hasGraph(): boolean;
  clearGraph(): void;
  getGraph(): Graph | undefined;
  setGraph(value?: Graph): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SetDopamineAdapterPositionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SetDopamineAdapterPositionRequest): SetDopamineAdapterPositionRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SetDopamineAdapterPositionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SetDopamineAdapterPositionRequest;
  static deserializeBinaryFromReader(message: SetDopamineAdapterPositionRequest, reader: jspb.BinaryReader): SetDopamineAdapterPositionRequest;
}

export namespace SetDopamineAdapterPositionRequest {
  export type AsObject = {
    id: string,
    graph?: Graph.AsObject,
  }
}

export class SetDopamineAdapterPositionResponse extends jspb.Message {
  getStatus(): string;
  setStatus(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SetDopamineAdapterPositionResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SetDopamineAdapterPositionResponse): SetDopamineAdapterPositionResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SetDopamineAdapterPositionResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SetDopamineAdapterPositionResponse;
  static deserializeBinaryFromReader(message: SetDopamineAdapterPositionResponse, reader: jspb.BinaryReader): SetDopamineAdapterPositionResponse;
}

export namespace SetDopamineAdapterPositionResponse {
  export type AsObject = {
    status: string,
  }
}

export class StreamInputRequest extends jspb.Message {
  getInputadapterid(): string;
  setInputadapterid(value: string): void;

  clearValuesList(): void;
  getValuesList(): Array<number>;
  setValuesList(value: Array<number>): void;
  addValues(value: number, index?: number): number;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StreamInputRequest.AsObject;
  static toObject(includeInstance: boolean, msg: StreamInputRequest): StreamInputRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: StreamInputRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StreamInputRequest;
  static deserializeBinaryFromReader(message: StreamInputRequest, reader: jspb.BinaryReader): StreamInputRequest;
}

export namespace StreamInputRequest {
  export type AsObject = {
    inputadapterid: string,
    valuesList: Array<number>,
  }
}

export class StreamInputResponse extends jspb.Message {
  getStatus(): string;
  setStatus(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StreamInputResponse.AsObject;
  static toObject(includeInstance: boolean, msg: StreamInputResponse): StreamInputResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: StreamInputResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StreamInputResponse;
  static deserializeBinaryFromReader(message: StreamInputResponse, reader: jspb.BinaryReader): StreamInputResponse;
}

export namespace StreamInputResponse {
  export type AsObject = {
    status: string,
  }
}

export class StreamOutputRequest extends jspb.Message {
  getOutputadapterid(): string;
  setOutputadapterid(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StreamOutputRequest.AsObject;
  static toObject(includeInstance: boolean, msg: StreamOutputRequest): StreamOutputRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: StreamOutputRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StreamOutputRequest;
  static deserializeBinaryFromReader(message: StreamOutputRequest, reader: jspb.BinaryReader): StreamOutputRequest;
}

export namespace StreamOutputRequest {
  export type AsObject = {
    outputadapterid: string,
  }
}

export class StreamOutputResponse extends jspb.Message {
  getOutputadapterid(): string;
  setOutputadapterid(value: string): void;

  clearValuesList(): void;
  getValuesList(): Array<number>;
  setValuesList(value: Array<number>): void;
  addValues(value: number, index?: number): number;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StreamOutputResponse.AsObject;
  static toObject(includeInstance: boolean, msg: StreamOutputResponse): StreamOutputResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: StreamOutputResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StreamOutputResponse;
  static deserializeBinaryFromReader(message: StreamOutputResponse, reader: jspb.BinaryReader): StreamOutputResponse;
}

export namespace StreamOutputResponse {
  export type AsObject = {
    outputadapterid: string,
    valuesList: Array<number>,
  }
}

export class StreamDopamineRequest extends jspb.Message {
  getDopamineadapterid(): string;
  setDopamineadapterid(value: string): void;

  getDopamine(): number;
  setDopamine(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StreamDopamineRequest.AsObject;
  static toObject(includeInstance: boolean, msg: StreamDopamineRequest): StreamDopamineRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: StreamDopamineRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StreamDopamineRequest;
  static deserializeBinaryFromReader(message: StreamDopamineRequest, reader: jspb.BinaryReader): StreamDopamineRequest;
}

export namespace StreamDopamineRequest {
  export type AsObject = {
    dopamineadapterid: string,
    dopamine: number,
  }
}

export class StreamDopamineResponse extends jspb.Message {
  getStatus(): string;
  setStatus(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StreamDopamineResponse.AsObject;
  static toObject(includeInstance: boolean, msg: StreamDopamineResponse): StreamDopamineResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: StreamDopamineResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StreamDopamineResponse;
  static deserializeBinaryFromReader(message: StreamDopamineResponse, reader: jspb.BinaryReader): StreamDopamineResponse;
}

export namespace StreamDopamineResponse {
  export type AsObject = {
    status: string,
  }
}

export class StreamNeuronSpikeEventRequest extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getComponent(): Component;
  setComponent(value: Component): void;

  getSamplinginterval(): number;
  setSamplinginterval(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StreamNeuronSpikeEventRequest.AsObject;
  static toObject(includeInstance: boolean, msg: StreamNeuronSpikeEventRequest): StreamNeuronSpikeEventRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: StreamNeuronSpikeEventRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StreamNeuronSpikeEventRequest;
  static deserializeBinaryFromReader(message: StreamNeuronSpikeEventRequest, reader: jspb.BinaryReader): StreamNeuronSpikeEventRequest;
}

export namespace StreamNeuronSpikeEventRequest {
  export type AsObject = {
    id: string,
    component: Component,
    samplinginterval: number,
  }
}

export class NeuronSpikeEvent extends jspb.Message {
  getNeuronclusterid(): string;
  setNeuronclusterid(value: string): void;

  getNeuronid(): string;
  setNeuronid(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): NeuronSpikeEvent.AsObject;
  static toObject(includeInstance: boolean, msg: NeuronSpikeEvent): NeuronSpikeEvent.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: NeuronSpikeEvent, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): NeuronSpikeEvent;
  static deserializeBinaryFromReader(message: NeuronSpikeEvent, reader: jspb.BinaryReader): NeuronSpikeEvent;
}

export namespace NeuronSpikeEvent {
  export type AsObject = {
    neuronclusterid: string,
    neuronid: string,
  }
}

export class StreamNeuronSpikeRelayEventRequest extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getComponent(): Component;
  setComponent(value: Component): void;

  getSamplinginterval(): number;
  setSamplinginterval(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StreamNeuronSpikeRelayEventRequest.AsObject;
  static toObject(includeInstance: boolean, msg: StreamNeuronSpikeRelayEventRequest): StreamNeuronSpikeRelayEventRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: StreamNeuronSpikeRelayEventRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StreamNeuronSpikeRelayEventRequest;
  static deserializeBinaryFromReader(message: StreamNeuronSpikeRelayEventRequest, reader: jspb.BinaryReader): StreamNeuronSpikeRelayEventRequest;
}

export namespace StreamNeuronSpikeRelayEventRequest {
  export type AsObject = {
    id: string,
    component: Component,
    samplinginterval: number,
  }
}

export class NeuronSpikeRelayEvent extends jspb.Message {
  getNeuronclusterid(): string;
  setNeuronclusterid(value: string): void;

  getNeuronid(): string;
  setNeuronid(value: string): void;

  getPotential(): number;
  setPotential(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): NeuronSpikeRelayEvent.AsObject;
  static toObject(includeInstance: boolean, msg: NeuronSpikeRelayEvent): NeuronSpikeRelayEvent.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: NeuronSpikeRelayEvent, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): NeuronSpikeRelayEvent;
  static deserializeBinaryFromReader(message: NeuronSpikeRelayEvent, reader: jspb.BinaryReader): NeuronSpikeRelayEvent;
}

export namespace NeuronSpikeRelayEvent {
  export type AsObject = {
    neuronclusterid: string,
    neuronid: string,
    potential: number,
  }
}

export class StreamSynapseWeightUpdateEventRequest extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getComponent(): Component;
  setComponent(value: Component): void;

  getSamplinginterval(): number;
  setSamplinginterval(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StreamSynapseWeightUpdateEventRequest.AsObject;
  static toObject(includeInstance: boolean, msg: StreamSynapseWeightUpdateEventRequest): StreamSynapseWeightUpdateEventRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: StreamSynapseWeightUpdateEventRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StreamSynapseWeightUpdateEventRequest;
  static deserializeBinaryFromReader(message: StreamSynapseWeightUpdateEventRequest, reader: jspb.BinaryReader): StreamSynapseWeightUpdateEventRequest;
}

export namespace StreamSynapseWeightUpdateEventRequest {
  export type AsObject = {
    id: string,
    component: Component,
    samplinginterval: number,
  }
}

export class SynapseWeightUpdateEvent extends jspb.Message {
  getPresynapticneuronid(): string;
  setPresynapticneuronid(value: string): void;

  getPostsynapticneuronid(): string;
  setPostsynapticneuronid(value: string): void;

  getWeightdelta(): number;
  setWeightdelta(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SynapseWeightUpdateEvent.AsObject;
  static toObject(includeInstance: boolean, msg: SynapseWeightUpdateEvent): SynapseWeightUpdateEvent.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SynapseWeightUpdateEvent, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SynapseWeightUpdateEvent;
  static deserializeBinaryFromReader(message: SynapseWeightUpdateEvent, reader: jspb.BinaryReader): SynapseWeightUpdateEvent;
}

export namespace SynapseWeightUpdateEvent {
  export type AsObject = {
    presynapticneuronid: string,
    postsynapticneuronid: string,
    weightdelta: number,
  }
}

export class Graph extends jspb.Message {
  getX(): number;
  setX(value: number): void;

  getY(): number;
  setY(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Graph.AsObject;
  static toObject(includeInstance: boolean, msg: Graph): Graph.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Graph, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Graph;
  static deserializeBinaryFromReader(message: Graph, reader: jspb.BinaryReader): Graph;
}

export namespace Graph {
  export type AsObject = {
    x: number,
    y: number,
  }
}

export enum Component {
  UNKNOWN = 0,
  NEURON_CLUSTER = 1,
  SYNAPSE_CLUSTER = 2,
  STDP_PRECEPTOR = 3,
  MODULATED_STDP_PRECEPTOR = 4,
  INPUT_ADAPTER = 5,
  OUTPUT_ADAPTER = 6,
  DOPAMINE_ADAPTER = 7,
}
