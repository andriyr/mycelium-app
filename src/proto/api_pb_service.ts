/* eslint-disable */
// package: api
// file: api.proto

import * as api_pb from "./api_pb";
export class Node {
  static serviceName = "api.Node";
}
export namespace Node {
  export class Node {
    static readonly methodName = "Node";
    static readonly service = Node;
    static readonly requestStream = false;
    static readonly responseStream = false;
    static readonly requestType = api_pb.NodeRequest;
    static readonly responseType = api_pb.NodeResponse;
  }
  export class CreateNeuronCluster {
    static readonly methodName = "CreateNeuronCluster";
    static readonly service = Node;
    static readonly requestStream = false;
    static readonly responseStream = false;
    static readonly requestType = api_pb.CreateNeuronClusterRequest;
    static readonly responseType = api_pb.NeuronClusterResponse;
  }
  export class DeleteNeuronCluster {
    static readonly methodName = "DeleteNeuronCluster";
    static readonly service = Node;
    static readonly requestStream = false;
    static readonly responseStream = false;
    static readonly requestType = api_pb.DeleteNeuronClusterRequest;
    static readonly responseType = api_pb.DeleteNeuronClusterResponse;
  }
  export class SetNeuronClusterPosition {
    static readonly methodName = "SetNeuronClusterPosition";
    static readonly service = Node;
    static readonly requestStream = false;
    static readonly responseStream = false;
    static readonly requestType = api_pb.SetNeuronClusterPositionRequest;
    static readonly responseType = api_pb.SetNeuronClusterPositionResponse;
  }
  export class CreateSynapseCluster {
    static readonly methodName = "CreateSynapseCluster";
    static readonly service = Node;
    static readonly requestStream = false;
    static readonly responseStream = false;
    static readonly requestType = api_pb.CreateSynapseClusterRequest;
    static readonly responseType = api_pb.SynapseClusterResponse;
  }
  export class DeleteSynapseCluster {
    static readonly methodName = "DeleteSynapseCluster";
    static readonly service = Node;
    static readonly requestStream = false;
    static readonly responseStream = false;
    static readonly requestType = api_pb.DeleteSynapseClusterRequest;
    static readonly responseType = api_pb.DeleteSynapseClusterResponse;
  }
  export class AttachSynapseCluster {
    static readonly methodName = "AttachSynapseCluster";
    static readonly service = Node;
    static readonly requestStream = false;
    static readonly responseStream = false;
    static readonly requestType = api_pb.AttachSynapseClusterRequest;
    static readonly responseType = api_pb.AttachSynapseClusterResponse;
  }
  export class DetachSynapseCluster {
    static readonly methodName = "DetachSynapseCluster";
    static readonly service = Node;
    static readonly requestStream = false;
    static readonly responseStream = false;
    static readonly requestType = api_pb.DetachSynapseClusterRequest;
    static readonly responseType = api_pb.DetachSynapseClusterResponse;
  }
  export class SetSynapseClusterPosition {
    static readonly methodName = "SetSynapseClusterPosition";
    static readonly service = Node;
    static readonly requestStream = false;
    static readonly responseStream = false;
    static readonly requestType = api_pb.SetSynapseClusterPositionRequest;
    static readonly responseType = api_pb.SetSynapseClusterPositionResponse;
  }
  export class CreateSTDPPreceptor {
    static readonly methodName = "CreateSTDPPreceptor";
    static readonly service = Node;
    static readonly requestStream = false;
    static readonly responseStream = false;
    static readonly requestType = api_pb.CreateSTDPPreceptorRequest;
    static readonly responseType = api_pb.STDPPreceptorResponse;
  }
  export class DeleteSTDPPreceptor {
    static readonly methodName = "DeleteSTDPPreceptor";
    static readonly service = Node;
    static readonly requestStream = false;
    static readonly responseStream = false;
    static readonly requestType = api_pb.DeleteSTDPPreceptorRequest;
    static readonly responseType = api_pb.DeleteSTDPPreceptorResponse;
  }
  export class AttachSTDPPreceptor {
    static readonly methodName = "AttachSTDPPreceptor";
    static readonly service = Node;
    static readonly requestStream = false;
    static readonly responseStream = false;
    static readonly requestType = api_pb.AttachSTDPPreceptorRequest;
    static readonly responseType = api_pb.AttachSTDPPreceptorResponse;
  }
  export class DetachSTDPPreceptor {
    static readonly methodName = "DetachSTDPPreceptor";
    static readonly service = Node;
    static readonly requestStream = false;
    static readonly responseStream = false;
    static readonly requestType = api_pb.DetachSTDPPreceptorRequest;
    static readonly responseType = api_pb.DetachSTDPPreceptorResponse;
  }
  export class SetSTDPPreceptorPosition {
    static readonly methodName = "SetSTDPPreceptorPosition";
    static readonly service = Node;
    static readonly requestStream = false;
    static readonly responseStream = false;
    static readonly requestType = api_pb.SetSTDPPreceptorPositionRequest;
    static readonly responseType = api_pb.SetSTDPPreceptorPositionResponse;
  }
  export class CreateModulatedSTDPPreceptor {
    static readonly methodName = "CreateModulatedSTDPPreceptor";
    static readonly service = Node;
    static readonly requestStream = false;
    static readonly responseStream = false;
    static readonly requestType = api_pb.CreateModulatedSTDPPreceptorRequest;
    static readonly responseType = api_pb.ModulatedSTDPPreceptorResponse;
  }
  export class DeleteModulatedSTDPPreceptor {
    static readonly methodName = "DeleteModulatedSTDPPreceptor";
    static readonly service = Node;
    static readonly requestStream = false;
    static readonly responseStream = false;
    static readonly requestType = api_pb.DeleteModulatedSTDPPreceptorRequest;
    static readonly responseType = api_pb.DeleteModulatedSTDPPreceptorResponse;
  }
  export class AttachModulatedSTDPPreceptor {
    static readonly methodName = "AttachModulatedSTDPPreceptor";
    static readonly service = Node;
    static readonly requestStream = false;
    static readonly responseStream = false;
    static readonly requestType = api_pb.AttachModulatedSTDPPreceptorRequest;
    static readonly responseType = api_pb.AttachModulatedSTDPPreceptorResponse;
  }
  export class DetachModulatedSTDPPreceptor {
    static readonly methodName = "DetachModulatedSTDPPreceptor";
    static readonly service = Node;
    static readonly requestStream = false;
    static readonly responseStream = false;
    static readonly requestType = api_pb.DetachModulatedSTDPPreceptorRequest;
    static readonly responseType = api_pb.DetachModulatedSTDPPreceptorResponse;
  }
  export class SetModulatedSTDPPreceptorPosition {
    static readonly methodName = "SetModulatedSTDPPreceptorPosition";
    static readonly service = Node;
    static readonly requestStream = false;
    static readonly responseStream = false;
    static readonly requestType = api_pb.SetModulatedSTDPPreceptorPositionRequest;
    static readonly responseType = api_pb.SetModulatedSTDPPreceptorPositionResponse;
  }
  export class CreateInputAdapter {
    static readonly methodName = "CreateInputAdapter";
    static readonly service = Node;
    static readonly requestStream = false;
    static readonly responseStream = false;
    static readonly requestType = api_pb.CreateInputAdapterRequest;
    static readonly responseType = api_pb.InputAdapterResponse;
  }
  export class DeleteInputAdapter {
    static readonly methodName = "DeleteInputAdapter";
    static readonly service = Node;
    static readonly requestStream = false;
    static readonly responseStream = false;
    static readonly requestType = api_pb.DeleteInputAdapterRequest;
    static readonly responseType = api_pb.DeleteInputAdapterResponse;
  }
  export class AttachInputAdapter {
    static readonly methodName = "AttachInputAdapter";
    static readonly service = Node;
    static readonly requestStream = false;
    static readonly responseStream = false;
    static readonly requestType = api_pb.AttachInputAdapterRequest;
    static readonly responseType = api_pb.AttachInputAdapterResponse;
  }
  export class DetachInputAdapter {
    static readonly methodName = "DetachInputAdapter";
    static readonly service = Node;
    static readonly requestStream = false;
    static readonly responseStream = false;
    static readonly requestType = api_pb.DetachInputAdapterRequest;
    static readonly responseType = api_pb.DetachInputAdapterResponse;
  }
  export class SetInputAdapterPosition {
    static readonly methodName = "SetInputAdapterPosition";
    static readonly service = Node;
    static readonly requestStream = false;
    static readonly responseStream = false;
    static readonly requestType = api_pb.SetInputAdapterPositionRequest;
    static readonly responseType = api_pb.SetInputAdapterPositionResponse;
  }
  export class CreateOutputAdapter {
    static readonly methodName = "CreateOutputAdapter";
    static readonly service = Node;
    static readonly requestStream = false;
    static readonly responseStream = false;
    static readonly requestType = api_pb.CreateOutputAdapterRequest;
    static readonly responseType = api_pb.OutputAdapterResponse;
  }
  export class DeleteOutputAdapter {
    static readonly methodName = "DeleteOutputAdapter";
    static readonly service = Node;
    static readonly requestStream = false;
    static readonly responseStream = false;
    static readonly requestType = api_pb.DeleteOutputAdapterRequest;
    static readonly responseType = api_pb.DeleteOutputAdapterResponse;
  }
  export class AttachOutputAdapter {
    static readonly methodName = "AttachOutputAdapter";
    static readonly service = Node;
    static readonly requestStream = false;
    static readonly responseStream = false;
    static readonly requestType = api_pb.AttachOutputAdapterRequest;
    static readonly responseType = api_pb.AttachOutputAdapterResponse;
  }
  export class DetachOutputAdapter {
    static readonly methodName = "DetachOutputAdapter";
    static readonly service = Node;
    static readonly requestStream = false;
    static readonly responseStream = false;
    static readonly requestType = api_pb.DetachOutputAdapterRequest;
    static readonly responseType = api_pb.DetachOutputAdapterResponse;
  }
  export class SetOutputAdapterPosition {
    static readonly methodName = "SetOutputAdapterPosition";
    static readonly service = Node;
    static readonly requestStream = false;
    static readonly responseStream = false;
    static readonly requestType = api_pb.SetOutputAdapterPositionRequest;
    static readonly responseType = api_pb.SetOutputAdapterPositionResponse;
  }
  export class CreateDopamineAdapter {
    static readonly methodName = "CreateDopamineAdapter";
    static readonly service = Node;
    static readonly requestStream = false;
    static readonly responseStream = false;
    static readonly requestType = api_pb.CreateDopamineAdapterRequest;
    static readonly responseType = api_pb.DopamineAdapterResponse;
  }
  export class DeleteDopamineAdapter {
    static readonly methodName = "DeleteDopamineAdapter";
    static readonly service = Node;
    static readonly requestStream = false;
    static readonly responseStream = false;
    static readonly requestType = api_pb.DeleteDopamineAdapterRequest;
    static readonly responseType = api_pb.DeleteDopamineAdapterResponse;
  }
  export class AttachDopamineAdapter {
    static readonly methodName = "AttachDopamineAdapter";
    static readonly service = Node;
    static readonly requestStream = false;
    static readonly responseStream = false;
    static readonly requestType = api_pb.AttachDopamineAdapterRequest;
    static readonly responseType = api_pb.AttachDopamineAdapterResponse;
  }
  export class DetachDopamineAdapter {
    static readonly methodName = "DetachDopamineAdapter";
    static readonly service = Node;
    static readonly requestStream = false;
    static readonly responseStream = false;
    static readonly requestType = api_pb.DetachDopamineAdapterRequest;
    static readonly responseType = api_pb.DetachDopamineAdapterResponse;
  }
  export class SetDopamineAdapterPosition {
    static readonly methodName = "SetDopamineAdapterPosition";
    static readonly service = Node;
    static readonly requestStream = false;
    static readonly responseStream = false;
    static readonly requestType = api_pb.SetDopamineAdapterPositionRequest;
    static readonly responseType = api_pb.SetDopamineAdapterPositionResponse;
  }
  export class StreamInput {
    static readonly methodName = "StreamInput";
    static readonly service = Node;
    static readonly requestStream = true;
    static readonly responseStream = false;
    static readonly requestType = api_pb.StreamInputRequest;
    static readonly responseType = api_pb.StreamInputResponse;
  }
  export class StreamOutput {
    static readonly methodName = "StreamOutput";
    static readonly service = Node;
    static readonly requestStream = false;
    static readonly responseStream = true;
    static readonly requestType = api_pb.StreamOutputRequest;
    static readonly responseType = api_pb.StreamOutputResponse;
  }
  export class StreamDopamine {
    static readonly methodName = "StreamDopamine";
    static readonly service = Node;
    static readonly requestStream = true;
    static readonly responseStream = false;
    static readonly requestType = api_pb.StreamDopamineRequest;
    static readonly responseType = api_pb.StreamDopamineResponse;
  }
  export class StreamNeuronSpikeEvent {
    static readonly methodName = "StreamNeuronSpikeEvent";
    static readonly service = Node;
    static readonly requestStream = false;
    static readonly responseStream = true;
    static readonly requestType = api_pb.StreamNeuronSpikeEventRequest;
    static readonly responseType = api_pb.NeuronSpikeEvent;
  }
  export class StreamNeuronSpikeRelayEvent {
    static readonly methodName = "StreamNeuronSpikeRelayEvent";
    static readonly service = Node;
    static readonly requestStream = false;
    static readonly responseStream = true;
    static readonly requestType = api_pb.StreamNeuronSpikeRelayEventRequest;
    static readonly responseType = api_pb.NeuronSpikeRelayEvent;
  }
  export class StreamSynapseWeightUpdateEvent {
    static readonly methodName = "StreamSynapseWeightUpdateEvent";
    static readonly service = Node;
    static readonly requestStream = false;
    static readonly responseStream = true;
    static readonly requestType = api_pb.StreamSynapseWeightUpdateEventRequest;
    static readonly responseType = api_pb.SynapseWeightUpdateEvent;
  }
}
