/* eslint-disable */
/**
 * @fileoverview
 * @enhanceable
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!

var jspb = require('google-protobuf');
var goog = jspb;
var global = Function('return this')();

goog.exportSymbol('proto.api.AttachDopamineAdapterRequest', null, global);
goog.exportSymbol('proto.api.AttachDopamineAdapterResponse', null, global);
goog.exportSymbol('proto.api.AttachInputAdapterRequest', null, global);
goog.exportSymbol('proto.api.AttachInputAdapterResponse', null, global);
goog.exportSymbol('proto.api.AttachModulatedSTDPPreceptorRequest', null, global);
goog.exportSymbol('proto.api.AttachModulatedSTDPPreceptorResponse', null, global);
goog.exportSymbol('proto.api.AttachOutputAdapterRequest', null, global);
goog.exportSymbol('proto.api.AttachOutputAdapterResponse', null, global);
goog.exportSymbol('proto.api.AttachSTDPPreceptorRequest', null, global);
goog.exportSymbol('proto.api.AttachSTDPPreceptorResponse', null, global);
goog.exportSymbol('proto.api.AttachSynapseClusterRequest', null, global);
goog.exportSymbol('proto.api.AttachSynapseClusterResponse', null, global);
goog.exportSymbol('proto.api.Component', null, global);
goog.exportSymbol('proto.api.CreateDopamineAdapterRequest', null, global);
goog.exportSymbol('proto.api.CreateInputAdapterRequest', null, global);
goog.exportSymbol('proto.api.CreateModulatedSTDPPreceptorRequest', null, global);
goog.exportSymbol('proto.api.CreateNeuronClusterRequest', null, global);
goog.exportSymbol('proto.api.CreateOutputAdapterRequest', null, global);
goog.exportSymbol('proto.api.CreateSTDPPreceptorRequest', null, global);
goog.exportSymbol('proto.api.CreateSynapseClusterRequest', null, global);
goog.exportSymbol('proto.api.DeleteDopamineAdapterRequest', null, global);
goog.exportSymbol('proto.api.DeleteDopamineAdapterResponse', null, global);
goog.exportSymbol('proto.api.DeleteInputAdapterRequest', null, global);
goog.exportSymbol('proto.api.DeleteInputAdapterResponse', null, global);
goog.exportSymbol('proto.api.DeleteModulatedSTDPPreceptorRequest', null, global);
goog.exportSymbol('proto.api.DeleteModulatedSTDPPreceptorResponse', null, global);
goog.exportSymbol('proto.api.DeleteNeuronClusterRequest', null, global);
goog.exportSymbol('proto.api.DeleteNeuronClusterResponse', null, global);
goog.exportSymbol('proto.api.DeleteOutputAdapterRequest', null, global);
goog.exportSymbol('proto.api.DeleteOutputAdapterResponse', null, global);
goog.exportSymbol('proto.api.DeleteSTDPPreceptorRequest', null, global);
goog.exportSymbol('proto.api.DeleteSTDPPreceptorResponse', null, global);
goog.exportSymbol('proto.api.DeleteSynapseClusterRequest', null, global);
goog.exportSymbol('proto.api.DeleteSynapseClusterResponse', null, global);
goog.exportSymbol('proto.api.DetachDopamineAdapterRequest', null, global);
goog.exportSymbol('proto.api.DetachDopamineAdapterResponse', null, global);
goog.exportSymbol('proto.api.DetachInputAdapterRequest', null, global);
goog.exportSymbol('proto.api.DetachInputAdapterResponse', null, global);
goog.exportSymbol('proto.api.DetachModulatedSTDPPreceptorRequest', null, global);
goog.exportSymbol('proto.api.DetachModulatedSTDPPreceptorResponse', null, global);
goog.exportSymbol('proto.api.DetachOutputAdapterRequest', null, global);
goog.exportSymbol('proto.api.DetachOutputAdapterResponse', null, global);
goog.exportSymbol('proto.api.DetachSTDPPreceptorRequest', null, global);
goog.exportSymbol('proto.api.DetachSTDPPreceptorResponse', null, global);
goog.exportSymbol('proto.api.DetachSynapseClusterRequest', null, global);
goog.exportSymbol('proto.api.DetachSynapseClusterResponse', null, global);
goog.exportSymbol('proto.api.DopamineAdapterRequest', null, global);
goog.exportSymbol('proto.api.DopamineAdapterResponse', null, global);
goog.exportSymbol('proto.api.Graph', null, global);
goog.exportSymbol('proto.api.InputAdapterRequest', null, global);
goog.exportSymbol('proto.api.InputAdapterResponse', null, global);
goog.exportSymbol('proto.api.ModulatedSTDPPreceptorRequest', null, global);
goog.exportSymbol('proto.api.ModulatedSTDPPreceptorResponse', null, global);
goog.exportSymbol('proto.api.NeuronClusterRequest', null, global);
goog.exportSymbol('proto.api.NeuronClusterResponse', null, global);
goog.exportSymbol('proto.api.NeuronSpikeEvent', null, global);
goog.exportSymbol('proto.api.NeuronSpikeRelayEvent', null, global);
goog.exportSymbol('proto.api.NodeRequest', null, global);
goog.exportSymbol('proto.api.NodeResponse', null, global);
goog.exportSymbol('proto.api.OutputAdapterRequest', null, global);
goog.exportSymbol('proto.api.OutputAdapterResponse', null, global);
goog.exportSymbol('proto.api.STDPPreceptorRequest', null, global);
goog.exportSymbol('proto.api.STDPPreceptorResponse', null, global);
goog.exportSymbol('proto.api.SetDopamineAdapterPositionRequest', null, global);
goog.exportSymbol('proto.api.SetDopamineAdapterPositionResponse', null, global);
goog.exportSymbol('proto.api.SetInputAdapterPositionRequest', null, global);
goog.exportSymbol('proto.api.SetInputAdapterPositionResponse', null, global);
goog.exportSymbol('proto.api.SetModulatedSTDPPreceptorPositionRequest', null, global);
goog.exportSymbol('proto.api.SetModulatedSTDPPreceptorPositionResponse', null, global);
goog.exportSymbol('proto.api.SetNeuronClusterPositionRequest', null, global);
goog.exportSymbol('proto.api.SetNeuronClusterPositionResponse', null, global);
goog.exportSymbol('proto.api.SetOutputAdapterPositionRequest', null, global);
goog.exportSymbol('proto.api.SetOutputAdapterPositionResponse', null, global);
goog.exportSymbol('proto.api.SetSTDPPreceptorPositionRequest', null, global);
goog.exportSymbol('proto.api.SetSTDPPreceptorPositionResponse', null, global);
goog.exportSymbol('proto.api.SetSynapseClusterPositionRequest', null, global);
goog.exportSymbol('proto.api.SetSynapseClusterPositionResponse', null, global);
goog.exportSymbol('proto.api.StreamDopamineRequest', null, global);
goog.exportSymbol('proto.api.StreamDopamineResponse', null, global);
goog.exportSymbol('proto.api.StreamInputRequest', null, global);
goog.exportSymbol('proto.api.StreamInputResponse', null, global);
goog.exportSymbol('proto.api.StreamNeuronSpikeEventRequest', null, global);
goog.exportSymbol('proto.api.StreamNeuronSpikeRelayEventRequest', null, global);
goog.exportSymbol('proto.api.StreamOutputRequest', null, global);
goog.exportSymbol('proto.api.StreamOutputResponse', null, global);
goog.exportSymbol('proto.api.StreamSynapseWeightUpdateEventRequest', null, global);
goog.exportSymbol('proto.api.SynapseClusterRequest', null, global);
goog.exportSymbol('proto.api.SynapseClusterResponse', null, global);
goog.exportSymbol('proto.api.SynapseWeightUpdateEvent', null, global);

/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.NodeRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.NodeRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.NodeRequest.displayName = 'proto.api.NodeRequest';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.NodeRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.NodeRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.NodeRequest} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.NodeRequest.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.NodeRequest}
 */
proto.api.NodeRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.NodeRequest;
  return proto.api.NodeRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.NodeRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.NodeRequest}
 */
proto.api.NodeRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.NodeRequest} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.NodeRequest.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.NodeRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.NodeRequest.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.NodeRequest} The clone.
 */
proto.api.NodeRequest.prototype.cloneMessage = function() {
  return /** @type {!proto.api.NodeRequest} */ (jspb.Message.cloneMessage(this));
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.NodeResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.api.NodeResponse.repeatedFields_, null);
};
goog.inherits(proto.api.NodeResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.NodeResponse.displayName = 'proto.api.NodeResponse';
}
/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.api.NodeResponse.repeatedFields_ = [1,2,3,4,5,6,7];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.NodeResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.NodeResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.NodeResponse} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.NodeResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    neuronclustersList: jspb.Message.toObjectList(msg.getNeuronclustersList(),
    proto.api.NeuronClusterResponse.toObject, includeInstance),
    synapseclustersList: jspb.Message.toObjectList(msg.getSynapseclustersList(),
    proto.api.SynapseClusterResponse.toObject, includeInstance),
    stdppreceptorsList: jspb.Message.toObjectList(msg.getStdppreceptorsList(),
    proto.api.STDPPreceptorResponse.toObject, includeInstance),
    modulatedstdppreceptorsList: jspb.Message.toObjectList(msg.getModulatedstdppreceptorsList(),
    proto.api.ModulatedSTDPPreceptorResponse.toObject, includeInstance),
    inputadaptersList: jspb.Message.toObjectList(msg.getInputadaptersList(),
    proto.api.InputAdapterResponse.toObject, includeInstance),
    outputadaptersList: jspb.Message.toObjectList(msg.getOutputadaptersList(),
    proto.api.OutputAdapterResponse.toObject, includeInstance),
    dopamineadaptersList: jspb.Message.toObjectList(msg.getDopamineadaptersList(),
    proto.api.DopamineAdapterResponse.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.NodeResponse}
 */
proto.api.NodeResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.NodeResponse;
  return proto.api.NodeResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.NodeResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.NodeResponse}
 */
proto.api.NodeResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.api.NeuronClusterResponse;
      reader.readMessage(value,proto.api.NeuronClusterResponse.deserializeBinaryFromReader);
      msg.getNeuronclustersList().push(value);
      msg.setNeuronclustersList(msg.getNeuronclustersList());
      break;
    case 2:
      var value = new proto.api.SynapseClusterResponse;
      reader.readMessage(value,proto.api.SynapseClusterResponse.deserializeBinaryFromReader);
      msg.getSynapseclustersList().push(value);
      msg.setSynapseclustersList(msg.getSynapseclustersList());
      break;
    case 3:
      var value = new proto.api.STDPPreceptorResponse;
      reader.readMessage(value,proto.api.STDPPreceptorResponse.deserializeBinaryFromReader);
      msg.getStdppreceptorsList().push(value);
      msg.setStdppreceptorsList(msg.getStdppreceptorsList());
      break;
    case 4:
      var value = new proto.api.ModulatedSTDPPreceptorResponse;
      reader.readMessage(value,proto.api.ModulatedSTDPPreceptorResponse.deserializeBinaryFromReader);
      msg.getModulatedstdppreceptorsList().push(value);
      msg.setModulatedstdppreceptorsList(msg.getModulatedstdppreceptorsList());
      break;
    case 5:
      var value = new proto.api.InputAdapterResponse;
      reader.readMessage(value,proto.api.InputAdapterResponse.deserializeBinaryFromReader);
      msg.getInputadaptersList().push(value);
      msg.setInputadaptersList(msg.getInputadaptersList());
      break;
    case 6:
      var value = new proto.api.OutputAdapterResponse;
      reader.readMessage(value,proto.api.OutputAdapterResponse.deserializeBinaryFromReader);
      msg.getOutputadaptersList().push(value);
      msg.setOutputadaptersList(msg.getOutputadaptersList());
      break;
    case 7:
      var value = new proto.api.DopamineAdapterResponse;
      reader.readMessage(value,proto.api.DopamineAdapterResponse.deserializeBinaryFromReader);
      msg.getDopamineadaptersList().push(value);
      msg.setDopamineadaptersList(msg.getDopamineadaptersList());
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.NodeResponse} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.NodeResponse.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.NodeResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.NodeResponse.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getNeuronclustersList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.api.NeuronClusterResponse.serializeBinaryToWriter
    );
  }
  f = this.getSynapseclustersList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      2,
      f,
      proto.api.SynapseClusterResponse.serializeBinaryToWriter
    );
  }
  f = this.getStdppreceptorsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      3,
      f,
      proto.api.STDPPreceptorResponse.serializeBinaryToWriter
    );
  }
  f = this.getModulatedstdppreceptorsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      4,
      f,
      proto.api.ModulatedSTDPPreceptorResponse.serializeBinaryToWriter
    );
  }
  f = this.getInputadaptersList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      5,
      f,
      proto.api.InputAdapterResponse.serializeBinaryToWriter
    );
  }
  f = this.getOutputadaptersList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      6,
      f,
      proto.api.OutputAdapterResponse.serializeBinaryToWriter
    );
  }
  f = this.getDopamineadaptersList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      7,
      f,
      proto.api.DopamineAdapterResponse.serializeBinaryToWriter
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.NodeResponse} The clone.
 */
proto.api.NodeResponse.prototype.cloneMessage = function() {
  return /** @type {!proto.api.NodeResponse} */ (jspb.Message.cloneMessage(this));
};


/**
 * repeated NeuronClusterResponse neuronClusters = 1;
 * If you change this array by adding, removing or replacing elements, or if you
 * replace the array itself, then you must call the setter to update it.
 * @return {!Array.<!proto.api.NeuronClusterResponse>}
 */
proto.api.NodeResponse.prototype.getNeuronclustersList = function() {
  return /** @type{!Array.<!proto.api.NeuronClusterResponse>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.api.NeuronClusterResponse, 1));
};


/** @param {Array.<!proto.api.NeuronClusterResponse>} value  */
proto.api.NodeResponse.prototype.setNeuronclustersList = function(value) {
  jspb.Message.setRepeatedWrapperField(this, 1, value);
};


proto.api.NodeResponse.prototype.clearNeuronclustersList = function() {
  this.setNeuronclustersList([]);
};


/**
 * repeated SynapseClusterResponse synapseClusters = 2;
 * If you change this array by adding, removing or replacing elements, or if you
 * replace the array itself, then you must call the setter to update it.
 * @return {!Array.<!proto.api.SynapseClusterResponse>}
 */
proto.api.NodeResponse.prototype.getSynapseclustersList = function() {
  return /** @type{!Array.<!proto.api.SynapseClusterResponse>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.api.SynapseClusterResponse, 2));
};


/** @param {Array.<!proto.api.SynapseClusterResponse>} value  */
proto.api.NodeResponse.prototype.setSynapseclustersList = function(value) {
  jspb.Message.setRepeatedWrapperField(this, 2, value);
};


proto.api.NodeResponse.prototype.clearSynapseclustersList = function() {
  this.setSynapseclustersList([]);
};


/**
 * repeated STDPPreceptorResponse STDPPreceptors = 3;
 * If you change this array by adding, removing or replacing elements, or if you
 * replace the array itself, then you must call the setter to update it.
 * @return {!Array.<!proto.api.STDPPreceptorResponse>}
 */
proto.api.NodeResponse.prototype.getStdppreceptorsList = function() {
  return /** @type{!Array.<!proto.api.STDPPreceptorResponse>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.api.STDPPreceptorResponse, 3));
};


/** @param {Array.<!proto.api.STDPPreceptorResponse>} value  */
proto.api.NodeResponse.prototype.setStdppreceptorsList = function(value) {
  jspb.Message.setRepeatedWrapperField(this, 3, value);
};


proto.api.NodeResponse.prototype.clearStdppreceptorsList = function() {
  this.setStdppreceptorsList([]);
};


/**
 * repeated ModulatedSTDPPreceptorResponse modulatedSTDPPreceptors = 4;
 * If you change this array by adding, removing or replacing elements, or if you
 * replace the array itself, then you must call the setter to update it.
 * @return {!Array.<!proto.api.ModulatedSTDPPreceptorResponse>}
 */
proto.api.NodeResponse.prototype.getModulatedstdppreceptorsList = function() {
  return /** @type{!Array.<!proto.api.ModulatedSTDPPreceptorResponse>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.api.ModulatedSTDPPreceptorResponse, 4));
};


/** @param {Array.<!proto.api.ModulatedSTDPPreceptorResponse>} value  */
proto.api.NodeResponse.prototype.setModulatedstdppreceptorsList = function(value) {
  jspb.Message.setRepeatedWrapperField(this, 4, value);
};


proto.api.NodeResponse.prototype.clearModulatedstdppreceptorsList = function() {
  this.setModulatedstdppreceptorsList([]);
};


/**
 * repeated InputAdapterResponse inputAdapters = 5;
 * If you change this array by adding, removing or replacing elements, or if you
 * replace the array itself, then you must call the setter to update it.
 * @return {!Array.<!proto.api.InputAdapterResponse>}
 */
proto.api.NodeResponse.prototype.getInputadaptersList = function() {
  return /** @type{!Array.<!proto.api.InputAdapterResponse>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.api.InputAdapterResponse, 5));
};


/** @param {Array.<!proto.api.InputAdapterResponse>} value  */
proto.api.NodeResponse.prototype.setInputadaptersList = function(value) {
  jspb.Message.setRepeatedWrapperField(this, 5, value);
};


proto.api.NodeResponse.prototype.clearInputadaptersList = function() {
  this.setInputadaptersList([]);
};


/**
 * repeated OutputAdapterResponse outputAdapters = 6;
 * If you change this array by adding, removing or replacing elements, or if you
 * replace the array itself, then you must call the setter to update it.
 * @return {!Array.<!proto.api.OutputAdapterResponse>}
 */
proto.api.NodeResponse.prototype.getOutputadaptersList = function() {
  return /** @type{!Array.<!proto.api.OutputAdapterResponse>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.api.OutputAdapterResponse, 6));
};


/** @param {Array.<!proto.api.OutputAdapterResponse>} value  */
proto.api.NodeResponse.prototype.setOutputadaptersList = function(value) {
  jspb.Message.setRepeatedWrapperField(this, 6, value);
};


proto.api.NodeResponse.prototype.clearOutputadaptersList = function() {
  this.setOutputadaptersList([]);
};


/**
 * repeated DopamineAdapterResponse dopamineAdapters = 7;
 * If you change this array by adding, removing or replacing elements, or if you
 * replace the array itself, then you must call the setter to update it.
 * @return {!Array.<!proto.api.DopamineAdapterResponse>}
 */
proto.api.NodeResponse.prototype.getDopamineadaptersList = function() {
  return /** @type{!Array.<!proto.api.DopamineAdapterResponse>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.api.DopamineAdapterResponse, 7));
};


/** @param {Array.<!proto.api.DopamineAdapterResponse>} value  */
proto.api.NodeResponse.prototype.setDopamineadaptersList = function(value) {
  jspb.Message.setRepeatedWrapperField(this, 7, value);
};


proto.api.NodeResponse.prototype.clearDopamineadaptersList = function() {
  this.setDopamineadaptersList([]);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.NeuronClusterRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.NeuronClusterRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.NeuronClusterRequest.displayName = 'proto.api.NeuronClusterRequest';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.NeuronClusterRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.NeuronClusterRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.NeuronClusterRequest} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.NeuronClusterRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: msg.getId()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.NeuronClusterRequest}
 */
proto.api.NeuronClusterRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.NeuronClusterRequest;
  return proto.api.NeuronClusterRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.NeuronClusterRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.NeuronClusterRequest}
 */
proto.api.NeuronClusterRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.NeuronClusterRequest} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.NeuronClusterRequest.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.NeuronClusterRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.NeuronClusterRequest.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.NeuronClusterRequest} The clone.
 */
proto.api.NeuronClusterRequest.prototype.cloneMessage = function() {
  return /** @type {!proto.api.NeuronClusterRequest} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string id = 1;
 * @return {string}
 */
proto.api.NeuronClusterRequest.prototype.getId = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.NeuronClusterRequest.prototype.setId = function(value) {
  jspb.Message.setField(this, 1, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.NeuronClusterResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.api.NeuronClusterResponse.repeatedFields_, null);
};
goog.inherits(proto.api.NeuronClusterResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.NeuronClusterResponse.displayName = 'proto.api.NeuronClusterResponse';
}
/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.api.NeuronClusterResponse.repeatedFields_ = [5,6];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.NeuronClusterResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.NeuronClusterResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.NeuronClusterResponse} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.NeuronClusterResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: msg.getId(),
    name: msg.getName(),
    size: msg.getSize(),
    ratio: msg.getRatio(),
    neuronidsList: jspb.Message.getField(msg, 5),
    inputadapteridsList: jspb.Message.getField(msg, 6),
    graph: (f = msg.getGraph()) && proto.api.Graph.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.NeuronClusterResponse}
 */
proto.api.NeuronClusterResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.NeuronClusterResponse;
  return proto.api.NeuronClusterResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.NeuronClusterResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.NeuronClusterResponse}
 */
proto.api.NeuronClusterResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setId(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setSize(value);
      break;
    case 4:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setRatio(value);
      break;
    case 5:
      var value = /** @type {string} */ (reader.readString());
      msg.getNeuronidsList().push(value);
      msg.setNeuronidsList(msg.getNeuronidsList());
      break;
    case 6:
      var value = /** @type {string} */ (reader.readString());
      msg.getInputadapteridsList().push(value);
      msg.setInputadapteridsList(msg.getInputadapteridsList());
      break;
    case 7:
      var value = new proto.api.Graph;
      reader.readMessage(value,proto.api.Graph.deserializeBinaryFromReader);
      msg.setGraph(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.NeuronClusterResponse} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.NeuronClusterResponse.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.NeuronClusterResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.NeuronClusterResponse.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = this.getName();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = this.getSize();
  if (f !== 0) {
    writer.writeInt32(
      3,
      f
    );
  }
  f = this.getRatio();
  if (f !== 0.0) {
    writer.writeDouble(
      4,
      f
    );
  }
  f = this.getNeuronidsList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      5,
      f
    );
  }
  f = this.getInputadapteridsList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      6,
      f
    );
  }
  f = this.getGraph();
  if (f != null) {
    writer.writeMessage(
      7,
      f,
      proto.api.Graph.serializeBinaryToWriter
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.NeuronClusterResponse} The clone.
 */
proto.api.NeuronClusterResponse.prototype.cloneMessage = function() {
  return /** @type {!proto.api.NeuronClusterResponse} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string id = 1;
 * @return {string}
 */
proto.api.NeuronClusterResponse.prototype.getId = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.NeuronClusterResponse.prototype.setId = function(value) {
  jspb.Message.setField(this, 1, value);
};


/**
 * optional string name = 2;
 * @return {string}
 */
proto.api.NeuronClusterResponse.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 2, ""));
};


/** @param {string} value  */
proto.api.NeuronClusterResponse.prototype.setName = function(value) {
  jspb.Message.setField(this, 2, value);
};


/**
 * optional int32 size = 3;
 * @return {number}
 */
proto.api.NeuronClusterResponse.prototype.getSize = function() {
  return /** @type {number} */ (jspb.Message.getFieldProto3(this, 3, 0));
};


/** @param {number} value  */
proto.api.NeuronClusterResponse.prototype.setSize = function(value) {
  jspb.Message.setField(this, 3, value);
};


/**
 * optional double ratio = 4;
 * @return {number}
 */
proto.api.NeuronClusterResponse.prototype.getRatio = function() {
  return /** @type {number} */ (jspb.Message.getFieldProto3(this, 4, 0));
};


/** @param {number} value  */
proto.api.NeuronClusterResponse.prototype.setRatio = function(value) {
  jspb.Message.setField(this, 4, value);
};


/**
 * repeated string neuronIds = 5;
 * If you change this array by adding, removing or replacing elements, or if you
 * replace the array itself, then you must call the setter to update it.
 * @return {!Array.<string>}
 */
proto.api.NeuronClusterResponse.prototype.getNeuronidsList = function() {
  return /** @type {!Array.<string>} */ (jspb.Message.getField(this, 5));
};


/** @param {Array.<string>} value  */
proto.api.NeuronClusterResponse.prototype.setNeuronidsList = function(value) {
  jspb.Message.setField(this, 5, value || []);
};


proto.api.NeuronClusterResponse.prototype.clearNeuronidsList = function() {
  jspb.Message.setField(this, 5, []);
};


/**
 * repeated string inputAdapterIds = 6;
 * If you change this array by adding, removing or replacing elements, or if you
 * replace the array itself, then you must call the setter to update it.
 * @return {!Array.<string>}
 */
proto.api.NeuronClusterResponse.prototype.getInputadapteridsList = function() {
  return /** @type {!Array.<string>} */ (jspb.Message.getField(this, 6));
};


/** @param {Array.<string>} value  */
proto.api.NeuronClusterResponse.prototype.setInputadapteridsList = function(value) {
  jspb.Message.setField(this, 6, value || []);
};


proto.api.NeuronClusterResponse.prototype.clearInputadapteridsList = function() {
  jspb.Message.setField(this, 6, []);
};


/**
 * optional Graph graph = 7;
 * @return {proto.api.Graph}
 */
proto.api.NeuronClusterResponse.prototype.getGraph = function() {
  return /** @type{proto.api.Graph} */ (
    jspb.Message.getWrapperField(this, proto.api.Graph, 7));
};


/** @param {proto.api.Graph|undefined} value  */
proto.api.NeuronClusterResponse.prototype.setGraph = function(value) {
  jspb.Message.setWrapperField(this, 7, value);
};


proto.api.NeuronClusterResponse.prototype.clearGraph = function() {
  this.setGraph(undefined);
};


/**
 * Returns whether this field is set.
 * @return{!boolean}
 */
proto.api.NeuronClusterResponse.prototype.hasGraph = function() {
  return jspb.Message.getField(this, 7) != null;
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.CreateNeuronClusterRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.CreateNeuronClusterRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.CreateNeuronClusterRequest.displayName = 'proto.api.CreateNeuronClusterRequest';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.CreateNeuronClusterRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.CreateNeuronClusterRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.CreateNeuronClusterRequest} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.CreateNeuronClusterRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    name: msg.getName(),
    size: msg.getSize(),
    ratio: msg.getRatio(),
    graph: (f = msg.getGraph()) && proto.api.Graph.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.CreateNeuronClusterRequest}
 */
proto.api.CreateNeuronClusterRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.CreateNeuronClusterRequest;
  return proto.api.CreateNeuronClusterRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.CreateNeuronClusterRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.CreateNeuronClusterRequest}
 */
proto.api.CreateNeuronClusterRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setSize(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setRatio(value);
      break;
    case 4:
      var value = new proto.api.Graph;
      reader.readMessage(value,proto.api.Graph.deserializeBinaryFromReader);
      msg.setGraph(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.CreateNeuronClusterRequest} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.CreateNeuronClusterRequest.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.CreateNeuronClusterRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.CreateNeuronClusterRequest.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = this.getSize();
  if (f !== 0) {
    writer.writeInt32(
      2,
      f
    );
  }
  f = this.getRatio();
  if (f !== 0.0) {
    writer.writeDouble(
      3,
      f
    );
  }
  f = this.getGraph();
  if (f != null) {
    writer.writeMessage(
      4,
      f,
      proto.api.Graph.serializeBinaryToWriter
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.CreateNeuronClusterRequest} The clone.
 */
proto.api.CreateNeuronClusterRequest.prototype.cloneMessage = function() {
  return /** @type {!proto.api.CreateNeuronClusterRequest} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string name = 1;
 * @return {string}
 */
proto.api.CreateNeuronClusterRequest.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.CreateNeuronClusterRequest.prototype.setName = function(value) {
  jspb.Message.setField(this, 1, value);
};


/**
 * optional int32 size = 2;
 * @return {number}
 */
proto.api.CreateNeuronClusterRequest.prototype.getSize = function() {
  return /** @type {number} */ (jspb.Message.getFieldProto3(this, 2, 0));
};


/** @param {number} value  */
proto.api.CreateNeuronClusterRequest.prototype.setSize = function(value) {
  jspb.Message.setField(this, 2, value);
};


/**
 * optional double ratio = 3;
 * @return {number}
 */
proto.api.CreateNeuronClusterRequest.prototype.getRatio = function() {
  return /** @type {number} */ (jspb.Message.getFieldProto3(this, 3, 0));
};


/** @param {number} value  */
proto.api.CreateNeuronClusterRequest.prototype.setRatio = function(value) {
  jspb.Message.setField(this, 3, value);
};


/**
 * optional Graph graph = 4;
 * @return {proto.api.Graph}
 */
proto.api.CreateNeuronClusterRequest.prototype.getGraph = function() {
  return /** @type{proto.api.Graph} */ (
    jspb.Message.getWrapperField(this, proto.api.Graph, 4));
};


/** @param {proto.api.Graph|undefined} value  */
proto.api.CreateNeuronClusterRequest.prototype.setGraph = function(value) {
  jspb.Message.setWrapperField(this, 4, value);
};


proto.api.CreateNeuronClusterRequest.prototype.clearGraph = function() {
  this.setGraph(undefined);
};


/**
 * Returns whether this field is set.
 * @return{!boolean}
 */
proto.api.CreateNeuronClusterRequest.prototype.hasGraph = function() {
  return jspb.Message.getField(this, 4) != null;
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.DeleteNeuronClusterRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.DeleteNeuronClusterRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.DeleteNeuronClusterRequest.displayName = 'proto.api.DeleteNeuronClusterRequest';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.DeleteNeuronClusterRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.DeleteNeuronClusterRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.DeleteNeuronClusterRequest} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.DeleteNeuronClusterRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: msg.getId()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.DeleteNeuronClusterRequest}
 */
proto.api.DeleteNeuronClusterRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.DeleteNeuronClusterRequest;
  return proto.api.DeleteNeuronClusterRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.DeleteNeuronClusterRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.DeleteNeuronClusterRequest}
 */
proto.api.DeleteNeuronClusterRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.DeleteNeuronClusterRequest} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DeleteNeuronClusterRequest.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.DeleteNeuronClusterRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DeleteNeuronClusterRequest.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.DeleteNeuronClusterRequest} The clone.
 */
proto.api.DeleteNeuronClusterRequest.prototype.cloneMessage = function() {
  return /** @type {!proto.api.DeleteNeuronClusterRequest} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string id = 1;
 * @return {string}
 */
proto.api.DeleteNeuronClusterRequest.prototype.getId = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.DeleteNeuronClusterRequest.prototype.setId = function(value) {
  jspb.Message.setField(this, 1, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.DeleteNeuronClusterResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.DeleteNeuronClusterResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.DeleteNeuronClusterResponse.displayName = 'proto.api.DeleteNeuronClusterResponse';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.DeleteNeuronClusterResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.DeleteNeuronClusterResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.DeleteNeuronClusterResponse} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.DeleteNeuronClusterResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    status: msg.getStatus()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.DeleteNeuronClusterResponse}
 */
proto.api.DeleteNeuronClusterResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.DeleteNeuronClusterResponse;
  return proto.api.DeleteNeuronClusterResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.DeleteNeuronClusterResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.DeleteNeuronClusterResponse}
 */
proto.api.DeleteNeuronClusterResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setStatus(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.DeleteNeuronClusterResponse} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DeleteNeuronClusterResponse.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.DeleteNeuronClusterResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DeleteNeuronClusterResponse.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getStatus();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.DeleteNeuronClusterResponse} The clone.
 */
proto.api.DeleteNeuronClusterResponse.prototype.cloneMessage = function() {
  return /** @type {!proto.api.DeleteNeuronClusterResponse} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string status = 1;
 * @return {string}
 */
proto.api.DeleteNeuronClusterResponse.prototype.getStatus = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.DeleteNeuronClusterResponse.prototype.setStatus = function(value) {
  jspb.Message.setField(this, 1, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.SetNeuronClusterPositionRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.SetNeuronClusterPositionRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.SetNeuronClusterPositionRequest.displayName = 'proto.api.SetNeuronClusterPositionRequest';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.SetNeuronClusterPositionRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.SetNeuronClusterPositionRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.SetNeuronClusterPositionRequest} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.SetNeuronClusterPositionRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: msg.getId(),
    graph: (f = msg.getGraph()) && proto.api.Graph.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.SetNeuronClusterPositionRequest}
 */
proto.api.SetNeuronClusterPositionRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.SetNeuronClusterPositionRequest;
  return proto.api.SetNeuronClusterPositionRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.SetNeuronClusterPositionRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.SetNeuronClusterPositionRequest}
 */
proto.api.SetNeuronClusterPositionRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setId(value);
      break;
    case 2:
      var value = new proto.api.Graph;
      reader.readMessage(value,proto.api.Graph.deserializeBinaryFromReader);
      msg.setGraph(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.SetNeuronClusterPositionRequest} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.SetNeuronClusterPositionRequest.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.SetNeuronClusterPositionRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.SetNeuronClusterPositionRequest.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = this.getGraph();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      proto.api.Graph.serializeBinaryToWriter
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.SetNeuronClusterPositionRequest} The clone.
 */
proto.api.SetNeuronClusterPositionRequest.prototype.cloneMessage = function() {
  return /** @type {!proto.api.SetNeuronClusterPositionRequest} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string id = 1;
 * @return {string}
 */
proto.api.SetNeuronClusterPositionRequest.prototype.getId = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.SetNeuronClusterPositionRequest.prototype.setId = function(value) {
  jspb.Message.setField(this, 1, value);
};


/**
 * optional Graph graph = 2;
 * @return {proto.api.Graph}
 */
proto.api.SetNeuronClusterPositionRequest.prototype.getGraph = function() {
  return /** @type{proto.api.Graph} */ (
    jspb.Message.getWrapperField(this, proto.api.Graph, 2));
};


/** @param {proto.api.Graph|undefined} value  */
proto.api.SetNeuronClusterPositionRequest.prototype.setGraph = function(value) {
  jspb.Message.setWrapperField(this, 2, value);
};


proto.api.SetNeuronClusterPositionRequest.prototype.clearGraph = function() {
  this.setGraph(undefined);
};


/**
 * Returns whether this field is set.
 * @return{!boolean}
 */
proto.api.SetNeuronClusterPositionRequest.prototype.hasGraph = function() {
  return jspb.Message.getField(this, 2) != null;
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.SetNeuronClusterPositionResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.SetNeuronClusterPositionResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.SetNeuronClusterPositionResponse.displayName = 'proto.api.SetNeuronClusterPositionResponse';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.SetNeuronClusterPositionResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.SetNeuronClusterPositionResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.SetNeuronClusterPositionResponse} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.SetNeuronClusterPositionResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    status: msg.getStatus()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.SetNeuronClusterPositionResponse}
 */
proto.api.SetNeuronClusterPositionResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.SetNeuronClusterPositionResponse;
  return proto.api.SetNeuronClusterPositionResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.SetNeuronClusterPositionResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.SetNeuronClusterPositionResponse}
 */
proto.api.SetNeuronClusterPositionResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setStatus(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.SetNeuronClusterPositionResponse} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.SetNeuronClusterPositionResponse.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.SetNeuronClusterPositionResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.SetNeuronClusterPositionResponse.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getStatus();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.SetNeuronClusterPositionResponse} The clone.
 */
proto.api.SetNeuronClusterPositionResponse.prototype.cloneMessage = function() {
  return /** @type {!proto.api.SetNeuronClusterPositionResponse} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string status = 1;
 * @return {string}
 */
proto.api.SetNeuronClusterPositionResponse.prototype.getStatus = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.SetNeuronClusterPositionResponse.prototype.setStatus = function(value) {
  jspb.Message.setField(this, 1, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.SynapseClusterRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.SynapseClusterRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.SynapseClusterRequest.displayName = 'proto.api.SynapseClusterRequest';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.SynapseClusterRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.SynapseClusterRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.SynapseClusterRequest} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.SynapseClusterRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: msg.getId()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.SynapseClusterRequest}
 */
proto.api.SynapseClusterRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.SynapseClusterRequest;
  return proto.api.SynapseClusterRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.SynapseClusterRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.SynapseClusterRequest}
 */
proto.api.SynapseClusterRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.SynapseClusterRequest} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.SynapseClusterRequest.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.SynapseClusterRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.SynapseClusterRequest.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.SynapseClusterRequest} The clone.
 */
proto.api.SynapseClusterRequest.prototype.cloneMessage = function() {
  return /** @type {!proto.api.SynapseClusterRequest} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string id = 1;
 * @return {string}
 */
proto.api.SynapseClusterRequest.prototype.getId = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.SynapseClusterRequest.prototype.setId = function(value) {
  jspb.Message.setField(this, 1, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.SynapseClusterResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.api.SynapseClusterResponse.repeatedFields_, null);
};
goog.inherits(proto.api.SynapseClusterResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.SynapseClusterResponse.displayName = 'proto.api.SynapseClusterResponse';
}
/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.api.SynapseClusterResponse.repeatedFields_ = [5];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.SynapseClusterResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.SynapseClusterResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.SynapseClusterResponse} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.SynapseClusterResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: msg.getId(),
    name: msg.getName(),
    neuronscovered: msg.getNeuronscovered(),
    initialsynapseweight: msg.getInitialsynapseweight(),
    neuronclusteridsList: jspb.Message.getField(msg, 5),
    graph: (f = msg.getGraph()) && proto.api.Graph.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.SynapseClusterResponse}
 */
proto.api.SynapseClusterResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.SynapseClusterResponse;
  return proto.api.SynapseClusterResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.SynapseClusterResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.SynapseClusterResponse}
 */
proto.api.SynapseClusterResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setId(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setNeuronscovered(value);
      break;
    case 4:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setInitialsynapseweight(value);
      break;
    case 5:
      var value = /** @type {string} */ (reader.readString());
      msg.getNeuronclusteridsList().push(value);
      msg.setNeuronclusteridsList(msg.getNeuronclusteridsList());
      break;
    case 6:
      var value = new proto.api.Graph;
      reader.readMessage(value,proto.api.Graph.deserializeBinaryFromReader);
      msg.setGraph(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.SynapseClusterResponse} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.SynapseClusterResponse.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.SynapseClusterResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.SynapseClusterResponse.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = this.getName();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = this.getNeuronscovered();
  if (f !== 0) {
    writer.writeInt32(
      3,
      f
    );
  }
  f = this.getInitialsynapseweight();
  if (f !== 0.0) {
    writer.writeDouble(
      4,
      f
    );
  }
  f = this.getNeuronclusteridsList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      5,
      f
    );
  }
  f = this.getGraph();
  if (f != null) {
    writer.writeMessage(
      6,
      f,
      proto.api.Graph.serializeBinaryToWriter
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.SynapseClusterResponse} The clone.
 */
proto.api.SynapseClusterResponse.prototype.cloneMessage = function() {
  return /** @type {!proto.api.SynapseClusterResponse} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string id = 1;
 * @return {string}
 */
proto.api.SynapseClusterResponse.prototype.getId = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.SynapseClusterResponse.prototype.setId = function(value) {
  jspb.Message.setField(this, 1, value);
};


/**
 * optional string name = 2;
 * @return {string}
 */
proto.api.SynapseClusterResponse.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 2, ""));
};


/** @param {string} value  */
proto.api.SynapseClusterResponse.prototype.setName = function(value) {
  jspb.Message.setField(this, 2, value);
};


/**
 * optional int32 neuronsCovered = 3;
 * @return {number}
 */
proto.api.SynapseClusterResponse.prototype.getNeuronscovered = function() {
  return /** @type {number} */ (jspb.Message.getFieldProto3(this, 3, 0));
};


/** @param {number} value  */
proto.api.SynapseClusterResponse.prototype.setNeuronscovered = function(value) {
  jspb.Message.setField(this, 3, value);
};


/**
 * optional double initialSynapseWeight = 4;
 * @return {number}
 */
proto.api.SynapseClusterResponse.prototype.getInitialsynapseweight = function() {
  return /** @type {number} */ (jspb.Message.getFieldProto3(this, 4, 0));
};


/** @param {number} value  */
proto.api.SynapseClusterResponse.prototype.setInitialsynapseweight = function(value) {
  jspb.Message.setField(this, 4, value);
};


/**
 * repeated string neuronClusterIds = 5;
 * If you change this array by adding, removing or replacing elements, or if you
 * replace the array itself, then you must call the setter to update it.
 * @return {!Array.<string>}
 */
proto.api.SynapseClusterResponse.prototype.getNeuronclusteridsList = function() {
  return /** @type {!Array.<string>} */ (jspb.Message.getField(this, 5));
};


/** @param {Array.<string>} value  */
proto.api.SynapseClusterResponse.prototype.setNeuronclusteridsList = function(value) {
  jspb.Message.setField(this, 5, value || []);
};


proto.api.SynapseClusterResponse.prototype.clearNeuronclusteridsList = function() {
  jspb.Message.setField(this, 5, []);
};


/**
 * optional Graph graph = 6;
 * @return {proto.api.Graph}
 */
proto.api.SynapseClusterResponse.prototype.getGraph = function() {
  return /** @type{proto.api.Graph} */ (
    jspb.Message.getWrapperField(this, proto.api.Graph, 6));
};


/** @param {proto.api.Graph|undefined} value  */
proto.api.SynapseClusterResponse.prototype.setGraph = function(value) {
  jspb.Message.setWrapperField(this, 6, value);
};


proto.api.SynapseClusterResponse.prototype.clearGraph = function() {
  this.setGraph(undefined);
};


/**
 * Returns whether this field is set.
 * @return{!boolean}
 */
proto.api.SynapseClusterResponse.prototype.hasGraph = function() {
  return jspb.Message.getField(this, 6) != null;
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.CreateSynapseClusterRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.CreateSynapseClusterRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.CreateSynapseClusterRequest.displayName = 'proto.api.CreateSynapseClusterRequest';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.CreateSynapseClusterRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.CreateSynapseClusterRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.CreateSynapseClusterRequest} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.CreateSynapseClusterRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    name: msg.getName(),
    neuronscovered: msg.getNeuronscovered(),
    initialsynapseweight: msg.getInitialsynapseweight(),
    graph: (f = msg.getGraph()) && proto.api.Graph.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.CreateSynapseClusterRequest}
 */
proto.api.CreateSynapseClusterRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.CreateSynapseClusterRequest;
  return proto.api.CreateSynapseClusterRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.CreateSynapseClusterRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.CreateSynapseClusterRequest}
 */
proto.api.CreateSynapseClusterRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setNeuronscovered(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setInitialsynapseweight(value);
      break;
    case 4:
      var value = new proto.api.Graph;
      reader.readMessage(value,proto.api.Graph.deserializeBinaryFromReader);
      msg.setGraph(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.CreateSynapseClusterRequest} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.CreateSynapseClusterRequest.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.CreateSynapseClusterRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.CreateSynapseClusterRequest.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = this.getNeuronscovered();
  if (f !== 0) {
    writer.writeInt32(
      2,
      f
    );
  }
  f = this.getInitialsynapseweight();
  if (f !== 0.0) {
    writer.writeDouble(
      3,
      f
    );
  }
  f = this.getGraph();
  if (f != null) {
    writer.writeMessage(
      4,
      f,
      proto.api.Graph.serializeBinaryToWriter
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.CreateSynapseClusterRequest} The clone.
 */
proto.api.CreateSynapseClusterRequest.prototype.cloneMessage = function() {
  return /** @type {!proto.api.CreateSynapseClusterRequest} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string name = 1;
 * @return {string}
 */
proto.api.CreateSynapseClusterRequest.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.CreateSynapseClusterRequest.prototype.setName = function(value) {
  jspb.Message.setField(this, 1, value);
};


/**
 * optional int32 neuronsCovered = 2;
 * @return {number}
 */
proto.api.CreateSynapseClusterRequest.prototype.getNeuronscovered = function() {
  return /** @type {number} */ (jspb.Message.getFieldProto3(this, 2, 0));
};


/** @param {number} value  */
proto.api.CreateSynapseClusterRequest.prototype.setNeuronscovered = function(value) {
  jspb.Message.setField(this, 2, value);
};


/**
 * optional double initialSynapseWeight = 3;
 * @return {number}
 */
proto.api.CreateSynapseClusterRequest.prototype.getInitialsynapseweight = function() {
  return /** @type {number} */ (jspb.Message.getFieldProto3(this, 3, 0));
};


/** @param {number} value  */
proto.api.CreateSynapseClusterRequest.prototype.setInitialsynapseweight = function(value) {
  jspb.Message.setField(this, 3, value);
};


/**
 * optional Graph graph = 4;
 * @return {proto.api.Graph}
 */
proto.api.CreateSynapseClusterRequest.prototype.getGraph = function() {
  return /** @type{proto.api.Graph} */ (
    jspb.Message.getWrapperField(this, proto.api.Graph, 4));
};


/** @param {proto.api.Graph|undefined} value  */
proto.api.CreateSynapseClusterRequest.prototype.setGraph = function(value) {
  jspb.Message.setWrapperField(this, 4, value);
};


proto.api.CreateSynapseClusterRequest.prototype.clearGraph = function() {
  this.setGraph(undefined);
};


/**
 * Returns whether this field is set.
 * @return{!boolean}
 */
proto.api.CreateSynapseClusterRequest.prototype.hasGraph = function() {
  return jspb.Message.getField(this, 4) != null;
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.DeleteSynapseClusterRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.DeleteSynapseClusterRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.DeleteSynapseClusterRequest.displayName = 'proto.api.DeleteSynapseClusterRequest';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.DeleteSynapseClusterRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.DeleteSynapseClusterRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.DeleteSynapseClusterRequest} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.DeleteSynapseClusterRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: msg.getId()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.DeleteSynapseClusterRequest}
 */
proto.api.DeleteSynapseClusterRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.DeleteSynapseClusterRequest;
  return proto.api.DeleteSynapseClusterRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.DeleteSynapseClusterRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.DeleteSynapseClusterRequest}
 */
proto.api.DeleteSynapseClusterRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.DeleteSynapseClusterRequest} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DeleteSynapseClusterRequest.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.DeleteSynapseClusterRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DeleteSynapseClusterRequest.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.DeleteSynapseClusterRequest} The clone.
 */
proto.api.DeleteSynapseClusterRequest.prototype.cloneMessage = function() {
  return /** @type {!proto.api.DeleteSynapseClusterRequest} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string id = 1;
 * @return {string}
 */
proto.api.DeleteSynapseClusterRequest.prototype.getId = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.DeleteSynapseClusterRequest.prototype.setId = function(value) {
  jspb.Message.setField(this, 1, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.DeleteSynapseClusterResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.DeleteSynapseClusterResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.DeleteSynapseClusterResponse.displayName = 'proto.api.DeleteSynapseClusterResponse';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.DeleteSynapseClusterResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.DeleteSynapseClusterResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.DeleteSynapseClusterResponse} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.DeleteSynapseClusterResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    status: msg.getStatus()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.DeleteSynapseClusterResponse}
 */
proto.api.DeleteSynapseClusterResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.DeleteSynapseClusterResponse;
  return proto.api.DeleteSynapseClusterResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.DeleteSynapseClusterResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.DeleteSynapseClusterResponse}
 */
proto.api.DeleteSynapseClusterResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setStatus(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.DeleteSynapseClusterResponse} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DeleteSynapseClusterResponse.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.DeleteSynapseClusterResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DeleteSynapseClusterResponse.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getStatus();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.DeleteSynapseClusterResponse} The clone.
 */
proto.api.DeleteSynapseClusterResponse.prototype.cloneMessage = function() {
  return /** @type {!proto.api.DeleteSynapseClusterResponse} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string status = 1;
 * @return {string}
 */
proto.api.DeleteSynapseClusterResponse.prototype.getStatus = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.DeleteSynapseClusterResponse.prototype.setStatus = function(value) {
  jspb.Message.setField(this, 1, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.AttachSynapseClusterRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.AttachSynapseClusterRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.AttachSynapseClusterRequest.displayName = 'proto.api.AttachSynapseClusterRequest';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.AttachSynapseClusterRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.AttachSynapseClusterRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.AttachSynapseClusterRequest} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.AttachSynapseClusterRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    synapseclusterid: msg.getSynapseclusterid(),
    neuronclusterid: msg.getNeuronclusterid()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.AttachSynapseClusterRequest}
 */
proto.api.AttachSynapseClusterRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.AttachSynapseClusterRequest;
  return proto.api.AttachSynapseClusterRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.AttachSynapseClusterRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.AttachSynapseClusterRequest}
 */
proto.api.AttachSynapseClusterRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setSynapseclusterid(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setNeuronclusterid(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.AttachSynapseClusterRequest} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.AttachSynapseClusterRequest.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.AttachSynapseClusterRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.AttachSynapseClusterRequest.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getSynapseclusterid();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = this.getNeuronclusterid();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.AttachSynapseClusterRequest} The clone.
 */
proto.api.AttachSynapseClusterRequest.prototype.cloneMessage = function() {
  return /** @type {!proto.api.AttachSynapseClusterRequest} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string synapseClusterId = 1;
 * @return {string}
 */
proto.api.AttachSynapseClusterRequest.prototype.getSynapseclusterid = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.AttachSynapseClusterRequest.prototype.setSynapseclusterid = function(value) {
  jspb.Message.setField(this, 1, value);
};


/**
 * optional string neuronClusterId = 2;
 * @return {string}
 */
proto.api.AttachSynapseClusterRequest.prototype.getNeuronclusterid = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 2, ""));
};


/** @param {string} value  */
proto.api.AttachSynapseClusterRequest.prototype.setNeuronclusterid = function(value) {
  jspb.Message.setField(this, 2, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.AttachSynapseClusterResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.AttachSynapseClusterResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.AttachSynapseClusterResponse.displayName = 'proto.api.AttachSynapseClusterResponse';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.AttachSynapseClusterResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.AttachSynapseClusterResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.AttachSynapseClusterResponse} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.AttachSynapseClusterResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    status: msg.getStatus()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.AttachSynapseClusterResponse}
 */
proto.api.AttachSynapseClusterResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.AttachSynapseClusterResponse;
  return proto.api.AttachSynapseClusterResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.AttachSynapseClusterResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.AttachSynapseClusterResponse}
 */
proto.api.AttachSynapseClusterResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setStatus(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.AttachSynapseClusterResponse} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.AttachSynapseClusterResponse.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.AttachSynapseClusterResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.AttachSynapseClusterResponse.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getStatus();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.AttachSynapseClusterResponse} The clone.
 */
proto.api.AttachSynapseClusterResponse.prototype.cloneMessage = function() {
  return /** @type {!proto.api.AttachSynapseClusterResponse} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string status = 1;
 * @return {string}
 */
proto.api.AttachSynapseClusterResponse.prototype.getStatus = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.AttachSynapseClusterResponse.prototype.setStatus = function(value) {
  jspb.Message.setField(this, 1, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.DetachSynapseClusterRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.DetachSynapseClusterRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.DetachSynapseClusterRequest.displayName = 'proto.api.DetachSynapseClusterRequest';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.DetachSynapseClusterRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.DetachSynapseClusterRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.DetachSynapseClusterRequest} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.DetachSynapseClusterRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    synapseclusterid: msg.getSynapseclusterid(),
    neuronclusterid: msg.getNeuronclusterid()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.DetachSynapseClusterRequest}
 */
proto.api.DetachSynapseClusterRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.DetachSynapseClusterRequest;
  return proto.api.DetachSynapseClusterRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.DetachSynapseClusterRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.DetachSynapseClusterRequest}
 */
proto.api.DetachSynapseClusterRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setSynapseclusterid(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setNeuronclusterid(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.DetachSynapseClusterRequest} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DetachSynapseClusterRequest.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.DetachSynapseClusterRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DetachSynapseClusterRequest.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getSynapseclusterid();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = this.getNeuronclusterid();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.DetachSynapseClusterRequest} The clone.
 */
proto.api.DetachSynapseClusterRequest.prototype.cloneMessage = function() {
  return /** @type {!proto.api.DetachSynapseClusterRequest} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string synapseClusterId = 1;
 * @return {string}
 */
proto.api.DetachSynapseClusterRequest.prototype.getSynapseclusterid = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.DetachSynapseClusterRequest.prototype.setSynapseclusterid = function(value) {
  jspb.Message.setField(this, 1, value);
};


/**
 * optional string neuronClusterId = 2;
 * @return {string}
 */
proto.api.DetachSynapseClusterRequest.prototype.getNeuronclusterid = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 2, ""));
};


/** @param {string} value  */
proto.api.DetachSynapseClusterRequest.prototype.setNeuronclusterid = function(value) {
  jspb.Message.setField(this, 2, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.DetachSynapseClusterResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.DetachSynapseClusterResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.DetachSynapseClusterResponse.displayName = 'proto.api.DetachSynapseClusterResponse';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.DetachSynapseClusterResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.DetachSynapseClusterResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.DetachSynapseClusterResponse} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.DetachSynapseClusterResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    status: msg.getStatus()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.DetachSynapseClusterResponse}
 */
proto.api.DetachSynapseClusterResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.DetachSynapseClusterResponse;
  return proto.api.DetachSynapseClusterResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.DetachSynapseClusterResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.DetachSynapseClusterResponse}
 */
proto.api.DetachSynapseClusterResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setStatus(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.DetachSynapseClusterResponse} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DetachSynapseClusterResponse.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.DetachSynapseClusterResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DetachSynapseClusterResponse.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getStatus();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.DetachSynapseClusterResponse} The clone.
 */
proto.api.DetachSynapseClusterResponse.prototype.cloneMessage = function() {
  return /** @type {!proto.api.DetachSynapseClusterResponse} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string status = 1;
 * @return {string}
 */
proto.api.DetachSynapseClusterResponse.prototype.getStatus = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.DetachSynapseClusterResponse.prototype.setStatus = function(value) {
  jspb.Message.setField(this, 1, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.SetSynapseClusterPositionRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.SetSynapseClusterPositionRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.SetSynapseClusterPositionRequest.displayName = 'proto.api.SetSynapseClusterPositionRequest';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.SetSynapseClusterPositionRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.SetSynapseClusterPositionRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.SetSynapseClusterPositionRequest} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.SetSynapseClusterPositionRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: msg.getId(),
    graph: (f = msg.getGraph()) && proto.api.Graph.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.SetSynapseClusterPositionRequest}
 */
proto.api.SetSynapseClusterPositionRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.SetSynapseClusterPositionRequest;
  return proto.api.SetSynapseClusterPositionRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.SetSynapseClusterPositionRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.SetSynapseClusterPositionRequest}
 */
proto.api.SetSynapseClusterPositionRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setId(value);
      break;
    case 2:
      var value = new proto.api.Graph;
      reader.readMessage(value,proto.api.Graph.deserializeBinaryFromReader);
      msg.setGraph(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.SetSynapseClusterPositionRequest} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.SetSynapseClusterPositionRequest.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.SetSynapseClusterPositionRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.SetSynapseClusterPositionRequest.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = this.getGraph();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      proto.api.Graph.serializeBinaryToWriter
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.SetSynapseClusterPositionRequest} The clone.
 */
proto.api.SetSynapseClusterPositionRequest.prototype.cloneMessage = function() {
  return /** @type {!proto.api.SetSynapseClusterPositionRequest} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string id = 1;
 * @return {string}
 */
proto.api.SetSynapseClusterPositionRequest.prototype.getId = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.SetSynapseClusterPositionRequest.prototype.setId = function(value) {
  jspb.Message.setField(this, 1, value);
};


/**
 * optional Graph graph = 2;
 * @return {proto.api.Graph}
 */
proto.api.SetSynapseClusterPositionRequest.prototype.getGraph = function() {
  return /** @type{proto.api.Graph} */ (
    jspb.Message.getWrapperField(this, proto.api.Graph, 2));
};


/** @param {proto.api.Graph|undefined} value  */
proto.api.SetSynapseClusterPositionRequest.prototype.setGraph = function(value) {
  jspb.Message.setWrapperField(this, 2, value);
};


proto.api.SetSynapseClusterPositionRequest.prototype.clearGraph = function() {
  this.setGraph(undefined);
};


/**
 * Returns whether this field is set.
 * @return{!boolean}
 */
proto.api.SetSynapseClusterPositionRequest.prototype.hasGraph = function() {
  return jspb.Message.getField(this, 2) != null;
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.SetSynapseClusterPositionResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.SetSynapseClusterPositionResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.SetSynapseClusterPositionResponse.displayName = 'proto.api.SetSynapseClusterPositionResponse';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.SetSynapseClusterPositionResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.SetSynapseClusterPositionResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.SetSynapseClusterPositionResponse} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.SetSynapseClusterPositionResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    status: msg.getStatus()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.SetSynapseClusterPositionResponse}
 */
proto.api.SetSynapseClusterPositionResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.SetSynapseClusterPositionResponse;
  return proto.api.SetSynapseClusterPositionResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.SetSynapseClusterPositionResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.SetSynapseClusterPositionResponse}
 */
proto.api.SetSynapseClusterPositionResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setStatus(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.SetSynapseClusterPositionResponse} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.SetSynapseClusterPositionResponse.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.SetSynapseClusterPositionResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.SetSynapseClusterPositionResponse.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getStatus();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.SetSynapseClusterPositionResponse} The clone.
 */
proto.api.SetSynapseClusterPositionResponse.prototype.cloneMessage = function() {
  return /** @type {!proto.api.SetSynapseClusterPositionResponse} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string status = 1;
 * @return {string}
 */
proto.api.SetSynapseClusterPositionResponse.prototype.getStatus = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.SetSynapseClusterPositionResponse.prototype.setStatus = function(value) {
  jspb.Message.setField(this, 1, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.STDPPreceptorRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.STDPPreceptorRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.STDPPreceptorRequest.displayName = 'proto.api.STDPPreceptorRequest';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.STDPPreceptorRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.STDPPreceptorRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.STDPPreceptorRequest} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.STDPPreceptorRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: msg.getId()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.STDPPreceptorRequest}
 */
proto.api.STDPPreceptorRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.STDPPreceptorRequest;
  return proto.api.STDPPreceptorRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.STDPPreceptorRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.STDPPreceptorRequest}
 */
proto.api.STDPPreceptorRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.STDPPreceptorRequest} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.STDPPreceptorRequest.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.STDPPreceptorRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.STDPPreceptorRequest.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.STDPPreceptorRequest} The clone.
 */
proto.api.STDPPreceptorRequest.prototype.cloneMessage = function() {
  return /** @type {!proto.api.STDPPreceptorRequest} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string id = 1;
 * @return {string}
 */
proto.api.STDPPreceptorRequest.prototype.getId = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.STDPPreceptorRequest.prototype.setId = function(value) {
  jspb.Message.setField(this, 1, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.STDPPreceptorResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.STDPPreceptorResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.STDPPreceptorResponse.displayName = 'proto.api.STDPPreceptorResponse';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.STDPPreceptorResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.STDPPreceptorResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.STDPPreceptorResponse} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.STDPPreceptorResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: msg.getId(),
    name: msg.getName(),
    maxqueuesize: msg.getMaxqueuesize(),
    synapseclusterid: msg.getSynapseclusterid(),
    graph: (f = msg.getGraph()) && proto.api.Graph.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.STDPPreceptorResponse}
 */
proto.api.STDPPreceptorResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.STDPPreceptorResponse;
  return proto.api.STDPPreceptorResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.STDPPreceptorResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.STDPPreceptorResponse}
 */
proto.api.STDPPreceptorResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setId(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setMaxqueuesize(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setSynapseclusterid(value);
      break;
    case 5:
      var value = new proto.api.Graph;
      reader.readMessage(value,proto.api.Graph.deserializeBinaryFromReader);
      msg.setGraph(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.STDPPreceptorResponse} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.STDPPreceptorResponse.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.STDPPreceptorResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.STDPPreceptorResponse.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = this.getName();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = this.getMaxqueuesize();
  if (f !== 0) {
    writer.writeInt32(
      3,
      f
    );
  }
  f = this.getSynapseclusterid();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = this.getGraph();
  if (f != null) {
    writer.writeMessage(
      5,
      f,
      proto.api.Graph.serializeBinaryToWriter
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.STDPPreceptorResponse} The clone.
 */
proto.api.STDPPreceptorResponse.prototype.cloneMessage = function() {
  return /** @type {!proto.api.STDPPreceptorResponse} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string id = 1;
 * @return {string}
 */
proto.api.STDPPreceptorResponse.prototype.getId = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.STDPPreceptorResponse.prototype.setId = function(value) {
  jspb.Message.setField(this, 1, value);
};


/**
 * optional string name = 2;
 * @return {string}
 */
proto.api.STDPPreceptorResponse.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 2, ""));
};


/** @param {string} value  */
proto.api.STDPPreceptorResponse.prototype.setName = function(value) {
  jspb.Message.setField(this, 2, value);
};


/**
 * optional int32 maxQueueSize = 3;
 * @return {number}
 */
proto.api.STDPPreceptorResponse.prototype.getMaxqueuesize = function() {
  return /** @type {number} */ (jspb.Message.getFieldProto3(this, 3, 0));
};


/** @param {number} value  */
proto.api.STDPPreceptorResponse.prototype.setMaxqueuesize = function(value) {
  jspb.Message.setField(this, 3, value);
};


/**
 * optional string synapseClusterId = 4;
 * @return {string}
 */
proto.api.STDPPreceptorResponse.prototype.getSynapseclusterid = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 4, ""));
};


/** @param {string} value  */
proto.api.STDPPreceptorResponse.prototype.setSynapseclusterid = function(value) {
  jspb.Message.setField(this, 4, value);
};


/**
 * optional Graph graph = 5;
 * @return {proto.api.Graph}
 */
proto.api.STDPPreceptorResponse.prototype.getGraph = function() {
  return /** @type{proto.api.Graph} */ (
    jspb.Message.getWrapperField(this, proto.api.Graph, 5));
};


/** @param {proto.api.Graph|undefined} value  */
proto.api.STDPPreceptorResponse.prototype.setGraph = function(value) {
  jspb.Message.setWrapperField(this, 5, value);
};


proto.api.STDPPreceptorResponse.prototype.clearGraph = function() {
  this.setGraph(undefined);
};


/**
 * Returns whether this field is set.
 * @return{!boolean}
 */
proto.api.STDPPreceptorResponse.prototype.hasGraph = function() {
  return jspb.Message.getField(this, 5) != null;
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.CreateSTDPPreceptorRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.CreateSTDPPreceptorRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.CreateSTDPPreceptorRequest.displayName = 'proto.api.CreateSTDPPreceptorRequest';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.CreateSTDPPreceptorRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.CreateSTDPPreceptorRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.CreateSTDPPreceptorRequest} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.CreateSTDPPreceptorRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    name: msg.getName(),
    maxqueuesize: msg.getMaxqueuesize(),
    graph: (f = msg.getGraph()) && proto.api.Graph.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.CreateSTDPPreceptorRequest}
 */
proto.api.CreateSTDPPreceptorRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.CreateSTDPPreceptorRequest;
  return proto.api.CreateSTDPPreceptorRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.CreateSTDPPreceptorRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.CreateSTDPPreceptorRequest}
 */
proto.api.CreateSTDPPreceptorRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setMaxqueuesize(value);
      break;
    case 3:
      var value = new proto.api.Graph;
      reader.readMessage(value,proto.api.Graph.deserializeBinaryFromReader);
      msg.setGraph(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.CreateSTDPPreceptorRequest} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.CreateSTDPPreceptorRequest.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.CreateSTDPPreceptorRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.CreateSTDPPreceptorRequest.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = this.getMaxqueuesize();
  if (f !== 0) {
    writer.writeInt32(
      2,
      f
    );
  }
  f = this.getGraph();
  if (f != null) {
    writer.writeMessage(
      3,
      f,
      proto.api.Graph.serializeBinaryToWriter
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.CreateSTDPPreceptorRequest} The clone.
 */
proto.api.CreateSTDPPreceptorRequest.prototype.cloneMessage = function() {
  return /** @type {!proto.api.CreateSTDPPreceptorRequest} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string name = 1;
 * @return {string}
 */
proto.api.CreateSTDPPreceptorRequest.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.CreateSTDPPreceptorRequest.prototype.setName = function(value) {
  jspb.Message.setField(this, 1, value);
};


/**
 * optional int32 maxQueueSize = 2;
 * @return {number}
 */
proto.api.CreateSTDPPreceptorRequest.prototype.getMaxqueuesize = function() {
  return /** @type {number} */ (jspb.Message.getFieldProto3(this, 2, 0));
};


/** @param {number} value  */
proto.api.CreateSTDPPreceptorRequest.prototype.setMaxqueuesize = function(value) {
  jspb.Message.setField(this, 2, value);
};


/**
 * optional Graph graph = 3;
 * @return {proto.api.Graph}
 */
proto.api.CreateSTDPPreceptorRequest.prototype.getGraph = function() {
  return /** @type{proto.api.Graph} */ (
    jspb.Message.getWrapperField(this, proto.api.Graph, 3));
};


/** @param {proto.api.Graph|undefined} value  */
proto.api.CreateSTDPPreceptorRequest.prototype.setGraph = function(value) {
  jspb.Message.setWrapperField(this, 3, value);
};


proto.api.CreateSTDPPreceptorRequest.prototype.clearGraph = function() {
  this.setGraph(undefined);
};


/**
 * Returns whether this field is set.
 * @return{!boolean}
 */
proto.api.CreateSTDPPreceptorRequest.prototype.hasGraph = function() {
  return jspb.Message.getField(this, 3) != null;
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.DeleteSTDPPreceptorRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.DeleteSTDPPreceptorRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.DeleteSTDPPreceptorRequest.displayName = 'proto.api.DeleteSTDPPreceptorRequest';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.DeleteSTDPPreceptorRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.DeleteSTDPPreceptorRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.DeleteSTDPPreceptorRequest} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.DeleteSTDPPreceptorRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: msg.getId()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.DeleteSTDPPreceptorRequest}
 */
proto.api.DeleteSTDPPreceptorRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.DeleteSTDPPreceptorRequest;
  return proto.api.DeleteSTDPPreceptorRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.DeleteSTDPPreceptorRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.DeleteSTDPPreceptorRequest}
 */
proto.api.DeleteSTDPPreceptorRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.DeleteSTDPPreceptorRequest} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DeleteSTDPPreceptorRequest.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.DeleteSTDPPreceptorRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DeleteSTDPPreceptorRequest.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.DeleteSTDPPreceptorRequest} The clone.
 */
proto.api.DeleteSTDPPreceptorRequest.prototype.cloneMessage = function() {
  return /** @type {!proto.api.DeleteSTDPPreceptorRequest} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string id = 1;
 * @return {string}
 */
proto.api.DeleteSTDPPreceptorRequest.prototype.getId = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.DeleteSTDPPreceptorRequest.prototype.setId = function(value) {
  jspb.Message.setField(this, 1, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.DeleteSTDPPreceptorResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.DeleteSTDPPreceptorResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.DeleteSTDPPreceptorResponse.displayName = 'proto.api.DeleteSTDPPreceptorResponse';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.DeleteSTDPPreceptorResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.DeleteSTDPPreceptorResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.DeleteSTDPPreceptorResponse} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.DeleteSTDPPreceptorResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    status: msg.getStatus()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.DeleteSTDPPreceptorResponse}
 */
proto.api.DeleteSTDPPreceptorResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.DeleteSTDPPreceptorResponse;
  return proto.api.DeleteSTDPPreceptorResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.DeleteSTDPPreceptorResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.DeleteSTDPPreceptorResponse}
 */
proto.api.DeleteSTDPPreceptorResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setStatus(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.DeleteSTDPPreceptorResponse} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DeleteSTDPPreceptorResponse.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.DeleteSTDPPreceptorResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DeleteSTDPPreceptorResponse.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getStatus();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.DeleteSTDPPreceptorResponse} The clone.
 */
proto.api.DeleteSTDPPreceptorResponse.prototype.cloneMessage = function() {
  return /** @type {!proto.api.DeleteSTDPPreceptorResponse} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string status = 1;
 * @return {string}
 */
proto.api.DeleteSTDPPreceptorResponse.prototype.getStatus = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.DeleteSTDPPreceptorResponse.prototype.setStatus = function(value) {
  jspb.Message.setField(this, 1, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.AttachSTDPPreceptorRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.AttachSTDPPreceptorRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.AttachSTDPPreceptorRequest.displayName = 'proto.api.AttachSTDPPreceptorRequest';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.AttachSTDPPreceptorRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.AttachSTDPPreceptorRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.AttachSTDPPreceptorRequest} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.AttachSTDPPreceptorRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    stdppreceptorid: msg.getStdppreceptorid(),
    synapseclusterid: msg.getSynapseclusterid()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.AttachSTDPPreceptorRequest}
 */
proto.api.AttachSTDPPreceptorRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.AttachSTDPPreceptorRequest;
  return proto.api.AttachSTDPPreceptorRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.AttachSTDPPreceptorRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.AttachSTDPPreceptorRequest}
 */
proto.api.AttachSTDPPreceptorRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setStdppreceptorid(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setSynapseclusterid(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.AttachSTDPPreceptorRequest} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.AttachSTDPPreceptorRequest.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.AttachSTDPPreceptorRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.AttachSTDPPreceptorRequest.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getStdppreceptorid();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = this.getSynapseclusterid();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.AttachSTDPPreceptorRequest} The clone.
 */
proto.api.AttachSTDPPreceptorRequest.prototype.cloneMessage = function() {
  return /** @type {!proto.api.AttachSTDPPreceptorRequest} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string STDPPreceptorId = 1;
 * @return {string}
 */
proto.api.AttachSTDPPreceptorRequest.prototype.getStdppreceptorid = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.AttachSTDPPreceptorRequest.prototype.setStdppreceptorid = function(value) {
  jspb.Message.setField(this, 1, value);
};


/**
 * optional string synapseClusterId = 2;
 * @return {string}
 */
proto.api.AttachSTDPPreceptorRequest.prototype.getSynapseclusterid = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 2, ""));
};


/** @param {string} value  */
proto.api.AttachSTDPPreceptorRequest.prototype.setSynapseclusterid = function(value) {
  jspb.Message.setField(this, 2, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.AttachSTDPPreceptorResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.AttachSTDPPreceptorResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.AttachSTDPPreceptorResponse.displayName = 'proto.api.AttachSTDPPreceptorResponse';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.AttachSTDPPreceptorResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.AttachSTDPPreceptorResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.AttachSTDPPreceptorResponse} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.AttachSTDPPreceptorResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    status: msg.getStatus()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.AttachSTDPPreceptorResponse}
 */
proto.api.AttachSTDPPreceptorResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.AttachSTDPPreceptorResponse;
  return proto.api.AttachSTDPPreceptorResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.AttachSTDPPreceptorResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.AttachSTDPPreceptorResponse}
 */
proto.api.AttachSTDPPreceptorResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setStatus(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.AttachSTDPPreceptorResponse} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.AttachSTDPPreceptorResponse.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.AttachSTDPPreceptorResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.AttachSTDPPreceptorResponse.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getStatus();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.AttachSTDPPreceptorResponse} The clone.
 */
proto.api.AttachSTDPPreceptorResponse.prototype.cloneMessage = function() {
  return /** @type {!proto.api.AttachSTDPPreceptorResponse} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string status = 1;
 * @return {string}
 */
proto.api.AttachSTDPPreceptorResponse.prototype.getStatus = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.AttachSTDPPreceptorResponse.prototype.setStatus = function(value) {
  jspb.Message.setField(this, 1, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.DetachSTDPPreceptorRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.DetachSTDPPreceptorRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.DetachSTDPPreceptorRequest.displayName = 'proto.api.DetachSTDPPreceptorRequest';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.DetachSTDPPreceptorRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.DetachSTDPPreceptorRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.DetachSTDPPreceptorRequest} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.DetachSTDPPreceptorRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    stdppreceptorid: msg.getStdppreceptorid()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.DetachSTDPPreceptorRequest}
 */
proto.api.DetachSTDPPreceptorRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.DetachSTDPPreceptorRequest;
  return proto.api.DetachSTDPPreceptorRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.DetachSTDPPreceptorRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.DetachSTDPPreceptorRequest}
 */
proto.api.DetachSTDPPreceptorRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setStdppreceptorid(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.DetachSTDPPreceptorRequest} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DetachSTDPPreceptorRequest.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.DetachSTDPPreceptorRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DetachSTDPPreceptorRequest.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getStdppreceptorid();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.DetachSTDPPreceptorRequest} The clone.
 */
proto.api.DetachSTDPPreceptorRequest.prototype.cloneMessage = function() {
  return /** @type {!proto.api.DetachSTDPPreceptorRequest} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string STDPPreceptorId = 1;
 * @return {string}
 */
proto.api.DetachSTDPPreceptorRequest.prototype.getStdppreceptorid = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.DetachSTDPPreceptorRequest.prototype.setStdppreceptorid = function(value) {
  jspb.Message.setField(this, 1, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.DetachSTDPPreceptorResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.DetachSTDPPreceptorResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.DetachSTDPPreceptorResponse.displayName = 'proto.api.DetachSTDPPreceptorResponse';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.DetachSTDPPreceptorResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.DetachSTDPPreceptorResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.DetachSTDPPreceptorResponse} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.DetachSTDPPreceptorResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    status: msg.getStatus()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.DetachSTDPPreceptorResponse}
 */
proto.api.DetachSTDPPreceptorResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.DetachSTDPPreceptorResponse;
  return proto.api.DetachSTDPPreceptorResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.DetachSTDPPreceptorResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.DetachSTDPPreceptorResponse}
 */
proto.api.DetachSTDPPreceptorResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setStatus(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.DetachSTDPPreceptorResponse} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DetachSTDPPreceptorResponse.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.DetachSTDPPreceptorResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DetachSTDPPreceptorResponse.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getStatus();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.DetachSTDPPreceptorResponse} The clone.
 */
proto.api.DetachSTDPPreceptorResponse.prototype.cloneMessage = function() {
  return /** @type {!proto.api.DetachSTDPPreceptorResponse} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string status = 1;
 * @return {string}
 */
proto.api.DetachSTDPPreceptorResponse.prototype.getStatus = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.DetachSTDPPreceptorResponse.prototype.setStatus = function(value) {
  jspb.Message.setField(this, 1, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.SetSTDPPreceptorPositionRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.SetSTDPPreceptorPositionRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.SetSTDPPreceptorPositionRequest.displayName = 'proto.api.SetSTDPPreceptorPositionRequest';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.SetSTDPPreceptorPositionRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.SetSTDPPreceptorPositionRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.SetSTDPPreceptorPositionRequest} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.SetSTDPPreceptorPositionRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: msg.getId(),
    graph: (f = msg.getGraph()) && proto.api.Graph.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.SetSTDPPreceptorPositionRequest}
 */
proto.api.SetSTDPPreceptorPositionRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.SetSTDPPreceptorPositionRequest;
  return proto.api.SetSTDPPreceptorPositionRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.SetSTDPPreceptorPositionRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.SetSTDPPreceptorPositionRequest}
 */
proto.api.SetSTDPPreceptorPositionRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setId(value);
      break;
    case 2:
      var value = new proto.api.Graph;
      reader.readMessage(value,proto.api.Graph.deserializeBinaryFromReader);
      msg.setGraph(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.SetSTDPPreceptorPositionRequest} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.SetSTDPPreceptorPositionRequest.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.SetSTDPPreceptorPositionRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.SetSTDPPreceptorPositionRequest.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = this.getGraph();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      proto.api.Graph.serializeBinaryToWriter
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.SetSTDPPreceptorPositionRequest} The clone.
 */
proto.api.SetSTDPPreceptorPositionRequest.prototype.cloneMessage = function() {
  return /** @type {!proto.api.SetSTDPPreceptorPositionRequest} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string id = 1;
 * @return {string}
 */
proto.api.SetSTDPPreceptorPositionRequest.prototype.getId = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.SetSTDPPreceptorPositionRequest.prototype.setId = function(value) {
  jspb.Message.setField(this, 1, value);
};


/**
 * optional Graph graph = 2;
 * @return {proto.api.Graph}
 */
proto.api.SetSTDPPreceptorPositionRequest.prototype.getGraph = function() {
  return /** @type{proto.api.Graph} */ (
    jspb.Message.getWrapperField(this, proto.api.Graph, 2));
};


/** @param {proto.api.Graph|undefined} value  */
proto.api.SetSTDPPreceptorPositionRequest.prototype.setGraph = function(value) {
  jspb.Message.setWrapperField(this, 2, value);
};


proto.api.SetSTDPPreceptorPositionRequest.prototype.clearGraph = function() {
  this.setGraph(undefined);
};


/**
 * Returns whether this field is set.
 * @return{!boolean}
 */
proto.api.SetSTDPPreceptorPositionRequest.prototype.hasGraph = function() {
  return jspb.Message.getField(this, 2) != null;
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.SetSTDPPreceptorPositionResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.SetSTDPPreceptorPositionResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.SetSTDPPreceptorPositionResponse.displayName = 'proto.api.SetSTDPPreceptorPositionResponse';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.SetSTDPPreceptorPositionResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.SetSTDPPreceptorPositionResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.SetSTDPPreceptorPositionResponse} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.SetSTDPPreceptorPositionResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    status: msg.getStatus()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.SetSTDPPreceptorPositionResponse}
 */
proto.api.SetSTDPPreceptorPositionResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.SetSTDPPreceptorPositionResponse;
  return proto.api.SetSTDPPreceptorPositionResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.SetSTDPPreceptorPositionResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.SetSTDPPreceptorPositionResponse}
 */
proto.api.SetSTDPPreceptorPositionResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setStatus(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.SetSTDPPreceptorPositionResponse} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.SetSTDPPreceptorPositionResponse.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.SetSTDPPreceptorPositionResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.SetSTDPPreceptorPositionResponse.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getStatus();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.SetSTDPPreceptorPositionResponse} The clone.
 */
proto.api.SetSTDPPreceptorPositionResponse.prototype.cloneMessage = function() {
  return /** @type {!proto.api.SetSTDPPreceptorPositionResponse} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string status = 1;
 * @return {string}
 */
proto.api.SetSTDPPreceptorPositionResponse.prototype.getStatus = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.SetSTDPPreceptorPositionResponse.prototype.setStatus = function(value) {
  jspb.Message.setField(this, 1, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.ModulatedSTDPPreceptorRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.ModulatedSTDPPreceptorRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.ModulatedSTDPPreceptorRequest.displayName = 'proto.api.ModulatedSTDPPreceptorRequest';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.ModulatedSTDPPreceptorRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.ModulatedSTDPPreceptorRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.ModulatedSTDPPreceptorRequest} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.ModulatedSTDPPreceptorRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: msg.getId()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.ModulatedSTDPPreceptorRequest}
 */
proto.api.ModulatedSTDPPreceptorRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.ModulatedSTDPPreceptorRequest;
  return proto.api.ModulatedSTDPPreceptorRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.ModulatedSTDPPreceptorRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.ModulatedSTDPPreceptorRequest}
 */
proto.api.ModulatedSTDPPreceptorRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.ModulatedSTDPPreceptorRequest} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.ModulatedSTDPPreceptorRequest.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.ModulatedSTDPPreceptorRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.ModulatedSTDPPreceptorRequest.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.ModulatedSTDPPreceptorRequest} The clone.
 */
proto.api.ModulatedSTDPPreceptorRequest.prototype.cloneMessage = function() {
  return /** @type {!proto.api.ModulatedSTDPPreceptorRequest} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string id = 1;
 * @return {string}
 */
proto.api.ModulatedSTDPPreceptorRequest.prototype.getId = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.ModulatedSTDPPreceptorRequest.prototype.setId = function(value) {
  jspb.Message.setField(this, 1, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.ModulatedSTDPPreceptorResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.ModulatedSTDPPreceptorResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.ModulatedSTDPPreceptorResponse.displayName = 'proto.api.ModulatedSTDPPreceptorResponse';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.ModulatedSTDPPreceptorResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.ModulatedSTDPPreceptorResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.ModulatedSTDPPreceptorResponse} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.ModulatedSTDPPreceptorResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: msg.getId(),
    name: msg.getName(),
    maxqueuesize: msg.getMaxqueuesize(),
    sensitivity: msg.getSensitivity(),
    synapseclusterid: msg.getSynapseclusterid(),
    dopamineadapterid: msg.getDopamineadapterid(),
    graph: (f = msg.getGraph()) && proto.api.Graph.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.ModulatedSTDPPreceptorResponse}
 */
proto.api.ModulatedSTDPPreceptorResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.ModulatedSTDPPreceptorResponse;
  return proto.api.ModulatedSTDPPreceptorResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.ModulatedSTDPPreceptorResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.ModulatedSTDPPreceptorResponse}
 */
proto.api.ModulatedSTDPPreceptorResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setId(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setMaxqueuesize(value);
      break;
    case 4:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setSensitivity(value);
      break;
    case 5:
      var value = /** @type {string} */ (reader.readString());
      msg.setSynapseclusterid(value);
      break;
    case 6:
      var value = /** @type {string} */ (reader.readString());
      msg.setDopamineadapterid(value);
      break;
    case 7:
      var value = new proto.api.Graph;
      reader.readMessage(value,proto.api.Graph.deserializeBinaryFromReader);
      msg.setGraph(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.ModulatedSTDPPreceptorResponse} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.ModulatedSTDPPreceptorResponse.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.ModulatedSTDPPreceptorResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.ModulatedSTDPPreceptorResponse.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = this.getName();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = this.getMaxqueuesize();
  if (f !== 0) {
    writer.writeInt32(
      3,
      f
    );
  }
  f = this.getSensitivity();
  if (f !== 0.0) {
    writer.writeDouble(
      4,
      f
    );
  }
  f = this.getSynapseclusterid();
  if (f.length > 0) {
    writer.writeString(
      5,
      f
    );
  }
  f = this.getDopamineadapterid();
  if (f.length > 0) {
    writer.writeString(
      6,
      f
    );
  }
  f = this.getGraph();
  if (f != null) {
    writer.writeMessage(
      7,
      f,
      proto.api.Graph.serializeBinaryToWriter
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.ModulatedSTDPPreceptorResponse} The clone.
 */
proto.api.ModulatedSTDPPreceptorResponse.prototype.cloneMessage = function() {
  return /** @type {!proto.api.ModulatedSTDPPreceptorResponse} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string id = 1;
 * @return {string}
 */
proto.api.ModulatedSTDPPreceptorResponse.prototype.getId = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.ModulatedSTDPPreceptorResponse.prototype.setId = function(value) {
  jspb.Message.setField(this, 1, value);
};


/**
 * optional string name = 2;
 * @return {string}
 */
proto.api.ModulatedSTDPPreceptorResponse.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 2, ""));
};


/** @param {string} value  */
proto.api.ModulatedSTDPPreceptorResponse.prototype.setName = function(value) {
  jspb.Message.setField(this, 2, value);
};


/**
 * optional int32 maxQueueSize = 3;
 * @return {number}
 */
proto.api.ModulatedSTDPPreceptorResponse.prototype.getMaxqueuesize = function() {
  return /** @type {number} */ (jspb.Message.getFieldProto3(this, 3, 0));
};


/** @param {number} value  */
proto.api.ModulatedSTDPPreceptorResponse.prototype.setMaxqueuesize = function(value) {
  jspb.Message.setField(this, 3, value);
};


/**
 * optional double sensitivity = 4;
 * @return {number}
 */
proto.api.ModulatedSTDPPreceptorResponse.prototype.getSensitivity = function() {
  return /** @type {number} */ (jspb.Message.getFieldProto3(this, 4, 0));
};


/** @param {number} value  */
proto.api.ModulatedSTDPPreceptorResponse.prototype.setSensitivity = function(value) {
  jspb.Message.setField(this, 4, value);
};


/**
 * optional string synapseClusterId = 5;
 * @return {string}
 */
proto.api.ModulatedSTDPPreceptorResponse.prototype.getSynapseclusterid = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 5, ""));
};


/** @param {string} value  */
proto.api.ModulatedSTDPPreceptorResponse.prototype.setSynapseclusterid = function(value) {
  jspb.Message.setField(this, 5, value);
};


/**
 * optional string dopamineAdapterId = 6;
 * @return {string}
 */
proto.api.ModulatedSTDPPreceptorResponse.prototype.getDopamineadapterid = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 6, ""));
};


/** @param {string} value  */
proto.api.ModulatedSTDPPreceptorResponse.prototype.setDopamineadapterid = function(value) {
  jspb.Message.setField(this, 6, value);
};


/**
 * optional Graph graph = 7;
 * @return {proto.api.Graph}
 */
proto.api.ModulatedSTDPPreceptorResponse.prototype.getGraph = function() {
  return /** @type{proto.api.Graph} */ (
    jspb.Message.getWrapperField(this, proto.api.Graph, 7));
};


/** @param {proto.api.Graph|undefined} value  */
proto.api.ModulatedSTDPPreceptorResponse.prototype.setGraph = function(value) {
  jspb.Message.setWrapperField(this, 7, value);
};


proto.api.ModulatedSTDPPreceptorResponse.prototype.clearGraph = function() {
  this.setGraph(undefined);
};


/**
 * Returns whether this field is set.
 * @return{!boolean}
 */
proto.api.ModulatedSTDPPreceptorResponse.prototype.hasGraph = function() {
  return jspb.Message.getField(this, 7) != null;
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.CreateModulatedSTDPPreceptorRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.CreateModulatedSTDPPreceptorRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.CreateModulatedSTDPPreceptorRequest.displayName = 'proto.api.CreateModulatedSTDPPreceptorRequest';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.CreateModulatedSTDPPreceptorRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.CreateModulatedSTDPPreceptorRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.CreateModulatedSTDPPreceptorRequest} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.CreateModulatedSTDPPreceptorRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    name: msg.getName(),
    maxqueuesize: msg.getMaxqueuesize(),
    sensitivity: msg.getSensitivity(),
    graph: (f = msg.getGraph()) && proto.api.Graph.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.CreateModulatedSTDPPreceptorRequest}
 */
proto.api.CreateModulatedSTDPPreceptorRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.CreateModulatedSTDPPreceptorRequest;
  return proto.api.CreateModulatedSTDPPreceptorRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.CreateModulatedSTDPPreceptorRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.CreateModulatedSTDPPreceptorRequest}
 */
proto.api.CreateModulatedSTDPPreceptorRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setMaxqueuesize(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setSensitivity(value);
      break;
    case 4:
      var value = new proto.api.Graph;
      reader.readMessage(value,proto.api.Graph.deserializeBinaryFromReader);
      msg.setGraph(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.CreateModulatedSTDPPreceptorRequest} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.CreateModulatedSTDPPreceptorRequest.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.CreateModulatedSTDPPreceptorRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.CreateModulatedSTDPPreceptorRequest.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = this.getMaxqueuesize();
  if (f !== 0) {
    writer.writeInt32(
      2,
      f
    );
  }
  f = this.getSensitivity();
  if (f !== 0.0) {
    writer.writeDouble(
      3,
      f
    );
  }
  f = this.getGraph();
  if (f != null) {
    writer.writeMessage(
      4,
      f,
      proto.api.Graph.serializeBinaryToWriter
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.CreateModulatedSTDPPreceptorRequest} The clone.
 */
proto.api.CreateModulatedSTDPPreceptorRequest.prototype.cloneMessage = function() {
  return /** @type {!proto.api.CreateModulatedSTDPPreceptorRequest} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string name = 1;
 * @return {string}
 */
proto.api.CreateModulatedSTDPPreceptorRequest.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.CreateModulatedSTDPPreceptorRequest.prototype.setName = function(value) {
  jspb.Message.setField(this, 1, value);
};


/**
 * optional int32 maxQueueSize = 2;
 * @return {number}
 */
proto.api.CreateModulatedSTDPPreceptorRequest.prototype.getMaxqueuesize = function() {
  return /** @type {number} */ (jspb.Message.getFieldProto3(this, 2, 0));
};


/** @param {number} value  */
proto.api.CreateModulatedSTDPPreceptorRequest.prototype.setMaxqueuesize = function(value) {
  jspb.Message.setField(this, 2, value);
};


/**
 * optional double sensitivity = 3;
 * @return {number}
 */
proto.api.CreateModulatedSTDPPreceptorRequest.prototype.getSensitivity = function() {
  return /** @type {number} */ (jspb.Message.getFieldProto3(this, 3, 0));
};


/** @param {number} value  */
proto.api.CreateModulatedSTDPPreceptorRequest.prototype.setSensitivity = function(value) {
  jspb.Message.setField(this, 3, value);
};


/**
 * optional Graph graph = 4;
 * @return {proto.api.Graph}
 */
proto.api.CreateModulatedSTDPPreceptorRequest.prototype.getGraph = function() {
  return /** @type{proto.api.Graph} */ (
    jspb.Message.getWrapperField(this, proto.api.Graph, 4));
};


/** @param {proto.api.Graph|undefined} value  */
proto.api.CreateModulatedSTDPPreceptorRequest.prototype.setGraph = function(value) {
  jspb.Message.setWrapperField(this, 4, value);
};


proto.api.CreateModulatedSTDPPreceptorRequest.prototype.clearGraph = function() {
  this.setGraph(undefined);
};


/**
 * Returns whether this field is set.
 * @return{!boolean}
 */
proto.api.CreateModulatedSTDPPreceptorRequest.prototype.hasGraph = function() {
  return jspb.Message.getField(this, 4) != null;
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.DeleteModulatedSTDPPreceptorRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.DeleteModulatedSTDPPreceptorRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.DeleteModulatedSTDPPreceptorRequest.displayName = 'proto.api.DeleteModulatedSTDPPreceptorRequest';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.DeleteModulatedSTDPPreceptorRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.DeleteModulatedSTDPPreceptorRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.DeleteModulatedSTDPPreceptorRequest} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.DeleteModulatedSTDPPreceptorRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: msg.getId()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.DeleteModulatedSTDPPreceptorRequest}
 */
proto.api.DeleteModulatedSTDPPreceptorRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.DeleteModulatedSTDPPreceptorRequest;
  return proto.api.DeleteModulatedSTDPPreceptorRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.DeleteModulatedSTDPPreceptorRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.DeleteModulatedSTDPPreceptorRequest}
 */
proto.api.DeleteModulatedSTDPPreceptorRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.DeleteModulatedSTDPPreceptorRequest} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DeleteModulatedSTDPPreceptorRequest.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.DeleteModulatedSTDPPreceptorRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DeleteModulatedSTDPPreceptorRequest.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.DeleteModulatedSTDPPreceptorRequest} The clone.
 */
proto.api.DeleteModulatedSTDPPreceptorRequest.prototype.cloneMessage = function() {
  return /** @type {!proto.api.DeleteModulatedSTDPPreceptorRequest} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string id = 1;
 * @return {string}
 */
proto.api.DeleteModulatedSTDPPreceptorRequest.prototype.getId = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.DeleteModulatedSTDPPreceptorRequest.prototype.setId = function(value) {
  jspb.Message.setField(this, 1, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.DeleteModulatedSTDPPreceptorResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.DeleteModulatedSTDPPreceptorResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.DeleteModulatedSTDPPreceptorResponse.displayName = 'proto.api.DeleteModulatedSTDPPreceptorResponse';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.DeleteModulatedSTDPPreceptorResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.DeleteModulatedSTDPPreceptorResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.DeleteModulatedSTDPPreceptorResponse} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.DeleteModulatedSTDPPreceptorResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    status: msg.getStatus()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.DeleteModulatedSTDPPreceptorResponse}
 */
proto.api.DeleteModulatedSTDPPreceptorResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.DeleteModulatedSTDPPreceptorResponse;
  return proto.api.DeleteModulatedSTDPPreceptorResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.DeleteModulatedSTDPPreceptorResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.DeleteModulatedSTDPPreceptorResponse}
 */
proto.api.DeleteModulatedSTDPPreceptorResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setStatus(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.DeleteModulatedSTDPPreceptorResponse} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DeleteModulatedSTDPPreceptorResponse.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.DeleteModulatedSTDPPreceptorResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DeleteModulatedSTDPPreceptorResponse.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getStatus();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.DeleteModulatedSTDPPreceptorResponse} The clone.
 */
proto.api.DeleteModulatedSTDPPreceptorResponse.prototype.cloneMessage = function() {
  return /** @type {!proto.api.DeleteModulatedSTDPPreceptorResponse} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string status = 1;
 * @return {string}
 */
proto.api.DeleteModulatedSTDPPreceptorResponse.prototype.getStatus = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.DeleteModulatedSTDPPreceptorResponse.prototype.setStatus = function(value) {
  jspb.Message.setField(this, 1, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.AttachModulatedSTDPPreceptorRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.AttachModulatedSTDPPreceptorRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.AttachModulatedSTDPPreceptorRequest.displayName = 'proto.api.AttachModulatedSTDPPreceptorRequest';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.AttachModulatedSTDPPreceptorRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.AttachModulatedSTDPPreceptorRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.AttachModulatedSTDPPreceptorRequest} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.AttachModulatedSTDPPreceptorRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    modulatedstdppreceptorid: msg.getModulatedstdppreceptorid(),
    synapseclusterid: msg.getSynapseclusterid()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.AttachModulatedSTDPPreceptorRequest}
 */
proto.api.AttachModulatedSTDPPreceptorRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.AttachModulatedSTDPPreceptorRequest;
  return proto.api.AttachModulatedSTDPPreceptorRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.AttachModulatedSTDPPreceptorRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.AttachModulatedSTDPPreceptorRequest}
 */
proto.api.AttachModulatedSTDPPreceptorRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setModulatedstdppreceptorid(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setSynapseclusterid(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.AttachModulatedSTDPPreceptorRequest} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.AttachModulatedSTDPPreceptorRequest.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.AttachModulatedSTDPPreceptorRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.AttachModulatedSTDPPreceptorRequest.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getModulatedstdppreceptorid();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = this.getSynapseclusterid();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.AttachModulatedSTDPPreceptorRequest} The clone.
 */
proto.api.AttachModulatedSTDPPreceptorRequest.prototype.cloneMessage = function() {
  return /** @type {!proto.api.AttachModulatedSTDPPreceptorRequest} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string modulatedSTDPPreceptorId = 1;
 * @return {string}
 */
proto.api.AttachModulatedSTDPPreceptorRequest.prototype.getModulatedstdppreceptorid = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.AttachModulatedSTDPPreceptorRequest.prototype.setModulatedstdppreceptorid = function(value) {
  jspb.Message.setField(this, 1, value);
};


/**
 * optional string synapseClusterId = 2;
 * @return {string}
 */
proto.api.AttachModulatedSTDPPreceptorRequest.prototype.getSynapseclusterid = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 2, ""));
};


/** @param {string} value  */
proto.api.AttachModulatedSTDPPreceptorRequest.prototype.setSynapseclusterid = function(value) {
  jspb.Message.setField(this, 2, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.AttachModulatedSTDPPreceptorResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.AttachModulatedSTDPPreceptorResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.AttachModulatedSTDPPreceptorResponse.displayName = 'proto.api.AttachModulatedSTDPPreceptorResponse';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.AttachModulatedSTDPPreceptorResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.AttachModulatedSTDPPreceptorResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.AttachModulatedSTDPPreceptorResponse} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.AttachModulatedSTDPPreceptorResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    status: msg.getStatus()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.AttachModulatedSTDPPreceptorResponse}
 */
proto.api.AttachModulatedSTDPPreceptorResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.AttachModulatedSTDPPreceptorResponse;
  return proto.api.AttachModulatedSTDPPreceptorResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.AttachModulatedSTDPPreceptorResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.AttachModulatedSTDPPreceptorResponse}
 */
proto.api.AttachModulatedSTDPPreceptorResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setStatus(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.AttachModulatedSTDPPreceptorResponse} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.AttachModulatedSTDPPreceptorResponse.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.AttachModulatedSTDPPreceptorResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.AttachModulatedSTDPPreceptorResponse.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getStatus();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.AttachModulatedSTDPPreceptorResponse} The clone.
 */
proto.api.AttachModulatedSTDPPreceptorResponse.prototype.cloneMessage = function() {
  return /** @type {!proto.api.AttachModulatedSTDPPreceptorResponse} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string status = 1;
 * @return {string}
 */
proto.api.AttachModulatedSTDPPreceptorResponse.prototype.getStatus = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.AttachModulatedSTDPPreceptorResponse.prototype.setStatus = function(value) {
  jspb.Message.setField(this, 1, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.DetachModulatedSTDPPreceptorRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.DetachModulatedSTDPPreceptorRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.DetachModulatedSTDPPreceptorRequest.displayName = 'proto.api.DetachModulatedSTDPPreceptorRequest';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.DetachModulatedSTDPPreceptorRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.DetachModulatedSTDPPreceptorRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.DetachModulatedSTDPPreceptorRequest} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.DetachModulatedSTDPPreceptorRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    modulatedstdppreceptorid: msg.getModulatedstdppreceptorid()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.DetachModulatedSTDPPreceptorRequest}
 */
proto.api.DetachModulatedSTDPPreceptorRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.DetachModulatedSTDPPreceptorRequest;
  return proto.api.DetachModulatedSTDPPreceptorRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.DetachModulatedSTDPPreceptorRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.DetachModulatedSTDPPreceptorRequest}
 */
proto.api.DetachModulatedSTDPPreceptorRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setModulatedstdppreceptorid(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.DetachModulatedSTDPPreceptorRequest} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DetachModulatedSTDPPreceptorRequest.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.DetachModulatedSTDPPreceptorRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DetachModulatedSTDPPreceptorRequest.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getModulatedstdppreceptorid();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.DetachModulatedSTDPPreceptorRequest} The clone.
 */
proto.api.DetachModulatedSTDPPreceptorRequest.prototype.cloneMessage = function() {
  return /** @type {!proto.api.DetachModulatedSTDPPreceptorRequest} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string modulatedSTDPPreceptorId = 1;
 * @return {string}
 */
proto.api.DetachModulatedSTDPPreceptorRequest.prototype.getModulatedstdppreceptorid = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.DetachModulatedSTDPPreceptorRequest.prototype.setModulatedstdppreceptorid = function(value) {
  jspb.Message.setField(this, 1, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.DetachModulatedSTDPPreceptorResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.DetachModulatedSTDPPreceptorResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.DetachModulatedSTDPPreceptorResponse.displayName = 'proto.api.DetachModulatedSTDPPreceptorResponse';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.DetachModulatedSTDPPreceptorResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.DetachModulatedSTDPPreceptorResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.DetachModulatedSTDPPreceptorResponse} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.DetachModulatedSTDPPreceptorResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    status: msg.getStatus()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.DetachModulatedSTDPPreceptorResponse}
 */
proto.api.DetachModulatedSTDPPreceptorResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.DetachModulatedSTDPPreceptorResponse;
  return proto.api.DetachModulatedSTDPPreceptorResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.DetachModulatedSTDPPreceptorResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.DetachModulatedSTDPPreceptorResponse}
 */
proto.api.DetachModulatedSTDPPreceptorResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setStatus(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.DetachModulatedSTDPPreceptorResponse} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DetachModulatedSTDPPreceptorResponse.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.DetachModulatedSTDPPreceptorResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DetachModulatedSTDPPreceptorResponse.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getStatus();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.DetachModulatedSTDPPreceptorResponse} The clone.
 */
proto.api.DetachModulatedSTDPPreceptorResponse.prototype.cloneMessage = function() {
  return /** @type {!proto.api.DetachModulatedSTDPPreceptorResponse} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string status = 1;
 * @return {string}
 */
proto.api.DetachModulatedSTDPPreceptorResponse.prototype.getStatus = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.DetachModulatedSTDPPreceptorResponse.prototype.setStatus = function(value) {
  jspb.Message.setField(this, 1, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.SetModulatedSTDPPreceptorPositionRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.SetModulatedSTDPPreceptorPositionRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.SetModulatedSTDPPreceptorPositionRequest.displayName = 'proto.api.SetModulatedSTDPPreceptorPositionRequest';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.SetModulatedSTDPPreceptorPositionRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.SetModulatedSTDPPreceptorPositionRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.SetModulatedSTDPPreceptorPositionRequest} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.SetModulatedSTDPPreceptorPositionRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: msg.getId(),
    graph: (f = msg.getGraph()) && proto.api.Graph.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.SetModulatedSTDPPreceptorPositionRequest}
 */
proto.api.SetModulatedSTDPPreceptorPositionRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.SetModulatedSTDPPreceptorPositionRequest;
  return proto.api.SetModulatedSTDPPreceptorPositionRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.SetModulatedSTDPPreceptorPositionRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.SetModulatedSTDPPreceptorPositionRequest}
 */
proto.api.SetModulatedSTDPPreceptorPositionRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setId(value);
      break;
    case 2:
      var value = new proto.api.Graph;
      reader.readMessage(value,proto.api.Graph.deserializeBinaryFromReader);
      msg.setGraph(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.SetModulatedSTDPPreceptorPositionRequest} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.SetModulatedSTDPPreceptorPositionRequest.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.SetModulatedSTDPPreceptorPositionRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.SetModulatedSTDPPreceptorPositionRequest.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = this.getGraph();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      proto.api.Graph.serializeBinaryToWriter
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.SetModulatedSTDPPreceptorPositionRequest} The clone.
 */
proto.api.SetModulatedSTDPPreceptorPositionRequest.prototype.cloneMessage = function() {
  return /** @type {!proto.api.SetModulatedSTDPPreceptorPositionRequest} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string id = 1;
 * @return {string}
 */
proto.api.SetModulatedSTDPPreceptorPositionRequest.prototype.getId = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.SetModulatedSTDPPreceptorPositionRequest.prototype.setId = function(value) {
  jspb.Message.setField(this, 1, value);
};


/**
 * optional Graph graph = 2;
 * @return {proto.api.Graph}
 */
proto.api.SetModulatedSTDPPreceptorPositionRequest.prototype.getGraph = function() {
  return /** @type{proto.api.Graph} */ (
    jspb.Message.getWrapperField(this, proto.api.Graph, 2));
};


/** @param {proto.api.Graph|undefined} value  */
proto.api.SetModulatedSTDPPreceptorPositionRequest.prototype.setGraph = function(value) {
  jspb.Message.setWrapperField(this, 2, value);
};


proto.api.SetModulatedSTDPPreceptorPositionRequest.prototype.clearGraph = function() {
  this.setGraph(undefined);
};


/**
 * Returns whether this field is set.
 * @return{!boolean}
 */
proto.api.SetModulatedSTDPPreceptorPositionRequest.prototype.hasGraph = function() {
  return jspb.Message.getField(this, 2) != null;
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.SetModulatedSTDPPreceptorPositionResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.SetModulatedSTDPPreceptorPositionResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.SetModulatedSTDPPreceptorPositionResponse.displayName = 'proto.api.SetModulatedSTDPPreceptorPositionResponse';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.SetModulatedSTDPPreceptorPositionResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.SetModulatedSTDPPreceptorPositionResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.SetModulatedSTDPPreceptorPositionResponse} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.SetModulatedSTDPPreceptorPositionResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    status: msg.getStatus()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.SetModulatedSTDPPreceptorPositionResponse}
 */
proto.api.SetModulatedSTDPPreceptorPositionResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.SetModulatedSTDPPreceptorPositionResponse;
  return proto.api.SetModulatedSTDPPreceptorPositionResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.SetModulatedSTDPPreceptorPositionResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.SetModulatedSTDPPreceptorPositionResponse}
 */
proto.api.SetModulatedSTDPPreceptorPositionResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setStatus(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.SetModulatedSTDPPreceptorPositionResponse} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.SetModulatedSTDPPreceptorPositionResponse.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.SetModulatedSTDPPreceptorPositionResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.SetModulatedSTDPPreceptorPositionResponse.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getStatus();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.SetModulatedSTDPPreceptorPositionResponse} The clone.
 */
proto.api.SetModulatedSTDPPreceptorPositionResponse.prototype.cloneMessage = function() {
  return /** @type {!proto.api.SetModulatedSTDPPreceptorPositionResponse} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string status = 1;
 * @return {string}
 */
proto.api.SetModulatedSTDPPreceptorPositionResponse.prototype.getStatus = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.SetModulatedSTDPPreceptorPositionResponse.prototype.setStatus = function(value) {
  jspb.Message.setField(this, 1, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.InputAdapterRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.InputAdapterRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.InputAdapterRequest.displayName = 'proto.api.InputAdapterRequest';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.InputAdapterRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.InputAdapterRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.InputAdapterRequest} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.InputAdapterRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: msg.getId()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.InputAdapterRequest}
 */
proto.api.InputAdapterRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.InputAdapterRequest;
  return proto.api.InputAdapterRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.InputAdapterRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.InputAdapterRequest}
 */
proto.api.InputAdapterRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.InputAdapterRequest} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.InputAdapterRequest.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.InputAdapterRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.InputAdapterRequest.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.InputAdapterRequest} The clone.
 */
proto.api.InputAdapterRequest.prototype.cloneMessage = function() {
  return /** @type {!proto.api.InputAdapterRequest} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string id = 1;
 * @return {string}
 */
proto.api.InputAdapterRequest.prototype.getId = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.InputAdapterRequest.prototype.setId = function(value) {
  jspb.Message.setField(this, 1, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.InputAdapterResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.InputAdapterResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.InputAdapterResponse.displayName = 'proto.api.InputAdapterResponse';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.InputAdapterResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.InputAdapterResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.InputAdapterResponse} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.InputAdapterResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: msg.getId(),
    name: msg.getName(),
    size: msg.getSize(),
    encodingwindow: msg.getEncodingwindow(),
    graph: (f = msg.getGraph()) && proto.api.Graph.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.InputAdapterResponse}
 */
proto.api.InputAdapterResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.InputAdapterResponse;
  return proto.api.InputAdapterResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.InputAdapterResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.InputAdapterResponse}
 */
proto.api.InputAdapterResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setId(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setSize(value);
      break;
    case 4:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setEncodingwindow(value);
      break;
    case 5:
      var value = new proto.api.Graph;
      reader.readMessage(value,proto.api.Graph.deserializeBinaryFromReader);
      msg.setGraph(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.InputAdapterResponse} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.InputAdapterResponse.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.InputAdapterResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.InputAdapterResponse.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = this.getName();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = this.getSize();
  if (f !== 0) {
    writer.writeInt32(
      3,
      f
    );
  }
  f = this.getEncodingwindow();
  if (f !== 0) {
    writer.writeInt32(
      4,
      f
    );
  }
  f = this.getGraph();
  if (f != null) {
    writer.writeMessage(
      5,
      f,
      proto.api.Graph.serializeBinaryToWriter
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.InputAdapterResponse} The clone.
 */
proto.api.InputAdapterResponse.prototype.cloneMessage = function() {
  return /** @type {!proto.api.InputAdapterResponse} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string id = 1;
 * @return {string}
 */
proto.api.InputAdapterResponse.prototype.getId = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.InputAdapterResponse.prototype.setId = function(value) {
  jspb.Message.setField(this, 1, value);
};


/**
 * optional string name = 2;
 * @return {string}
 */
proto.api.InputAdapterResponse.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 2, ""));
};


/** @param {string} value  */
proto.api.InputAdapterResponse.prototype.setName = function(value) {
  jspb.Message.setField(this, 2, value);
};


/**
 * optional int32 size = 3;
 * @return {number}
 */
proto.api.InputAdapterResponse.prototype.getSize = function() {
  return /** @type {number} */ (jspb.Message.getFieldProto3(this, 3, 0));
};


/** @param {number} value  */
proto.api.InputAdapterResponse.prototype.setSize = function(value) {
  jspb.Message.setField(this, 3, value);
};


/**
 * optional int32 encodingWindow = 4;
 * @return {number}
 */
proto.api.InputAdapterResponse.prototype.getEncodingwindow = function() {
  return /** @type {number} */ (jspb.Message.getFieldProto3(this, 4, 0));
};


/** @param {number} value  */
proto.api.InputAdapterResponse.prototype.setEncodingwindow = function(value) {
  jspb.Message.setField(this, 4, value);
};


/**
 * optional Graph graph = 5;
 * @return {proto.api.Graph}
 */
proto.api.InputAdapterResponse.prototype.getGraph = function() {
  return /** @type{proto.api.Graph} */ (
    jspb.Message.getWrapperField(this, proto.api.Graph, 5));
};


/** @param {proto.api.Graph|undefined} value  */
proto.api.InputAdapterResponse.prototype.setGraph = function(value) {
  jspb.Message.setWrapperField(this, 5, value);
};


proto.api.InputAdapterResponse.prototype.clearGraph = function() {
  this.setGraph(undefined);
};


/**
 * Returns whether this field is set.
 * @return{!boolean}
 */
proto.api.InputAdapterResponse.prototype.hasGraph = function() {
  return jspb.Message.getField(this, 5) != null;
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.CreateInputAdapterRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.CreateInputAdapterRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.CreateInputAdapterRequest.displayName = 'proto.api.CreateInputAdapterRequest';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.CreateInputAdapterRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.CreateInputAdapterRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.CreateInputAdapterRequest} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.CreateInputAdapterRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    name: msg.getName(),
    size: msg.getSize(),
    encodingwindow: msg.getEncodingwindow(),
    graph: (f = msg.getGraph()) && proto.api.Graph.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.CreateInputAdapterRequest}
 */
proto.api.CreateInputAdapterRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.CreateInputAdapterRequest;
  return proto.api.CreateInputAdapterRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.CreateInputAdapterRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.CreateInputAdapterRequest}
 */
proto.api.CreateInputAdapterRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setSize(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setEncodingwindow(value);
      break;
    case 4:
      var value = new proto.api.Graph;
      reader.readMessage(value,proto.api.Graph.deserializeBinaryFromReader);
      msg.setGraph(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.CreateInputAdapterRequest} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.CreateInputAdapterRequest.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.CreateInputAdapterRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.CreateInputAdapterRequest.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = this.getSize();
  if (f !== 0) {
    writer.writeInt32(
      2,
      f
    );
  }
  f = this.getEncodingwindow();
  if (f !== 0) {
    writer.writeInt32(
      3,
      f
    );
  }
  f = this.getGraph();
  if (f != null) {
    writer.writeMessage(
      4,
      f,
      proto.api.Graph.serializeBinaryToWriter
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.CreateInputAdapterRequest} The clone.
 */
proto.api.CreateInputAdapterRequest.prototype.cloneMessage = function() {
  return /** @type {!proto.api.CreateInputAdapterRequest} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string name = 1;
 * @return {string}
 */
proto.api.CreateInputAdapterRequest.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.CreateInputAdapterRequest.prototype.setName = function(value) {
  jspb.Message.setField(this, 1, value);
};


/**
 * optional int32 size = 2;
 * @return {number}
 */
proto.api.CreateInputAdapterRequest.prototype.getSize = function() {
  return /** @type {number} */ (jspb.Message.getFieldProto3(this, 2, 0));
};


/** @param {number} value  */
proto.api.CreateInputAdapterRequest.prototype.setSize = function(value) {
  jspb.Message.setField(this, 2, value);
};


/**
 * optional int32 encodingWindow = 3;
 * @return {number}
 */
proto.api.CreateInputAdapterRequest.prototype.getEncodingwindow = function() {
  return /** @type {number} */ (jspb.Message.getFieldProto3(this, 3, 0));
};


/** @param {number} value  */
proto.api.CreateInputAdapterRequest.prototype.setEncodingwindow = function(value) {
  jspb.Message.setField(this, 3, value);
};


/**
 * optional Graph graph = 4;
 * @return {proto.api.Graph}
 */
proto.api.CreateInputAdapterRequest.prototype.getGraph = function() {
  return /** @type{proto.api.Graph} */ (
    jspb.Message.getWrapperField(this, proto.api.Graph, 4));
};


/** @param {proto.api.Graph|undefined} value  */
proto.api.CreateInputAdapterRequest.prototype.setGraph = function(value) {
  jspb.Message.setWrapperField(this, 4, value);
};


proto.api.CreateInputAdapterRequest.prototype.clearGraph = function() {
  this.setGraph(undefined);
};


/**
 * Returns whether this field is set.
 * @return{!boolean}
 */
proto.api.CreateInputAdapterRequest.prototype.hasGraph = function() {
  return jspb.Message.getField(this, 4) != null;
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.DeleteInputAdapterRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.DeleteInputAdapterRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.DeleteInputAdapterRequest.displayName = 'proto.api.DeleteInputAdapterRequest';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.DeleteInputAdapterRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.DeleteInputAdapterRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.DeleteInputAdapterRequest} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.DeleteInputAdapterRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: msg.getId()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.DeleteInputAdapterRequest}
 */
proto.api.DeleteInputAdapterRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.DeleteInputAdapterRequest;
  return proto.api.DeleteInputAdapterRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.DeleteInputAdapterRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.DeleteInputAdapterRequest}
 */
proto.api.DeleteInputAdapterRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.DeleteInputAdapterRequest} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DeleteInputAdapterRequest.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.DeleteInputAdapterRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DeleteInputAdapterRequest.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.DeleteInputAdapterRequest} The clone.
 */
proto.api.DeleteInputAdapterRequest.prototype.cloneMessage = function() {
  return /** @type {!proto.api.DeleteInputAdapterRequest} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string id = 1;
 * @return {string}
 */
proto.api.DeleteInputAdapterRequest.prototype.getId = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.DeleteInputAdapterRequest.prototype.setId = function(value) {
  jspb.Message.setField(this, 1, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.DeleteInputAdapterResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.DeleteInputAdapterResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.DeleteInputAdapterResponse.displayName = 'proto.api.DeleteInputAdapterResponse';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.DeleteInputAdapterResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.DeleteInputAdapterResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.DeleteInputAdapterResponse} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.DeleteInputAdapterResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    status: msg.getStatus()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.DeleteInputAdapterResponse}
 */
proto.api.DeleteInputAdapterResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.DeleteInputAdapterResponse;
  return proto.api.DeleteInputAdapterResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.DeleteInputAdapterResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.DeleteInputAdapterResponse}
 */
proto.api.DeleteInputAdapterResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setStatus(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.DeleteInputAdapterResponse} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DeleteInputAdapterResponse.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.DeleteInputAdapterResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DeleteInputAdapterResponse.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getStatus();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.DeleteInputAdapterResponse} The clone.
 */
proto.api.DeleteInputAdapterResponse.prototype.cloneMessage = function() {
  return /** @type {!proto.api.DeleteInputAdapterResponse} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string status = 1;
 * @return {string}
 */
proto.api.DeleteInputAdapterResponse.prototype.getStatus = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.DeleteInputAdapterResponse.prototype.setStatus = function(value) {
  jspb.Message.setField(this, 1, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.AttachInputAdapterRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.AttachInputAdapterRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.AttachInputAdapterRequest.displayName = 'proto.api.AttachInputAdapterRequest';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.AttachInputAdapterRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.AttachInputAdapterRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.AttachInputAdapterRequest} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.AttachInputAdapterRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    inputadapterid: msg.getInputadapterid(),
    neuronclusterid: msg.getNeuronclusterid()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.AttachInputAdapterRequest}
 */
proto.api.AttachInputAdapterRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.AttachInputAdapterRequest;
  return proto.api.AttachInputAdapterRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.AttachInputAdapterRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.AttachInputAdapterRequest}
 */
proto.api.AttachInputAdapterRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setInputadapterid(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setNeuronclusterid(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.AttachInputAdapterRequest} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.AttachInputAdapterRequest.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.AttachInputAdapterRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.AttachInputAdapterRequest.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getInputadapterid();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = this.getNeuronclusterid();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.AttachInputAdapterRequest} The clone.
 */
proto.api.AttachInputAdapterRequest.prototype.cloneMessage = function() {
  return /** @type {!proto.api.AttachInputAdapterRequest} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string inputAdapterId = 1;
 * @return {string}
 */
proto.api.AttachInputAdapterRequest.prototype.getInputadapterid = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.AttachInputAdapterRequest.prototype.setInputadapterid = function(value) {
  jspb.Message.setField(this, 1, value);
};


/**
 * optional string neuronClusterId = 2;
 * @return {string}
 */
proto.api.AttachInputAdapterRequest.prototype.getNeuronclusterid = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 2, ""));
};


/** @param {string} value  */
proto.api.AttachInputAdapterRequest.prototype.setNeuronclusterid = function(value) {
  jspb.Message.setField(this, 2, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.AttachInputAdapterResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.AttachInputAdapterResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.AttachInputAdapterResponse.displayName = 'proto.api.AttachInputAdapterResponse';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.AttachInputAdapterResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.AttachInputAdapterResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.AttachInputAdapterResponse} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.AttachInputAdapterResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    status: msg.getStatus()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.AttachInputAdapterResponse}
 */
proto.api.AttachInputAdapterResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.AttachInputAdapterResponse;
  return proto.api.AttachInputAdapterResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.AttachInputAdapterResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.AttachInputAdapterResponse}
 */
proto.api.AttachInputAdapterResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setStatus(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.AttachInputAdapterResponse} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.AttachInputAdapterResponse.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.AttachInputAdapterResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.AttachInputAdapterResponse.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getStatus();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.AttachInputAdapterResponse} The clone.
 */
proto.api.AttachInputAdapterResponse.prototype.cloneMessage = function() {
  return /** @type {!proto.api.AttachInputAdapterResponse} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string status = 1;
 * @return {string}
 */
proto.api.AttachInputAdapterResponse.prototype.getStatus = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.AttachInputAdapterResponse.prototype.setStatus = function(value) {
  jspb.Message.setField(this, 1, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.DetachInputAdapterRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.DetachInputAdapterRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.DetachInputAdapterRequest.displayName = 'proto.api.DetachInputAdapterRequest';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.DetachInputAdapterRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.DetachInputAdapterRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.DetachInputAdapterRequest} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.DetachInputAdapterRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    inputadapterid: msg.getInputadapterid(),
    neuronclusterid: msg.getNeuronclusterid()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.DetachInputAdapterRequest}
 */
proto.api.DetachInputAdapterRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.DetachInputAdapterRequest;
  return proto.api.DetachInputAdapterRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.DetachInputAdapterRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.DetachInputAdapterRequest}
 */
proto.api.DetachInputAdapterRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setInputadapterid(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setNeuronclusterid(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.DetachInputAdapterRequest} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DetachInputAdapterRequest.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.DetachInputAdapterRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DetachInputAdapterRequest.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getInputadapterid();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = this.getNeuronclusterid();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.DetachInputAdapterRequest} The clone.
 */
proto.api.DetachInputAdapterRequest.prototype.cloneMessage = function() {
  return /** @type {!proto.api.DetachInputAdapterRequest} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string inputAdapterId = 1;
 * @return {string}
 */
proto.api.DetachInputAdapterRequest.prototype.getInputadapterid = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.DetachInputAdapterRequest.prototype.setInputadapterid = function(value) {
  jspb.Message.setField(this, 1, value);
};


/**
 * optional string neuronClusterId = 2;
 * @return {string}
 */
proto.api.DetachInputAdapterRequest.prototype.getNeuronclusterid = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 2, ""));
};


/** @param {string} value  */
proto.api.DetachInputAdapterRequest.prototype.setNeuronclusterid = function(value) {
  jspb.Message.setField(this, 2, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.DetachInputAdapterResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.DetachInputAdapterResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.DetachInputAdapterResponse.displayName = 'proto.api.DetachInputAdapterResponse';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.DetachInputAdapterResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.DetachInputAdapterResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.DetachInputAdapterResponse} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.DetachInputAdapterResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    status: msg.getStatus()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.DetachInputAdapterResponse}
 */
proto.api.DetachInputAdapterResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.DetachInputAdapterResponse;
  return proto.api.DetachInputAdapterResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.DetachInputAdapterResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.DetachInputAdapterResponse}
 */
proto.api.DetachInputAdapterResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setStatus(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.DetachInputAdapterResponse} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DetachInputAdapterResponse.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.DetachInputAdapterResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DetachInputAdapterResponse.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getStatus();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.DetachInputAdapterResponse} The clone.
 */
proto.api.DetachInputAdapterResponse.prototype.cloneMessage = function() {
  return /** @type {!proto.api.DetachInputAdapterResponse} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string status = 1;
 * @return {string}
 */
proto.api.DetachInputAdapterResponse.prototype.getStatus = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.DetachInputAdapterResponse.prototype.setStatus = function(value) {
  jspb.Message.setField(this, 1, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.SetInputAdapterPositionRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.SetInputAdapterPositionRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.SetInputAdapterPositionRequest.displayName = 'proto.api.SetInputAdapterPositionRequest';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.SetInputAdapterPositionRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.SetInputAdapterPositionRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.SetInputAdapterPositionRequest} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.SetInputAdapterPositionRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: msg.getId(),
    graph: (f = msg.getGraph()) && proto.api.Graph.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.SetInputAdapterPositionRequest}
 */
proto.api.SetInputAdapterPositionRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.SetInputAdapterPositionRequest;
  return proto.api.SetInputAdapterPositionRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.SetInputAdapterPositionRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.SetInputAdapterPositionRequest}
 */
proto.api.SetInputAdapterPositionRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setId(value);
      break;
    case 2:
      var value = new proto.api.Graph;
      reader.readMessage(value,proto.api.Graph.deserializeBinaryFromReader);
      msg.setGraph(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.SetInputAdapterPositionRequest} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.SetInputAdapterPositionRequest.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.SetInputAdapterPositionRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.SetInputAdapterPositionRequest.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = this.getGraph();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      proto.api.Graph.serializeBinaryToWriter
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.SetInputAdapterPositionRequest} The clone.
 */
proto.api.SetInputAdapterPositionRequest.prototype.cloneMessage = function() {
  return /** @type {!proto.api.SetInputAdapterPositionRequest} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string id = 1;
 * @return {string}
 */
proto.api.SetInputAdapterPositionRequest.prototype.getId = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.SetInputAdapterPositionRequest.prototype.setId = function(value) {
  jspb.Message.setField(this, 1, value);
};


/**
 * optional Graph graph = 2;
 * @return {proto.api.Graph}
 */
proto.api.SetInputAdapterPositionRequest.prototype.getGraph = function() {
  return /** @type{proto.api.Graph} */ (
    jspb.Message.getWrapperField(this, proto.api.Graph, 2));
};


/** @param {proto.api.Graph|undefined} value  */
proto.api.SetInputAdapterPositionRequest.prototype.setGraph = function(value) {
  jspb.Message.setWrapperField(this, 2, value);
};


proto.api.SetInputAdapterPositionRequest.prototype.clearGraph = function() {
  this.setGraph(undefined);
};


/**
 * Returns whether this field is set.
 * @return{!boolean}
 */
proto.api.SetInputAdapterPositionRequest.prototype.hasGraph = function() {
  return jspb.Message.getField(this, 2) != null;
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.SetInputAdapterPositionResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.SetInputAdapterPositionResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.SetInputAdapterPositionResponse.displayName = 'proto.api.SetInputAdapterPositionResponse';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.SetInputAdapterPositionResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.SetInputAdapterPositionResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.SetInputAdapterPositionResponse} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.SetInputAdapterPositionResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    status: msg.getStatus()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.SetInputAdapterPositionResponse}
 */
proto.api.SetInputAdapterPositionResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.SetInputAdapterPositionResponse;
  return proto.api.SetInputAdapterPositionResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.SetInputAdapterPositionResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.SetInputAdapterPositionResponse}
 */
proto.api.SetInputAdapterPositionResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setStatus(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.SetInputAdapterPositionResponse} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.SetInputAdapterPositionResponse.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.SetInputAdapterPositionResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.SetInputAdapterPositionResponse.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getStatus();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.SetInputAdapterPositionResponse} The clone.
 */
proto.api.SetInputAdapterPositionResponse.prototype.cloneMessage = function() {
  return /** @type {!proto.api.SetInputAdapterPositionResponse} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string status = 1;
 * @return {string}
 */
proto.api.SetInputAdapterPositionResponse.prototype.getStatus = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.SetInputAdapterPositionResponse.prototype.setStatus = function(value) {
  jspb.Message.setField(this, 1, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.OutputAdapterRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.OutputAdapterRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.OutputAdapterRequest.displayName = 'proto.api.OutputAdapterRequest';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.OutputAdapterRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.OutputAdapterRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.OutputAdapterRequest} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.OutputAdapterRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: msg.getId()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.OutputAdapterRequest}
 */
proto.api.OutputAdapterRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.OutputAdapterRequest;
  return proto.api.OutputAdapterRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.OutputAdapterRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.OutputAdapterRequest}
 */
proto.api.OutputAdapterRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.OutputAdapterRequest} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.OutputAdapterRequest.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.OutputAdapterRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.OutputAdapterRequest.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.OutputAdapterRequest} The clone.
 */
proto.api.OutputAdapterRequest.prototype.cloneMessage = function() {
  return /** @type {!proto.api.OutputAdapterRequest} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string id = 1;
 * @return {string}
 */
proto.api.OutputAdapterRequest.prototype.getId = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.OutputAdapterRequest.prototype.setId = function(value) {
  jspb.Message.setField(this, 1, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.OutputAdapterResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.OutputAdapterResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.OutputAdapterResponse.displayName = 'proto.api.OutputAdapterResponse';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.OutputAdapterResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.OutputAdapterResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.OutputAdapterResponse} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.OutputAdapterResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: msg.getId(),
    name: msg.getName(),
    size: msg.getSize(),
    decodingwindow: msg.getDecodingwindow(),
    neuronclusterid: msg.getNeuronclusterid(),
    graph: (f = msg.getGraph()) && proto.api.Graph.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.OutputAdapterResponse}
 */
proto.api.OutputAdapterResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.OutputAdapterResponse;
  return proto.api.OutputAdapterResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.OutputAdapterResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.OutputAdapterResponse}
 */
proto.api.OutputAdapterResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setId(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setSize(value);
      break;
    case 4:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setDecodingwindow(value);
      break;
    case 5:
      var value = /** @type {string} */ (reader.readString());
      msg.setNeuronclusterid(value);
      break;
    case 6:
      var value = new proto.api.Graph;
      reader.readMessage(value,proto.api.Graph.deserializeBinaryFromReader);
      msg.setGraph(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.OutputAdapterResponse} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.OutputAdapterResponse.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.OutputAdapterResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.OutputAdapterResponse.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = this.getName();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = this.getSize();
  if (f !== 0) {
    writer.writeInt32(
      3,
      f
    );
  }
  f = this.getDecodingwindow();
  if (f !== 0) {
    writer.writeInt32(
      4,
      f
    );
  }
  f = this.getNeuronclusterid();
  if (f.length > 0) {
    writer.writeString(
      5,
      f
    );
  }
  f = this.getGraph();
  if (f != null) {
    writer.writeMessage(
      6,
      f,
      proto.api.Graph.serializeBinaryToWriter
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.OutputAdapterResponse} The clone.
 */
proto.api.OutputAdapterResponse.prototype.cloneMessage = function() {
  return /** @type {!proto.api.OutputAdapterResponse} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string id = 1;
 * @return {string}
 */
proto.api.OutputAdapterResponse.prototype.getId = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.OutputAdapterResponse.prototype.setId = function(value) {
  jspb.Message.setField(this, 1, value);
};


/**
 * optional string name = 2;
 * @return {string}
 */
proto.api.OutputAdapterResponse.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 2, ""));
};


/** @param {string} value  */
proto.api.OutputAdapterResponse.prototype.setName = function(value) {
  jspb.Message.setField(this, 2, value);
};


/**
 * optional int32 size = 3;
 * @return {number}
 */
proto.api.OutputAdapterResponse.prototype.getSize = function() {
  return /** @type {number} */ (jspb.Message.getFieldProto3(this, 3, 0));
};


/** @param {number} value  */
proto.api.OutputAdapterResponse.prototype.setSize = function(value) {
  jspb.Message.setField(this, 3, value);
};


/**
 * optional int32 decodingWindow = 4;
 * @return {number}
 */
proto.api.OutputAdapterResponse.prototype.getDecodingwindow = function() {
  return /** @type {number} */ (jspb.Message.getFieldProto3(this, 4, 0));
};


/** @param {number} value  */
proto.api.OutputAdapterResponse.prototype.setDecodingwindow = function(value) {
  jspb.Message.setField(this, 4, value);
};


/**
 * optional string neuronClusterId = 5;
 * @return {string}
 */
proto.api.OutputAdapterResponse.prototype.getNeuronclusterid = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 5, ""));
};


/** @param {string} value  */
proto.api.OutputAdapterResponse.prototype.setNeuronclusterid = function(value) {
  jspb.Message.setField(this, 5, value);
};


/**
 * optional Graph graph = 6;
 * @return {proto.api.Graph}
 */
proto.api.OutputAdapterResponse.prototype.getGraph = function() {
  return /** @type{proto.api.Graph} */ (
    jspb.Message.getWrapperField(this, proto.api.Graph, 6));
};


/** @param {proto.api.Graph|undefined} value  */
proto.api.OutputAdapterResponse.prototype.setGraph = function(value) {
  jspb.Message.setWrapperField(this, 6, value);
};


proto.api.OutputAdapterResponse.prototype.clearGraph = function() {
  this.setGraph(undefined);
};


/**
 * Returns whether this field is set.
 * @return{!boolean}
 */
proto.api.OutputAdapterResponse.prototype.hasGraph = function() {
  return jspb.Message.getField(this, 6) != null;
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.CreateOutputAdapterRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.CreateOutputAdapterRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.CreateOutputAdapterRequest.displayName = 'proto.api.CreateOutputAdapterRequest';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.CreateOutputAdapterRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.CreateOutputAdapterRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.CreateOutputAdapterRequest} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.CreateOutputAdapterRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    name: msg.getName(),
    size: msg.getSize(),
    decodingwindow: msg.getDecodingwindow(),
    graph: (f = msg.getGraph()) && proto.api.Graph.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.CreateOutputAdapterRequest}
 */
proto.api.CreateOutputAdapterRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.CreateOutputAdapterRequest;
  return proto.api.CreateOutputAdapterRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.CreateOutputAdapterRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.CreateOutputAdapterRequest}
 */
proto.api.CreateOutputAdapterRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setSize(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setDecodingwindow(value);
      break;
    case 4:
      var value = new proto.api.Graph;
      reader.readMessage(value,proto.api.Graph.deserializeBinaryFromReader);
      msg.setGraph(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.CreateOutputAdapterRequest} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.CreateOutputAdapterRequest.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.CreateOutputAdapterRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.CreateOutputAdapterRequest.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = this.getSize();
  if (f !== 0) {
    writer.writeInt32(
      2,
      f
    );
  }
  f = this.getDecodingwindow();
  if (f !== 0) {
    writer.writeInt32(
      3,
      f
    );
  }
  f = this.getGraph();
  if (f != null) {
    writer.writeMessage(
      4,
      f,
      proto.api.Graph.serializeBinaryToWriter
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.CreateOutputAdapterRequest} The clone.
 */
proto.api.CreateOutputAdapterRequest.prototype.cloneMessage = function() {
  return /** @type {!proto.api.CreateOutputAdapterRequest} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string name = 1;
 * @return {string}
 */
proto.api.CreateOutputAdapterRequest.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.CreateOutputAdapterRequest.prototype.setName = function(value) {
  jspb.Message.setField(this, 1, value);
};


/**
 * optional int32 size = 2;
 * @return {number}
 */
proto.api.CreateOutputAdapterRequest.prototype.getSize = function() {
  return /** @type {number} */ (jspb.Message.getFieldProto3(this, 2, 0));
};


/** @param {number} value  */
proto.api.CreateOutputAdapterRequest.prototype.setSize = function(value) {
  jspb.Message.setField(this, 2, value);
};


/**
 * optional int32 decodingWindow = 3;
 * @return {number}
 */
proto.api.CreateOutputAdapterRequest.prototype.getDecodingwindow = function() {
  return /** @type {number} */ (jspb.Message.getFieldProto3(this, 3, 0));
};


/** @param {number} value  */
proto.api.CreateOutputAdapterRequest.prototype.setDecodingwindow = function(value) {
  jspb.Message.setField(this, 3, value);
};


/**
 * optional Graph graph = 4;
 * @return {proto.api.Graph}
 */
proto.api.CreateOutputAdapterRequest.prototype.getGraph = function() {
  return /** @type{proto.api.Graph} */ (
    jspb.Message.getWrapperField(this, proto.api.Graph, 4));
};


/** @param {proto.api.Graph|undefined} value  */
proto.api.CreateOutputAdapterRequest.prototype.setGraph = function(value) {
  jspb.Message.setWrapperField(this, 4, value);
};


proto.api.CreateOutputAdapterRequest.prototype.clearGraph = function() {
  this.setGraph(undefined);
};


/**
 * Returns whether this field is set.
 * @return{!boolean}
 */
proto.api.CreateOutputAdapterRequest.prototype.hasGraph = function() {
  return jspb.Message.getField(this, 4) != null;
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.DeleteOutputAdapterRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.DeleteOutputAdapterRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.DeleteOutputAdapterRequest.displayName = 'proto.api.DeleteOutputAdapterRequest';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.DeleteOutputAdapterRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.DeleteOutputAdapterRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.DeleteOutputAdapterRequest} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.DeleteOutputAdapterRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: msg.getId()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.DeleteOutputAdapterRequest}
 */
proto.api.DeleteOutputAdapterRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.DeleteOutputAdapterRequest;
  return proto.api.DeleteOutputAdapterRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.DeleteOutputAdapterRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.DeleteOutputAdapterRequest}
 */
proto.api.DeleteOutputAdapterRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.DeleteOutputAdapterRequest} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DeleteOutputAdapterRequest.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.DeleteOutputAdapterRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DeleteOutputAdapterRequest.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.DeleteOutputAdapterRequest} The clone.
 */
proto.api.DeleteOutputAdapterRequest.prototype.cloneMessage = function() {
  return /** @type {!proto.api.DeleteOutputAdapterRequest} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string id = 1;
 * @return {string}
 */
proto.api.DeleteOutputAdapterRequest.prototype.getId = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.DeleteOutputAdapterRequest.prototype.setId = function(value) {
  jspb.Message.setField(this, 1, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.DeleteOutputAdapterResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.DeleteOutputAdapterResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.DeleteOutputAdapterResponse.displayName = 'proto.api.DeleteOutputAdapterResponse';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.DeleteOutputAdapterResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.DeleteOutputAdapterResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.DeleteOutputAdapterResponse} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.DeleteOutputAdapterResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    status: msg.getStatus()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.DeleteOutputAdapterResponse}
 */
proto.api.DeleteOutputAdapterResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.DeleteOutputAdapterResponse;
  return proto.api.DeleteOutputAdapterResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.DeleteOutputAdapterResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.DeleteOutputAdapterResponse}
 */
proto.api.DeleteOutputAdapterResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setStatus(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.DeleteOutputAdapterResponse} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DeleteOutputAdapterResponse.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.DeleteOutputAdapterResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DeleteOutputAdapterResponse.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getStatus();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.DeleteOutputAdapterResponse} The clone.
 */
proto.api.DeleteOutputAdapterResponse.prototype.cloneMessage = function() {
  return /** @type {!proto.api.DeleteOutputAdapterResponse} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string status = 1;
 * @return {string}
 */
proto.api.DeleteOutputAdapterResponse.prototype.getStatus = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.DeleteOutputAdapterResponse.prototype.setStatus = function(value) {
  jspb.Message.setField(this, 1, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.AttachOutputAdapterRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.AttachOutputAdapterRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.AttachOutputAdapterRequest.displayName = 'proto.api.AttachOutputAdapterRequest';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.AttachOutputAdapterRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.AttachOutputAdapterRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.AttachOutputAdapterRequest} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.AttachOutputAdapterRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    outputadapterid: msg.getOutputadapterid(),
    neuronclusterid: msg.getNeuronclusterid()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.AttachOutputAdapterRequest}
 */
proto.api.AttachOutputAdapterRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.AttachOutputAdapterRequest;
  return proto.api.AttachOutputAdapterRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.AttachOutputAdapterRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.AttachOutputAdapterRequest}
 */
proto.api.AttachOutputAdapterRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setOutputadapterid(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setNeuronclusterid(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.AttachOutputAdapterRequest} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.AttachOutputAdapterRequest.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.AttachOutputAdapterRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.AttachOutputAdapterRequest.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getOutputadapterid();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = this.getNeuronclusterid();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.AttachOutputAdapterRequest} The clone.
 */
proto.api.AttachOutputAdapterRequest.prototype.cloneMessage = function() {
  return /** @type {!proto.api.AttachOutputAdapterRequest} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string outputAdapterId = 1;
 * @return {string}
 */
proto.api.AttachOutputAdapterRequest.prototype.getOutputadapterid = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.AttachOutputAdapterRequest.prototype.setOutputadapterid = function(value) {
  jspb.Message.setField(this, 1, value);
};


/**
 * optional string neuronClusterId = 2;
 * @return {string}
 */
proto.api.AttachOutputAdapterRequest.prototype.getNeuronclusterid = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 2, ""));
};


/** @param {string} value  */
proto.api.AttachOutputAdapterRequest.prototype.setNeuronclusterid = function(value) {
  jspb.Message.setField(this, 2, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.AttachOutputAdapterResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.AttachOutputAdapterResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.AttachOutputAdapterResponse.displayName = 'proto.api.AttachOutputAdapterResponse';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.AttachOutputAdapterResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.AttachOutputAdapterResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.AttachOutputAdapterResponse} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.AttachOutputAdapterResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    status: msg.getStatus()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.AttachOutputAdapterResponse}
 */
proto.api.AttachOutputAdapterResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.AttachOutputAdapterResponse;
  return proto.api.AttachOutputAdapterResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.AttachOutputAdapterResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.AttachOutputAdapterResponse}
 */
proto.api.AttachOutputAdapterResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setStatus(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.AttachOutputAdapterResponse} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.AttachOutputAdapterResponse.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.AttachOutputAdapterResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.AttachOutputAdapterResponse.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getStatus();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.AttachOutputAdapterResponse} The clone.
 */
proto.api.AttachOutputAdapterResponse.prototype.cloneMessage = function() {
  return /** @type {!proto.api.AttachOutputAdapterResponse} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string status = 1;
 * @return {string}
 */
proto.api.AttachOutputAdapterResponse.prototype.getStatus = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.AttachOutputAdapterResponse.prototype.setStatus = function(value) {
  jspb.Message.setField(this, 1, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.DetachOutputAdapterRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.DetachOutputAdapterRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.DetachOutputAdapterRequest.displayName = 'proto.api.DetachOutputAdapterRequest';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.DetachOutputAdapterRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.DetachOutputAdapterRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.DetachOutputAdapterRequest} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.DetachOutputAdapterRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    outputadapterid: msg.getOutputadapterid()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.DetachOutputAdapterRequest}
 */
proto.api.DetachOutputAdapterRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.DetachOutputAdapterRequest;
  return proto.api.DetachOutputAdapterRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.DetachOutputAdapterRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.DetachOutputAdapterRequest}
 */
proto.api.DetachOutputAdapterRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setOutputadapterid(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.DetachOutputAdapterRequest} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DetachOutputAdapterRequest.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.DetachOutputAdapterRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DetachOutputAdapterRequest.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getOutputadapterid();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.DetachOutputAdapterRequest} The clone.
 */
proto.api.DetachOutputAdapterRequest.prototype.cloneMessage = function() {
  return /** @type {!proto.api.DetachOutputAdapterRequest} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string outputAdapterId = 1;
 * @return {string}
 */
proto.api.DetachOutputAdapterRequest.prototype.getOutputadapterid = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.DetachOutputAdapterRequest.prototype.setOutputadapterid = function(value) {
  jspb.Message.setField(this, 1, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.DetachOutputAdapterResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.DetachOutputAdapterResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.DetachOutputAdapterResponse.displayName = 'proto.api.DetachOutputAdapterResponse';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.DetachOutputAdapterResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.DetachOutputAdapterResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.DetachOutputAdapterResponse} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.DetachOutputAdapterResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    status: msg.getStatus()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.DetachOutputAdapterResponse}
 */
proto.api.DetachOutputAdapterResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.DetachOutputAdapterResponse;
  return proto.api.DetachOutputAdapterResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.DetachOutputAdapterResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.DetachOutputAdapterResponse}
 */
proto.api.DetachOutputAdapterResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setStatus(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.DetachOutputAdapterResponse} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DetachOutputAdapterResponse.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.DetachOutputAdapterResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DetachOutputAdapterResponse.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getStatus();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.DetachOutputAdapterResponse} The clone.
 */
proto.api.DetachOutputAdapterResponse.prototype.cloneMessage = function() {
  return /** @type {!proto.api.DetachOutputAdapterResponse} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string status = 1;
 * @return {string}
 */
proto.api.DetachOutputAdapterResponse.prototype.getStatus = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.DetachOutputAdapterResponse.prototype.setStatus = function(value) {
  jspb.Message.setField(this, 1, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.SetOutputAdapterPositionRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.SetOutputAdapterPositionRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.SetOutputAdapterPositionRequest.displayName = 'proto.api.SetOutputAdapterPositionRequest';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.SetOutputAdapterPositionRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.SetOutputAdapterPositionRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.SetOutputAdapterPositionRequest} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.SetOutputAdapterPositionRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: msg.getId(),
    graph: (f = msg.getGraph()) && proto.api.Graph.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.SetOutputAdapterPositionRequest}
 */
proto.api.SetOutputAdapterPositionRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.SetOutputAdapterPositionRequest;
  return proto.api.SetOutputAdapterPositionRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.SetOutputAdapterPositionRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.SetOutputAdapterPositionRequest}
 */
proto.api.SetOutputAdapterPositionRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setId(value);
      break;
    case 2:
      var value = new proto.api.Graph;
      reader.readMessage(value,proto.api.Graph.deserializeBinaryFromReader);
      msg.setGraph(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.SetOutputAdapterPositionRequest} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.SetOutputAdapterPositionRequest.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.SetOutputAdapterPositionRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.SetOutputAdapterPositionRequest.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = this.getGraph();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      proto.api.Graph.serializeBinaryToWriter
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.SetOutputAdapterPositionRequest} The clone.
 */
proto.api.SetOutputAdapterPositionRequest.prototype.cloneMessage = function() {
  return /** @type {!proto.api.SetOutputAdapterPositionRequest} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string id = 1;
 * @return {string}
 */
proto.api.SetOutputAdapterPositionRequest.prototype.getId = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.SetOutputAdapterPositionRequest.prototype.setId = function(value) {
  jspb.Message.setField(this, 1, value);
};


/**
 * optional Graph graph = 2;
 * @return {proto.api.Graph}
 */
proto.api.SetOutputAdapterPositionRequest.prototype.getGraph = function() {
  return /** @type{proto.api.Graph} */ (
    jspb.Message.getWrapperField(this, proto.api.Graph, 2));
};


/** @param {proto.api.Graph|undefined} value  */
proto.api.SetOutputAdapterPositionRequest.prototype.setGraph = function(value) {
  jspb.Message.setWrapperField(this, 2, value);
};


proto.api.SetOutputAdapterPositionRequest.prototype.clearGraph = function() {
  this.setGraph(undefined);
};


/**
 * Returns whether this field is set.
 * @return{!boolean}
 */
proto.api.SetOutputAdapterPositionRequest.prototype.hasGraph = function() {
  return jspb.Message.getField(this, 2) != null;
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.SetOutputAdapterPositionResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.SetOutputAdapterPositionResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.SetOutputAdapterPositionResponse.displayName = 'proto.api.SetOutputAdapterPositionResponse';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.SetOutputAdapterPositionResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.SetOutputAdapterPositionResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.SetOutputAdapterPositionResponse} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.SetOutputAdapterPositionResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    status: msg.getStatus()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.SetOutputAdapterPositionResponse}
 */
proto.api.SetOutputAdapterPositionResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.SetOutputAdapterPositionResponse;
  return proto.api.SetOutputAdapterPositionResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.SetOutputAdapterPositionResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.SetOutputAdapterPositionResponse}
 */
proto.api.SetOutputAdapterPositionResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setStatus(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.SetOutputAdapterPositionResponse} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.SetOutputAdapterPositionResponse.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.SetOutputAdapterPositionResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.SetOutputAdapterPositionResponse.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getStatus();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.SetOutputAdapterPositionResponse} The clone.
 */
proto.api.SetOutputAdapterPositionResponse.prototype.cloneMessage = function() {
  return /** @type {!proto.api.SetOutputAdapterPositionResponse} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string status = 1;
 * @return {string}
 */
proto.api.SetOutputAdapterPositionResponse.prototype.getStatus = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.SetOutputAdapterPositionResponse.prototype.setStatus = function(value) {
  jspb.Message.setField(this, 1, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.DopamineAdapterRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.DopamineAdapterRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.DopamineAdapterRequest.displayName = 'proto.api.DopamineAdapterRequest';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.DopamineAdapterRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.DopamineAdapterRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.DopamineAdapterRequest} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.DopamineAdapterRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: msg.getId()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.DopamineAdapterRequest}
 */
proto.api.DopamineAdapterRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.DopamineAdapterRequest;
  return proto.api.DopamineAdapterRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.DopamineAdapterRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.DopamineAdapterRequest}
 */
proto.api.DopamineAdapterRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.DopamineAdapterRequest} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DopamineAdapterRequest.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.DopamineAdapterRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DopamineAdapterRequest.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.DopamineAdapterRequest} The clone.
 */
proto.api.DopamineAdapterRequest.prototype.cloneMessage = function() {
  return /** @type {!proto.api.DopamineAdapterRequest} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string id = 1;
 * @return {string}
 */
proto.api.DopamineAdapterRequest.prototype.getId = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.DopamineAdapterRequest.prototype.setId = function(value) {
  jspb.Message.setField(this, 1, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.DopamineAdapterResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.DopamineAdapterResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.DopamineAdapterResponse.displayName = 'proto.api.DopamineAdapterResponse';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.DopamineAdapterResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.DopamineAdapterResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.DopamineAdapterResponse} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.DopamineAdapterResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: msg.getId(),
    name: msg.getName(),
    graph: (f = msg.getGraph()) && proto.api.Graph.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.DopamineAdapterResponse}
 */
proto.api.DopamineAdapterResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.DopamineAdapterResponse;
  return proto.api.DopamineAdapterResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.DopamineAdapterResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.DopamineAdapterResponse}
 */
proto.api.DopamineAdapterResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setId(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    case 3:
      var value = new proto.api.Graph;
      reader.readMessage(value,proto.api.Graph.deserializeBinaryFromReader);
      msg.setGraph(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.DopamineAdapterResponse} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DopamineAdapterResponse.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.DopamineAdapterResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DopamineAdapterResponse.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = this.getName();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = this.getGraph();
  if (f != null) {
    writer.writeMessage(
      3,
      f,
      proto.api.Graph.serializeBinaryToWriter
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.DopamineAdapterResponse} The clone.
 */
proto.api.DopamineAdapterResponse.prototype.cloneMessage = function() {
  return /** @type {!proto.api.DopamineAdapterResponse} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string id = 1;
 * @return {string}
 */
proto.api.DopamineAdapterResponse.prototype.getId = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.DopamineAdapterResponse.prototype.setId = function(value) {
  jspb.Message.setField(this, 1, value);
};


/**
 * optional string name = 2;
 * @return {string}
 */
proto.api.DopamineAdapterResponse.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 2, ""));
};


/** @param {string} value  */
proto.api.DopamineAdapterResponse.prototype.setName = function(value) {
  jspb.Message.setField(this, 2, value);
};


/**
 * optional Graph graph = 3;
 * @return {proto.api.Graph}
 */
proto.api.DopamineAdapterResponse.prototype.getGraph = function() {
  return /** @type{proto.api.Graph} */ (
    jspb.Message.getWrapperField(this, proto.api.Graph, 3));
};


/** @param {proto.api.Graph|undefined} value  */
proto.api.DopamineAdapterResponse.prototype.setGraph = function(value) {
  jspb.Message.setWrapperField(this, 3, value);
};


proto.api.DopamineAdapterResponse.prototype.clearGraph = function() {
  this.setGraph(undefined);
};


/**
 * Returns whether this field is set.
 * @return{!boolean}
 */
proto.api.DopamineAdapterResponse.prototype.hasGraph = function() {
  return jspb.Message.getField(this, 3) != null;
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.CreateDopamineAdapterRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.CreateDopamineAdapterRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.CreateDopamineAdapterRequest.displayName = 'proto.api.CreateDopamineAdapterRequest';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.CreateDopamineAdapterRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.CreateDopamineAdapterRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.CreateDopamineAdapterRequest} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.CreateDopamineAdapterRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    name: msg.getName(),
    graph: (f = msg.getGraph()) && proto.api.Graph.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.CreateDopamineAdapterRequest}
 */
proto.api.CreateDopamineAdapterRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.CreateDopamineAdapterRequest;
  return proto.api.CreateDopamineAdapterRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.CreateDopamineAdapterRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.CreateDopamineAdapterRequest}
 */
proto.api.CreateDopamineAdapterRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    case 2:
      var value = new proto.api.Graph;
      reader.readMessage(value,proto.api.Graph.deserializeBinaryFromReader);
      msg.setGraph(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.CreateDopamineAdapterRequest} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.CreateDopamineAdapterRequest.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.CreateDopamineAdapterRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.CreateDopamineAdapterRequest.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = this.getGraph();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      proto.api.Graph.serializeBinaryToWriter
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.CreateDopamineAdapterRequest} The clone.
 */
proto.api.CreateDopamineAdapterRequest.prototype.cloneMessage = function() {
  return /** @type {!proto.api.CreateDopamineAdapterRequest} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string name = 1;
 * @return {string}
 */
proto.api.CreateDopamineAdapterRequest.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.CreateDopamineAdapterRequest.prototype.setName = function(value) {
  jspb.Message.setField(this, 1, value);
};


/**
 * optional Graph graph = 2;
 * @return {proto.api.Graph}
 */
proto.api.CreateDopamineAdapterRequest.prototype.getGraph = function() {
  return /** @type{proto.api.Graph} */ (
    jspb.Message.getWrapperField(this, proto.api.Graph, 2));
};


/** @param {proto.api.Graph|undefined} value  */
proto.api.CreateDopamineAdapterRequest.prototype.setGraph = function(value) {
  jspb.Message.setWrapperField(this, 2, value);
};


proto.api.CreateDopamineAdapterRequest.prototype.clearGraph = function() {
  this.setGraph(undefined);
};


/**
 * Returns whether this field is set.
 * @return{!boolean}
 */
proto.api.CreateDopamineAdapterRequest.prototype.hasGraph = function() {
  return jspb.Message.getField(this, 2) != null;
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.DeleteDopamineAdapterRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.DeleteDopamineAdapterRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.DeleteDopamineAdapterRequest.displayName = 'proto.api.DeleteDopamineAdapterRequest';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.DeleteDopamineAdapterRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.DeleteDopamineAdapterRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.DeleteDopamineAdapterRequest} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.DeleteDopamineAdapterRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: msg.getId()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.DeleteDopamineAdapterRequest}
 */
proto.api.DeleteDopamineAdapterRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.DeleteDopamineAdapterRequest;
  return proto.api.DeleteDopamineAdapterRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.DeleteDopamineAdapterRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.DeleteDopamineAdapterRequest}
 */
proto.api.DeleteDopamineAdapterRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.DeleteDopamineAdapterRequest} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DeleteDopamineAdapterRequest.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.DeleteDopamineAdapterRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DeleteDopamineAdapterRequest.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.DeleteDopamineAdapterRequest} The clone.
 */
proto.api.DeleteDopamineAdapterRequest.prototype.cloneMessage = function() {
  return /** @type {!proto.api.DeleteDopamineAdapterRequest} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string id = 1;
 * @return {string}
 */
proto.api.DeleteDopamineAdapterRequest.prototype.getId = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.DeleteDopamineAdapterRequest.prototype.setId = function(value) {
  jspb.Message.setField(this, 1, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.DeleteDopamineAdapterResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.DeleteDopamineAdapterResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.DeleteDopamineAdapterResponse.displayName = 'proto.api.DeleteDopamineAdapterResponse';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.DeleteDopamineAdapterResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.DeleteDopamineAdapterResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.DeleteDopamineAdapterResponse} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.DeleteDopamineAdapterResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    status: msg.getStatus()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.DeleteDopamineAdapterResponse}
 */
proto.api.DeleteDopamineAdapterResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.DeleteDopamineAdapterResponse;
  return proto.api.DeleteDopamineAdapterResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.DeleteDopamineAdapterResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.DeleteDopamineAdapterResponse}
 */
proto.api.DeleteDopamineAdapterResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setStatus(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.DeleteDopamineAdapterResponse} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DeleteDopamineAdapterResponse.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.DeleteDopamineAdapterResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DeleteDopamineAdapterResponse.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getStatus();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.DeleteDopamineAdapterResponse} The clone.
 */
proto.api.DeleteDopamineAdapterResponse.prototype.cloneMessage = function() {
  return /** @type {!proto.api.DeleteDopamineAdapterResponse} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string status = 1;
 * @return {string}
 */
proto.api.DeleteDopamineAdapterResponse.prototype.getStatus = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.DeleteDopamineAdapterResponse.prototype.setStatus = function(value) {
  jspb.Message.setField(this, 1, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.AttachDopamineAdapterRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.AttachDopamineAdapterRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.AttachDopamineAdapterRequest.displayName = 'proto.api.AttachDopamineAdapterRequest';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.AttachDopamineAdapterRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.AttachDopamineAdapterRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.AttachDopamineAdapterRequest} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.AttachDopamineAdapterRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    dopamineadapterid: msg.getDopamineadapterid(),
    modulatedstdppreceptorid: msg.getModulatedstdppreceptorid()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.AttachDopamineAdapterRequest}
 */
proto.api.AttachDopamineAdapterRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.AttachDopamineAdapterRequest;
  return proto.api.AttachDopamineAdapterRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.AttachDopamineAdapterRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.AttachDopamineAdapterRequest}
 */
proto.api.AttachDopamineAdapterRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setDopamineadapterid(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setModulatedstdppreceptorid(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.AttachDopamineAdapterRequest} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.AttachDopamineAdapterRequest.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.AttachDopamineAdapterRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.AttachDopamineAdapterRequest.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getDopamineadapterid();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = this.getModulatedstdppreceptorid();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.AttachDopamineAdapterRequest} The clone.
 */
proto.api.AttachDopamineAdapterRequest.prototype.cloneMessage = function() {
  return /** @type {!proto.api.AttachDopamineAdapterRequest} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string dopamineAdapterId = 1;
 * @return {string}
 */
proto.api.AttachDopamineAdapterRequest.prototype.getDopamineadapterid = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.AttachDopamineAdapterRequest.prototype.setDopamineadapterid = function(value) {
  jspb.Message.setField(this, 1, value);
};


/**
 * optional string modulatedSTDPPreceptorId = 2;
 * @return {string}
 */
proto.api.AttachDopamineAdapterRequest.prototype.getModulatedstdppreceptorid = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 2, ""));
};


/** @param {string} value  */
proto.api.AttachDopamineAdapterRequest.prototype.setModulatedstdppreceptorid = function(value) {
  jspb.Message.setField(this, 2, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.AttachDopamineAdapterResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.AttachDopamineAdapterResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.AttachDopamineAdapterResponse.displayName = 'proto.api.AttachDopamineAdapterResponse';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.AttachDopamineAdapterResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.AttachDopamineAdapterResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.AttachDopamineAdapterResponse} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.AttachDopamineAdapterResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    status: msg.getStatus()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.AttachDopamineAdapterResponse}
 */
proto.api.AttachDopamineAdapterResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.AttachDopamineAdapterResponse;
  return proto.api.AttachDopamineAdapterResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.AttachDopamineAdapterResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.AttachDopamineAdapterResponse}
 */
proto.api.AttachDopamineAdapterResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setStatus(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.AttachDopamineAdapterResponse} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.AttachDopamineAdapterResponse.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.AttachDopamineAdapterResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.AttachDopamineAdapterResponse.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getStatus();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.AttachDopamineAdapterResponse} The clone.
 */
proto.api.AttachDopamineAdapterResponse.prototype.cloneMessage = function() {
  return /** @type {!proto.api.AttachDopamineAdapterResponse} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string status = 1;
 * @return {string}
 */
proto.api.AttachDopamineAdapterResponse.prototype.getStatus = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.AttachDopamineAdapterResponse.prototype.setStatus = function(value) {
  jspb.Message.setField(this, 1, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.DetachDopamineAdapterRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.DetachDopamineAdapterRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.DetachDopamineAdapterRequest.displayName = 'proto.api.DetachDopamineAdapterRequest';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.DetachDopamineAdapterRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.DetachDopamineAdapterRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.DetachDopamineAdapterRequest} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.DetachDopamineAdapterRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    dopamineadapterid: msg.getDopamineadapterid(),
    modulatedstdppreceptorid: msg.getModulatedstdppreceptorid()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.DetachDopamineAdapterRequest}
 */
proto.api.DetachDopamineAdapterRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.DetachDopamineAdapterRequest;
  return proto.api.DetachDopamineAdapterRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.DetachDopamineAdapterRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.DetachDopamineAdapterRequest}
 */
proto.api.DetachDopamineAdapterRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setDopamineadapterid(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setModulatedstdppreceptorid(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.DetachDopamineAdapterRequest} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DetachDopamineAdapterRequest.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.DetachDopamineAdapterRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DetachDopamineAdapterRequest.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getDopamineadapterid();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = this.getModulatedstdppreceptorid();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.DetachDopamineAdapterRequest} The clone.
 */
proto.api.DetachDopamineAdapterRequest.prototype.cloneMessage = function() {
  return /** @type {!proto.api.DetachDopamineAdapterRequest} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string dopamineAdapterId = 1;
 * @return {string}
 */
proto.api.DetachDopamineAdapterRequest.prototype.getDopamineadapterid = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.DetachDopamineAdapterRequest.prototype.setDopamineadapterid = function(value) {
  jspb.Message.setField(this, 1, value);
};


/**
 * optional string modulatedSTDPPreceptorId = 2;
 * @return {string}
 */
proto.api.DetachDopamineAdapterRequest.prototype.getModulatedstdppreceptorid = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 2, ""));
};


/** @param {string} value  */
proto.api.DetachDopamineAdapterRequest.prototype.setModulatedstdppreceptorid = function(value) {
  jspb.Message.setField(this, 2, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.DetachDopamineAdapterResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.DetachDopamineAdapterResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.DetachDopamineAdapterResponse.displayName = 'proto.api.DetachDopamineAdapterResponse';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.DetachDopamineAdapterResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.DetachDopamineAdapterResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.DetachDopamineAdapterResponse} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.DetachDopamineAdapterResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    status: msg.getStatus()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.DetachDopamineAdapterResponse}
 */
proto.api.DetachDopamineAdapterResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.DetachDopamineAdapterResponse;
  return proto.api.DetachDopamineAdapterResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.DetachDopamineAdapterResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.DetachDopamineAdapterResponse}
 */
proto.api.DetachDopamineAdapterResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setStatus(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.DetachDopamineAdapterResponse} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DetachDopamineAdapterResponse.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.DetachDopamineAdapterResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.DetachDopamineAdapterResponse.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getStatus();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.DetachDopamineAdapterResponse} The clone.
 */
proto.api.DetachDopamineAdapterResponse.prototype.cloneMessage = function() {
  return /** @type {!proto.api.DetachDopamineAdapterResponse} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string status = 1;
 * @return {string}
 */
proto.api.DetachDopamineAdapterResponse.prototype.getStatus = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.DetachDopamineAdapterResponse.prototype.setStatus = function(value) {
  jspb.Message.setField(this, 1, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.SetDopamineAdapterPositionRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.SetDopamineAdapterPositionRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.SetDopamineAdapterPositionRequest.displayName = 'proto.api.SetDopamineAdapterPositionRequest';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.SetDopamineAdapterPositionRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.SetDopamineAdapterPositionRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.SetDopamineAdapterPositionRequest} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.SetDopamineAdapterPositionRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: msg.getId(),
    graph: (f = msg.getGraph()) && proto.api.Graph.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.SetDopamineAdapterPositionRequest}
 */
proto.api.SetDopamineAdapterPositionRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.SetDopamineAdapterPositionRequest;
  return proto.api.SetDopamineAdapterPositionRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.SetDopamineAdapterPositionRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.SetDopamineAdapterPositionRequest}
 */
proto.api.SetDopamineAdapterPositionRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setId(value);
      break;
    case 2:
      var value = new proto.api.Graph;
      reader.readMessage(value,proto.api.Graph.deserializeBinaryFromReader);
      msg.setGraph(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.SetDopamineAdapterPositionRequest} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.SetDopamineAdapterPositionRequest.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.SetDopamineAdapterPositionRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.SetDopamineAdapterPositionRequest.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = this.getGraph();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      proto.api.Graph.serializeBinaryToWriter
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.SetDopamineAdapterPositionRequest} The clone.
 */
proto.api.SetDopamineAdapterPositionRequest.prototype.cloneMessage = function() {
  return /** @type {!proto.api.SetDopamineAdapterPositionRequest} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string id = 1;
 * @return {string}
 */
proto.api.SetDopamineAdapterPositionRequest.prototype.getId = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.SetDopamineAdapterPositionRequest.prototype.setId = function(value) {
  jspb.Message.setField(this, 1, value);
};


/**
 * optional Graph graph = 2;
 * @return {proto.api.Graph}
 */
proto.api.SetDopamineAdapterPositionRequest.prototype.getGraph = function() {
  return /** @type{proto.api.Graph} */ (
    jspb.Message.getWrapperField(this, proto.api.Graph, 2));
};


/** @param {proto.api.Graph|undefined} value  */
proto.api.SetDopamineAdapterPositionRequest.prototype.setGraph = function(value) {
  jspb.Message.setWrapperField(this, 2, value);
};


proto.api.SetDopamineAdapterPositionRequest.prototype.clearGraph = function() {
  this.setGraph(undefined);
};


/**
 * Returns whether this field is set.
 * @return{!boolean}
 */
proto.api.SetDopamineAdapterPositionRequest.prototype.hasGraph = function() {
  return jspb.Message.getField(this, 2) != null;
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.SetDopamineAdapterPositionResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.SetDopamineAdapterPositionResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.SetDopamineAdapterPositionResponse.displayName = 'proto.api.SetDopamineAdapterPositionResponse';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.SetDopamineAdapterPositionResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.SetDopamineAdapterPositionResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.SetDopamineAdapterPositionResponse} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.SetDopamineAdapterPositionResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    status: msg.getStatus()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.SetDopamineAdapterPositionResponse}
 */
proto.api.SetDopamineAdapterPositionResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.SetDopamineAdapterPositionResponse;
  return proto.api.SetDopamineAdapterPositionResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.SetDopamineAdapterPositionResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.SetDopamineAdapterPositionResponse}
 */
proto.api.SetDopamineAdapterPositionResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setStatus(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.SetDopamineAdapterPositionResponse} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.SetDopamineAdapterPositionResponse.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.SetDopamineAdapterPositionResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.SetDopamineAdapterPositionResponse.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getStatus();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.SetDopamineAdapterPositionResponse} The clone.
 */
proto.api.SetDopamineAdapterPositionResponse.prototype.cloneMessage = function() {
  return /** @type {!proto.api.SetDopamineAdapterPositionResponse} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string status = 1;
 * @return {string}
 */
proto.api.SetDopamineAdapterPositionResponse.prototype.getStatus = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.SetDopamineAdapterPositionResponse.prototype.setStatus = function(value) {
  jspb.Message.setField(this, 1, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.StreamInputRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.api.StreamInputRequest.repeatedFields_, null);
};
goog.inherits(proto.api.StreamInputRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.StreamInputRequest.displayName = 'proto.api.StreamInputRequest';
}
/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.api.StreamInputRequest.repeatedFields_ = [2];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.StreamInputRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.StreamInputRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.StreamInputRequest} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.StreamInputRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    inputadapterid: msg.getInputadapterid(),
    valuesList: jspb.Message.getRepeatedFloatingPointField(msg, 2)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.StreamInputRequest}
 */
proto.api.StreamInputRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.StreamInputRequest;
  return proto.api.StreamInputRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.StreamInputRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.StreamInputRequest}
 */
proto.api.StreamInputRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setInputadapterid(value);
      break;
    case 2:
      var value = /** @type {!Array.<number>} */ (reader.readPackedDouble());
      msg.setValuesList(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.StreamInputRequest} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.StreamInputRequest.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.StreamInputRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.StreamInputRequest.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getInputadapterid();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = this.getValuesList();
  if (f.length > 0) {
    writer.writePackedDouble(
      2,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.StreamInputRequest} The clone.
 */
proto.api.StreamInputRequest.prototype.cloneMessage = function() {
  return /** @type {!proto.api.StreamInputRequest} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string inputAdapterId = 1;
 * @return {string}
 */
proto.api.StreamInputRequest.prototype.getInputadapterid = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.StreamInputRequest.prototype.setInputadapterid = function(value) {
  jspb.Message.setField(this, 1, value);
};


/**
 * repeated double values = 2;
 * If you change this array by adding, removing or replacing elements, or if you
 * replace the array itself, then you must call the setter to update it.
 * @return {!Array.<number>}
 */
proto.api.StreamInputRequest.prototype.getValuesList = function() {
  return /** @type {!Array.<number>} */ (jspb.Message.getRepeatedFloatingPointField(this, 2));
};


/** @param {Array.<number>} value  */
proto.api.StreamInputRequest.prototype.setValuesList = function(value) {
  jspb.Message.setField(this, 2, value || []);
};


proto.api.StreamInputRequest.prototype.clearValuesList = function() {
  jspb.Message.setField(this, 2, []);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.StreamInputResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.StreamInputResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.StreamInputResponse.displayName = 'proto.api.StreamInputResponse';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.StreamInputResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.StreamInputResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.StreamInputResponse} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.StreamInputResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    status: msg.getStatus()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.StreamInputResponse}
 */
proto.api.StreamInputResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.StreamInputResponse;
  return proto.api.StreamInputResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.StreamInputResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.StreamInputResponse}
 */
proto.api.StreamInputResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setStatus(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.StreamInputResponse} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.StreamInputResponse.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.StreamInputResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.StreamInputResponse.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getStatus();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.StreamInputResponse} The clone.
 */
proto.api.StreamInputResponse.prototype.cloneMessage = function() {
  return /** @type {!proto.api.StreamInputResponse} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string status = 1;
 * @return {string}
 */
proto.api.StreamInputResponse.prototype.getStatus = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.StreamInputResponse.prototype.setStatus = function(value) {
  jspb.Message.setField(this, 1, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.StreamOutputRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.StreamOutputRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.StreamOutputRequest.displayName = 'proto.api.StreamOutputRequest';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.StreamOutputRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.StreamOutputRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.StreamOutputRequest} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.StreamOutputRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    outputadapterid: msg.getOutputadapterid()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.StreamOutputRequest}
 */
proto.api.StreamOutputRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.StreamOutputRequest;
  return proto.api.StreamOutputRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.StreamOutputRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.StreamOutputRequest}
 */
proto.api.StreamOutputRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setOutputadapterid(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.StreamOutputRequest} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.StreamOutputRequest.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.StreamOutputRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.StreamOutputRequest.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getOutputadapterid();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.StreamOutputRequest} The clone.
 */
proto.api.StreamOutputRequest.prototype.cloneMessage = function() {
  return /** @type {!proto.api.StreamOutputRequest} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string outputAdapterId = 1;
 * @return {string}
 */
proto.api.StreamOutputRequest.prototype.getOutputadapterid = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.StreamOutputRequest.prototype.setOutputadapterid = function(value) {
  jspb.Message.setField(this, 1, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.StreamOutputResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.api.StreamOutputResponse.repeatedFields_, null);
};
goog.inherits(proto.api.StreamOutputResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.StreamOutputResponse.displayName = 'proto.api.StreamOutputResponse';
}
/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.api.StreamOutputResponse.repeatedFields_ = [2];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.StreamOutputResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.StreamOutputResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.StreamOutputResponse} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.StreamOutputResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    outputadapterid: msg.getOutputadapterid(),
    valuesList: jspb.Message.getRepeatedFloatingPointField(msg, 2)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.StreamOutputResponse}
 */
proto.api.StreamOutputResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.StreamOutputResponse;
  return proto.api.StreamOutputResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.StreamOutputResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.StreamOutputResponse}
 */
proto.api.StreamOutputResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setOutputadapterid(value);
      break;
    case 2:
      var value = /** @type {!Array.<number>} */ (reader.readPackedDouble());
      msg.setValuesList(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.StreamOutputResponse} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.StreamOutputResponse.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.StreamOutputResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.StreamOutputResponse.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getOutputadapterid();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = this.getValuesList();
  if (f.length > 0) {
    writer.writePackedDouble(
      2,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.StreamOutputResponse} The clone.
 */
proto.api.StreamOutputResponse.prototype.cloneMessage = function() {
  return /** @type {!proto.api.StreamOutputResponse} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string outputAdapterId = 1;
 * @return {string}
 */
proto.api.StreamOutputResponse.prototype.getOutputadapterid = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.StreamOutputResponse.prototype.setOutputadapterid = function(value) {
  jspb.Message.setField(this, 1, value);
};


/**
 * repeated double values = 2;
 * If you change this array by adding, removing or replacing elements, or if you
 * replace the array itself, then you must call the setter to update it.
 * @return {!Array.<number>}
 */
proto.api.StreamOutputResponse.prototype.getValuesList = function() {
  return /** @type {!Array.<number>} */ (jspb.Message.getRepeatedFloatingPointField(this, 2));
};


/** @param {Array.<number>} value  */
proto.api.StreamOutputResponse.prototype.setValuesList = function(value) {
  jspb.Message.setField(this, 2, value || []);
};


proto.api.StreamOutputResponse.prototype.clearValuesList = function() {
  jspb.Message.setField(this, 2, []);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.StreamDopamineRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.StreamDopamineRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.StreamDopamineRequest.displayName = 'proto.api.StreamDopamineRequest';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.StreamDopamineRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.StreamDopamineRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.StreamDopamineRequest} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.StreamDopamineRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    dopamineadapterid: msg.getDopamineadapterid(),
    dopamine: msg.getDopamine()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.StreamDopamineRequest}
 */
proto.api.StreamDopamineRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.StreamDopamineRequest;
  return proto.api.StreamDopamineRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.StreamDopamineRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.StreamDopamineRequest}
 */
proto.api.StreamDopamineRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setDopamineadapterid(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setDopamine(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.StreamDopamineRequest} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.StreamDopamineRequest.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.StreamDopamineRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.StreamDopamineRequest.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getDopamineadapterid();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = this.getDopamine();
  if (f !== 0.0) {
    writer.writeDouble(
      2,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.StreamDopamineRequest} The clone.
 */
proto.api.StreamDopamineRequest.prototype.cloneMessage = function() {
  return /** @type {!proto.api.StreamDopamineRequest} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string dopamineAdapterId = 1;
 * @return {string}
 */
proto.api.StreamDopamineRequest.prototype.getDopamineadapterid = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.StreamDopamineRequest.prototype.setDopamineadapterid = function(value) {
  jspb.Message.setField(this, 1, value);
};


/**
 * optional double dopamine = 2;
 * @return {number}
 */
proto.api.StreamDopamineRequest.prototype.getDopamine = function() {
  return /** @type {number} */ (jspb.Message.getFieldProto3(this, 2, 0));
};


/** @param {number} value  */
proto.api.StreamDopamineRequest.prototype.setDopamine = function(value) {
  jspb.Message.setField(this, 2, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.StreamDopamineResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.StreamDopamineResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.StreamDopamineResponse.displayName = 'proto.api.StreamDopamineResponse';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.StreamDopamineResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.StreamDopamineResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.StreamDopamineResponse} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.StreamDopamineResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    status: msg.getStatus()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.StreamDopamineResponse}
 */
proto.api.StreamDopamineResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.StreamDopamineResponse;
  return proto.api.StreamDopamineResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.StreamDopamineResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.StreamDopamineResponse}
 */
proto.api.StreamDopamineResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setStatus(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.StreamDopamineResponse} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.StreamDopamineResponse.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.StreamDopamineResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.StreamDopamineResponse.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getStatus();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.StreamDopamineResponse} The clone.
 */
proto.api.StreamDopamineResponse.prototype.cloneMessage = function() {
  return /** @type {!proto.api.StreamDopamineResponse} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string status = 1;
 * @return {string}
 */
proto.api.StreamDopamineResponse.prototype.getStatus = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.StreamDopamineResponse.prototype.setStatus = function(value) {
  jspb.Message.setField(this, 1, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.StreamNeuronSpikeEventRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.StreamNeuronSpikeEventRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.StreamNeuronSpikeEventRequest.displayName = 'proto.api.StreamNeuronSpikeEventRequest';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.StreamNeuronSpikeEventRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.StreamNeuronSpikeEventRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.StreamNeuronSpikeEventRequest} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.StreamNeuronSpikeEventRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: msg.getId(),
    component: msg.getComponent(),
    samplinginterval: msg.getSamplinginterval()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.StreamNeuronSpikeEventRequest}
 */
proto.api.StreamNeuronSpikeEventRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.StreamNeuronSpikeEventRequest;
  return proto.api.StreamNeuronSpikeEventRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.StreamNeuronSpikeEventRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.StreamNeuronSpikeEventRequest}
 */
proto.api.StreamNeuronSpikeEventRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setId(value);
      break;
    case 2:
      var value = /** @type {!proto.api.Component} */ (reader.readEnum());
      msg.setComponent(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setSamplinginterval(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.StreamNeuronSpikeEventRequest} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.StreamNeuronSpikeEventRequest.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.StreamNeuronSpikeEventRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.StreamNeuronSpikeEventRequest.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = this.getComponent();
  if (f !== 0.0) {
    writer.writeEnum(
      2,
      f
    );
  }
  f = this.getSamplinginterval();
  if (f !== 0) {
    writer.writeInt32(
      3,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.StreamNeuronSpikeEventRequest} The clone.
 */
proto.api.StreamNeuronSpikeEventRequest.prototype.cloneMessage = function() {
  return /** @type {!proto.api.StreamNeuronSpikeEventRequest} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string id = 1;
 * @return {string}
 */
proto.api.StreamNeuronSpikeEventRequest.prototype.getId = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.StreamNeuronSpikeEventRequest.prototype.setId = function(value) {
  jspb.Message.setField(this, 1, value);
};


/**
 * optional Component component = 2;
 * @return {!proto.api.Component}
 */
proto.api.StreamNeuronSpikeEventRequest.prototype.getComponent = function() {
  return /** @type {!proto.api.Component} */ (jspb.Message.getFieldProto3(this, 2, 0));
};


/** @param {!proto.api.Component} value  */
proto.api.StreamNeuronSpikeEventRequest.prototype.setComponent = function(value) {
  jspb.Message.setField(this, 2, value);
};


/**
 * optional int32 samplingInterval = 3;
 * @return {number}
 */
proto.api.StreamNeuronSpikeEventRequest.prototype.getSamplinginterval = function() {
  return /** @type {number} */ (jspb.Message.getFieldProto3(this, 3, 0));
};


/** @param {number} value  */
proto.api.StreamNeuronSpikeEventRequest.prototype.setSamplinginterval = function(value) {
  jspb.Message.setField(this, 3, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.NeuronSpikeEvent = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.NeuronSpikeEvent, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.NeuronSpikeEvent.displayName = 'proto.api.NeuronSpikeEvent';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.NeuronSpikeEvent.prototype.toObject = function(opt_includeInstance) {
  return proto.api.NeuronSpikeEvent.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.NeuronSpikeEvent} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.NeuronSpikeEvent.toObject = function(includeInstance, msg) {
  var f, obj = {
    neuronclusterid: msg.getNeuronclusterid(),
    neuronid: msg.getNeuronid()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.NeuronSpikeEvent}
 */
proto.api.NeuronSpikeEvent.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.NeuronSpikeEvent;
  return proto.api.NeuronSpikeEvent.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.NeuronSpikeEvent} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.NeuronSpikeEvent}
 */
proto.api.NeuronSpikeEvent.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setNeuronclusterid(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setNeuronid(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.NeuronSpikeEvent} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.NeuronSpikeEvent.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.NeuronSpikeEvent.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.NeuronSpikeEvent.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getNeuronclusterid();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = this.getNeuronid();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.NeuronSpikeEvent} The clone.
 */
proto.api.NeuronSpikeEvent.prototype.cloneMessage = function() {
  return /** @type {!proto.api.NeuronSpikeEvent} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string neuronClusterId = 1;
 * @return {string}
 */
proto.api.NeuronSpikeEvent.prototype.getNeuronclusterid = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.NeuronSpikeEvent.prototype.setNeuronclusterid = function(value) {
  jspb.Message.setField(this, 1, value);
};


/**
 * optional string neuronId = 2;
 * @return {string}
 */
proto.api.NeuronSpikeEvent.prototype.getNeuronid = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 2, ""));
};


/** @param {string} value  */
proto.api.NeuronSpikeEvent.prototype.setNeuronid = function(value) {
  jspb.Message.setField(this, 2, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.StreamNeuronSpikeRelayEventRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.StreamNeuronSpikeRelayEventRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.StreamNeuronSpikeRelayEventRequest.displayName = 'proto.api.StreamNeuronSpikeRelayEventRequest';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.StreamNeuronSpikeRelayEventRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.StreamNeuronSpikeRelayEventRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.StreamNeuronSpikeRelayEventRequest} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.StreamNeuronSpikeRelayEventRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: msg.getId(),
    component: msg.getComponent(),
    samplinginterval: msg.getSamplinginterval()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.StreamNeuronSpikeRelayEventRequest}
 */
proto.api.StreamNeuronSpikeRelayEventRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.StreamNeuronSpikeRelayEventRequest;
  return proto.api.StreamNeuronSpikeRelayEventRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.StreamNeuronSpikeRelayEventRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.StreamNeuronSpikeRelayEventRequest}
 */
proto.api.StreamNeuronSpikeRelayEventRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setId(value);
      break;
    case 2:
      var value = /** @type {!proto.api.Component} */ (reader.readEnum());
      msg.setComponent(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setSamplinginterval(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.StreamNeuronSpikeRelayEventRequest} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.StreamNeuronSpikeRelayEventRequest.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.StreamNeuronSpikeRelayEventRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.StreamNeuronSpikeRelayEventRequest.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = this.getComponent();
  if (f !== 0.0) {
    writer.writeEnum(
      2,
      f
    );
  }
  f = this.getSamplinginterval();
  if (f !== 0) {
    writer.writeInt32(
      3,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.StreamNeuronSpikeRelayEventRequest} The clone.
 */
proto.api.StreamNeuronSpikeRelayEventRequest.prototype.cloneMessage = function() {
  return /** @type {!proto.api.StreamNeuronSpikeRelayEventRequest} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string id = 1;
 * @return {string}
 */
proto.api.StreamNeuronSpikeRelayEventRequest.prototype.getId = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.StreamNeuronSpikeRelayEventRequest.prototype.setId = function(value) {
  jspb.Message.setField(this, 1, value);
};


/**
 * optional Component component = 2;
 * @return {!proto.api.Component}
 */
proto.api.StreamNeuronSpikeRelayEventRequest.prototype.getComponent = function() {
  return /** @type {!proto.api.Component} */ (jspb.Message.getFieldProto3(this, 2, 0));
};


/** @param {!proto.api.Component} value  */
proto.api.StreamNeuronSpikeRelayEventRequest.prototype.setComponent = function(value) {
  jspb.Message.setField(this, 2, value);
};


/**
 * optional int32 samplingInterval = 3;
 * @return {number}
 */
proto.api.StreamNeuronSpikeRelayEventRequest.prototype.getSamplinginterval = function() {
  return /** @type {number} */ (jspb.Message.getFieldProto3(this, 3, 0));
};


/** @param {number} value  */
proto.api.StreamNeuronSpikeRelayEventRequest.prototype.setSamplinginterval = function(value) {
  jspb.Message.setField(this, 3, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.NeuronSpikeRelayEvent = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.NeuronSpikeRelayEvent, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.NeuronSpikeRelayEvent.displayName = 'proto.api.NeuronSpikeRelayEvent';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.NeuronSpikeRelayEvent.prototype.toObject = function(opt_includeInstance) {
  return proto.api.NeuronSpikeRelayEvent.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.NeuronSpikeRelayEvent} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.NeuronSpikeRelayEvent.toObject = function(includeInstance, msg) {
  var f, obj = {
    neuronclusterid: msg.getNeuronclusterid(),
    neuronid: msg.getNeuronid(),
    potential: msg.getPotential()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.NeuronSpikeRelayEvent}
 */
proto.api.NeuronSpikeRelayEvent.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.NeuronSpikeRelayEvent;
  return proto.api.NeuronSpikeRelayEvent.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.NeuronSpikeRelayEvent} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.NeuronSpikeRelayEvent}
 */
proto.api.NeuronSpikeRelayEvent.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setNeuronclusterid(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setNeuronid(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setPotential(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.NeuronSpikeRelayEvent} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.NeuronSpikeRelayEvent.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.NeuronSpikeRelayEvent.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.NeuronSpikeRelayEvent.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getNeuronclusterid();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = this.getNeuronid();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = this.getPotential();
  if (f !== 0.0) {
    writer.writeDouble(
      3,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.NeuronSpikeRelayEvent} The clone.
 */
proto.api.NeuronSpikeRelayEvent.prototype.cloneMessage = function() {
  return /** @type {!proto.api.NeuronSpikeRelayEvent} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string neuronClusterId = 1;
 * @return {string}
 */
proto.api.NeuronSpikeRelayEvent.prototype.getNeuronclusterid = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.NeuronSpikeRelayEvent.prototype.setNeuronclusterid = function(value) {
  jspb.Message.setField(this, 1, value);
};


/**
 * optional string neuronId = 2;
 * @return {string}
 */
proto.api.NeuronSpikeRelayEvent.prototype.getNeuronid = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 2, ""));
};


/** @param {string} value  */
proto.api.NeuronSpikeRelayEvent.prototype.setNeuronid = function(value) {
  jspb.Message.setField(this, 2, value);
};


/**
 * optional double potential = 3;
 * @return {number}
 */
proto.api.NeuronSpikeRelayEvent.prototype.getPotential = function() {
  return /** @type {number} */ (jspb.Message.getFieldProto3(this, 3, 0));
};


/** @param {number} value  */
proto.api.NeuronSpikeRelayEvent.prototype.setPotential = function(value) {
  jspb.Message.setField(this, 3, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.StreamSynapseWeightUpdateEventRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.StreamSynapseWeightUpdateEventRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.StreamSynapseWeightUpdateEventRequest.displayName = 'proto.api.StreamSynapseWeightUpdateEventRequest';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.StreamSynapseWeightUpdateEventRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.StreamSynapseWeightUpdateEventRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.StreamSynapseWeightUpdateEventRequest} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.StreamSynapseWeightUpdateEventRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: msg.getId(),
    component: msg.getComponent(),
    samplinginterval: msg.getSamplinginterval()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.StreamSynapseWeightUpdateEventRequest}
 */
proto.api.StreamSynapseWeightUpdateEventRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.StreamSynapseWeightUpdateEventRequest;
  return proto.api.StreamSynapseWeightUpdateEventRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.StreamSynapseWeightUpdateEventRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.StreamSynapseWeightUpdateEventRequest}
 */
proto.api.StreamSynapseWeightUpdateEventRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setId(value);
      break;
    case 2:
      var value = /** @type {!proto.api.Component} */ (reader.readEnum());
      msg.setComponent(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setSamplinginterval(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.StreamSynapseWeightUpdateEventRequest} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.StreamSynapseWeightUpdateEventRequest.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.StreamSynapseWeightUpdateEventRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.StreamSynapseWeightUpdateEventRequest.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = this.getComponent();
  if (f !== 0.0) {
    writer.writeEnum(
      2,
      f
    );
  }
  f = this.getSamplinginterval();
  if (f !== 0) {
    writer.writeInt32(
      3,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.StreamSynapseWeightUpdateEventRequest} The clone.
 */
proto.api.StreamSynapseWeightUpdateEventRequest.prototype.cloneMessage = function() {
  return /** @type {!proto.api.StreamSynapseWeightUpdateEventRequest} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string id = 1;
 * @return {string}
 */
proto.api.StreamSynapseWeightUpdateEventRequest.prototype.getId = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.StreamSynapseWeightUpdateEventRequest.prototype.setId = function(value) {
  jspb.Message.setField(this, 1, value);
};


/**
 * optional Component component = 2;
 * @return {!proto.api.Component}
 */
proto.api.StreamSynapseWeightUpdateEventRequest.prototype.getComponent = function() {
  return /** @type {!proto.api.Component} */ (jspb.Message.getFieldProto3(this, 2, 0));
};


/** @param {!proto.api.Component} value  */
proto.api.StreamSynapseWeightUpdateEventRequest.prototype.setComponent = function(value) {
  jspb.Message.setField(this, 2, value);
};


/**
 * optional int32 samplingInterval = 3;
 * @return {number}
 */
proto.api.StreamSynapseWeightUpdateEventRequest.prototype.getSamplinginterval = function() {
  return /** @type {number} */ (jspb.Message.getFieldProto3(this, 3, 0));
};


/** @param {number} value  */
proto.api.StreamSynapseWeightUpdateEventRequest.prototype.setSamplinginterval = function(value) {
  jspb.Message.setField(this, 3, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.SynapseWeightUpdateEvent = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.SynapseWeightUpdateEvent, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.SynapseWeightUpdateEvent.displayName = 'proto.api.SynapseWeightUpdateEvent';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.SynapseWeightUpdateEvent.prototype.toObject = function(opt_includeInstance) {
  return proto.api.SynapseWeightUpdateEvent.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.SynapseWeightUpdateEvent} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.SynapseWeightUpdateEvent.toObject = function(includeInstance, msg) {
  var f, obj = {
    presynapticneuronid: msg.getPresynapticneuronid(),
    postsynapticneuronid: msg.getPostsynapticneuronid(),
    weightdelta: msg.getWeightdelta()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.SynapseWeightUpdateEvent}
 */
proto.api.SynapseWeightUpdateEvent.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.SynapseWeightUpdateEvent;
  return proto.api.SynapseWeightUpdateEvent.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.SynapseWeightUpdateEvent} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.SynapseWeightUpdateEvent}
 */
proto.api.SynapseWeightUpdateEvent.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setPresynapticneuronid(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setPostsynapticneuronid(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setWeightdelta(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.SynapseWeightUpdateEvent} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.SynapseWeightUpdateEvent.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.SynapseWeightUpdateEvent.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.SynapseWeightUpdateEvent.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getPresynapticneuronid();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = this.getPostsynapticneuronid();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = this.getWeightdelta();
  if (f !== 0.0) {
    writer.writeDouble(
      3,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.SynapseWeightUpdateEvent} The clone.
 */
proto.api.SynapseWeightUpdateEvent.prototype.cloneMessage = function() {
  return /** @type {!proto.api.SynapseWeightUpdateEvent} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional string PreSynapticNeuronId = 1;
 * @return {string}
 */
proto.api.SynapseWeightUpdateEvent.prototype.getPresynapticneuronid = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 1, ""));
};


/** @param {string} value  */
proto.api.SynapseWeightUpdateEvent.prototype.setPresynapticneuronid = function(value) {
  jspb.Message.setField(this, 1, value);
};


/**
 * optional string PostSynapticNeuronId = 2;
 * @return {string}
 */
proto.api.SynapseWeightUpdateEvent.prototype.getPostsynapticneuronid = function() {
  return /** @type {string} */ (jspb.Message.getFieldProto3(this, 2, ""));
};


/** @param {string} value  */
proto.api.SynapseWeightUpdateEvent.prototype.setPostsynapticneuronid = function(value) {
  jspb.Message.setField(this, 2, value);
};


/**
 * optional double WeightDelta = 3;
 * @return {number}
 */
proto.api.SynapseWeightUpdateEvent.prototype.getWeightdelta = function() {
  return /** @type {number} */ (jspb.Message.getFieldProto3(this, 3, 0));
};


/** @param {number} value  */
proto.api.SynapseWeightUpdateEvent.prototype.setWeightdelta = function(value) {
  jspb.Message.setField(this, 3, value);
};



/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.Graph = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.Graph, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.api.Graph.displayName = 'proto.api.Graph';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.Graph.prototype.toObject = function(opt_includeInstance) {
  return proto.api.Graph.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.Graph} msg The msg instance to transform.
 * @return {!Object}
 */
proto.api.Graph.toObject = function(includeInstance, msg) {
  var f, obj = {
    x: msg.getX(),
    y: msg.getY()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.Graph}
 */
proto.api.Graph.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.Graph;
  return proto.api.Graph.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.Graph} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.Graph}
 */
proto.api.Graph.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setX(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setY(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Class method variant: serializes the given message to binary data
 * (in protobuf wire format), writing to the given BinaryWriter.
 * @param {!proto.api.Graph} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.Graph.serializeBinaryToWriter = function(message, writer) {
  message.serializeBinaryToWriter(writer);
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.Graph.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  this.serializeBinaryToWriter(writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the message to binary data (in protobuf wire format),
 * writing to the given BinaryWriter.
 * @param {!jspb.BinaryWriter} writer
 */
proto.api.Graph.prototype.serializeBinaryToWriter = function (writer) {
  var f = undefined;
  f = this.getX();
  if (f !== 0.0) {
    writer.writeDouble(
      1,
      f
    );
  }
  f = this.getY();
  if (f !== 0.0) {
    writer.writeDouble(
      2,
      f
    );
  }
};


/**
 * Creates a deep clone of this proto. No data is shared with the original.
 * @return {!proto.api.Graph} The clone.
 */
proto.api.Graph.prototype.cloneMessage = function() {
  return /** @type {!proto.api.Graph} */ (jspb.Message.cloneMessage(this));
};


/**
 * optional double x = 1;
 * @return {number}
 */
proto.api.Graph.prototype.getX = function() {
  return /** @type {number} */ (jspb.Message.getFieldProto3(this, 1, 0));
};


/** @param {number} value  */
proto.api.Graph.prototype.setX = function(value) {
  jspb.Message.setField(this, 1, value);
};


/**
 * optional double y = 2;
 * @return {number}
 */
proto.api.Graph.prototype.getY = function() {
  return /** @type {number} */ (jspb.Message.getFieldProto3(this, 2, 0));
};


/** @param {number} value  */
proto.api.Graph.prototype.setY = function(value) {
  jspb.Message.setField(this, 2, value);
};


/**
 * @enum {number}
 */
proto.api.Component = {
  UNKNOWN: 0,
  NEURON_CLUSTER: 1,
  SYNAPSE_CLUSTER: 2,
  STDP_PRECEPTOR: 3,
  MODULATED_STDP_PRECEPTOR: 4,
  INPUT_ADAPTER: 5,
  OUTPUT_ADAPTER: 6,
  DOPAMINE_ADAPTER: 7
};

goog.object.extend(exports, proto.api);
