/* eslint-disable */
// package: api
// file: api.proto

var jspb = require("google-protobuf");
var api_pb = require("./api_pb");
var Node = {
  serviceName: "api.Node"
};
Node.Node = {
  methodName: "Node",
  service: Node,
  requestStream: false,
  responseStream: false,
  requestType: api_pb.NodeRequest,
  responseType: api_pb.NodeResponse
};
Node.CreateNeuronCluster = {
  methodName: "CreateNeuronCluster",
  service: Node,
  requestStream: false,
  responseStream: false,
  requestType: api_pb.CreateNeuronClusterRequest,
  responseType: api_pb.NeuronClusterResponse
};
Node.DeleteNeuronCluster = {
  methodName: "DeleteNeuronCluster",
  service: Node,
  requestStream: false,
  responseStream: false,
  requestType: api_pb.DeleteNeuronClusterRequest,
  responseType: api_pb.DeleteNeuronClusterResponse
};
Node.SetNeuronClusterPosition = {
  methodName: "SetNeuronClusterPosition",
  service: Node,
  requestStream: false,
  responseStream: false,
  requestType: api_pb.SetNeuronClusterPositionRequest,
  responseType: api_pb.SetNeuronClusterPositionResponse
};
Node.CreateSynapseCluster = {
  methodName: "CreateSynapseCluster",
  service: Node,
  requestStream: false,
  responseStream: false,
  requestType: api_pb.CreateSynapseClusterRequest,
  responseType: api_pb.SynapseClusterResponse
};
Node.DeleteSynapseCluster = {
  methodName: "DeleteSynapseCluster",
  service: Node,
  requestStream: false,
  responseStream: false,
  requestType: api_pb.DeleteSynapseClusterRequest,
  responseType: api_pb.DeleteSynapseClusterResponse
};
Node.AttachSynapseCluster = {
  methodName: "AttachSynapseCluster",
  service: Node,
  requestStream: false,
  responseStream: false,
  requestType: api_pb.AttachSynapseClusterRequest,
  responseType: api_pb.AttachSynapseClusterResponse
};
Node.DetachSynapseCluster = {
  methodName: "DetachSynapseCluster",
  service: Node,
  requestStream: false,
  responseStream: false,
  requestType: api_pb.DetachSynapseClusterRequest,
  responseType: api_pb.DetachSynapseClusterResponse
};
Node.SetSynapseClusterPosition = {
  methodName: "SetSynapseClusterPosition",
  service: Node,
  requestStream: false,
  responseStream: false,
  requestType: api_pb.SetSynapseClusterPositionRequest,
  responseType: api_pb.SetSynapseClusterPositionResponse
};
Node.CreateSTDPPreceptor = {
  methodName: "CreateSTDPPreceptor",
  service: Node,
  requestStream: false,
  responseStream: false,
  requestType: api_pb.CreateSTDPPreceptorRequest,
  responseType: api_pb.STDPPreceptorResponse
};
Node.DeleteSTDPPreceptor = {
  methodName: "DeleteSTDPPreceptor",
  service: Node,
  requestStream: false,
  responseStream: false,
  requestType: api_pb.DeleteSTDPPreceptorRequest,
  responseType: api_pb.DeleteSTDPPreceptorResponse
};
Node.AttachSTDPPreceptor = {
  methodName: "AttachSTDPPreceptor",
  service: Node,
  requestStream: false,
  responseStream: false,
  requestType: api_pb.AttachSTDPPreceptorRequest,
  responseType: api_pb.AttachSTDPPreceptorResponse
};
Node.DetachSTDPPreceptor = {
  methodName: "DetachSTDPPreceptor",
  service: Node,
  requestStream: false,
  responseStream: false,
  requestType: api_pb.DetachSTDPPreceptorRequest,
  responseType: api_pb.DetachSTDPPreceptorResponse
};
Node.SetSTDPPreceptorPosition = {
  methodName: "SetSTDPPreceptorPosition",
  service: Node,
  requestStream: false,
  responseStream: false,
  requestType: api_pb.SetSTDPPreceptorPositionRequest,
  responseType: api_pb.SetSTDPPreceptorPositionResponse
};
Node.CreateModulatedSTDPPreceptor = {
  methodName: "CreateModulatedSTDPPreceptor",
  service: Node,
  requestStream: false,
  responseStream: false,
  requestType: api_pb.CreateModulatedSTDPPreceptorRequest,
  responseType: api_pb.ModulatedSTDPPreceptorResponse
};
Node.DeleteModulatedSTDPPreceptor = {
  methodName: "DeleteModulatedSTDPPreceptor",
  service: Node,
  requestStream: false,
  responseStream: false,
  requestType: api_pb.DeleteModulatedSTDPPreceptorRequest,
  responseType: api_pb.DeleteModulatedSTDPPreceptorResponse
};
Node.AttachModulatedSTDPPreceptor = {
  methodName: "AttachModulatedSTDPPreceptor",
  service: Node,
  requestStream: false,
  responseStream: false,
  requestType: api_pb.AttachModulatedSTDPPreceptorRequest,
  responseType: api_pb.AttachModulatedSTDPPreceptorResponse
};
Node.DetachModulatedSTDPPreceptor = {
  methodName: "DetachModulatedSTDPPreceptor",
  service: Node,
  requestStream: false,
  responseStream: false,
  requestType: api_pb.DetachModulatedSTDPPreceptorRequest,
  responseType: api_pb.DetachModulatedSTDPPreceptorResponse
};
Node.SetModulatedSTDPPreceptorPosition = {
  methodName: "SetModulatedSTDPPreceptorPosition",
  service: Node,
  requestStream: false,
  responseStream: false,
  requestType: api_pb.SetModulatedSTDPPreceptorPositionRequest,
  responseType: api_pb.SetModulatedSTDPPreceptorPositionResponse
};
Node.CreateInputAdapter = {
  methodName: "CreateInputAdapter",
  service: Node,
  requestStream: false,
  responseStream: false,
  requestType: api_pb.CreateInputAdapterRequest,
  responseType: api_pb.InputAdapterResponse
};
Node.DeleteInputAdapter = {
  methodName: "DeleteInputAdapter",
  service: Node,
  requestStream: false,
  responseStream: false,
  requestType: api_pb.DeleteInputAdapterRequest,
  responseType: api_pb.DeleteInputAdapterResponse
};
Node.AttachInputAdapter = {
  methodName: "AttachInputAdapter",
  service: Node,
  requestStream: false,
  responseStream: false,
  requestType: api_pb.AttachInputAdapterRequest,
  responseType: api_pb.AttachInputAdapterResponse
};
Node.DetachInputAdapter = {
  methodName: "DetachInputAdapter",
  service: Node,
  requestStream: false,
  responseStream: false,
  requestType: api_pb.DetachInputAdapterRequest,
  responseType: api_pb.DetachInputAdapterResponse
};
Node.SetInputAdapterPosition = {
  methodName: "SetInputAdapterPosition",
  service: Node,
  requestStream: false,
  responseStream: false,
  requestType: api_pb.SetInputAdapterPositionRequest,
  responseType: api_pb.SetInputAdapterPositionResponse
};
Node.CreateOutputAdapter = {
  methodName: "CreateOutputAdapter",
  service: Node,
  requestStream: false,
  responseStream: false,
  requestType: api_pb.CreateOutputAdapterRequest,
  responseType: api_pb.OutputAdapterResponse
};
Node.DeleteOutputAdapter = {
  methodName: "DeleteOutputAdapter",
  service: Node,
  requestStream: false,
  responseStream: false,
  requestType: api_pb.DeleteOutputAdapterRequest,
  responseType: api_pb.DeleteOutputAdapterResponse
};
Node.AttachOutputAdapter = {
  methodName: "AttachOutputAdapter",
  service: Node,
  requestStream: false,
  responseStream: false,
  requestType: api_pb.AttachOutputAdapterRequest,
  responseType: api_pb.AttachOutputAdapterResponse
};
Node.DetachOutputAdapter = {
  methodName: "DetachOutputAdapter",
  service: Node,
  requestStream: false,
  responseStream: false,
  requestType: api_pb.DetachOutputAdapterRequest,
  responseType: api_pb.DetachOutputAdapterResponse
};
Node.SetOutputAdapterPosition = {
  methodName: "SetOutputAdapterPosition",
  service: Node,
  requestStream: false,
  responseStream: false,
  requestType: api_pb.SetOutputAdapterPositionRequest,
  responseType: api_pb.SetOutputAdapterPositionResponse
};
Node.CreateDopamineAdapter = {
  methodName: "CreateDopamineAdapter",
  service: Node,
  requestStream: false,
  responseStream: false,
  requestType: api_pb.CreateDopamineAdapterRequest,
  responseType: api_pb.DopamineAdapterResponse
};
Node.DeleteDopamineAdapter = {
  methodName: "DeleteDopamineAdapter",
  service: Node,
  requestStream: false,
  responseStream: false,
  requestType: api_pb.DeleteDopamineAdapterRequest,
  responseType: api_pb.DeleteDopamineAdapterResponse
};
Node.AttachDopamineAdapter = {
  methodName: "AttachDopamineAdapter",
  service: Node,
  requestStream: false,
  responseStream: false,
  requestType: api_pb.AttachDopamineAdapterRequest,
  responseType: api_pb.AttachDopamineAdapterResponse
};
Node.DetachDopamineAdapter = {
  methodName: "DetachDopamineAdapter",
  service: Node,
  requestStream: false,
  responseStream: false,
  requestType: api_pb.DetachDopamineAdapterRequest,
  responseType: api_pb.DetachDopamineAdapterResponse
};
Node.SetDopamineAdapterPosition = {
  methodName: "SetDopamineAdapterPosition",
  service: Node,
  requestStream: false,
  responseStream: false,
  requestType: api_pb.SetDopamineAdapterPositionRequest,
  responseType: api_pb.SetDopamineAdapterPositionResponse
};
Node.StreamInput = {
  methodName: "StreamInput",
  service: Node,
  requestStream: true,
  responseStream: false,
  requestType: api_pb.StreamInputRequest,
  responseType: api_pb.StreamInputResponse
};
Node.StreamOutput = {
  methodName: "StreamOutput",
  service: Node,
  requestStream: false,
  responseStream: true,
  requestType: api_pb.StreamOutputRequest,
  responseType: api_pb.StreamOutputResponse
};
Node.StreamDopamine = {
  methodName: "StreamDopamine",
  service: Node,
  requestStream: true,
  responseStream: false,
  requestType: api_pb.StreamDopamineRequest,
  responseType: api_pb.StreamDopamineResponse
};
Node.StreamNeuronSpikeEvent = {
  methodName: "StreamNeuronSpikeEvent",
  service: Node,
  requestStream: false,
  responseStream: true,
  requestType: api_pb.StreamNeuronSpikeEventRequest,
  responseType: api_pb.NeuronSpikeEvent
};
Node.StreamNeuronSpikeRelayEvent = {
  methodName: "StreamNeuronSpikeRelayEvent",
  service: Node,
  requestStream: false,
  responseStream: true,
  requestType: api_pb.StreamNeuronSpikeRelayEventRequest,
  responseType: api_pb.NeuronSpikeRelayEvent
};
Node.StreamSynapseWeightUpdateEvent = {
  methodName: "StreamSynapseWeightUpdateEvent",
  service: Node,
  requestStream: false,
  responseStream: true,
  requestType: api_pb.StreamSynapseWeightUpdateEventRequest,
  responseType: api_pb.SynapseWeightUpdateEvent
};
module.exports = {
  Node: Node,
};
