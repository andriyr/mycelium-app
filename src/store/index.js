import Api from "../Api";
import { createStore, applyMiddleware } from "redux";
import thunk from 'redux-thunk';
import initSubscriber from 'redux-subscriber';
import rootReducer from "../reducers/index";

const api = new Api("http://localhost:8080");

const store = createStore(rootReducer, applyMiddleware(thunk.withExtraArgument(api)));

const subscribe = initSubscriber(store);

window.store = store;

export default store;
