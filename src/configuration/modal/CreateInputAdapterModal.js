import React, { Component } from "react";
import { Modal, Form, Button, Icon } from 'semantic-ui-react'

class CreateInputAdapterModal extends Component {

    constructor(props) {
        super(props);
        this.state = {
            open: false,
            points: {},
            name: '',
            size: '',
            encodingWindow: '',
            nodeX: 0,
            nodeY: 0
        };

        this.open = this.open.bind(this);
        this.close = this.close.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleCreateClick = this.handleCreateClick.bind(this);
    }

    open(nodeX, nodeY) {
        this.setState({
            open: true,
            nodeX: nodeX,
            nodeY: nodeY
        });
    }

    close() {
        this.setState({open: false});
    }

    handleChange(e, {name, value}) {
        this.setState({ [name]: value });
    }

    handleCreateClick() {
        this.props.createInputAdapter(this.state.name, this.state.size, this.state.encodingWindow, this.state.nodeX, this.state.nodeY);
        this.resetForm();
        this.close();
    }

    resetForm() {
        this.setState({ name: '', size: '', encodingWindow: '' });
    }

    render() {
        const { name, size, encodingWindow } = this.state

        return(
            <Modal size="mini" open={this.state.open} onClose={this.close} ref="modal">
                <Modal.Header>Create Input Adapter</Modal.Header>
                <Modal.Content>
                    <Form>
                        <Form.Input required
                            placeholder='e.g. Input Adapter One'
                            label='Name'
                            name='name'
                            value={name}
                            onChange={this.handleChange}/>
                        <Form.Input required
                            placeholder='e.g. 500'
                            label='Size'
                            name='size'
                            value={size}
                            onChange={this.handleChange}/>
                        <Form.Input required
                            placeholder='e.g. 10'
                            label='Encoding Window (in ms)'
                            name='encodingWindow'
                            value={encodingWindow}
                            onChange={this.handleChange}/>
                        <div className="formButtonContainer">
                        <Button color='red' onClick={this.close}>
                            <Icon name='remove'/> Cancel
                        </Button>
                        <Button color='green' onClick={this.handleCreateClick}>
                            <Icon name='checkmark'/> Create
                        </Button>
                        </div>
                    </Form>
                </Modal.Content>
            </Modal>
        );
    }
}

export default CreateInputAdapterModal;
