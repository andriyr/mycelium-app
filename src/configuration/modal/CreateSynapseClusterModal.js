import React, { Component } from "react";
import { Modal, Form, Button, Icon } from 'semantic-ui-react'

class CreateSynapseClusterModal extends Component {

    constructor(props) {
        super(props);
        this.state = {
            open: false,
            name: '',
            neuronsCovered: '',
            initialSynapseWeight: '',
            nodeX: 0,
            nodeY: 0
        };

        this.open = this.open.bind(this);
        this.close = this.close.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleCreateClick = this.handleCreateClick.bind(this);
    }

    open(nodeX, nodeY) {
        this.setState({
            open: true,
            nodeX: nodeX,
            nodeY: nodeY
        });
    }

    close() {
        this.setState({open: false});
    }

    handleChange(e, {name, value}) {
        this.setState({ [name]: value });
    }

    handleCreateClick() {
        this.props.createSynapseCluster(this.state.name, this.state.neuronsCovered, this.state.initialSynapseWeight, this.state.nodeX, this.state.nodeY);
        this.resetForm();
        this.close();
    }

    resetForm() {
        this.setState({ name: '', neuronsCovered: '', initialSynapseWeight: '' });
    }

    render() {
        const {name, neuronsCovered, initialSynapseWeight} = this.state

        return(
            <Modal size="mini" open={this.state.open} onClose={this.close} ref="modal">
                <Modal.Header>Create Synapse Cluster</Modal.Header>
                <Modal.Content>
                    <Form>
                        <Form.Input required
                            placeholder='e.g. Synapse Cluster One'
                            label='Name'
                            name='name'
                            value={name}
                            onChange={this.handleChange}/>
                        <Form.Input required
                            placeholder='e.g. 100'
                            label='Neurons Covered'
                            name='neuronsCovered'
                            value={neuronsCovered}
                            onChange={this.handleChange}/>
                        <Form.Input required
                            placeholder='e.g. 0.5'
                            label='Initial Synapse Weight'
                            name='initialSynapseWeight'
                            value={initialSynapseWeight}
                            onChange={this.handleChange}/>
                        <div className="formButtonContainer">
                        <Button color='red' onClick={this.close}>
                            <Icon name='remove'/> Cancel
                        </Button>
                        <Button color='green' onClick={this.handleCreateClick}>
                            <Icon name='checkmark'/> Create
                        </Button>
                        </div>
                    </Form>
                </Modal.Content>
            </Modal>
        );
    }
}

export default CreateSynapseClusterModal;
