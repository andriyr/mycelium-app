import React, { Component } from "react";
import { Modal, Form, Button, Icon } from 'semantic-ui-react'

class CreateNeuronClusterModal extends Component {

    constructor(props) {
        super(props);
        this.state = {
            open: false,
            name: '',
            size: '',
            ratio: '',
            nodeX: 0,
            nodeY: 0
        };

        this.open = this.open.bind(this);
        this.close = this.close.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleCreateClick = this.handleCreateClick.bind(this);
    }

    open(nodeX, nodeY) {
        this.setState({
            open: true,
            nodeX: nodeX,
            nodeY: nodeY
        });
    }

    close() {
        this.setState({open: false});
    }

    handleChange(e, {name, value}) {
        this.setState({ [name]: value });
    }

    handleCreateClick() {
        this.props.createNeuronCluster(this.state.name, this.state.size, this.state.ratio, this.state.nodeX, this.state.nodeY);
        this.resetForm();
        this.close();
    }

    resetForm() {
        this.setState({ name: '', size: '', ratio: '' });
    }

    render() {
        const { name, size, ratio} = this.state

        return(
            <Modal size="mini" open={this.state.open} onClose={this.close} ref="modal">
                <Modal.Header>Create Neuron Cluster</Modal.Header>
                <Modal.Content>
                    <Form>
                        <Form.Input required
                            placeholder='e.g. Neuron Cluster One'
                            label='Name'
                            name='name'
                            value={name}
                            onChange={this.handleChange}/>
                        <Form.Input required
                            placeholder='e.g. 500'
                            label='Size'
                            name='size'
                            value={size}
                            onChange={this.handleChange}/>
                        <Form.Input required
                            placeholder='e.g. 0.8'
                            label='Ratio'
                            name='ratio'
                            value={ratio}
                            onChange={this.handleChange}/>
                        <div className="formButtonContainer">
                        <Button color='red' onClick={this.close}>
                            <Icon name='remove'/> Cancel
                        </Button>
                        <Button color='green' onClick={this.handleCreateClick}>
                            <Icon name='checkmark'/> Create
                        </Button>
                        </div>
                    </Form>
                </Modal.Content>
            </Modal>
        );
    }
}

export default CreateNeuronClusterModal;
