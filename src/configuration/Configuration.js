import React, { Component } from "react";
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {
    DiagramListener,
    LinkModelListener
} from "storm-react-diagrams";
import { Menu, Segment } from 'semantic-ui-react';
import { Slider } from 'react-semantic-ui-range';
import {
    ConcreteDiagramWidget,
	ConcreteDiagramModel,
	ConcreteDiagramEngine
} from "./graph/index";
import {
    loadNode,
    createNeuronCluster,
    deleteNeuronCluster,
    setNeuronClusterPosition,
    createSynapseCluster,
    deleteSynapseCluster,
    attachSynapseCluster,
    detachSynapseCluster,
    setSynapseClusterPosition,
    createSTDPPreceptor,
    deleteSTDPPreceptor,
    attachSTDPPreceptor,
    detachSTDPPreceptor,
    setSTDPPreceptorPosition,
    createModulatedSTDPPreceptor,
    deleteModulatedSTDPPreceptor,
    attachModulatedSTDPPreceptor,
    detachModulatedSTDPPreceptor,
    setModulatedSTDPPreceptorPosition,
    createDopamineAdapter,
    deleteDopamineAdapter,
    attachDopamineAdapter,
    detachDopamineAdapter,
    setDopamineAdapterPosition,
    createInputAdapter,
    deleteInputAdapter,
    attachInputAdapter,
    detachInputAdapter,
    setInputAdapterPosition,
    createOutputAdapter,
    deleteOutputAdapter,
    attachOutputAdapter,
    detachOutputAdapter,
    setOutputAdapterPosition
} from '../actions/index';
import CreateNeuronClusterModal from "./modal/CreateNeuronClusterModal";
import CreateSynapseClusterModal from "./modal/CreateSynapseClusterModal";
import CreateSTDPPreceptorModal from "./modal/CreateSTDPPreceptorModal";
import CreateModulatedSTDPPreceptorModal from "./modal/CreateModulatedSTDPPreceptorModal";
import CreateDopamineAdapterModal from "./modal/CreateDopamineAdapterModal";
import CreateInputAdapterModal from "./modal/CreateInputAdapterModal";
import CreateOutputAdapterModal from "./modal/CreateOutputAdapterModal";
import TrayWidget from "./tray/TrayWidget";
import TrayItemWidget from "./tray/TrayItemWidget";

class Configuration extends Component implements DiagramListener, LinkModelListener {

    constructor(props) {
        super(props);

        this.state = {
            zoomValue: 100
        };

        this.engine = new ConcreteDiagramEngine();
        this.model = new ConcreteDiagramModel();

        this.openNeuronClusterModal = this.openNeuronClusterModal.bind(this);
        this.createNeuronCluster = this.createNeuronCluster.bind(this);
        this.deleteNeuronCluster = this.deleteNeuronCluster.bind(this);

        this.openSynapseClusterModal = this.openSynapseClusterModal.bind(this);
        this.createSynapseCluster = this.createSynapseCluster.bind(this);
        this.deleteSynapseCluster = this.deleteSynapseCluster.bind(this);
        this.attachSynapseCluster = this.attachSynapseCluster.bind(this);
        this.detachSynapseCluster = this.detachSynapseCluster.bind(this);

        this.openSTDPPreceptorModal = this.openSTDPPreceptorModal.bind(this);
        this.createSTDPPreceptor = this.createSTDPPreceptor.bind(this);
        this.deleteSTDPPreceptor = this.deleteSTDPPreceptor.bind(this);
        this.attachSTDPPreceptor = this.attachSTDPPreceptor.bind(this);
        this.detachSTDPPreceptor = this.detachSTDPPreceptor.bind(this);

        this.openModulatedSTDPPreceptorModal = this.openModulatedSTDPPreceptorModal.bind(this);
        this.createModulatedSTDPPreceptor = this.createModulatedSTDPPreceptor.bind(this);
        this.deleteModulatedSTDPPreceptor = this.deleteModulatedSTDPPreceptor.bind(this);
        this.attachModulatedSTDPPreceptor = this.attachModulatedSTDPPreceptor.bind(this);
        this.detachModulatedSTDPPreceptor = this.detachModulatedSTDPPreceptor.bind(this);

        this.openDopamineAdapterModal = this.openDopamineAdapterModal.bind(this);
        this.createDopamineAdapter = this.createDopamineAdapter.bind(this);
        this.deleteDopamineAdapter = this.deleteDopamineAdapter.bind(this);
        this.attachDopamineAdapter = this.attachDopamineAdapter.bind(this);
        this.detachDopamineAdapter = this.detachDopamineAdapter.bind(this);

        this.openInputAdapterModal = this.openInputAdapterModal.bind(this);
        this.createInputAdapter = this.createInputAdapter.bind(this);
        this.deleteInputAdapter = this.deleteInputAdapter.bind(this);
        this.attachInputAdapter = this.attachInputAdapter.bind(this);
        this.detachInputAdapter = this.detachInputAdapter.bind(this);

        this.openOutputAdapterModal = this.openOutputAdapterModal.bind(this);
        this.createOutputAdapter = this.createOutputAdapter.bind(this);
        this.deleteOutputAdapter = this.deleteOutputAdapter.bind(this);
        this.attachOutputAdapter = this.attachOutputAdapter.bind(this);
        this.detachOutputAdapter = this.detachOutputAdapter.bind(this);

        this.updateNodePosition = this.updateNodePosition.bind(this);

        this.model.addListener(this);
        this.engine.setDiagramModel(this.model);

        this.props.loadNode();
    }

    nodesUpdated(event) {
        if (!event.isCreated) {
            switch(event.node.type) {
                case "neuronCluster":
                    this.deleteNeuronCluster(event.node);
                    break;
                case "synapseCluster":
                    this.deleteSynapseCluster(event.node);
                    break;
                case "STDPPreceptor":
                    this.deleteSTDPPreceptor(event.node);
                    break;
                case "modulatedSTDPPreceptor":
                    this.deleteModulatedSTDPPreceptor(event.node);
                    break;
                case "dopamineAdapter":
                    this.deleteDopamineAdapter(event.node);
                    break;
                case "inputAdapter":
                    this.deleteInputAdapter(event.node);
                    break;
                case "outputAdapter":
                    this.deleteOutputAdapter(event.node);
                    break;
                default:
                    //TODO
            }
        }
    }

    linksUpdated(event) {
        var source, target;

        if (event.isCreated) {
            event.link.addListener(this);
        }
        else
        {
            source = event.link.sourcePort.parentNode.type;
            target = event.link.targetPort.parentNode.type;

            if (source === "neuronCluster" && target === "synapseCluster") {
                this.detachSynapseCluster(event.link);
            }
            if (source === "synapseCluster" && target === "STDPPreceptor") {
                this.detachSTDPPreceptor(event.link);
            }
            if (source === "synapseCluster" && target === "modulatedSTDPPreceptor") {
                this.detachModulatedSTDPPreceptor(event.link);
            }
            if (source === "dopamineAdapter" && target === "modulatedSTDPPreceptor") {
                this.detachDopamineAdapter(event.link);
            }
            if (source === "inputAdapter" && target === "neuronCluster") {
                this.detachInputAdapter(event.link);
            }
            if (source === "neuronCluster" && target === "outputAdapter") {
                this.detachOutputAdapter(event.link);
            }
        }
    }

    targetPortChanged(event) {
        var source, target;
        var links = event.port.links;

        Object.keys(links).forEach((id) => {
            source = links[id].sourcePort.parentNode.type;
            target = links[id].targetPort.parentNode.type;

            if (source === "neuronCluster" && target === "synapseCluster") {
                this.attachSynapseCluster(links[id]);
            }
            if (source === "synapseCluster" && target === "STDPPreceptor") {
                this.attachSTDPPreceptor(links[id]);
            }
            if (source === "synapseCluster" && target === "modulatedSTDPPreceptor") {
                this.attachModulatedSTDPPreceptor(links[id]);
            }
            if (source === "dopamineAdapter" && target === "modulatedSTDPPreceptor") {
                this.attachDopamineAdapter(links[id]);
            }
            if (source === "inputAdapter" && target === "neuronCluster") {
                this.attachInputAdapter(links[id]);
            }
            if (source === "neuronCluster" && target === "outputAdapter") {
                this.attachOutputAdapter(links[id]);
            }
        });
    }

    updateNodePosition(event)
    {
        if (event.type === "neuronCluster") {
            this.props.setNeuronClusterPosition(event);
        }
        if (event.type === "synapseCluster") {
            this.props.setSynapseClusterPosition(event);
        }
        if (event.type === "STDPPreceptor") {
            this.props.setSTDPPreceptorPosition(event);
        }
        if (event.type === "modulatedSTDPPreceptor") {
            this.props.setModulatedSTDPPreceptorPosition(event);
        }
        if (event.type === "dopamineAdapter") {
            this.props.setDopamineAdapterPosition(event);
        }
        if (event.type === "inputAdapter") {
            this.props.setInputAdapterPosition(event);
        }
        if (event.type === "outputAdapter") {
            this.props.setOutputAdapterPosition(event);
        }
    }

    offsetUpdated(event) {
        // TODO
    }

	zoomUpdated(event) {
        // TODO
    }

	gridUpdated(event) {
        // TODO
    }

    openNeuronClusterModal(nodeX, nodeY) {
        this.refs.createNeuronClusterModal.open(nodeX, nodeY);
    }

    createNeuronCluster(node) {
        this.props.createNeuronCluster({name: node.name, size: node.size, ratio: node.ratio, x: node.x, y: node.y});
    }

    deleteNeuronCluster(node) {
        this.props.deleteNeuronCluster({id: node.id});
    }

    openSynapseClusterModal(nodeX, nodeY) {
        this.refs.createSynapseClusterModal.open(nodeX, nodeY);
    }

    createSynapseCluster(node) {
        this.props.createSynapseCluster({name: node.name, neuronsCovered: node.neuronsCovered, initialSynapseWeight: node.initialSynapseWeight, x: node.x, y: node.y});
    }

    deleteSynapseCluster(node) {
        this.props.deleteSynapseCluster({id: node.id});
    }

    attachSynapseCluster(link) {
        var synapseClusterId = link.targetPort.parentNode.id;
        var neuronClusterId = link.sourcePort.parentNode.id;
        this.props.attachSynapseCluster({synapseClusterId: synapseClusterId, neuronClusterId: neuronClusterId});
    }

    detachSynapseCluster(link) {
        var synapseClusterId = link.targetPort.parentNode.id;
        var neuronClusterId = link.sourcePort.parentNode.id;
        this.props.detachSynapseCluster({synapseClusterId: synapseClusterId, neuronClusterId: neuronClusterId});
    }

    openSTDPPreceptorModal(nodeX, nodeY) {
        this.refs.createSTDPPreceptorModal.open(nodeX, nodeY);
    }

    createSTDPPreceptor(node) {
        this.props.createSTDPPreceptor({name: node.name, maxQueueSize: node.maxQueueSize, x: node.x, y: node.y});
    }

    deleteSTDPPreceptor(node) {
        this.props.deleteSTDPPreceptor({id: node.id});
    }

    attachSTDPPreceptor(link) {
        var STDPPreceptorId = link.targetPort.parentNode.id;
        var synapseClusterId = link.sourcePort.parentNode.id;
        this.props.attachSTDPPreceptor({STDPPreceptorId: STDPPreceptorId, synapseClusterId: synapseClusterId});
    }

    detachSTDPPreceptor(link) {
        var STDPPreceptorId = link.targetPort.parentNode.id;
        this.props.detachSTDPPreceptor({STDPPreceptorId: STDPPreceptorId});
    }

    openModulatedSTDPPreceptorModal(nodeX, nodeY) {
        this.refs.createModulatedSTDPPreceptorModal.open(nodeX, nodeY);
    }

    createModulatedSTDPPreceptor(node) {
        this.props.createModulatedSTDPPreceptor({name: node.name, maxQueueSize: node.maxQueueSize, sensitivity: node.sensitivity, x: node.x, y: node.y});
    }

    deleteModulatedSTDPPreceptor(node) {
        this.props.deleteModulatedSTDPPreceptor({id: node.id});
    }

    attachModulatedSTDPPreceptor(link) {
        var modulatedSTDPPreceptorId = link.targetPort.parentNode.id;
        var synapseClusterId = link.sourcePort.parentNode.id;
        this.props.attachModulatedSTDPPreceptor({modulatedSTDPPreceptorId: modulatedSTDPPreceptorId, synapseClusterId: synapseClusterId});
    }

    detachModulatedSTDPPreceptor(link) {
        var modulatedSTDPPreceptorId = link.targetPort.parentNode.id;
        var synapseClusterId = link.sourcePort.parentNode.id;
        this.props.detachModulatedSTDPPreceptor({modulatedSTDPPreceptorId: modulatedSTDPPreceptorId, synapseClusterId: synapseClusterId});
    }

    openDopamineAdapterModal(nodeX, nodeY) {
        this.refs.createDopamineAdapterModal.open(nodeX, nodeY);
    }

    createDopamineAdapter(node) {
        this.props.createDopamineAdapter({name: node.name, x: node.x, y: node.y});
    }

    deleteDopamineAdapter(node) {
        this.props.deleteDopamineAdapter({id: node.id});
    }

    attachDopamineAdapter(link) {
        var dopamineAdapterId = link.sourcePort.parentNode.id;
        var modulatedSTDPPreceptorId = link.targetPort.parentNode.id;
        this.props.attachDopamineAdapter({dopamineAdapterId: dopamineAdapterId, modulatedSTDPPreceptorId: modulatedSTDPPreceptorId});
    }

    detachDopamineAdapter(link) {
        var dopamineAdapterId = link.sourcePort.parentNode.id;
        var modulatedSTDPPreceptorId = link.targetPort.parentNode.id;
        this.props.detachDopamineAdapter({dopamineAdapterId: dopamineAdapterId, modulatedSTDPPreceptorId: modulatedSTDPPreceptorId});
    }

    openInputAdapterModal(nodeX, nodeY) {
        this.refs.createInputAdapterModal.open(nodeX, nodeY);
    }

    createInputAdapter(node) {
        this.props.createInputAdapter({name: node.name, size: node.size, encodingWindow: node.encodingWindow, x: node.x, y: node.y});
    }

    deleteInputAdapter(node) {
        this.props.deleteInputAdapter({id: node.id});
    }

    attachInputAdapter(link) {
        var inputAdapterId = link.sourcePort.parentNode.id;
        var neuronClusterId = link.targetPort.parentNode.id;
        this.props.attachInputAdapter({inputAdapterId: inputAdapterId, neuronClusterId: neuronClusterId});
    }

    detachInputAdapter(link) {
        var inputAdapterId = link.sourcePort.parentNode.id;
        var neuronClusterId = link.targetPort.parentNode.id;
        this.props.detachInputAdapter({inputAdapterId: inputAdapterId, neuronClusterId: neuronClusterId});
    }

    openOutputAdapterModal(nodeX, nodeY) {
        this.refs.createOutputAdapterModal.open(nodeX, nodeY);
    }

    createOutputAdapter(node) {
        this.props.createOutputAdapter({name: node.name, size: node.size, decodingWindow: node.decodingWindow, x: node.x, y: node.y});
    }

    deleteOutputAdapter(node) {
        this.props.deleteOutputAdapter({id: node.id});
    }

    attachOutputAdapter(link) {
        var outputAdapterId = link.targetPort.parentNode.id;
        var neuronClusterId = link.sourcePort.parentNode.id;
        this.props.attachOutputAdapter({outputAdapterId: outputAdapterId, neuronClusterId: neuronClusterId});
    }

    detachOutputAdapter(link) {
        var outputAdapterId = link.targetPort.parentNode.id;
        this.props.detachOutputAdapter({outputAdapterId: outputAdapterId});
    }

    render() {
        this.model.rebuildGraph(this.props.neuronClusters, this.props.synapseClusters, this.props.STDPPreceptors, this.props.modulatedSTDPPreceptors,  this.props.dopamineAdapters, this.props.inputAdapters, this.props.outputAdapters);

        return (
            <div className="configuration">
                <div className="configurationMenu">
                    <Segment inverted attached>
                        <Menu inverted vertical>
                            <Menu.Item>
                                <Menu.Header>Zoom</Menu.Header>
                                <Menu.Menu>
                                    <Slider color="grey" inverted={false} settings={{
                                        start: this.state.zoomValue,
                                        min: 50,
                                        max: 150,
                                        step: 1,
                                        onChange: (value) => {
                                            this.setState({
                                                zoomValue: value
                                            })
                                            this.model.setZoomLevel(value);
                                        },
                                    }}/>
                                </Menu.Menu>
                            </Menu.Item>
                            <Menu.Item>
                                <Menu.Header>Add Component</Menu.Header>
                                <Menu.Menu>
                                    <TrayWidget>
                                        <TrayItemWidget model={{ type: "neuronCluster" }} name="Neuron Cluster" color="rgb(0,192,255)" />
                                        <TrayItemWidget model={{ type: "synapseCluster" }} name="Synapse Cluster" color="rgb(192,255,0)" />
                                        <TrayItemWidget model={{ type: "STDPPreceptor" }} name="STDP Preceptor" color="rgb(192,0,0)" />
                                        <TrayItemWidget model={{ type: "modulatedSTDPPreceptor" }} name="Modulated STDP Preceptor" color="rgb(192,0,255)" />
                                        <TrayItemWidget model={{ type: "dopamineAdapter" }} name="Dopamine Adapter" color="rgb(0,0)" />
                                        <TrayItemWidget model={{ type: "inputAdapter" }} name="Input Adapter" color="rgb(0,0)" />
                                        <TrayItemWidget model={{ type: "outputAdapter" }} name="Output Adapter" color="rgb(0,0)" />
                                    </TrayWidget>
                                    <CreateNeuronClusterModal ref="createNeuronClusterModal" createNeuronCluster={(name, size, ratio, x, y) => {
										this.createNeuronCluster({name: name, size: size, ratio: ratio, x: x, y: y});
									}}/>
                                    <CreateSynapseClusterModal ref="createSynapseClusterModal" createSynapseCluster={(name, neuronsCovered, initialSynapseWeight, x, y) => {
										this.createSynapseCluster({name: name, neuronsCovered: neuronsCovered, initialSynapseWeight: initialSynapseWeight, x: x, y: y});
                                    }}/>
                                    <CreateSTDPPreceptorModal ref="createSTDPPreceptorModal" createSTDPPreceptor={(name, maxQueueSize, x, y) => {
										this.createSTDPPreceptor({name: name, maxQueueSize: maxQueueSize, x: x, y: y});
                                    }}/>
                                    <CreateModulatedSTDPPreceptorModal ref="createModulatedSTDPPreceptorModal" createModulatedSTDPPreceptor={(name, maxQueueSize, sensitivity, x, y) => {
										this.createModulatedSTDPPreceptor({name: name, maxQueueSize: maxQueueSize, sensitivity: sensitivity, x: x, y: y});
                                    }}/>
                                    <CreateDopamineAdapterModal ref="createDopamineAdapterModal" createDopamineAdapter={(name, x, y) => {
										this.createDopamineAdapter({name: name, x: x, y: y});
									}}/>
                                    <CreateInputAdapterModal ref="createInputAdapterModal" createInputAdapter={(name, size, encodingWindow, x, y) => {
										this.createInputAdapter({name: name, size: size, encodingWindow: encodingWindow, x: x, y: y});
									}}/>
                                    <CreateOutputAdapterModal ref="createOutputAdapterModal" createOutputAdapter={(name, size, decodingWindow, x, y) => {
										this.createOutputAdapter({name: name, size: size, decodingWindow: decodingWindow, x: x, y: y});
									}}/>
                                </Menu.Menu>
                            </Menu.Item>
                        </Menu>
                    </Segment>
                </div>
                <div className="configurationContent"
                    onDrop={event => {
                        var data = JSON.parse(event.dataTransfer.getData("storm-diagram-node"));

                        var points = this.engine.getRelativeMousePoint(event);

						switch(data.type) {
							case "neuronCluster":
								this.openNeuronClusterModal(points.x, points.y);
								break;
							case "synapseCluster":
								this.openSynapseClusterModal(points.x, points.y);
								break;
                            case "STDPPreceptor":
    							this.openSTDPPreceptorModal(points.x, points.y);
    							break;
                            case "modulatedSTDPPreceptor":
        						this.openModulatedSTDPPreceptorModal(points.x, points.y);
        						break;
                            case "dopamineAdapter":
    							this.openDopamineAdapterModal(points.x, points.y);
    							break;
							case "inputAdapter":
								this.openInputAdapterModal(points.x, points.y);
								break;
							case "outputAdapter":
								this.openOutputAdapterModal(points.x, points.y);
								break;
							default:
								// TODO
						}
                    }}
                    onDragOver={event => {
                        event.preventDefault();
                    }}
                >
                    <ConcreteDiagramWidget diagramEngine={this.engine} updateNodePosition={this.updateNodePosition}/>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        neuronClusters: state.configuration.neuronClusters,
        synapseClusters: state.configuration.synapseClusters,
        STDPPreceptors: state.configuration.STDPPreceptors,
        modulatedSTDPPreceptors: state.configuration.modulatedSTDPPreceptors,
        dopamineAdapters: state.configuration.dopamineAdapters,
        inputAdapters: state.configuration.inputAdapters,
        outputAdapters: state.configuration.outputAdapters,
        nodeLoaded: state.configuration.nodeLoaded
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        loadNode: loadNode,
        createNeuronCluster: createNeuronCluster,
        deleteNeuronCluster: deleteNeuronCluster,
        setNeuronClusterPosition: setNeuronClusterPosition,
        createSynapseCluster: createSynapseCluster,
        deleteSynapseCluster: deleteSynapseCluster,
        attachSynapseCluster: attachSynapseCluster,
        detachSynapseCluster: detachSynapseCluster,
        setSynapseClusterPosition: setSynapseClusterPosition,
        createSTDPPreceptor: createSTDPPreceptor,
        deleteSTDPPreceptor: deleteSTDPPreceptor,
        attachSTDPPreceptor: attachSTDPPreceptor,
        detachSTDPPreceptor: detachSTDPPreceptor,
        setSTDPPreceptorPosition: setSTDPPreceptorPosition,
        createModulatedSTDPPreceptor: createModulatedSTDPPreceptor,
        deleteModulatedSTDPPreceptor: deleteModulatedSTDPPreceptor,
        attachModulatedSTDPPreceptor: attachModulatedSTDPPreceptor,
        detachModulatedSTDPPreceptor: detachModulatedSTDPPreceptor,
        setModulatedSTDPPreceptorPosition: setModulatedSTDPPreceptorPosition,
        createDopamineAdapter: createDopamineAdapter,
        deleteDopamineAdapter: deleteDopamineAdapter,
        attachDopamineAdapter: attachDopamineAdapter,
        detachDopamineAdapter: detachDopamineAdapter,
        setDopamineAdapterPosition: setDopamineAdapterPosition,
        createInputAdapter: createInputAdapter,
        deleteInputAdapter: deleteInputAdapter,
        attachInputAdapter: attachInputAdapter,
        detachInputAdapter: detachInputAdapter,
        setInputAdapterPosition: setInputAdapterPosition,
        createOutputAdapter: createOutputAdapter,
        deleteOutputAdapter: deleteOutputAdapter,
        attachOutputAdapter: attachOutputAdapter,
        detachOutputAdapter: detachOutputAdapter,
        setOutputAdapterPosition: setOutputAdapterPosition
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Configuration)
