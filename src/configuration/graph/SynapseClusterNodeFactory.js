import * as React from "react";
import {
	DiagramEngine,
	NodeFactory
} from "storm-react-diagrams";
import SynapseClusterNodeModel from "./SynapseClusterNodeModel";
import SynapseClusterNodeWidget from "./SynapseClusterNodeWidget";

export default class SynapseClusterNodeFactory extends NodeFactory<SynapseClusterNodeModel> {
	constructor() {
		super("synapseCluster");
	}

	generateReactWidget(diagramEngine: DiagramEngine, node: SynapseClusterNodeModel): JSX.Element {
		return React.createElement(SynapseClusterNodeWidget, {
			node: node,
			diagramEngine: diagramEngine
		});
	}

	getNewInstance(initialConfig?: any): DefaultNodeModel {
		return new SynapseClusterNodeModel();
	}
}
