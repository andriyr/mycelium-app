import * as React from "react";
import * as _ from "lodash";
import {
	DefaultPortLabel,
	DiagramEngine
} from "storm-react-diagrams";
import OutputAdapterNodeModel from "./OutputAdapterNodeModel";

export interface OutputAdapterNodeProps {
	node: OutputAdapterNodeModel;
	diagramEngine: DiagramEngine;
}

export interface DefaultNodeState {}

export default class OutputAdapterNodeWidget extends React.Component<OutputAdapterNodeProps, DefaultNodeState> {
	constructor(props: OutputAdapterNodeProps) {
		super(props);
		this.state = {};
	}

	generatePort(port) {
		return <DefaultPortLabel model={port} key={port.id} />;
	}

	render() {
		return (
			<div className="basic-node" style={{ background: "rgb(0,0)" }}>
				<div className="title">
					<div className="name">{this.props.node.name}</div>
					<div className="id">{this.props.node.id}</div>
				</div>
                <div className="properties">
                    <div className="property">Size: {this.props.node.size}</div>
					<div className="property">Decoding Window: {this.props.node.decodingWindow}</div>
                </div>
				<div className="ports">
					<div className="in">{_.map(this.props.node.getInPorts(), this.generatePort.bind(this))}</div>
					<div className="out">{_.map(this.props.node.getOutPorts(), this.generatePort.bind(this))}</div>
				</div>
			</div>
		);
	}
}
