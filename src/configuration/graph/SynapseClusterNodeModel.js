import * as _ from "lodash";

import {
	NodeModel
} from "storm-react-diagrams";
import ConcretePortModel from "./ConcretePortModel";

export default class SynapseClusterNodeModel extends NodeModel {
	id: string;
	name: string;
	neuronsCovered: number;
	initialSynapseWeight: number;
	ports: { [s: string]: ConcretePortModel };

	constructor(id: string = "", name: string = "", neuronsCovered: number = 0, initialSynapseWeight: number = 0, x: number = 0, y: number = 0) {
		super("synapseCluster");
		this.id = id;
		this.name = name;
		this.neuronsCovered = neuronsCovered;
		this.initialSynapseWeight = initialSynapseWeight;
		this.x = x;
		this.y = y;

		this.addPort(new ConcretePortModel(true, "in", "Neuron Cluster(s)"));
		this.addPort(new ConcretePortModel(false, "out", "Out"));
	}

	setPosition(x, y) {
		super.setPosition(x, y);
		this.iterateListeners((listener, event) => {
			if (listener.positionUpdated) {
				listener.positionUpdated({ ...event, x: x, y: y });
			}
		});
	}

	deSerialize(object) {
		super.deSerialize(object);
		this.id = object.id;
		this.name = object.name;
		this.neuronsCovered = object.neuronsCovered;
		this.initialSynapseWeight = object.initialSynapseWeight;
	}

	serialize() {
		return _.merge(super.serialize(), {
			id: this.id,
			name: this.name,
			neuronsCovered: this.neuronsCovered,
			initialSynapseWeight: this.initialSynapseWeight
		});
	}

	getInPorts(): ConcretePortModel[] {
		return _.filter(this.ports, portModel => {
			return portModel.in;
		});
	}

	getOutPorts(): ConcretePortModel[] {
		return _.filter(this.ports, portModel => {
			return !portModel.in;
		});
	}
}
