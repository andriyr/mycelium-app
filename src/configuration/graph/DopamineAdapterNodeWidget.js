import * as React from "react";
import * as _ from "lodash";
import {
	DefaultPortLabel,
	DiagramEngine
} from "storm-react-diagrams";
import DopamineAdapterNodeModel from "./DopamineAdapterNodeModel";

export interface DopamineAdapterNodeProps {
	node: DopamineAdapterNodeModel;
	diagramEngine: DiagramEngine;
}

export interface DefaultNodeState {}

export default class DopamineAdapterNodeWidget extends React.Component<DopamineAdapterNodeProps, DefaultNodeState> {
	constructor(props: DopamineAdapterNodeProps) {
		super(props);
		this.state = {};
	}

	generatePort(port) {
		return <DefaultPortLabel model={port} key={port.id} />;
	}

	render() {
		return (
			<div className="basic-node" style={{ background: "rgb(0,0)" }}>
				<div className="title">
					<div className="name">{this.props.node.name}</div>
					<div className="id">{this.props.node.id}</div>
				</div>
				<div className="ports">
					<div className="out">{_.map(this.props.node.getOutPorts(), this.generatePort.bind(this))}</div>
				</div>
			</div>
		);
	}
}
