import {
	DiagramWidget,
    NodeModel
} from "storm-react-diagrams";

export default class ConcreteDiagramWidget extends DiagramWidget {

    onMouseUp(event) {
        super.onMouseUp(event);

        var element = this.getMouseElement(event);

        if (element && element.model instanceof NodeModel) {
            var model = element.model;
            this.props.updateNodePosition({id: model.id, type: model.type, x: model.x, y: model.y});
        }
    }

}
