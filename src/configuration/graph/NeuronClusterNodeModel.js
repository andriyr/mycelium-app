import * as _ from "lodash";

import {
	NodeModel
} from "storm-react-diagrams";
import ConcretePortModel from "./ConcretePortModel";

export default class NeuronClusterNodeModel extends NodeModel {
	id: string;
	name: string;
	size: number;
	ratio: number;
	neuronIds: string[];
	ports: { [s: string]: ConcretePortModel };

	constructor(id: string = "", name: string = "", size: string = "", ratio: string = "", neuronIds: string[] = [], x: number = 0, y: number = 0) {
		super("neuronCluster");
		this.id = id;
		this.name = name;
		this.size = size;
		this.ratio = ratio;
		this.neuronIds = neuronIds;
		this.x = x;
		this.y = y;

		this.addPort(new ConcretePortModel(true, "in", "Input Adapter(s)"));
        this.addPort(new ConcretePortModel(false, "out", "Out"));
	}

	setPosition(x, y) {
		super.setPosition(x, y);
		this.iterateListeners((listener, event) => {
			if (listener.positionUpdated) {
				listener.positionUpdated({ ...event, x: x, y: y });
			}
		});
	}

	deSerialize(object) {
		super.deSerialize(object);
		this.id = object.id;
		this.name = object.name;
		this.size = object.size;
		this.ratio = object.ratio;
		this.neuronIds = object.neuronIds;
	}

	serialize() {
		return _.merge(super.serialize(), {
			id: this.id,
			name: this.name,
			size: this.size,
			ratio: this.ratio,
			neuronIds: this.neuronIds
		});
	}

	getInPorts(): ConcretePortModel[] {
		return _.filter(this.ports, portModel => {
			return portModel.in;
		});
	}

	getOutPorts(): ConcretePortModel[] {
		return _.filter(this.ports, portModel => {
			return !portModel.in;
		});
	}
}
