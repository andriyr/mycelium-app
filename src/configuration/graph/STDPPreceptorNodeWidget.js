import * as React from "react";
import * as _ from "lodash";
import {
	DefaultPortLabel,
	DiagramEngine
} from "storm-react-diagrams";
import STDPPreceptorNodeModel from "./STDPPreceptorNodeModel";

export interface STDPPreceptorNodeProps {
	node: STDPPreceptorNodeModel;
	diagramEngine: DiagramEngine;
}

export interface DefaultNodeState {}

export default class STDPPreceptorNodeWidget extends React.Component<STDPPreceptorNodeProps, DefaultNodeState> {
	constructor(props: STDPPreceptorNodeProps) {
		super(props);
		this.state = {};
	}

	generatePort(port) {
		return <DefaultPortLabel model={port} key={port.id} />;
	}

	render() {
		return (
			<div className="basic-node" style={{ background: "rgb(192,0,0)" }}>
				<div className="title">
					<div className="name">{this.props.node.name}</div>
					<div className="id">{this.props.node.id}</div>
				</div>
                <div className="properties">
                    <div className="property">Max Queue size: {this.props.node.maxQueueSize}</div>
                </div>
				<div className="ports">
					<div className="in">{_.map(this.props.node.getInPorts(), this.generatePort.bind(this))}</div>
					<div className="out">{_.map(this.props.node.getOutPorts(), this.generatePort.bind(this))}</div>
				</div>
			</div>
		);
	}
}
