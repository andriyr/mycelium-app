import * as React from "react";
import * as _ from "lodash";
import {
	DefaultPortLabel,
	DiagramEngine
} from "storm-react-diagrams";
import ModulatedSTDPPreceptorNodeModel from "./ModulatedSTDPPreceptorNodeModel";

export interface ModulatedSTDPPreceptorNodeProps {
	node: ModulatedSTDPPreceptorNodeModel;
	diagramEngine: DiagramEngine;
}

export interface DefaultNodeState {}

export default class ModulatedSTDPPreceptorNodeWidget extends React.Component<ModulatedSTDPPreceptorNodeProps, DefaultNodeState> {
	constructor(props: ModulatedSTDPPreceptorNodeProps) {
		super(props);
		this.state = {};
	}

	generatePort(port) {
		return <DefaultPortLabel model={port} key={port.id} />;
	}

	render() {
		return (
			<div className="basic-node" style={{ background: "rgb(192,0,255)" }}>
				<div className="title">
					<div className="name">{this.props.node.name}</div>
					<div className="id">{this.props.node.id}</div>
				</div>
                <div className="properties">
					<div className="property">Max Queue Size: {this.props.node.maxQueueSize}</div>
                    <div className="property">Sensitivity: {this.props.node.sensitivity}</div>
                </div>
				<div className="ports">
					<div className="in">{_.map(this.props.node.getInPorts(), this.generatePort.bind(this))}</div>
					<div className="out">{_.map(this.props.node.getOutPorts(), this.generatePort.bind(this))}</div>
				</div>
			</div>
		);
	}
}
