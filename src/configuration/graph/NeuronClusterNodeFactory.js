import * as React from "react";
import {
	DiagramEngine,
	NodeFactory
} from "storm-react-diagrams";
import NeuronClusterNodeModel from "./NeuronClusterNodeModel";
import NeuronClusterNodeWidget from "./NeuronClusterNodeWidget";

export default class NeuronClusterNodeFactory extends NodeFactory<NeuronClusterNodeModel> {
	constructor() {
		super("neuronCluster");
	}

	generateReactWidget(diagramEngine: DiagramEngine, node: NeuronClusterNodeModel): JSX.Element {
		return React.createElement(NeuronClusterNodeWidget, {
			node: node,
			diagramEngine: diagramEngine
		});
	}

	getNewInstance(initialConfig?: any): DefaultNodeModel {
		return new NeuronClusterNodeModel();
	}
}
