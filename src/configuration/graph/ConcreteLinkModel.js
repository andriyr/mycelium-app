import { LinkModel } from "storm-react-diagrams";
import ConcretePortModel from "./ConcretePortModel";

export default class ConcreteLinkModel extends LinkModel {
	constructor() {
		super("concrete");
	}

	setSourcePort(port: ConcretePortModel) {
		if (port.in) {
			super.setTargetPort(port);
		} else {
			super.setSourcePort(port);
		}
	}

	setTargetPort(port: ConcretePortModel) {
		if (port.in) {
			super.setTargetPort(port);
		} else {
			super.setSourcePort(port);
		}
	}
}
