import * as _ from "lodash";

import {
	NodeModel
} from "storm-react-diagrams";
import ConcretePortModel from "./ConcretePortModel";

export default class ModulatedSTDPPreceptorNodeModel extends NodeModel {
	id: string;
	name: string;
	maxQueueSize: number;
	sensitivity: number;
	ports: { [s: string]: ConcretePortModel };

	constructor(id: string="", name: string="", maxQueueSize: number=0, sensitivity: number=0, x: number=0, y: number=0) {
		super("modulatedSTDPPreceptor");
		this.id = id;
		this.name = name;
		this.maxQueueSize = maxQueueSize;
		this.sensitivity = sensitivity;
		this.x = x;
		this.y = y;

		this.addPort(new ConcretePortModel(true, "in", "Synapse Cluster/Dopamine Adapter"));
	}

	setPosition(x, y) {
		super.setPosition(x, y);
		this.iterateListeners((listener, event) => {
			if (listener.positionUpdated) {
				listener.positionUpdated({ ...event, x: x, y: y });
			}
		});
	}

	deSerialize(object) {
		super.deSerialize(object);
		this.id = object.id;
		this.name = object.name;
		this.maxQueueSize = object.maxQueueSize;
		this.sensitivity = object.sensitivity;
	}

	serialize() {
		return _.merge(super.serialize(), {
			id: this.id,
			name: this.name,
			maxQueueSize: this.maxQueueSize,
			sensitivity: this.sensitivity
		});
	}

	getInPorts(): ConcretePortModel[] {
		return _.filter(this.ports, portModel => {
			return portModel.in;
		});
	}

	getOutPorts(): ConcretePortModel[] {
		return _.filter(this.ports, portModel => {
			return !portModel.in;
		});
	}
}
