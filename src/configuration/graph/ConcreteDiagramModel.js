import { DiagramModel } from "storm-react-diagrams";
import {
    ConcreteLinkModel,
    NeuronClusterNodeModel,
    SynapseClusterNodeModel,
    STDPPreceptorNodeModel,
    ModulatedSTDPPreceptorNodeModel,
    DopamineAdapterNodeModel,
    InputAdapterNodeModel,
    OutputAdapterNodeModel
} from "./index";

export default class ConcreteDiagramModel extends DiagramModel {

    rebuildGraph(neuronClusters, synapseClusters, STDPPreceptors, modulatedSTDPPreceptors, dopamineAdapters, inputAdapters, outputAdapters) {
        neuronClusters.forEach((cluster) => {
            var node = new NeuronClusterNodeModel(cluster.id, cluster.name, cluster.size, cluster.ratio, cluster.neuronIds, cluster.x, cluster.y);
            node.addListener({
    			entityRemoved: () => {
    				this.removeNode(node);
    			}
    		});
    		this.nodes[node.getID()] = node;
        });

        synapseClusters.forEach((cluster) => {
            var node = new SynapseClusterNodeModel(cluster.id, cluster.name, cluster.neuronsCovered, cluster.initialSynapseWeight, cluster.x, cluster.y);
            node.addListener({
    			entityRemoved: () => {
    				this.removeNode(node);
    			}
    		});
    		this.nodes[node.getID()] = node;
        });

        STDPPreceptors.forEach((STDPPreceptor) => {
            var node = new STDPPreceptorNodeModel(STDPPreceptor.id, STDPPreceptor.name, STDPPreceptor.maxQueueSize, STDPPreceptor.x, STDPPreceptor.y);
            node.addListener({
    			entityRemoved: () => {
    				this.removeNode(node);
    			}
    		});
    		this.nodes[node.getID()] = node;
        });

        modulatedSTDPPreceptors.forEach((modulatedSTDPPreceptor) => {
            var node = new ModulatedSTDPPreceptorNodeModel(modulatedSTDPPreceptor.id, modulatedSTDPPreceptor.name, modulatedSTDPPreceptor.maxQueueSize, modulatedSTDPPreceptor.sensitivity, modulatedSTDPPreceptor.x, modulatedSTDPPreceptor.y);
            node.addListener({
    			entityRemoved: () => {
    				this.removeNode(node);
    			}
    		});
    		this.nodes[node.getID()] = node;
        });

        dopamineAdapters.forEach((adapter) => {
            var node = new DopamineAdapterNodeModel(adapter.id, adapter.name, adapter.x, adapter.y);
            node.addListener({
    			entityRemoved: () => {
    				this.removeNode(node);
    			}
    		});
    		this.nodes[node.getID()] = node;
        });

        inputAdapters.forEach((adapter) => {
            var node = new InputAdapterNodeModel(adapter.id, adapter.name, adapter.size, adapter.encodingWindow, adapter.x, adapter.y);
            node.addListener({
    			entityRemoved: () => {
    				this.removeNode(node);
    			}
    		});
    		this.nodes[node.getID()] = node;
        });

        outputAdapters.forEach((adapter) => {
            var node = new OutputAdapterNodeModel(adapter.id, adapter.name, adapter.size, adapter.decodingWindow, adapter.x, adapter.y);
            node.addListener({
    			entityRemoved: () => {
    				this.removeNode(node);
    			}
    		});
    		this.nodes[node.getID()] = node;
        });


        for (const [id, link] of Object.entries(this.getLinks())) {
            delete this.links[id];
        }

        neuronClusters.forEach((neuronCluster) => {
            neuronCluster.inputAdapterIds.forEach((inputAdapterId) => {
                var neuronClusterNode = this.getNode(neuronCluster.id);
                var inputAdapterNode = this.getNode(inputAdapterId);

                if (neuronClusterNode != null && inputAdapterNode != null) {
                    var link = new ConcreteLinkModel();
                    link.setSourcePort(inputAdapterNode.ports.out);
                    link.setTargetPort(neuronClusterNode.ports.in);
                    link.addListener({
                    	entityRemoved: () => {
                    		this.removeLink(link);
                    	}
                    });
                    this.links[link.getID()] = link;
                }
            });
        });

        outputAdapters.forEach((outputAdapter) => {
            if (outputAdapter.neuronClusterId) {
                var neuronClusterNode = this.getNode(outputAdapter.neuronClusterId);
                var outputAdapterNode = this.getNode(outputAdapter.id);

                if (neuronClusterNode != null && outputAdapterNode != null) {
                    var link = new ConcreteLinkModel();
                    link.setSourcePort(neuronClusterNode.ports.out);
                    link.setTargetPort(outputAdapterNode.ports.in);
                    link.addListener({
                		entityRemoved: () => {
                			this.removeLink(link);
                		}
                	});
                	this.links[link.getID()] = link;
                }
            }
        });

        synapseClusters.forEach((synapseCluster) => {
            synapseCluster.neuronClusterIds.forEach((neuronClusterId) => {
                var neuronClusterNode = this.getNode(neuronClusterId);
                var synapseClusterNode = this.getNode(synapseCluster.id);

                if (neuronClusterNode != null && synapseClusterNode != null) {
                    var link = new ConcreteLinkModel();
                    link.setSourcePort(neuronClusterNode.ports.out);
                    link.setTargetPort(synapseClusterNode.ports.in);
                    link.addListener({
                    	entityRemoved: () => {
                    		this.removeLink(link);
                    	}
                    });
                    this.links[link.getID()] = link;
                }
            });
        });

        STDPPreceptors.forEach((STDPPreceptor) => {
            if (STDPPreceptor.synapseClusterId) {
                var synapseClusterNode = this.getNode(STDPPreceptor.synapseClusterId);
                var STDPPreceptorNode = this.getNode(STDPPreceptor.id);

                if (synapseClusterNode != null && STDPPreceptorNode != null) {
                    var link = new ConcreteLinkModel();
                    link.setSourcePort(synapseClusterNode.ports.out);
                    link.setTargetPort(STDPPreceptorNode.ports.in);
                    link.addListener({
            			entityRemoved: () => {
            				this.removeLink(link);
            			}
            		});
            		this.links[link.getID()] = link;
                }
            }
        });

        modulatedSTDPPreceptors.forEach((modulatedSTDPPreceptor) => {
            if (modulatedSTDPPreceptor.synapseClusterId) {
                var synapseClusterNode = this.getNode(modulatedSTDPPreceptor.synapseClusterId);
                var modulatedSTDPPreceptorNode = this.getNode(modulatedSTDPPreceptor.id);

                if (synapseClusterNode != null && modulatedSTDPPreceptorNode != null) {
                    var link = new ConcreteLinkModel();
                    link.setSourcePort(synapseClusterNode.ports.out);
                    link.setTargetPort(modulatedSTDPPreceptorNode.ports.in);
                    link.addListener({
            			entityRemoved: () => {
            				this.removeLink(link);
            			}
            		});
            		this.links[link.getID()] = link;
                }
            }

            if (modulatedSTDPPreceptor.dopamineAdapterId) {
                var dopamineAdapterNode = this.getNode(modulatedSTDPPreceptor.dopamineAdapterId);
                var modulatedSTDPPreceptorNode = this.getNode(modulatedSTDPPreceptor.id);

                if (dopamineAdapterNode != null && modulatedSTDPPreceptorNode != null) {
                    var link = new ConcreteLinkModel();
                    link.setSourcePort(dopamineAdapterNode.ports.out);
                    link.setTargetPort(modulatedSTDPPreceptorNode.ports.in);
                    link.addListener({
            			entityRemoved: () => {
            				this.removeLink(link);
            			}
            		});
            		this.links[link.getID()] = link;
                }
            }
        });
    }
}
