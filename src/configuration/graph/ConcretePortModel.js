import * as _ from "lodash";
import { PortModel } from "storm-react-diagrams";
import ConcreteLinkModel from "./ConcreteLinkModel";

export default class ConcretePortModel extends PortModel {
	in: boolean;
	label: string;

	constructor(isInput: boolean, name: string, label: string = null, id?: string) {
		super(name, "concrete", id);
		this.in = isInput;
		this.label = label || name;
	}

	deSerialize(object) {
		super.deSerialize(object);
		this.in = object.in;
		this.label = object.label;
	}

	createLinkModel(): ConcreteLinkModel | null {
		var concreteLinkModel =  new ConcreteLinkModel();
		concreteLinkModel.setSourcePort(this);
		return concreteLinkModel;
	}

	serialize() {
		return _.merge(super.serialize(), {
			in: this.in,
			label: this.label
		});
	}
}
