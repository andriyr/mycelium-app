import * as _ from "lodash";

import {
	NodeModel
} from "storm-react-diagrams";
import ConcretePortModel from "./ConcretePortModel";

export default class InputAdapterNodeModel extends NodeModel {
	id: string;
	name: string;
	size: number;
	enodingWindow: number;
	ports: { [s: string]: ConcretePortModel };

	constructor(id: string="", name: string="", size: number=0, encodingWindow: encodingWindow=0 , x: number=0, y: number=0) {
		super("inputAdapter");
		this.id = id;
		this.name = name;
		this.size = size;
		this.encodingWindow = encodingWindow;
		this.x = x;
		this.y = y;

		this.addPort(new ConcretePortModel(false, "out", "Out"));
	}

	setPosition(x, y) {
		super.setPosition(x, y);
		this.iterateListeners((listener, event) => {
			if (listener.positionUpdated) {
				listener.positionUpdated({ ...event, x: x, y: y });
			}
		});
	}

	deSerialize(object) {
		super.deSerialize(object);
		this.id = object.id;
		this.name = object.name;
		this.size = object.size;
		this.encodingWindow = object.encodingWindow;
	}

	serialize() {
		return _.merge(super.serialize(), {
			id: this.id,
			name: this.name,
			size: this.size,
			encodingWindow: this.encodingWindow
		});
	}

	getInPorts(): ConcretePortModel[] {
		return _.filter(this.ports, portModel => {
			return portModel.in;
		});
	}

	getOutPorts(): ConcretePortModel[] {
		return _.filter(this.ports, portModel => {
			return !portModel.in;
		});
	}
}
