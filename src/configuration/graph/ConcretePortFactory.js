import { PortFactory } from "storm-react-diagrams";
import ConcretePortModel from "./ConcretePortModel";

export default class ConcretePortFactory extends PortFactory<ConcretePortModel> {
	constructor() {
		super("concrete");
	}

	getNewInstance(initialConfig?: any): ConcretePortModel {
		return new ConcretePortModel(true, "unknown");
	}
}
