import * as React from "react";
import { LinkFactory, LinkModel, DefaultLinkWidget } from "storm-react-diagrams";
import ConcreteLinkModel from "./ConcreteLinkModel";

export default class ConcreteLinkFactory extends LinkFactory {
	constructor() {
		super("concrete");
	}

	generateReactWidget(diagramEngine: DiagramEngine, link: LinkModel): JSX.Element {
		return React.createElement(DefaultLinkWidget, {
			color: "white",
			width: 2,
			link: link,
			diagramEngine: diagramEngine
		});
	}

	getNewInstance(initialConfig?: any): LinkModel {
		return new ConcreteLinkModel();
	}
}
