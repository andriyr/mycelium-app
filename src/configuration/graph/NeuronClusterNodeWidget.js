import * as React from "react";
import * as _ from "lodash";
import {
	DefaultPortLabel,
	DiagramEngine
} from "storm-react-diagrams";
import NeuronClusterNodeModel from "./NeuronClusterNodeModel";

export interface NeuronClusterNodeProps {
	node: NeuronClusterNodeModel;
	diagramEngine: DiagramEngine;
}

export interface DefaultNodeState {}

export default class NeuronClusterNodeWidget extends React.Component<NeuronClusterNodeProps, DefaultNodeState> {
	constructor(props: NeuronClusterNodeProps) {
		super(props);
		this.state = {};
	}

	generatePort(port) {
		return <DefaultPortLabel model={port} key={port.id} />;
	}

	render() {
		return (
			<div className="basic-node" style={{ background: "rgb(0,192,255)" }}>
				<div className="title">
					<div className="name">{this.props.node.name}</div>
					<div className="id">{this.props.node.id}</div>
				</div>
                <div className="properties">
                    <div className="property">Size: {this.props.node.size}</div>
                    <div className="property">Ratio: {this.props.node.ratio}</div>
                </div>
				<div className="ports">
					<div className="in">{_.map(this.props.node.getInPorts(), this.generatePort.bind(this))}</div>
					<div className="out">{_.map(this.props.node.getOutPorts(), this.generatePort.bind(this))}</div>
				</div>
			</div>
		);
	}
}
