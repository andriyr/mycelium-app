import * as React from "react";
import * as _ from "lodash";
import {
	DefaultPortLabel,
	DiagramEngine
} from "storm-react-diagrams";
import SynapseClusterNodeModel from "./SynapseClusterNodeModel";

export interface SynapseClusterNodeProps {
	node: SynapseClusterNodeModel;
	diagramEngine: DiagramEngine;
}

export interface DefaultNodeState {}

export default class SynapseClusterNodeWidget extends React.Component<SynapseClusterNodeProps, DefaultNodeState> {
	constructor(props: SynapseClusterNodeProps) {
		super(props);
		this.state = {};
	}

	generatePort(port) {
		return <DefaultPortLabel model={port} key={port.id} />;
	}

	render() {
		return (
			<div className="basic-node" style={{ background: "rgb(192,255,0)" }}>
				<div className="title">
					<div className="name">{this.props.node.name}</div>
					<div className="id">{this.props.node.id}</div>
				</div>
                <div className="properties">
                    <div className="property">Neurons Covered: {this.props.node.neuronsCovered}</div>
					<div className="property">Initial synapse weight: {this.props.node.initialSynapseWeight}</div>
                </div>
				<div className="ports">
					<div className="in">{_.map(this.props.node.getInPorts(), this.generatePort.bind(this))}</div>
					<div className="out">{_.map(this.props.node.getOutPorts(), this.generatePort.bind(this))}</div>
				</div>
			</div>
		);
	}
}
