import {DiagramEngine} from "storm-react-diagrams";
import ConcretePortFactory from "./ConcretePortFactory";
import ConcreteLinkFactory from "./ConcreteLinkFactory";
import NeuronClusterNodeFactory from "./NeuronClusterNodeFactory";
import SynapseClusterNodeFactory from "./SynapseClusterNodeFactory";
import STDPPreceptorNodeFactory from "./STDPPreceptorNodeFactory";
import ModulatedSTDPPreceptorNodeFactory from "./ModulatedSTDPPreceptorNodeFactory";
import DopamineAdapterNodeFactory from "./DopamineAdapterNodeFactory";
import InputAdapterNodeFactory from "./InputAdapterNodeFactory";
import OutputAdapterNodeFactory from "./OutputAdapterNodeFactory";

export default class ConcreteDiagramEngine extends DiagramEngine {
    constructor() {
        super();
        this.registerPortFactory(new ConcretePortFactory());
        this.registerLinkFactory(new ConcreteLinkFactory());
        this.registerNodeFactory(new NeuronClusterNodeFactory());
        this.registerNodeFactory(new SynapseClusterNodeFactory());
        this.registerNodeFactory(new STDPPreceptorNodeFactory());
        this.registerNodeFactory(new ModulatedSTDPPreceptorNodeFactory());
        this.registerNodeFactory(new DopamineAdapterNodeFactory());
        this.registerNodeFactory(new InputAdapterNodeFactory());
        this.registerNodeFactory(new OutputAdapterNodeFactory());

    }
}
