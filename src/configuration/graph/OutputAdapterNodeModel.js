import * as _ from "lodash";

import {
	NodeModel
} from "storm-react-diagrams";
import ConcretePortModel from "./ConcretePortModel";

export default class OutputAdapterNodeModel extends NodeModel {
	id: string;
	name: string;
	size: number;
	decodingWindow: number;
	ports: { [s: string]: ConcretePortModel };

	constructor(id: string="", name: string="", size: number=0, decodingWindow: decodingWindow=0, x: number=0, y: number=0) {
		super("outputAdapter");
		this.id = id;
		this.name = name;
		this.size = size;
		this.decodingWindow = decodingWindow;
		this.x = x;
		this.y = y;

		this.addPort(new ConcretePortModel(true, "in", "Neuron Cluster"));
	}

	setPosition(x, y) {
		super.setPosition(x, y);
		this.iterateListeners((listener, event) => {
			if (listener.positionUpdated) {
				listener.positionUpdated({ ...event, x: x, y: y });
			}
		});
	}

	deSerialize(object) {
		super.deSerialize(object);
		this.id = object.id;
		this.name = object.name;
		this.size = object.size;
		this.decodingWindow = object.decodingWindow;
	}

	serialize() {
		return _.merge(super.serialize(), {
			id: this.id,
			name: this.name,
			size: this.size,
			decodingWindow: this.decodingWindow
		});
	}

	getInPorts(): ConcretePortModel[] {
		return _.filter(this.ports, portModel => {
			return portModel.in;
		});
	}

	getOutPorts(): ConcretePortModel[] {
		return _.filter(this.ports, portModel => {
			return !portModel.in;
		});
	}
}
