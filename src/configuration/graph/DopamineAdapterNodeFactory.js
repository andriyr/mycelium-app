import * as React from "react";
import {
	DiagramEngine,
	NodeFactory
} from "storm-react-diagrams";
import DopamineAdapterNodeModel from "./DopamineAdapterNodeModel";
import DopamineAdapterNodeWidget from "./DopamineAdapterNodeWidget";

export default class DopamineAdapterNodeFactory extends NodeFactory<DopamineAdapterNodeModel> {
	constructor() {
		super("dopamineAdapter");
	}

	generateReactWidget(diagramEngine: DiagramEngine, node: DopamineAdapterNodeModel): JSX.Element {
		return React.createElement(DopamineAdapterNodeWidget, {
			node: node,
			diagramEngine: diagramEngine
		});
	}

	getNewInstance(initialConfig?: any): DefaultNodeModel {
		return new DopamineAdapterNodeModel();
	}
}
