import * as React from "react";
import {
	DiagramEngine,
	NodeFactory
} from "storm-react-diagrams";
import STDPPreceptorNodeModel from "./STDPPreceptorNodeModel";
import STDPPreceptorNodeWidget from "./STDPPreceptorNodeWidget";

export default class STDPPreceptorNodeFactory extends NodeFactory<STDPPreceptorNodeModel> {
	constructor() {
		super("STDPPreceptor");
	}

	generateReactWidget(diagramEngine: DiagramEngine, node: STDPPreceptorNodeModel): JSX.Element {
		return React.createElement(STDPPreceptorNodeWidget, {
			node: node,
			diagramEngine: diagramEngine
		});
	}

	getNewInstance(initialConfig?: any): DefaultNodeModel {
		return new STDPPreceptorNodeModel();
	}
}
