import * as _ from "lodash";

import {
	NodeModel
} from "storm-react-diagrams";
import ConcretePortModel from "./ConcretePortModel";

export default class STDPPreceptorNodeModel extends NodeModel {
	id: string;
	name: string;
	maxQueueSize: number;
	ports: { [s: string]: ConcretePortModel };

	constructor(id: string="", name: string="", maxQueueSize: number=0, x: number=0, y: number=0) {
		super("STDPPreceptor");
		this.id = id;
		this.name = name;
		this.maxQueueSize = maxQueueSize;
		this.x = x;
		this.y = y;

		this.addPort(new ConcretePortModel(true, "in", "Synapse Cluster"));
	}

	setPosition(x, y) {
		super.setPosition(x, y);
		this.iterateListeners((listener, event) => {
			if (listener.positionUpdated) {
				listener.positionUpdated({ ...event, x: x, y: y });
			}
		});
	}

	deSerialize(object) {
		super.deSerialize(object);
		this.id = object.id;
		this.name = object.name;
		this.maxQueueSize = object.maxQueueSize;
	}

	serialize() {
		return _.merge(super.serialize(), {
			id: this.id,
			name: this.name,
			maxQueueSize: this.maxQueueSize
		});
	}

	getInPorts(): ConcretePortModel[] {
		return _.filter(this.ports, portModel => {
			return portModel.in;
		});
	}

	getOutPorts(): ConcretePortModel[] {
		return _.filter(this.ports, portModel => {
			return !portModel.in;
		});
	}
}
