import * as React from "react";
import {
	DiagramEngine,
	NodeFactory
} from "storm-react-diagrams";
import InputAdapterNodeModel from "./InputAdapterNodeModel";
import InputAdapterNodeWidget from "./InputAdapterNodeWidget";

export default class InputAdapterNodeFactory extends NodeFactory<InputAdapterNodeModel> {
	constructor() {
		super("inputAdapter");
	}

	generateReactWidget(diagramEngine: DiagramEngine, node: InputAdapterNodeModel): JSX.Element {
		return React.createElement(InputAdapterNodeWidget, {
			node: node,
			diagramEngine: diagramEngine
		});
	}

	getNewInstance(initialConfig?: any): DefaultNodeModel {
		return new InputAdapterNodeModel();
	}
}
