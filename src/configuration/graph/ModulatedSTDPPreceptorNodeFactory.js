import * as React from "react";
import {
	DiagramEngine,
	NodeFactory
} from "storm-react-diagrams";
import ModulatedSTDPPreceptorNodeModel from "./ModulatedSTDPPreceptorNodeModel";
import ModulatedSTDPPreceptorNodeWidget from "./ModulatedSTDPPreceptorNodeWidget";

export default class ModulatedSTDPPreceptorNodeFactory extends NodeFactory<ModulatedSTDPPreceptorNodeModel> {
	constructor() {
		super("modulatedSTDPPreceptor");
	}

	generateReactWidget(diagramEngine: DiagramEngine, node: ModulatedSTDPPreceptorNodeModel): JSX.Element {
		return React.createElement(ModulatedSTDPPreceptorNodeWidget, {
			node: node,
			diagramEngine: diagramEngine
		});
	}

	getNewInstance(initialConfig?: any): DefaultNodeModel {
		return new ModulatedSTDPPreceptorNodeModel();
	}
}
