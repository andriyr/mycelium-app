import * as React from "react";
import {
	DiagramEngine,
	NodeFactory
} from "storm-react-diagrams";
import OutputAdapterNodeModel from "./OutputAdapterNodeModel";
import OutputAdapterNodeWidget from "./OutputAdapterNodeWidget";

export default class OutputAdapterNodeFactory extends NodeFactory<OutputAdapterNodeModel> {
	constructor() {
		super("outputAdapter");
	}

	generateReactWidget(diagramEngine: DiagramEngine, node: OutputAdapterNodeModel): JSX.Element {
		return React.createElement(OutputAdapterNodeWidget, {
			node: node,
			diagramEngine: diagramEngine
		});
	}

	getNewInstance(initialConfig?: any): DefaultNodeModel {
		return new OutputAdapterNodeModel();
	}
}
