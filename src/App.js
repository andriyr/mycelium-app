import React, { Component } from "react";
import {
    Route,
    NavLink,
    HashRouter
} from "react-router-dom";
import { Menu, Segment} from 'semantic-ui-react';
import Configuration from "./configuration/Configuration";
import Monitor from "./monitor/Monitor";

class App extends Component {

    render() {
        return (
            <HashRouter>
                <div className="hashRouter">
                    <Segment inverted attached>
                        <Menu inverted pointing secondary>
                            <Menu.Item header>mycelium</Menu.Item>
                            <Menu.Item as={NavLink} exact to="/" name='Configuration' icon='settings'/>
                            <Menu.Item as={NavLink} to="/monitor" name='Monitor' icon='heartbeat'/>
                        </Menu>
                    </Segment>
                    <div className="content">
                        <Route exact path="/" render={() => <Configuration/>}/>
                        <Route path="/monitor" render={() => <Monitor/>}/>
                    </div>
                </div>
            </HashRouter>
        );
    }
}

export default App;
