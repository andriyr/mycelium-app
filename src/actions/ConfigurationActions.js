export const CREATE_NEURON_CLUSTER_SUCCESS = "CREATE_NEURON_CLUSTER_SUCCESS";
export const CREATE_NEURON_CLUSTER_FAILURE = "CREATE_NEURON_CLUSTER_FAILURE";
export const DELETE_NEURON_CLUSTER_SUCCESS = "DELETE_NEURON_CLUSTER_SUCCESS";
export const DELETE_NEURON_CLUSTER_FAILURE = "DELETE_NEURON_CLUSTER_FAILURE";
export const SET_NEURON_CLUSTER_POSITION_SUCCESS = "SET_NEURON_CLUSTER_POSITION_SUCCESS";
export const SET_NEURON_CLUSTER_POSITION_FAILURE = "SET_NEURON_CLUSTER_POSITION_FAILURE";
export const CREATE_SYNAPSE_CLUSTER_SUCCESS = "CREATE_SYNAPSE_CLUSTER_SUCCESS";
export const CREATE_SYNAPSE_CLUSTER_FAILURE = "CREATE_SYNAPSE_CLUSTER_FAILURE";
export const DELETE_SYNAPSE_CLUSTER_SUCCESS = "DELETE_SYNAPSE_CLUSTER_SUCCESS";
export const DELETE_SYNAPSE_CLUSTER_FAILURE = "DELETE_SYNAPSE_CLUSTER_FAILURE";
export const ATTACH_SYNAPSE_CLUSTER_SUCCESS = "ATTACH_SYNAPSE_CLUSTER_SUCCESS";
export const ATTACH_SYNAPSE_CLUSTER_FAILURE = "ATTACH_SYNAPSE_CLUSTER_FAILURE";
export const DETACH_SYNAPSE_CLUSTER_SUCCESS = "DETACH_SYNAPSE_CLUSTER_SUCCESS";
export const DETACH_SYNAPSE_CLUSTER_FAILURE = "DETACH_SYNAPSE_CLUSTER_FAILURE";
export const SET_SYNAPSE_CLUSTER_POSITION_SUCCESS = "SET_SYNAPSE_CLUSTER_POSITION_SUCCESS";
export const SET_SYNAPSE_CLUSTER_POSITION_FAILURE = "SET_SYNAPSE_CLUSTER_POSITION_FAILURE";
export const CREATE_STDP_PRECEPTOR_SUCCESS = "CREATE_STDP_PRECEPTOR_SUCCESS";
export const CREATE_STDP_PRECEPTOR_FAILURE = "CREATE_STDP_PRECEPTOR_FAILURE";
export const DELETE_STDP_PRECEPTOR_SUCCESS = "DELETE_STDP_PRECEPTOR_SUCCESS";
export const DELETE_STDP_PRECEPTOR_FAILURE = "DELETE_STDP_PRECEPTOR_FAILURE";
export const ATTACH_STDP_PRECEPTOR_SUCCESS = "ATTACH_STDP_PRECEPTOR_SUCCESS";
export const ATTACH_STDP_PRECEPTOR_FAILURE = "ATTACH_STDP_PRECEPTOR_FAILURE";
export const DETACH_STDP_PRECEPTOR_SUCCESS = "DETACH_STDP_PRECEPTOR_SUCCESS";
export const DETACH_STDP_PRECEPTOR_FAILURE = "DETACH_STDP_PRECEPTOR_FAILURE";
export const SET_STDP_PRECEPTOR_POSITION_SUCCESS = "SET_STDP_PRECEPTOR_POSITION_SUCCESS";
export const SET_STDP_PRECEPTOR_POSITION_FAILURE = "SET_STDP_PRECEPTOR_POSITION_FAILURE";
export const CREATE_MODULATED_STDP_PRECEPTOR_SUCCESS = "CREATE_MODULATED_STDP_PRECEPTOR_SUCCESS";
export const CREATE_MODULATED_STDP_PRECEPTOR_FAILURE = "CREATE_MODULATED_STDP_PRECEPTOR_FAILURE";
export const DELETE_MODULATED_STDP_PRECEPTOR_SUCCESS = "DELETE_MODULATED_STDP_PRECEPTOR_SUCCESS";
export const DELETE_MODULATED_STDP_PRECEPTOR_FAILURE = "DELETE_MODULATED_STDP_PRECEPTOR_FAILURE";
export const ATTACH_MODULATED_STDP_PRECEPTOR_SUCCESS = "ATTACH_MODULATED_STDP_PRECEPTOR_SUCCESS";
export const ATTACH_MODULATED_STDP_PRECEPTOR_FAILURE = "ATTACH_MODULATED_STDP_PRECEPTOR_FAILURE";
export const DETACH_MODULATED_STDP_PRECEPTOR_SUCCESS = "DETACH_MODULATED_STDP_PRECEPTOR_SUCCESS";
export const DETACH_MODULATED_STDP_PRECEPTOR_FAILURE = "DETACH_MODULATED_STDP_PRECEPTOR_FAILURE";
export const SET_MODULATED_STDP_PRECEPTOR_POSITION_SUCCESS = "SET_MODULATED_STDP_PRECEPTOR_POSITION_SUCCESS";
export const SET_MODULATED_STDP_PRECEPTOR_POSITION_FAILURE = "SET_MODULATED_STDP_PRECEPTOR_POSITION_FAILURE";
export const CREATE_INPUT_ADAPTER_SUCCESS = "CREATE_INPUT_ADAPTER_SUCCESS";
export const CREATE_INPUT_ADAPTER_FAILURE = "CREATE_INPUT_ADAPTER_FAILURE";
export const DELETE_INPUT_ADAPTER_SUCCESS = "DELETE_INPUT_ADAPTER_SUCCESS";
export const DELETE_INPUT_ADAPTER_FAILURE = "DELETE_INPUT_ADAPTER_FAILURE";
export const ATTACH_INPUT_ADAPTER_SUCCESS = "ATTACH_INPUT_ADAPTER_SUCCESS";
export const ATTACH_INPUT_ADAPTER_FAILURE = "ATTACH_INPUT_ADAPTER_FAILURE";
export const DETACH_INPUT_ADAPTER_SUCCESS = "DETACH_INPUT_ADAPTER_SUCCESS";
export const DETACH_INPUT_ADAPTER_FAILURE = "DETACH_INPUT_ADAPTER_FAILURE";
export const SET_INPUT_ADAPTER_POSITION_SUCCESS = "SET_INPUT_ADAPTER_POSITION_SUCCESS";
export const SET_INPUT_ADAPTER_POSITION_FAILURE = "SET_INPUT_ADAPTER_POSITION_FAILURE";
export const CREATE_OUTPUT_ADAPTER_SUCCESS = "CREATE_OUTPUT_ADAPTER_SUCCESS";
export const CREATE_OUTPUT_ADAPTER_FAILURE = "CREATE_OUTPUT_ADAPTER_FAILURE";
export const DELETE_OUTPUT_ADAPTER_SUCCESS = "DELETE_OUTPUT_ADAPTER_SUCCESS";
export const DELETE_OUTPUT_ADAPTER_FAILURE = "DELETE_OUTPUT_ADAPTER_FAILURE";
export const ATTACH_OUTPUT_ADAPTER_SUCCESS = "ATTACH_OUTPUT_ADAPTER_SUCCESS";
export const ATTACH_OUTPUT_ADAPTER_FAILURE = "ATTACH_OUTPUT_ADAPTER_FAILURE";
export const DETACH_OUTPUT_ADAPTER_SUCCESS = "DETACH_OUTPUT_ADAPTER_SUCCESS";
export const DETACH_OUTPUT_ADAPTER_FAILURE = "DETACH_OUTPUT_ADAPTER_FAILURE";
export const SET_OUTPUT_ADAPTER_POSITION_SUCCESS = "SET_OUTPUT_ADAPTER_POSITION_SUCCESS";
export const SET_OUTPUT_ADAPTER_POSITION_FAILURE = "SET_OUTPUT_ADAPTER_POSITION_FAILURE";
export const CREATE_DOPAMINE_ADAPTER_SUCCESS = "CREATE_DOPAMINE_ADAPTER_SUCCESS";
export const CREATE_DOPAMINE_ADAPTER_FAILURE = "CREATE_DOPAMINE_ADAPTER_FAILURE";
export const DELETE_DOPAMINE_ADAPTER_SUCCESS = "DELETE_DOPAMINE_ADAPTER_SUCCESS";
export const DELETE_DOPAMINE_ADAPTER_FAILURE = "DELETE_DOPAMINE_ADAPTER_FAILURE";
export const ATTACH_DOPAMINE_ADAPTER_SUCCESS = "ATTACH_DOPAMINE_ADAPTER_SUCCESS";
export const ATTACH_DOPAMINE_ADAPTER_FAILURE = "ATTACH_DOPAMINE_ADAPTER_FAILURE";
export const DETACH_DOPAMINE_ADAPTER_SUCCESS = "DETACH_DOPAMINE_ADAPTER_SUCCESS";
export const DETACH_DOPAMINE_ADAPTER_FAILURE = "DETACH_DOPAMINE_ADAPTER_FAILURE";
export const SET_DOPAMINE_ADAPTER_POSITION_SUCCESS = "SET_DOPAMINE_ADAPTER_POSITION_SUCCESS";
export const SET_DOPAMINE_ADAPTER_POSITION_FAILURE = "SET_DOPAMINE_ADAPTER_POSITION_FAILURE";
export const NODE_LOADED = "NODE_LOADED";

export function loadNode() {
    return (dispatch, getState, api) => {
        api.loadNode((response) => {
            response.getInputadaptersList().forEach((inputAdapter) => {
                dispatch({type: CREATE_INPUT_ADAPTER_SUCCESS, payload: {
                    id: inputAdapter.getId(),
                    name: inputAdapter.getName(),
                    size: inputAdapter.getSize(),
                    encodingWindow: inputAdapter.getEncodingwindow(),
                    x: inputAdapter.getGraph().getX(),
                    y: inputAdapter.getGraph().getY()
                }});
            });
            response.getNeuronclustersList().forEach((neuronCluster) => {
                dispatch({type: CREATE_NEURON_CLUSTER_SUCCESS, payload: {
                    id: neuronCluster.getId(),
                    name: neuronCluster.getName(),
                    size: neuronCluster.getSize(),
                    ratio: neuronCluster.getRatio(),
                    neuronIds: neuronCluster.getNeuronidsList(),
                    inputAdapterIds: neuronCluster.getInputadapteridsList(),
                    x: neuronCluster.getGraph().getX(),
                    y: neuronCluster.getGraph().getY()
                }});
            });
            response.getOutputadaptersList().forEach((outputAdapter) => {
                dispatch({type: CREATE_OUTPUT_ADAPTER_SUCCESS, payload: {
                    id: outputAdapter.getId(),
                    name: outputAdapter.getName(),
                    size: outputAdapter.getSize(),
                    decodingWindow: outputAdapter.getDecodingwindow(),
                    neuronClusterId: outputAdapter.getNeuronclusterid(),
                    x: outputAdapter.getGraph().getX(),
                    y: outputAdapter.getGraph().getY()
                }});
            });
            response.getSynapseclustersList().forEach((synapseCluster) => {
                dispatch({type: CREATE_SYNAPSE_CLUSTER_SUCCESS, payload: {
                    id: synapseCluster.getId(),
                    name: synapseCluster.getName(),
                    neuronsCovered: synapseCluster.getNeuronscovered(),
                    initialSynapseWeight: synapseCluster.getInitialsynapseweight(),
                    neuronClusterIds: synapseCluster.getNeuronclusteridsList(),
                    x: synapseCluster.getGraph().getX(),
                    y: synapseCluster.getGraph().getY()
                }});
            });
            response.getStdppreceptorsList().forEach((STDPPreceptor) => {
                dispatch({type: CREATE_STDP_PRECEPTOR_SUCCESS, payload: {
                    id: STDPPreceptor.getId(),
                    name: STDPPreceptor.getName(),
                    maxQueueSize: STDPPreceptor.getMaxqueuesize(),
                    synapseClusterId: STDPPreceptor.getSynapseclusterid(),
                    x: STDPPreceptor.getGraph().getX(),
                    y: STDPPreceptor.getGraph().getY()
                }});
            });
            response.getDopamineadaptersList().forEach((dopamineAdapter) => {
                dispatch({type: CREATE_DOPAMINE_ADAPTER_SUCCESS, payload: {
                    id: dopamineAdapter.getId(),
                    name: dopamineAdapter.getName(),
                    x: dopamineAdapter.getGraph().getX(),
                    y: dopamineAdapter.getGraph().getY()
                }});
            });
            response.getModulatedstdppreceptorsList().forEach((modulatedSTDPPreceptor) => {
                dispatch({type: CREATE_MODULATED_STDP_PRECEPTOR_SUCCESS, payload: {
                    id: modulatedSTDPPreceptor.getId(),
                    name: modulatedSTDPPreceptor.getName(),
                    maxQueueSize: modulatedSTDPPreceptor.getMaxqueuesize(),
                    sensitivity: modulatedSTDPPreceptor.getSensitivity(),
                    synapseClusterId: modulatedSTDPPreceptor.getSynapseclusterid(),
                    dopamineAdapterId: modulatedSTDPPreceptor.getDopamineadapterid(),
                    x: modulatedSTDPPreceptor.getGraph().getX(),
                    y: modulatedSTDPPreceptor.getGraph().getY()
                }});
            });

            dispatch({type: NODE_LOADED, payload: {success: true}});
        }, (response) => {
        });
    }
}

export function createNeuronCluster(request) {
    return (dispatch, getState, api) => {
        api.createNeuronCluster(request.name, request.size, request.ratio, request.x, request.y, (response) => {
            dispatch({type: CREATE_NEURON_CLUSTER_SUCCESS, payload: {
                id: response.getId(),
                name: response.getName(),
                size: response.getSize(),
                ratio: response.getRatio(),
                neuronIds: response.getNeuronidsList(),
                inputAdapterIds: response.getInputadapteridsList(),
                x: response.getGraph().getX(),
                y: response.getGraph().getY()
            }});
        }, (response) => {
            dispatch({type: CREATE_NEURON_CLUSTER_FAILURE, payload: {}});
        });
    }
}

export function deleteNeuronCluster(request) {
    return (dispatch, getState, api) => {
        api.deleteNeuronCluster(request.id, (response) => {
            dispatch({type: DELETE_NEURON_CLUSTER_SUCCESS, payload: {
                neuronClusterId: request.id
            }});

            getState().configuration.synapseClusters.forEach((synapseCluster) => {
                if (synapseCluster.neuronClusterIds.indexOf(request.id) > -1) {
                    dispatch({type: DETACH_SYNAPSE_CLUSTER_SUCCESS, payload: {
                        neuronClusterId: request.id,
                        synapseClusterId: request.id
                    }})
                }
            });

            getState().configuration.outputAdapters.forEach((outputAdapter) => {
                if (outputAdapter.neuronClusterId == request.id) {
                    dispatch({type: DETACH_OUTPUT_ADAPTER_SUCCESS, payload: {
                        outputAdapterId: outputAdapter.id
                    }})
                }
            });
        }, (response) => {
            dispatch({type: DELETE_NEURON_CLUSTER_FAILURE, payload: {}});
        });
    }
}

export function setNeuronClusterPosition(request) {
    return (dispatch, getState, api) => {
        api.setNeuronClusterPosition(request.id, request.x, request.y , (response) => {
            dispatch({type: SET_NEURON_CLUSTER_POSITION_SUCCESS, payload: {
                id: request.id,
                x: request.x,
                y: request.y
            }});
        }, (response) => {
            dispatch({type: SET_NEURON_CLUSTER_POSITION_FAILURE, payload: {}});
        });
    }
}

export function createSynapseCluster(request) {
    return (dispatch, getState, api) => {
        api.createSynapseCluster(request.name, request.neuronsCovered, request.initialSynapseWeight, request.x, request.y, (response) => {
            dispatch({type: CREATE_SYNAPSE_CLUSTER_SUCCESS, payload: {
                id: response.getId(),
                name: response.getName(),
                neuronsCovered: response.getNeuronscovered(),
                initialSynapseWeight: response.getInitialsynapseweight(),
                neuronClusterIds: response.getNeuronclusteridsList(),
                x: response.getGraph().getX(),
                y: response.getGraph().getY()
            }});
        }, (response) => {
            dispatch({type: CREATE_SYNAPSE_CLUSTER_FAILURE, payload: {}});
        });
    }
}

export function deleteSynapseCluster(request) {
    return (dispatch, getState, api) => {
        api.deleteSynapseCluster(request.id, (response) => {
            dispatch({type: DELETE_SYNAPSE_CLUSTER_SUCCESS, payload: {
                synapseClusterId: request.id
            }});

            getState().configuration.dopaminePreceptors.forEach((dopaminePreceptor) => {
                if (dopaminePreceptor.synapseClusterIds.indexOf(request.id) > -1) {
                    dispatch({type: DETACH_MODULATED_STDP_PRECEPTOR_SUCCESS, payload: {
                        dopaminePreceptorId: dopaminePreceptor.id,
                        synapseClusterId: request.id
                    }})
                }
            });

            getState().configuration.STDPPreceptors.forEach((STDPPreceptor) => {
                if (STDPPreceptor.synapseClusterId == request.id) {
                    dispatch({type: DETACH_STDP_PRECEPTOR_SUCCESS, payload: {
                        STDPPreceptorId: STDPPreceptor.id
                    }})
                }
            });
        }, (response) => {
            dispatch({type: DELETE_SYNAPSE_CLUSTER_FAILURE, payload: {}});
        });
    }
}

export function attachSynapseCluster(request) {
    return (dispatch, getState, api) => {
        api.attachSynapseCluster(request.synapseClusterId, request.neuronClusterId, (response) => {
            dispatch({type: ATTACH_SYNAPSE_CLUSTER_SUCCESS, payload: {
                synapseClusterId: request.synapseClusterId,
                neuronClusterId: request.neuronClusterId
            }});
        }, (response) => {
            dispatch({type: ATTACH_SYNAPSE_CLUSTER_FAILURE, payload: {}});
        });
    }
};

export function detachSynapseCluster(request) {
    return (dispatch, getState, api) => {
        api.detachSynapseCluster(request.synapseClusterId, request.neuronClusterId, (response) => {
            dispatch({type: DETACH_SYNAPSE_CLUSTER_SUCCESS, payload: {
                synapseClusterId: request.synapseClusterId,
                neuronClusterId: request.neuronClusterId
            }});
        }, (response) => {
            dispatch({type: DETACH_SYNAPSE_CLUSTER_FAILURE, payload: {}});
        });
    }
};

export function setSynapseClusterPosition(request) {
    return (dispatch, getState, api) => {
        api.setSynapseClusterPosition(request.id, request.x, request.y , (response) => {
            dispatch({type: SET_SYNAPSE_CLUSTER_POSITION_SUCCESS, payload: {
                id: request.id,
                x: request.x,
                y: request.y
            }});
        }, (response) => {
            dispatch({type: SET_SYNAPSE_CLUSTER_POSITION_FAILURE, payload: {}});
        });
    }
}

export function createSTDPPreceptor(request) {
    return (dispatch, getState, api) => {
        api.createSTDPPreceptor(request.name, request.maxQueueSize, request.x, request.y, (response) => {
            dispatch({type: CREATE_STDP_PRECEPTOR_SUCCESS, payload: {
                id: response.getId(),
                name: response.getName(),
                maxQueueSize: response.getMaxqueuesize(),
                x: response.getGraph().getX(),
                y: response.getGraph().getY()
            }});
        }, (response) => {
            dispatch({type: CREATE_STDP_PRECEPTOR_FAILURE, payload: {}});
        });
    }
}

export function deleteSTDPPreceptor(request) {
    return (dispatch, getState, api) => {
        api.deleteSTDPPreceptor(request.id, (response) => {
            dispatch({type: DELETE_STDP_PRECEPTOR_SUCCESS, payload: {
                STDPPreceptorId: request.id
            }});
        }, (response) => {
            dispatch({type: DELETE_STDP_PRECEPTOR_FAILURE, payload: {}});
        });
    }
}

export function attachSTDPPreceptor(request) {
    return (dispatch, getState, api) => {
        api.attachSTDPPreceptor(request.STDPPreceptorId, request.synapseClusterId, (response) => {
            dispatch({type: ATTACH_STDP_PRECEPTOR_SUCCESS, payload: {
                STDPPreceptorId: request.STDPPreceptorId,
                synapseClusterId: request.synapseClusterId
            }});
        }, (response) => {
            dispatch({type: ATTACH_STDP_PRECEPTOR_FAILURE, payload: {}});
        });
    }
};

export function detachSTDPPreceptor(request) {
    return (dispatch, getState, api) => {
        api.detachSTDPPreceptor(request.STDPPreceptorId, (response) => {
            dispatch({type: DETACH_STDP_PRECEPTOR_SUCCESS, payload: {
                STDPPreceptorId: request.STDPPreceptorId
            }});
        }, (response) => {
            dispatch({type: DETACH_STDP_PRECEPTOR_FAILURE, payload: {}});
        });
    }
};

export function setSTDPPreceptorPosition(request) {
    return (dispatch, getState, api) => {
        api.setSTDPPreceptorPosition(request.id, request.x, request.y , (response) => {
            dispatch({type: SET_STDP_PRECEPTOR_POSITION_SUCCESS, payload: {
                id: request.id,
                x: request.x,
                y: request.y
            }});
        }, (response) => {
            dispatch({type: SET_STDP_PRECEPTOR_POSITION_FAILURE, payload: {}});
        });
    }
}

export function createModulatedSTDPPreceptor(request) {
    return (dispatch, getState, api) => {
        api.createModulatedSTDPPreceptor(request.name, request.maxQueueSize, request.sensitivity, request.x, request.y, (response) => {
            dispatch({type: CREATE_MODULATED_STDP_PRECEPTOR_SUCCESS, payload: {
                id: response.getId(),
                name: response.getName(),
                maxQueueSize: response.getMaxqueuesize(),
                sensitivity: response.getSensitivity(),
                synapseClusterId: response.getSynapseclusterid(),
                dopamineAdapterId: response.getDopamineadapterid(),
                x: response.getGraph().getX(),
                y: response.getGraph().getY()
            }});
        }, (response) => {
            dispatch({type: CREATE_MODULATED_STDP_PRECEPTOR_FAILURE, payload: {}});
        });
    }
}

export function deleteModulatedSTDPPreceptor(request) {
    return (dispatch, getState, api) => {
        api.deleteModulatedSTDPPreceptor(request.id, (response) => {
            dispatch({type: DELETE_MODULATED_STDP_PRECEPTOR_SUCCESS, payload: {
                modulatedSTDPPreceptorId: request.id
            }});
        }, (response) => {
            dispatch({type: DELETE_MODULATED_STDP_PRECEPTOR_FAILURE, payload: {}});
        });
    }
}

export function attachModulatedSTDPPreceptor(request) {
    return (dispatch, getState, api) => {
        api.attachModulatedSTDPPreceptor(request.modulatedSTDPPreceptorId, request.synapseClusterId, (response) => {
            dispatch({type: ATTACH_MODULATED_STDP_PRECEPTOR_SUCCESS, payload: {
                modulatedSTDPPreceptorId: request.modulatedSTDPPreceptorId,
                synapseClusterId: request.synapseClusterId
            }});
        }, (response) => {
            dispatch({type: ATTACH_MODULATED_STDP_PRECEPTOR_FAILURE, payload: {}});
        });
    }
};

export function detachModulatedSTDPPreceptor(request) {
    return (dispatch, getState, api) => {
        api.detachModulatedSTDPPreceptor(request.modulatedSTDPPreceptorId, (response) => {
            dispatch({type: DETACH_MODULATED_STDP_PRECEPTOR_SUCCESS, payload: {
                modulatedSTDPPreceptorId: request.modulatedSTDPPreceptorId
            }});
        }, (response) => {
            dispatch({type: DETACH_MODULATED_STDP_PRECEPTOR_FAILURE, payload: {}});
        });
    }
};

export function setModulatedSTDPPreceptorPosition(request) {
    return (dispatch, getState, api) => {
        api.setModulatedSTDPPreceptorPosition(request.id, request.x, request.y , (response) => {
            dispatch({type: SET_MODULATED_STDP_PRECEPTOR_POSITION_SUCCESS, payload: {
                id: request.id,
                x: request.x,
                y: request.y
            }});
        }, (response) => {
            dispatch({type: SET_MODULATED_STDP_PRECEPTOR_POSITION_FAILURE, payload: {}});
        });
    }
}

export function createInputAdapter(request) {
    return (dispatch, getState, api) => {
        api.createInputAdapter(request.name, request.size, request.encodingWindow, request.x, request.y, (response) => {
            dispatch({type: CREATE_INPUT_ADAPTER_SUCCESS, payload: {
                id: response.getId(),
                name: response.getName(),
                size: response.getSize(),
                encodingWindow: response.getEncodingwindow(),
                x: response.getGraph().getX(),
                y: response.getGraph().getY()
            }});
        }, (response) => {
            dispatch({type: CREATE_INPUT_ADAPTER_FAILURE, payload: {}});
        });
    }
};

export function deleteInputAdapter(request) {
    return (dispatch, getState, api) => {
        api.deleteInputAdapter(request.id, (response) => {
            dispatch({type: DELETE_INPUT_ADAPTER_SUCCESS, payload: {
                inputAdapterId: request.id
            }});

            getState().configuration.neuronClusters.forEach((neuronCluster) => {
                if (neuronCluster.inputAdapterIds.indexOf(request.id) > -1) {
                    dispatch({type: DETACH_INPUT_ADAPTER_SUCCESS, payload: {
                        inputAdapterId: request.id,
                        neuronClusterId: neuronCluster.id
                    }})
                }
            });
        }, (response) => {
            dispatch({type: DELETE_INPUT_ADAPTER_FAILURE, payload: {}});
        });
    }
}

export function attachInputAdapter(request) {
    return (dispatch, getState, api) => {
        api.attachInputAdapter(request.inputAdapterId, request.neuronClusterId, (response) => {
            dispatch({type: ATTACH_INPUT_ADAPTER_SUCCESS, payload: {
                inputAdapterId: request.inputAdapterId,
                neuronClusterId: request.neuronClusterId
            }});
        }, (response) => {
            dispatch({type: ATTACH_INPUT_ADAPTER_FAILURE, payload: {}});
        });
    }
};

export function detachInputAdapter(request) {
    return (dispatch, getState, api) => {
        api.detachInputAdapter(request.inputAdapterId, request.neuronClusterId, (response) => {
            dispatch({type: DETACH_INPUT_ADAPTER_SUCCESS, payload: {
                inputAdapterId: request.inputAdapterId,
                neuronClusterId: request.neuronClusterId
            }});
        }, (response) => {
            dispatch({type: DETACH_INPUT_ADAPTER_FAILURE, payload: {}});
        });
    }
};

export function setInputAdapterPosition(request) {
    return (dispatch, getState, api) => {
        api.setInputAdapterPosition(request.id, request.x, request.y , (response) => {
            dispatch({type: SET_INPUT_ADAPTER_POSITION_SUCCESS, payload: {
                id: request.id,
                x: request.x,
                y: request.y
            }});
        }, (response) => {
            dispatch({type: SET_INPUT_ADAPTER_POSITION_FAILURE, payload: {}});
        });
    }
}

export function createDopamineAdapter(request) {
    return (dispatch, getState, api) => {
        api.createDopamineAdapter(request.name, request.x, request.y, (response) => {
            dispatch({type: CREATE_DOPAMINE_ADAPTER_SUCCESS, payload: {
                id: response.getId(),
                name: response.getName(),
                x: response.getGraph().getX(),
                y: response.getGraph().getY()
            }});
        }, (response) => {
            dispatch({type: CREATE_DOPAMINE_ADAPTER_FAILURE, payload: {}});
        });
    }
};

export function deleteDopamineAdapter(request) {
    return (dispatch, getState, api) => {
        api.deleteDopamineAdapter(request.id, (response) => {
            dispatch({type: DELETE_DOPAMINE_ADAPTER_SUCCESS, payload: {
                dopamineAdapterId: request.id
            }});

            getState().configuration.modulatedSTDPPreceptors.forEach((modulatedSTDPPreceptor) => {
                if (modulatedSTDPPreceptor.dopamineAdapterId == request.id) {
                    dispatch({type: DETACH_DOPAMINE_ADAPTER_SUCCESS, payload: {
                        dopamineAdapterId: request.id,
                        modulatedSTDPPreceptorId: modulatedSTDPPreceptor.id
                    }})
                }
            });
        }, (response) => {
            dispatch({type: DELETE_DOPAMINE_ADAPTER_FAILURE, payload: {}});
        });
    }
}

export function attachDopamineAdapter(request) {
    return (dispatch, getState, api) => {
        api.attachDopamineAdapter(request.dopamineAdapterId, request.modulatedSTDPPreceptorId, (response) => {
            dispatch({type: ATTACH_DOPAMINE_ADAPTER_SUCCESS, payload: {
                dopamineAdapterId: request.dopamineAdapterId,
                modulatedSTDPPreceptorId: request.modulatedSTDPPreceptorId
            }});
        }, (response) => {
            dispatch({type: ATTACH_DOPAMINE_ADAPTER_FAILURE, payload: {}});
        });
    }
};

export function detachDopamineAdapter(request) {
    return (dispatch, getState, api) => {
        api.detachDopamineAdapter(request.dopamineAdapterId, request.modulatedSTDPPreceptorId, (response) => {
            dispatch({type: DETACH_DOPAMINE_ADAPTER_SUCCESS, payload: {
                dopamineAdapterId: request.dopamineAdapterId,
                modulatedSTDPPreceptorId: request.modulatedSTDPPreceptorId
            }});
        }, (response) => {
            dispatch({type: DETACH_DOPAMINE_ADAPTER_FAILURE, payload: {}});
        });
    }
};

export function setDopamineAdapterPosition(request) {
    return (dispatch, getState, api) => {
        api.setDopamineAdapterPosition(request.id, request.x, request.y , (response) => {
            dispatch({type: SET_DOPAMINE_ADAPTER_POSITION_SUCCESS, payload: {
                id: request.id,
                x: request.x,
                y: request.y
            }});
        }, (response) => {
            dispatch({type: SET_DOPAMINE_ADAPTER_POSITION_FAILURE, payload: {}});
        });
    }
}

export function createOutputAdapter(request) {
    return (dispatch, getState, api) => {
        api.createOutputAdapter(request.name, request.size, request.decodingWindow, request.x, request.y, (response) => {
            dispatch({type: CREATE_OUTPUT_ADAPTER_SUCCESS, payload: {
                id: response.getId(),
                name: response.getName(),
                size: response.getSize(),
                decodingWindow: response.getDecodingwindow(),
                neuronClusterId: response.getNeuronclusterid(),
                x: response.getGraph().getX(),
                y: response.getGraph().getY()
            }});
        }, (response) => {
            dispatch({type: CREATE_OUTPUT_ADAPTER_FAILURE, payload: {}})
        });
    }
};

export function deleteOutputAdapter(request) {
    return (dispatch, getState, api) => {
        api.deleteOutputAdapter(request.id, (response) => {
            dispatch({type: DELETE_OUTPUT_ADAPTER_SUCCESS, payload: {
                outputAdapterId: request.id
            }});
        }, (response) => {
            dispatch({type: DELETE_OUTPUT_ADAPTER_FAILURE, payload: {}});
        });
    }
}

export function attachOutputAdapter(request) {
    return (dispatch, getState, api) => {
        api.attachOutputAdapter(request.outputAdapterId, request.neuronClusterId, (response) => {
            dispatch({type: ATTACH_OUTPUT_ADAPTER_SUCCESS, payload: {
                outputAdapterId: request.outputAdapterId,
                neuronClusterId: request.neuronClusterId
            }});
        }, (response) => {
            dispatch({type: ATTACH_OUTPUT_ADAPTER_FAILURE, payload: {}});
        });
    }
};

export function detachOutputAdapter(request) {
    return (dispatch, getState, api) => {
        api.detachOutputAdapter(request.outputAdapterId, request.neuronClusterId, (response) => {
            dispatch({type: DETACH_OUTPUT_ADAPTER_SUCCESS, payload: {
                outputAdapterId: request.outputAdapterId
            }});
        }, (response) => {
            dispatch({type: DETACH_OUTPUT_ADAPTER_FAILURE, payload: {}});
        });
    }
};

export function setOutputAdapterPosition(request) {
    return (dispatch, getState, api) => {
        api.setOutputAdapterPosition(request.id, request.x, request.y , (response) => {
            dispatch({type: SET_OUTPUT_ADAPTER_POSITION_SUCCESS, payload: {
                id: request.id,
                x: request.x,
                y: request.y
            }});
        }, (response) => {
            dispatch({type: SET_OUTPUT_ADAPTER_POSITION_FAILURE, payload: {}});
        });
    }
}
