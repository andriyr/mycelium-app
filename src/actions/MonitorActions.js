export const ADD_WIDGET = "ADD_WIDGET";
export const REMOVE_WIDGET = "REMOVE_WIDGET";
export const STREAM_NEURON_SPIKE_EVENT_SUCCESS = "STREAM_NEURON_SPIKE_EVENT_SUCCESS";
export const STREAM_NEURON_SPIKE_EVENT_FAILURE = "STREAM_NEURON_SPIKE_EVENT_FAILURE";

export function addWidget(widget) {
    return (dispatch, getState, api) => {
        dispatch({type: ADD_WIDGET, payload: {
            id: widget.id,
            type: widget.type
        }});
    }
}

export function removeWidget(widget) {
    return (dispatch, getState, api) => {
        dispatch({type: REMOVE_WIDGET, payload: {
            id: widget.id
        }});
    }
}

export function streamNeuronSpikeEvent(neuronClusterId, onMessage, onFailure) {
    return (dispatch, getState, api) => {
        api.streamNeuronSpikeEvent(neuronClusterId, (response) => {
            onMessage(response);
        }, (response) => {
            onFailure(response);
        })
    }
};

export function streamNeuronSpikeRelayEvent(synapseClusterId, onMessage, onFailure) {
    return (dispatch, getState, api) => {
        api.streamNeuronSpikeRelayEvent(synapseClusterId, (response) => {
            onMessage(response);
        }, (response) => {
            onFailure(response);
        })
    }
};
